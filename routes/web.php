<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
/*Route::get('/terms-conditions', function () {
    return view('terms-conditions');
});*/
Route::get('/terms-conditions', 'TermsController@index')->name('index');
Route::get('/privacy-policy', 'PrivacyController@index')->name('index');
Route::get('/FAQs', 'FaqController@index')->name('index');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/occasions', 'HomeController@occasions')->name('occasions');
Auth::routes();

Route::post('/forgotpassword', 'BookingController@forgotpassword');
Route::get('/booking', 'BookingController@index');
Route::get('/payment/{id}', 'BookingController@payment');
Route::get('/usersignup/{id}', 'BookingController@usersignup');
Route::get('/calendardates', 'BookingController@calendardates');
Route::POST('/bookingdata', 'BookingController@bookingdata');
Route::POST('/storeeventdata', 'BookingController@storeeventdata');
Route::POST('/discountpayment', 'BookingController@discountpayment');
Route::POST('/stripepayment', 'BookingController@stripepayment');
Route::get('/paypalpayment', 'BookingController@paypalpayment');
Route::get('/checkuseremail', 'BookingController@checkuseremail');
Route::get('/checkedituseremail', 'BookingController@checkedituseremail');
Route::resource('/volunteerssession', 'VolunteerssessionController');
Route::get('/client-album', 'CommunityController@clientalbum');
Route::get('/client-view', 'CommunityController@clientview');


Route::get('auth/google', 'Auth\LoginController@redirectTOGoogle');
Route::get('googlecallback', 'Auth\LoginController@Googlecallback');
Route::resource('/learn', 'LearnController');
Route::resource('/blog', 'BlogController');
Route::resource('/community', 'CommunityController');
Route::resource('/blog', 'BlogController');
Route::resource('/about', 'AboutController');
Route::resource('/occasions', 'OccasionsController');
Route::get('classpaypalpayment', 'BookingController@classpaypalpayment');
Route::post('classstripepayment', 'BookingController@classstripepayment');
Route::POST('/userdiscountpayment', 'UserbookingController@discountpayment');
Route::get('userclasspaypalpayment', 'UserbookingController@classpaypalpayment');
Route::post('userclassstripepayment', 'UserbookingController@classstripepayment');
Route::get('usereventpaypalpayment', 'UserbookingController@paypalpayment');
Route::post('usereventstripepayment', 'UserbookingController@stripepayment');
Route::resource('/customerdashboard', 'Customer\ProfileController');
Route::resource('/customerevents', 'Customer\EventController');
Route::resource('/customerpayments', 'Customer\PaymentController');
Route::resource('/customerclasses', 'Customer\ClassesController');
Route::resource('/customerfeedback','Customer\FeedbackController');
Route::get('login',array('as'=>'login',function(){
    return view('auth.login');
}));



Route::resource('/photographerdashboard', 'Photographer\ProfileController');
Route::resource('/photographersession', 'Photographer\SessionController');
Route::resource('/photographerclasses', 'Photographer\ClassesController');
Route::resource('/photgrapherfeedback','Photographer\FeedbackController');
Route::resource('/photographerpayments', 'Photographer\PaymentController');
Route::resource('/photographergallery', 'Photographer\GalleryController');

Route::get('/photographergallery/{id}/index/delete_image/{pcgalleryid}','Photographer\GalleryController@delete_image');

Route::resource('/admindashboard', 'Admin\DashboardController');
// Route::resource('/adminfaq', 'Admin\FaqController');
Route::resource('/adminprofiles', 'Admin\ProfilesController');
Route::resource('/adminbusinesssetting', 'Admin\BusinesssettingController');
Route::resource('/admincategory','Admin\CategoryController');
Route::resource('/adminposts', 'Admin\PostsController');
Route::resource('/adminaboutus', 'Admin\AboutusController');
Route::resource('/admincontactus', 'Admin\ContactusController');
Route::resource('/adminhowitworks', 'Admin\HowitworksController');
Route::resource('/adminhomephotographers', 'Admin\HomephotographersController');
Route::resource('/adminhomecomingsoons', 'Admin\HomecomingsoonsController');
Route::resource('/postsevent', 'Admin\PosteventController');

Route::resource('/admincontacts', 'Admin\ContactsController');
// Route::get('/adminposts', 'PostsController@co_destroy')
Route::resource('/admincustomer', 'Admin\CustomersController');
Route::resource('/admincustomers', 'Admin\CustomerRegisterController');
Route::resource('/admincategory','Admin\CategoryController');
Route::get('/admincategory/{id}/edit/delete_image/{catgalleryid}','Admin\CategoryController@delete_image');

Route::resource('/adminclasses','Admin\ClassesController');
Route::get('/adminclasses/{id}/edit/delete_image/{pcgalleryid}','CategoryController@delete_image'); 
Route::resource('/adminorganiser','Admin\OrganiserController');
Route::resource('/adminaddons','Admin\AddonsController');
Route::resource('/adminvolunteers', 'Admin\VolunteersController');
Route::resource('/admineventbookings','Admin\EventbookingsController');
Route::resource('/ticket','Admin\TicketsController');
Route::resource('/adminclassbooking','Admin\ClassbookingController');
Route::resource('/orders','Admin\OrdersController');
Route::post('image/upload/store','UserbookingController@fileStore');
Route::get('/photographersession/{id}/index/pc_delete/{pgalleryid}','UserbookingController@pc_delete');
Route::get('/loadmore','UserbookingController@loadDataAjax');
Route::get('/notification/{id}','UserbookingController@notification');
Route::get('/notitysec', 'UserbookingController@updatenoti');
Route::get('/feedback/{id}','UserbookingController@feedback');
Route::get('/photogharnotify', 'UserbookingController@updatephotonotify');
Route::get('subscribe', 'NewsletterController@subscribe');
Route::post('classfeedback', 'UserbookingController@classfeedback');
Route::get('/usernotiy', 'UserbookingController@adminupdatenotiy');
Route::get('ticket/create/{id}', 'Admin\TicketsController@create')->name('ticket.create');
Route::get('ticket/edit/{id}', 'Admin\TicketsController@edit')->name('ticket.edit');
Route::get('ticket/index/{id}', 'Admin\TicketsController@index')->name('ticket.index');
Route::get('/organiserregistration', 'OrganiserregistrationController@index')->name('organiserregistration');
Route::post('/organiserregistration/insert', 'OrganiserregistrationController@insert');
Route::get('/customersregistration', 'CustomersregistrationController@index')->name('customersregistration');
Route::post('/customersregistration/insert', 'CustomersregistrationController@insert');
Route::resource('/organiserdashboard', 'Organiser\ProfileController');
Route::resource('/eventbookings','Admin\EventbookingsController');
Route::get('event-listings', 'EventListingController@index');
Route::post('event-listings', 'EventListingController@index');
Route::get('event-details/{eventName}', 'EventListingController@getEventDetails')->name('event-details.getEventDetails');
Route::get('/requestdata', 'UserbookingController@requestdata');
Route::get('/sendmail', 'Admin\PosteventController@sendmail');
Route::get('customerdata/index/{id}', 'Admin\CustomerdataController@index')->name('customerdata.index');
Route::get('exportorders', 'Admin\OrdersController@exportorders');
Route::get('exportcustomer/{id}', 'Admin\CustomerdataController@exportcustomer');
Route::get('cancelorder/{id}', 'Admin\OrdersController@cancelorder');
Route::resource('contact', 'ContactController');
Route::get('/onboard-prereq','OrganiserregistrationController@organiserOnBoarding')->name('onboard-prereq.organiserOnBoarding');
Route::get('/OnBoardingRedirect','OrganiserregistrationController@OnBoardingRedirect');

/************************ start discount************************/
Route::resource('/discounts','Admin\DiscountsController');
Route::get('discounts/index/{id}', 'Admin\DiscountsController@index')->name('discounts.index');
Route::get('discounts/create/{id}', 'Admin\DiscountsController@create')->name('discounts.create');
Route::get('/discountsticket/{id}', 'Admin\DiscountsController@discountsticket')->name('discountsticket');
Route::get('/earlybirddiscounts/{id}', 'Admin\DiscountsController@earlybirddiscounts')->name('earlybirddiscounts');
Route::get('/bundleofferdiscounts/{id}', 'Admin\DiscountsController@bundleofferdiscounts')->name('bundleofferdiscounts');
Route::get('/ticketxdiscounts/{id}', 'Admin\DiscountsController@ticketxdiscounts')->name('ticketxdiscounts');
Route::get('/discountsticketedit/{id}', 'Admin\DiscountsController@discountsticketedit')->name('discountsticketedit');
Route::get('/earlybirddiscountsedit/{id}', 'Admin\DiscountsController@earlybirddiscountsedit')->name('earlybirddiscountsedit');
Route::get('/bundleofferdiscountsedit/{id}', 'Admin\DiscountsController@bundleofferdiscountsedit')->name('bundleofferdiscountsedit');
Route::get('/ticketxdiscountsedit/{id}', 'Admin\DiscountsController@ticketxdiscountsedit')->name('ticketxdiscountsedit');

/************************ end discount************************/
/************************ start ticket discount************************/
Route::resource('/ticketcollabration','Admin\TicketcollabrationController');
Route::get('/collabration/{id}', 'Admin\TicketcollabrationController@collabration')->name('collabration');
Route::get('/selfevents', 'Admin\TicketcollabrationController@selfevents')->name('selfevents');
Route::get('/collabration_org/{id}', 'Admin\TicketcollabrationController@collabration_org')->name('collabration_org'); 
Route::get('/collabrationevent', 'Admin\TicketcollabrationController@collabrationevent')->name('collabrationevent');
Route::get('/selfeventsedit/{id}', 'Admin\TicketcollabrationController@selfeventsedit')->name('selfeventsedit');
Route::get('/earlybirdedit/{id}', 'Admin\TicketcollabrationController@earlybirdedit')->name('earlybirdedit');
Route::get('/destroyticketcoll/{id}', 'Admin\TicketcollabrationController@destroyticketcoll')->name('destroyticketcoll');
Route::get('/destroyticket/{id}', 'Admin\TicketsController@destroyticket')->name('destroyticket');
Route::get('/destroydiscount/{id}', 'Admin\DiscountsController@destroydiscount')->name('destroydiscount');

Route::get('/cart', 'CartingController@index');
Route::resource('/cart', 'CartingController');
Route::get('/addtocart', 'CartingController@addtocart');
Route::get('/minCart', 'CartingController@minCart');
Route::get('/orderdetails/{orderid}', 'OrdersController@orderdetails');

Route::group(['middleware' => 'auth'], function () {
    Route::get('checkout','CheckoutController@shipping')->name('checkout.shipping');
    Route::resource('review','ProductReviewController');
});
Route::get('payment','CheckoutController@payment')->name('checkout.payment');
Route::post('store-payment','CheckoutController@storePayment')->name('payment.store');
Route::get('checkout','CheckoutController@shipping');
Route::post('/shippingstore/{id}', 'CheckoutController@shippingstore')->name('shippingstore');
Route::get('/updateorder', 'CartingController@updateorder');
Route::get('/ajaxdiscount', 'CartingController@ajaxdiscount');
Route::get('/destroy/{id}', 'Admin\CustomersController@destroy')->name('destroy');
Route::get('/destroyorganiser/{id}', 'Admin\OrganiserController@destroyorganiser')->name('destroyorganiser');
Route::get('/destroyevent/{id}', 'Admin\EventbookingsController@destroyevent')->name('destroyevent');
Route::get('/destroycontact/{id}', 'Admin\ContactusController@destroycontact')->name('destroycontact');
Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'AddMoneyController@payWithStripe'));
Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'AddMoneyController@postPaymentWithStripe'));
Route::post('/charge', 'NewCheckoutController@charge');
Route::post('addmoney/stripe', 'AddMoneyController@postPaymentWithStripe');
Route::get('/paynow', 'Admin\PosteventController@paynow')->name('paynow');
Route::post('/chargeorg', 'Admin\PosteventController@chargeorg');

/************************ Start Deals************************/
Route::get('/featured-events', 'DealsController@index');

/************************ end Deals************************/
Route::get('/ticket-deals', 'TicketdealsController@index');
Route::get('/addtocartticket', 'CartingController@addtocartticket');
Route::get('/press-enquiries', 'EnquiryController@index')->name('index');
Route::resource('/adminmarketingmaterial','Admin\MarketingController');
Route::get('/sendmailpetal', 'Admin\EventbookingsController@sendmailpetal')->name('sendmailpetal');
Route::resource('/adminpayment', 'Admin\MailController');
Route::POST('/authPayment/{id}', 'Admin\MailController@authPayment')->name('authPayment');


Route::resource('/faq','Organiser\FaqController');
Route::get('/applypromo', 'CartingController@applypromo');

Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);

Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);



/*Route::get('/titlecheck', 'Admin\EventbookingsController@titlecheck')->name('titlecheck');
*/

