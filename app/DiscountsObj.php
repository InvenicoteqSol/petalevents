<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


 class DiscountsObj  extends Model
{
	public $fillable = ['discounted_amount','discount_given','subtotal_amount','event_id','Event_Name','TicketTitle','Quantity','TicketId','rowId','tick_flag','Isbooking_fee','booking_fee'];
	
}
