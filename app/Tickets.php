<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
 				public $fillable = [
 					'event_id', 'start_date', 'end_date','ticket_type','prices','stock','start_time','end_time','ticket_details','Free_booking_fee','stock_status'
 				];
}
