<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organiserprofile extends Model
{
 				public $fillable = [
 					 'name', 'email', 'password', 'hdpwd', 'first_name', 'last_name', 'country', 'phone', 'gender', 'dob', 'profile_picture', 'user_type', 'photography_role', 'website', 'occassion_interest', 'description', 'status', 'username','organisation_name','organisation_website','address_line_1','address_line_2','postcode','security_question','security_answer',];
}
