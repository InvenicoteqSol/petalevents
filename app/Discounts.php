<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discounts extends Model
{
 	public $fillable = ['name','code_name','event_id','discount_type','discount','expiry_date','start_date','Status','is_deleted','created_by','updated_by','created_at','updated_at','user_id','bundle_offer_ticket','bundle_offer_free','ticketx','tickety','availability',];
}


