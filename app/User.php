<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'hdpwd', 'first_name', 'last_name', 'country', 'phone', 'gender', 'dob', 'profile_picture', 'user_type', 'photography_role', 'website', 'occassion_interest', 'description', 'status', 'username','organisation_name','organisation_website','address_line_1','address_line_2','postcode','security_question','security_answer','prereqComplete','stripeId','refreshToken','accessToken'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
