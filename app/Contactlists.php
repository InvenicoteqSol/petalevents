<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactlists extends Model
{
 				public $fillable = ['cont_name','cont_email','cont_phon','cont_msg','cont_sub','status','deleted','created_by','updated_by','created_at','updated_at'];
}
