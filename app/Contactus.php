<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
 				public $fillable = ['name','email','message','status','deleted','updated_at','updated_by'];
}


