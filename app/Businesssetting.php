<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businesssetting extends Model
{
 				public $fillable = [
 					'business_name', 'business_email', 'business_phone', 'business_city', 'business_state', 'business_country', 'business_address', 'business_zipcode','twitter_link','facebook_link','google_link','mailchip_keys','stripe_mode','live_publish_key','live_secret_key','test_publish_key','test_secret_key','paypal_mode','paypal_live_email','paypal_test_email'];


}
	