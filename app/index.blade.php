@extends('layouts.userdefault')
@section('content')

      <div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-9 dashboard_right">
        @include('layouts.flash-message')
        <div class="col-sm-5">
        <h3>Volunteers</h3>
      </div>

   <div class="col-sm-7 searchform">
          <div class="col-sm-8 searchbar">
        <form class="navbar-form navbar-left" name="searchfrm" id="searchfrm" method="GET" action="{{ route('adminvolunteers.index') }}">
        <div class="form-group">
          <label>Search</label>
          <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="{{ $search_data }}" placeholder="Search here">
        </div>
      </form>
    </div>
    <div class="col-sm-4 selectdiv topbar_selectiv text-right createbtn">
  <a class="editbtn" href="{{ route('adminvolunteers.create') }}"> Create New Volunteers </a>
  </div>
      </div>
      <div class="col-sm-12 tablediv spacertop">
<div class="tablewrapper">
<table class="table-bordered table-striped">

        	
               
                <thead>
                    <tr class="headings">
                        <th class="column4">Image</th>
                        <th class="column5">Volunteers Title Name</th>
                        <th class="column8">Volunteers Title Name</th>                      
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($volunteers)>0)
                     @foreach ($volunteers as $volunteer)
                    <tr class="tabledata blogtable">
                       
                       <td class="column2">
                        @if($volunteer->feature_images!='') 
                        <img src="{{ url('public') }}/uploads/volunteer/{{ $volunteer->feature_images }}" class="img-responsive" style="width: 60px; height: 50px;">
                         @else
                        <img src="{{ url('public') }}/uploads/photoclassimages/dummy.jpg" class="img-responsive" style="width: 60px; height: 50px;">
                       @endif
                        
                      </td>

                        <td class="column5">{{ $volunteer->volunteer_name }}</td>
                        <th class="column8">{{ $volunteer->volunteer_des }}</th> 

                    <td class="action_btns column3"> 
                     <a class="tablebtn" href="{{ route('adminvolunteers.edit',$volunteer->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i></a>
                     <a class="tablebtn" href="{{ route('adminvolunteers.show',$volunteer->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                     {!! Form::open(['method' => 'DELETE','route' => ['adminvolunteers.destroy', $volunteer->id],'style'=>'display:inline']) !!}
                     {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']) !!}

                       {!! Form::close() !!}
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>
  </div>
  @if(count($volunteers) > 0)
<div class="pagination_section">
<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

  {!! $posts->render() !!}
</nav>

  </div>
  
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="{{ url('/adminvolunteers') }}" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" @if($perpage=='10'){{'selected'}}@endif>10</option>
      <option value="20" @if($perpage=='20'){{'selected'}}@endif>20</option>
      <option value="50" @if($perpage=='50'){{'selected'}}@endif>50</option>
      <option value="100" @if($perpage=='100'){{'selected'}}@endif>100</option>
    </select>
    </form>
  </div>

</div>
@endif
  
<div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
                       

            
        </div>
    </div>
    

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>

@endsection
