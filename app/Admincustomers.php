<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admincustomers extends Model
{
 				public $fillable = [
 					'name', 'email', 'password', 'hdpwd', 'first_name', 'last_name', 'country', 'phone', 'gender', 'dob', 'profile_picture', 'user_type', 'status',
 				];
}
