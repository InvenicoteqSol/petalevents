<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class BookingController extends Controller
{    

    use RegistersUsers;

    protected $redirectTo = '/customerdashboard';

    public function __construct()
    {
    }

      public function index(Request $request)
    {

        try { 

                //$eventid = $request->eventid;

                if($request->eventid){
                    $eventdata = DB::table('eventbookings')->where('id', '=', $request->eventid)->first();
                } else {
                    $json = '{ "id": "", "start_date": "", "occassions": "", "shooting_locations": "", "take_all_photos": "", "booking_amount": "", "additional_hours": "", "additional_notes":""}';
                    $eventdata = json_decode($json);
                }

                $calendar = new CalendarClass();
                $calendardata = $calendar->show();

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                return view('booking.index',compact('categories','calendardata', 'eventdata'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       } 
         
    }

    public function bookingdata(Request $request){

            $created_at = date('Y-m-d H:i:s');
            $additional_hours = $request->additional_hours;

            if($request->event_dates!=''){
                   $start_date = date('Y-m-d H:i:s', strtotime($request->event_dates));

                   $eventhour = $additional_hours + 2;

                   $end_date = date('Y-m-d H:i:s',strtotime('+'.$eventhour.' hours',strtotime($start_date)));
            } else {
                $start_date = '';
                $end_date ='';
            }


        $eventamount = '3600';

        if($request->occassions!=''){
            $occassions = $request->occassions;
        } else {
            $occassions = '';
        }

        $shooting_locations = $request->shooting_locations;

        if(count($request->take_all_photos) > 0){
        $allphotos = implode(',', $request->take_all_photos);
        } else {
             $allphotos = '';
        }


        $addontotal = $request->addontotal;


        if($additional_hours!='0'){
            $eventamount = $eventamount + ($additional_hours * 1800);
        }

        $eventamount = $eventamount + $addontotal;        

        $event_id = $request->eventid;
        $additional_notes =  $request->additional_notes;

        if($event_id==''){
        $event_id = DB::table('eventbookings')->insertGetId([
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'occassions' => $occassions, 
                    'shooting_locations' => $shooting_locations, 
                    'additional_hours' => $additional_hours,
                    'take_all_photos' => $allphotos,
                    'booking_amount' => $eventamount,
                    'additional_notes' => $additional_notes,
                    'created_at' => $created_at
                 ]);
    } else {

        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('eventbookings')
                ->where('id', $event_id)
                ->update(['start_date' => $start_date, 'end_date' => $end_date, 'occassions' => $occassions, 'shooting_locations' => $shooting_locations, 'additional_hours' => $additional_hours, 'take_all_photos' => $allphotos, 'booking_amount' => $eventamount, 'additional_notes' => $additional_notes, 'updated_at' => $updated_at]);
        }

        //return view('booking.payment');

        return redirect()->action(
    'BookingController@payment', ['id' => $event_id]
);
    }


    public function payment(Request $request){
        $eventid = $request->id;
        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();

        return view('booking.payment', compact('eventid', 'BusinessInfo'));
    }

    public function usersignup(Request $request){

        $eventid = $request->id;
        $type = $request->type;
        $paymentid = $request->paymentid;
        return view('booking.signup', compact('eventid', 'type', 'paymentid'));

    }

    public function storeeventdata(Request $request)
    {   

        if($request->first_name!=''){
            $first_name = trim($request->first_name);
            } else { $first_name=''; }

            if($request->last_name!=''){
            $last_name = trim($request->last_name);
            } else { $last_name=''; }

            if($request->last_name!='' || $request->first_name!=''){
            $name = trim($request->first_name).' '.trim($request->last_name);
            } else { $name=''; }

            if($request->email!=''){
            $email = trim($request->email);
            } else { $email=''; }

            if($request->phone!=''){
            $phone = trim($request->phone);
            } else { $phone=''; }

            if($request->country!=''){
            $country = trim($request->country);
            } else { $country='';}

            if($request->password){
            $password = trim($request->password);
            } else { $password=''; }

            if($request->user_type){
            $user_type = trim($request->user_type);
            } else { $user_type=''; }

            if($request->username){
            $username = trim($request->username);
            } else { $username=''; }

            $eventid = $request->eventid;
            $type = $request->type;
            $paymentid = $request->paymentid;

            $created_at = date('Y-m-d H:i:s');

        $user = User::create([
            'first_name' => ucfirst($first_name),
            'last_name' => ucfirst($last_name),
            'name' => ucfirst($first_name).' '.ucfirst($last_name),
            'email' => $email,
            'username' => $username,
            'password' => bcrypt($password),
            'hdpwd' => $password,
            'country' => $country,
            'phone' => $phone,
            'user_type' => $user_type,
            'created_at' => $created_at
        ]);

        $user_id = $user->id;

        $updated_at = date('Y-m-d H:i:s');

        if($type=='eventbooking'){

            $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['user_id' => $user_id, 'created_by' => $user_id, 'created_by' => $user_id, 'updated_by' => $user_id, 'updated_at' => $updated_at]);

            $uppayments = DB::table('payments')
                ->where('id', $paymentid)
                ->update(['user_id' => $user_id, 'created_by' => $user_id]);

        } 

        if($type=='classbooking'){

            $upQry = DB::table('classbookings')
                ->where('id', $eventid)
                ->update(['user_id' => $user_id, 'booking_status' => '1', 'created_by' => $user_id, 'updated_by' => $user_id, 'updated_at' => $updated_at]);
                
            $uppayments = DB::table('payments')
                ->where('id', $paymentid)
                ->update(['user_id' => $user_id, 'created_by' => $user_id]);

        }

                $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
                $business_name = $BusinessInfo->business_name;
                $business_email = $BusinessInfo->business_email;
                       
                        $to = $email;
                        $subject = "Registration successfull";
                        $message = '';
                        $message .= '<html><body>';
                        $message .= "Hello ".$name.",<br />";
                        $message .=  "<br />";
                        $message .= "<b> Greeting of the day.</b><br />";
                        $message .= "<b> Congratulations!</b><br />";
                        $message .=  "<br />";
                        $message .= "<b> Your registration has been done successfully on ".$business_name.".</b><br />";
                        $message .=  "<br />";
                        $message .= "<b> Here is your login details: </b> <br />";
                        $message .= "<b> Login Email: </b> ".$email."<br />";
                        $message .= "<b> Password: </b> ".$password."<br />";
                        $message .= "<br /><br />Thanks & best regards<br />";
                        $message .= "<b> ".$business_name."</b><br />";
                        $message .= "</font>";
                        $message .= "</body></html>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: '.$business_name.'<'.$business_email.'>' . "\r\n";
                        mail($to,$subject,$message,$headers);
                            
                        $adminemail =  $business_email;
                        $adminsubject = "New user registration";
                        $adminmessage = '';
                        $adminmessage .= '<html><body>';
                        $adminmessage .= "Hello Admin,<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Greeting of the day.</b><br />";
                        $adminmessage .= "<b> Congratulations!</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> ".$name." registred successfully on ".$business_name."</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Here is user details: </b> <br />";
                        $adminmessage .= "<b> Name : </b> ".$name."<br />";
                        $adminmessage .= "<b> Email: </b> ".$email."<br />";
                        $adminmessage .= "<b> Phone: </b> ".$phone."<br />";
                        $adminmessage .= "<b> Country: </b> ".$country."<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<br /><br />Thanks & best regards<br />";
                        $adminmessage .= "<b> ".$business_name."</b><br />";
                        $adminmessage .= "</font>";
                        $adminmessage .= "</body></html>";
                        $adminheaders = "MIME-Version: 1.0" . "\r\n";
                        $adminheaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $adminheaders .= 'From: '.$business_name.'<'.$business_email.'>' . "\r\n";
                        mail($adminemail,$adminsubject,$adminmessage,$adminheaders);

       Auth::login($user);

       return redirect('/customerdashboard')
                ->with('success','Booking created successfully');
       
    }

    public function calendardates(Request $request){

        $calendar = new CalendarClass();
        $calendardata = $calendar->show(); 

        echo $calendardata;      

} 

    public function checkuseremail(Request $request){

        $email = $request->email;

    $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->get();

    if(count($existingemail) > '0'){

      echo "false";

    } else {

      echo "true";
    }        

} 


public function checkedituseremail(Request $request){

        $email = $request->email;
        $userid = $request->userid;

    $existingemail = DB::table('users')
                      ->where('email', '=', $email)
                      ->where('id', '!=', $userid)
                      ->get();

    if(count($existingemail) > '0'){

      echo "false";

    } else {

      echo "true";
    }        

} 

    function stripepayment(Request $request){

        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
        $business_name = $BusinessInfo->business_name;
        $business_email = $BusinessInfo->business_email;


         if(!empty($request->stripeToken)){

    $token  = $request->stripeToken;
    $amount = $request->amount;
    $email = $request->stripeEmail;
    $eventid = $request->eventid;
    $payment_date = date('Y-m-d');
    $created_at = date('Y-m-d H:i:s');
    
    //include Stripe PHP library
    require_once('stripe/init.php');

    if($BusinessInfo->stripe_mode=='Live'){
            $publish_key =  $BusinessInfo->live_publish_key;
            $secret_key =  $BusinessInfo->live_secret_key;
      } else {
            $publish_key =  $BusinessInfo->test_publish_key;
            $secret_key =  $BusinessInfo->test_secret_key;
      }
    
    //set api key
    $stripe = array(
      "secret_key"      => $secret_key,
      "publishable_key" => $publish_key
    );
    
    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    //add customer to stripe
    $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
    ));
    
    //item information
    $itemName = "Mrs.Portrait Event Payment";
    $currency = "HKD";
    $orderID = "Mrsportrait_".$eventid;
    
    //charge a credit or a debit card
    $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $amount.'00',
        'currency' => $currency,
        'description' => $itemName,
        'metadata' => array(
        'order_id' => $orderID
        )
    ));
    
    //retrieve charge details
    $chargeJson = $charge->jsonSerialize();

    //check whether the charge is successful
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
        //order details 
        //$amount = $chargeJson['amount'];
        $balance_transaction = $chargeJson['balance_transaction'];
        $currency = $chargeJson['currency'];
        $status = $chargeJson['status'];
        $date = date("Y-m-d H:i:s");

        $paymentid = DB::table('payments')->insertGetId(['event_id'=> $eventid, 'payment_type'=> 'eventbooking', 'payment_method'=> 'Stripe','amount' =>$amount, 'transaction_id'=>$customer->id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at]);

        $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['payment_status' => '1', 'booking_status' => '1', 'updated_at' => $date]);

                //$appurl = $_SERVER['APP_URL'];
   
        //if order inserted successfully
                if($paymentid && $status == 'succeeded'){
                        $adminemail =  $business_email;
                        $adminsubject = "Mrs.Portrait Event Booking Payment successfull ";
                        $adminmessage = '';
                        $adminmessage .= '<html><body>';
                        $adminmessage .= "Hello User,<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Greeting of the day.</b><br />";
                        $adminmessage .= "<b> Congratulations!</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Your Event Payment is done successfully on ".$business_name."</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> If you can't create your account  : </b> <br />";
                        $adminmessage .= " Please click the give below button :  <br />";
                        $adminmessage .= "<b> <a href='".url('usersignup')."/".$eventid."'> click Here </a> </b> <br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<br /><br />Thanks & best regards<br />";
                        $adminmessage .= "<b> ".$business_name."</b><br />";
                        $adminmessage .= "</font>";
                        $adminmessage .= "</body></html>";
                        $adminheaders = "MIME-Version: 1.0" . "\r\n";
                        $adminheaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $adminheaders .= 'From: '.$business_name.'<'.$business_email.'>' . "\r\n";
                        mail($email,$adminsubject,$adminmessage,$adminheaders);

                            return redirect()->action(
    'BookingController@usersignup', ['id' => $eventid, 'type' => 'eventbooking', 'paymentid' => $paymentid]
)->with('success','Payment Transaction done successfully');
    
        }else{
            return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Transaction has been failed');
          //  $statusMsg = "Transaction has been failed";
        }
    }else{
       return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Transaction has been failed');
    }
}else{

    return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Form submission error.......');

}

    }


    function paypalpayment(Request $request){
        
        $amount = $request->amt;
        $eventid = $request->item_number;
        $transaction_id = $request->tx;
        $date = date("Y-m-d H:i:s");
        $payment_date = date('Y-m-d');

        $paymentid = DB::table('payments')->insertGetId(['event_id'=> $eventid, 'payment_type'=> 'eventbooking', 'payment_method'=> 'Paypal','amount' =>$amount, 'transaction_id'=>$transaction_id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $date]);

        $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['payment_status' => '1', 'booking_status' => '1', 'updated_at' => $date]);

        $appurl = $_SERVER['APP_URL'];

        if($paymentid ){

                            return redirect()->action(
    'BookingController@usersignup', ['id' => $eventid, 'type' => 'eventbooking', 'paymentid' => $paymentid]
)->with('success','Payment Transaction done successfully');
    
        } else {

            return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Transaction has been failed');

        }
    }


     function classstripepayment(Request $request){

        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
        $business_name = $BusinessInfo->business_name;
        $business_email = $BusinessInfo->business_email;


         if(!empty($request->stripeToken)){

     $token  = $request->stripeToken;
     $amount = $request->amount;
     $email = $request->stripeEmail;
     $classid = $request->classid;
     $tutor_id = $request->tutor_id;
     $course = $request->course;
     $payment_date = date('Y-m-d');
     $created_at = date('Y-m-d H:i:s');

    
    //include Stripe PHP library
    require_once('stripe/init.php');

    if($BusinessInfo->stripe_mode=='Live'){
            $publish_key =  $BusinessInfo->live_publish_key;
            $secret_key =  $BusinessInfo->live_secret_key;
      } else {
            $publish_key =  $BusinessInfo->test_publish_key;
            $secret_key =  $BusinessInfo->test_secret_key;
      }
    
    //set api key
    $stripe = array(
      "secret_key"      => $secret_key,
      "publishable_key" => $publish_key
    );
    
    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    //add customer to stripe
    $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
    ));
    
    //item information
    $itemName = "Mrs.Portrait Class Booking Payment";
    $currency = "HKD";
    $orderID = "Mrsportrait_".$classid;
    
    //charge a credit or a debit card
    $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $amount.'00',
        'currency' => $currency,
        'description' => $itemName,
        'metadata' => array(
        'order_id' => $orderID
        )
    ));
    
    //retrieve charge details
    $chargeJson = $charge->jsonSerialize();

    //check whether the charge is successful
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
        //order details 
        //$amount = $chargeJson['amount'];
        //$balance_transaction = $chargeJson['balance_transaction'];
        //$currency = $chargeJson['currency'];
        $status = $chargeJson['status'];
        //$date = date("Y-m-d H:i:s");

        $classbookings_id = DB::table('classbookings')->insertGetId([
                    'class_id' => $classid,
                    'tutor_id' => $tutor_id,
                    'course' => $course,
                    'booking_amount' => $amount, 
                    'payment_status' => '1', 
                    'booking_status' => '0',
                    'created_at' => $created_at
                 ]);

        $paymentid = DB::table('payments')->insertGetId(['class_booking_id'=> $classbookings_id, 'payment_type'=> 'classbooking', 'payment_method'=> 'Stripe','amount' =>$amount, 'transaction_id'=>$customer->id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at]);

                $appurl = $_SERVER['APP_URL'];
   
        //if order inserted successfully
                if($paymentid && $status == 'succeeded'){
                        $adminemail =  $business_email;
                        $adminsubject = "Mrs.Portrait Class Booking Payment successfull ";
                        $adminmessage = '';
                        $adminmessage .= '<html><body>';
                        $adminmessage .= "Hello User,<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Greeting of the day.</b><br />";
                        $adminmessage .= "<b> Congratulations!</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Your Class Booking Payment is done successfully on ".$business_name."</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> If you can't create your account  : </b> <br />";
                        $adminmessage .= " Please click the give below button :  <br />";
                        $adminmessage .= "<b> <a href='".$appurl."usersignup/".$classbookings_id."'> click Here </a> </b> <br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<br /><br />Thanks & best regards<br />";
                        $adminmessage .= "<b> ".$business_name."</b><br />";
                        $adminmessage .= "</font>";
                        $adminmessage .= "</body></html>";
                        $adminheaders = "MIME-Version: 1.0" . "\r\n";
                        $adminheaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $adminheaders .= 'From: '.$business_name.'<'.$business_email.'>' . "\r\n";
                        mail($email,$adminsubject,$adminmessage,$adminheaders);

                            return redirect()->action(
    'BookingController@usersignup', ['id' => $classbookings_id, 'type' => 'classbooking', 'paymentid' => $paymentid]
)->with('success','Payment Transaction done successfully');
    
        }else{
            return redirect()->route('learn.show', $classid)
                ->with('error','Transaction has been failed');
        }
    }else{
        return redirect()->route('learn.show', $classid)
            ->with('error','Transaction has been failed');
    }
}else{

    return redirect()->route('learn.show', $classid)
            ->with('error','Form submission error.......');

}

    }

    function classpaypalpayment(Request $request){
        
         $amount = $request->amt;
         $class_id = $request->item_number;
         $course = $request->item_name;
         $tutor_id = $request->cm;
         $transaction_id = $request->tx;
         $created_at = date("Y-m-d H:i:s");
         $payment_date = date('Y-m-d');

         $classbookings_id = DB::table('classbookings')->insertGetId([
                    'class_id' => $class_id,
                    'tutor_id' => $tutor_id,
                    'course' => $course,
                    'booking_amount' => $amount, 
                    'payment_status' => '1', 
                    'booking_status' => '0',
                    'created_at' => $created_at
                 ]);

         $paymentid = DB::table('payments')->insertGetId(['class_booking_id'=> $classbookings_id, 'payment_type'=> 'classbooking', 'payment_method'=> 'Paypal','amount' =>$amount, 'transaction_id'=>$transaction_id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at]);

        $appurl = $_SERVER['APP_URL'];

        if($paymentid ){

                           return redirect()->action(
    'BookingController@usersignup', ['id' => $classbookings_id, 'type' => 'classbooking', 'paymentid' => $paymentid]
)->with('success','Payment Transaction done successfully');
    
        } else {

            return redirect()->route('learn.show', $class_id)
                ->with('error','Transaction has been failed');

        }
    }

    function forgotpassword(Request $request){
        $email = $request->email;
        $userdetail = DB::table('users')
            ->where('email', '=', $email)
            ->where('status', '=', '1')
            ->where('deleted', '=', '0')
            ->first();

            $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
            $business_name = $BusinessInfo->business_name;
            $business_email = $BusinessInfo->business_email;

            if($userdetail){

                        $adminemail =  $business_email;
                        $adminsubject = "MrsPortrait Forgot Password Request";
                        $adminmessage = '';
                        $adminmessage .= '<html><body>';
                        $adminmessage .= "Hello User,<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> You have requested for forgot password on ".$business_name."</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Here is your details: </b> <br />";
                        $adminmessage .= "<b> Username : </b> ".$userdetail->username."<br />";
                        $adminmessage .= "<b> Email: </b> ".$userdetail->email."<br />";
                        $adminmessage .= "<b> Password: </b> ".$userdetail->hdpwd."<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<br /><br />Thanks & best regards<br />";
                        $adminmessage .= "<b> ".$business_name."</b><br />";
                        $adminmessage .= "</font>";
                        $adminmessage .= "</body></html>";
                        $adminheaders = "MIME-Version: 1.0" . "\r\n";
                        $adminheaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $adminheaders .= 'From: '.$business_name.'<'.$business_email.'>' . "\r\n";

                        if(mail($email,$adminsubject,$adminmessage,$adminheaders)){
                            //echo 'yes';
                            return redirect('password/reset')->with('success', 'Email send successfully');
                        //return Redirect::back()->with('success', 'Email send successfully');
                    } else {
                        //echo 'no';
                        return redirect('password/reset')->with('error', 'Email not Send');
                        //return Redirect::back()->with('error', 'Email not Send');
                    }
            } else {
                return redirect('password/reset')->with('error', 'Email does not exist');
            }

    }

     function discountpayment(Request $request){
            $couponcode = $request->couponcode;
            $eventid = $request->eventid;
            $today = date('Y-m-d');
            $created_at = date("Y-m-d H:i:s");

            $coupondata = DB::table('coupens')
                ->where('coupen_code', '=', $couponcode)
                ->where('vaild_start_date', '<', $today)
                ->where('vaild_end_date', '>=', $today)
                ->where('status', '=', '1')
                ->where('deleted', '=', '0')->first();

            if($coupondata){
                $reedem_id = DB::table('payments')->insertGetId([
                    'event_id' => $eventid,
                    'payment_type' => 'eventbooking', 
                    'payment_method' => 'Coupon', 
                    'amount' => $coupondata->coupen_prices,
                    'transaction_id' => $coupondata->coupen_code,
                    'payment_status' => '1',
                    'payment_date' => $today,
                    'created_at' => $created_at
                 ]);

                if($reedem_id){

                     $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['coupon_id' => $coupondata->id]);

                    return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('success','Coupon discount apply successfully');

                } else {
                    return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Coupon discount not apply');
                }
            } else {
                return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Coupon code is not valid');
            }
     }

    
}

