<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Classbooking;
use App\Contactlists;
use Session;
use DB;
use Hash;
use Image;
use Mail;
use App\Classes\ErrorsClass;


class OrganiserregistrationController extends Controller
{    
   
     protected $redirectTo = '/adminorganiser';


    public function __construct()
    {
    }

     public function index(Request $request)
    {
        $showsearch="true";
        return view('pages.organiserregistration',compact('showsearch'));

    }

    public function insert(Request $request)
    {   
        $showsearch="true";
    
        $user = new User;
        $user->first_name = ucfirst(trim($request->first_name));
        $user->last_name =   trim($request->last_name);
        $user->organisation_name = $request->organisation_name; 
        $user->website = $request->website ; 
        $user->address_line_1 = $request->address_line_1; 
        $user->address_line_2 = $request->address_line_2; 
        $user->postcode = $request->postcode; 
        $user->country = $request->country;
        $user->phone = $request->phone;
        $user->email = $request->email; 
        $user->username = $request->username; 
        $user->password = bcrypt($request->password);
        $user->hdpwd = $request->password;
        $user->security_question = $request->security_question; 
        $user->security_answer = $request->security_answer; 
        $user->user_type = $request->user_type;
    if($request->user_type==4)
    {
    $user->prereqComplete=0;
    }
    
        $user->created_by = Auth::id();
        $user->save();


       $data = array('first_name'=>$user->first_name,
                     'email'=>$user->email,
                     'phone'=>$user->phone,
                     'subject'=>'Petal Events - Welcome Aboard - '.$user->first_name.' !!!'
        );
      
      Mail::send(['text'=>'mail'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.reply@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });

      return redirect()->route('organiserregistration',compact('showsearch'))

                        ->with('success','Registration Successful.');
         

         }
     public function OnBoardingRedirect(Request $request)
     {
       $showsearch="true";
      function curl_get_contents($authCode)
              {
                  $key=DB::table('business_settings')->first();
                  $test=$key->stripe_mode;
                  $test_secret_key=$key->test_secret_key;
                  $live=$key->stripe_mode;
                  $live_secret_key=$key->live_secret_key;
         
        $ch = curl_init();
                  $timeout = 5;
    if($test=="Test"){ 
        $headers = array('Authorization: Bearer '.$test_secret_key);
        $post_data='client_secret='.$test_secret_key.'&code='.$authCode.'&grant_type=authorization_code';
     }else {
       $headers = array('Authorization: Bearer '.$live_secret_key );
       $post_data='client_secret='.$live_secret_key.'&code='.$authCode.'&grant_type=authorization_code';
    }
                  curl_setopt($ch, CURLOPT_URL, "https://connect.stripe.com/oauth/token");
                  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($ch, CURLOPT_HEADER, false);
                  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
                  $data = curl_exec($ch);
                  return $data1=json_decode($data);
        
        }
      if( $_REQUEST['code'] !='')
      {
         $dataresult = curl_get_contents($_REQUEST['code']);
         if(array_key_exists('error',$dataresult))
         {
          $stripe=$dataresult->{'error'};
           return view('Organiser.obboardingPrereq',compact('showsearch'))

                        ->with('error',$dataresult->{'error_description'});
        }
        else{
        $stripeId=$dataresult->{'stripe_user_id'};
        $refreshToken=$dataresult->{'refresh_token'};
        $access_token=$dataresult->{'access_token'};
         $user = User::find(Auth::id());
         $user->stripeId=$stripeId;
         $user->refreshToken=$refreshToken;
         $user->accessToken=$access_token;
         $user->prereqComplete=1;
         $user->save();
         return redirect()->intended('/organiserdashboard');
        }
            
      }
      else{
      return view('Organiser.obboardingPrereq',compact('showsearch'))

                        ->with('error','Error in processing your request, please try again later.');
      
      }
      
     
     }
     public function organiserOnBoarding(Request $request)
     {
      $showsearch="true";
       return view('Organiser.obboardingPrereq',compact('showsearch')); 
     
     }
    
   
        
 }
