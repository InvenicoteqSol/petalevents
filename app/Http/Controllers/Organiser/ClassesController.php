<?php
namespace App\Http\Controllers\Photographer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Photoclasses;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class ClassesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)
    {
         try { 
             
             $id = Auth::user()->id;
             
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '6';
               }
                $query = DB::table('photoclasses');
                               $query->where('deleted', '=', 0);
                               if($search_data!='') {
                                $query->where('class_title', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('pc_learn', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('pc_requirement', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('pc_description', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('pc_price', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('pc_duration', 'LIKE', '%'.$search_data.'%');
                               }
                               $query->where('user_id', '=' , $id);
                               $query->orderBy('id', '=' , 'DESC');

                $photoclasses  = $query->paginate($perpage);
                $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();                

             return view('Photographer.Classes.index',compact('photoclasses', 'search_data','eventbookings','perpage'))

                        ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    // public function index(Request $request)
    // {
    //      try { 
    //         $id = Auth::user()->id;
    //          $search_data =  trim($request->search_input); 

    //          if($request->perpage!=''){
    //                 $perpage = $request->perpage;
    //            } else {
    //                 $perpage = '10';
    //            } 

    //          if($search_data!='') {

    //             $photoclasses = DB::table('photoclasses')
    //                             ->where('deleted', '=', 0)
    //                             ->where('status', '=', 1)
    //                             ->where('user_id', '=', $id)
    //                             ->where(function($query) use ($search_data){
    //                             $query->where('class_title', 'LIKE', '%'.$search_data.'%');
    //                             $query->orWhere('pc_learn', 'LIKE', '%'.$search_data.'%');
    //                             $query->orWhere('pc_requirement', 'LIKE', '%'.$search_data.'%');
    //                             $query->orWhere('pc_description', 'LIKE', '%'.$search_data.'%');
    //                             $query->orWhere('pc_price', 'LIKE', '%'.$search_data.'%');
    //                             $query->orWhere('pc_duration', 'LIKE', '%'.$search_data.'%');
    //                         })
    //                         ->orderBy('id','DESC')
    //                         ->paginate($perpage);

    //          } else {
                
    //             $photoclasses = DB::table('photoclasses')
    //                            ->where('deleted', '=', 0)
    //                             ->where('status', '=', 1)
    //                             ->where('user_id', '=', $id)
    //                            ->orderBy('id','DESC')
    //                            ->paginate($perpage);

    //          }
               
    //          return view('Photographer.Classes.index',compact('photoclasses', 'search_data', 'perpage'))

    //                     ->with('i', ($request->input('page', 1) - 1) * $perpage);
    //         } catch (\Illuminate\Database\QueryException $e) {
    //             $errorClass = new ErrorsClass();
    //             $errors = $errorClass->saveErrors($e);
    //         } catch (\Exception $e) {
    //            $errorClass = new ErrorsClass();
    //            $errors = $errorClass->saveErrors($e);
    //     }   
    // }

     public function create()
    {
         $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            // ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();                     
     return view('Photographer.Classes.create',compact('eventbookings'));

    }

   public function store(Request $request)
    {
        $this->validate($request, array(

            'class_title' => 'required|string|max:191|unique:photoclasses',
        ));
        
        $photoclass = new Photoclasses;

        if(trim($request->pc_start_date)!='') {
            $pc_start_date_org = trim($request->pc_start_date);
            $pc_start_date_Arr = explode('-',$pc_start_date_org);
            $pc_start_date = $pc_start_date_Arr[2].'-'.$pc_start_date_Arr[1].'-'.$pc_start_date_Arr[0];
        } else {
            $pc_start_date = '';
        }

        if(trim($request->pc_end_date)!='') {
            $pc_end_date_org = trim($request->pc_end_date);
            $pc_end_date_Arr = explode('-',$pc_end_date_org);
            $pc_end_date = $pc_end_date_Arr[2].'-'.$pc_end_date_Arr[1].'-'.$pc_end_date_Arr[0];
        } else {
            $pc_end_date = '';
        }
       
        $photoclass->user_id= Auth::id();
        $photoclass->class_title = ucfirst(trim($request->class_title));
        $photoclass->pc_price = trim($request->pc_price);
        $photoclass->pc_learn = ucfirst(trim($request->pc_learn));
        $photoclass->pc_requirement = ucfirst(trim($request->pc_requirement));
        $photoclass->pc_description = ucfirst(trim($request->pc_description));
        $photoclass->pc_start_date = trim($pc_start_date);
        $photoclass->pc_end_date = trim($pc_end_date);
        $photoclass->pc_duration = trim($request->pc_duration);
        $photoclass->pc_assign = Auth::id();
        $photoclass->created_by = Auth::id();

        if($request->hasFile('pc_image')) {
            $image = $request->file('pc_image');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/photoclassimages/'), $filename); 
            $photoclass->pc_image = $filename;
        }

        if($request->hasFile('pc_video')) {
            $image = $request->file('pc_video');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/photoclassvideos/'), $filename); 
            $photoclass->pc_video = $filename;
        }  

        $photoclass->save();

        $insertedId = $photoclass->id;

        if($request->hasfile('pcgallery_images'))
         {
            foreach($request->file('pcgallery_images') as $image )
            {
                $filename = time() . '-' . $image->getClientOriginalName();
                $image->move(public_path('uploads/photoclassimages/'), $filename); 

                DB::table('pcgallery')->insert(
                    ['pc_id' => $insertedId,'pcglry_image' => $filename]
                );
            }
         }

        return redirect()->route('photographerclasses.index')

                        ->with('success','Class created successfully');
    }



    public function show($id)
    {
         try { 
        $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();

        $photoclass = Photoclasses::findOrFail($id); 
                               
        $pcgalleryimages = DB::table('pcgallery')
                            ->where('pc_id', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();                        
        return view('Photographer.Classes.show',compact('photoclass', 'pcgalleryimages','eventbookings')); 



        } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

public function edit($id)
    {
        try {   
                /*$addon = User::find($id);*/
                //$addon = User::findOrFail($id);
                $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();

                $photoclass = Photoclasses::where('id', $id)
                                ->first();

                $pcgalleryimages = DB::table('pcgallery')
                            ->where('pc_id', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
                                            
                return view('Photographer.Classes.edit',compact('photoclass', 'pcgalleryimages','eventbookings'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            
         $input=Input::all();
         $values=array( 
                    'class_title' => 'required|string|max:191|unique:photoclasses,class_title,'.$id,
                    );

            $validator=Validator::make($input, $values);
            
            if($validator->fails()) {
               return Redirect::to('Photographerclasses/'.$id.'/edit')->withErrors($validator);
               // return Redirect::to('photoclasses/'.$id.'/edit')->withErrors(['class_title'=>'The class name has already been taken.']);
            } else {      

        if(trim($request->pc_start_date)!='') {
            $pc_start_date_org = trim($request->pc_start_date);
            $pc_start_date_Arr = explode('-',$pc_start_date_org);
            $pc_start_date = $pc_start_date_Arr[2].'-'.$pc_start_date_Arr[1].'-'.$pc_start_date_Arr[0];
        } else {
            $pc_start_date = '';
        }

        if(trim($request->pc_end_date)!='') {
            $pc_end_date_org = trim($request->pc_end_date);
            $pc_end_date_Arr = explode('-',$pc_end_date_org);
            $pc_end_date = $pc_end_date_Arr[2].'-'.$pc_end_date_Arr[1].'-'.$pc_end_date_Arr[0];
        } else {
            $pc_end_date = '';
        }

        $class_title = ucfirst(trim($request->class_title));
        $pc_price = trim($request->pc_price);
        $pc_learn = ucfirst(trim($request->pc_learn));
        $pc_requirement = ucfirst(trim($request->pc_requirement));
        $pc_description = ucfirst(trim($request->pc_description));
        $pc_start_date = trim($pc_start_date);
        $pc_end_date = trim($pc_end_date);
        $pc_duration = trim($request->pc_duration);
        /*$pc_assign = trim($request->pc_assign);*/

        $status = trim($request->status);
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('photoclasses')
                ->where('id', $id)
                ->update(['class_title' => $class_title, 'pc_price' => $pc_price, 'pc_learn' => $pc_learn, 'pc_requirement' => $pc_requirement, 'pc_description' => $pc_description, 'pc_start_date' => $pc_start_date, 'pc_end_date' => $pc_end_date, 'pc_duration' => $pc_duration ,'status' => $status, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);
        

        if($request->hasFile('pc_image')) {
            $image = $request->file('pc_image');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/photoclassimages/'), $filename); 
            $upImgQry = DB::table('photoclasses')
            ->where('id', $id)
            ->update(['pc_image' => $filename]);
        } 

        if($request->hasfile('pcgallery_images'))
         {
            foreach($request->file('pcgallery_images') as $image )
            {
                $filename = time() . '-' . $image->getClientOriginalName();
                $image->move(public_path('uploads/photoclassimages/'), $filename); 

                DB::table('pcgallery')->insert(
                    ['pc_id' => $id,'pcglry_image' => $filename]
                );
            }
         }

         if($request->hasFile('pc_video')) {
            $image = $request->file('pc_video');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/photoclassvideos/'), $filename); 
            $upImgQry = DB::table('photoclasses')
            ->where('id', $id)
            ->update(['pc_video' => $filename]);
        } 
                                  
        return redirect()->route('photographerclasses.index')

                        ->with('success','Class data updated successfully');

          }             

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('photoclasses')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('photographerclasses.index')
                           ->with('success','Class deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('addons.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('addons.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    public static function delete_image(Request $request)
    {    
      try { 
         
        $pcgalleryid = $request->pcgalleryid;
        $delquery = DB::table('pcgallery')->where('id', $pcgalleryid)->delete();
        if($delquery) {
           echo '1';
         } else {
            echo '0';
          }

          } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }




















}



