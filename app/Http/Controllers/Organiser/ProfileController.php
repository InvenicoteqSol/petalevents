<?php

namespace App\Http\Controllers\Organiser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)
    {
    	$id = Auth::user()->id;
        $organiser = User::findOrFail($id);

      return view('Organiser.Profile.index',compact('organiser'));
    }

    public function edit($id)
    {
         
                $organiser = User::where('id', $id)
                                ->first();
                $towns = DB::table('all_towns')->pluck('id', 'town_name');

                $organiser_role = DB::table('photographer_role')
                            ->where('role_status', '=', 1)
                             ->get();

                return view('Organiser.Profile.edit',compact('organiser', 'towns', 'organiser_role'));

       
    }

    public function show()
    {
    	
    }

    public function update(Request $request, $id)
    {
        
        if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }
        if(trim($request->organisation_name)!='') {
            $organisation_name = trim($request->organisation_name);
        } else {
            $organisation_name = '';
        }

        if(trim($request->website)!='') {
            $website = trim($request->website);
        } else {
            $website = '';
        }

        if(trim($request->address_line_1)!='') {
            $address_line_1 = trim($request->address_line_1);
        } else {
            $address_line_1 = '';
        }

        if(trim($request->address_line_2)!='') {
            $address_line_2 = trim($request->address_line_2);
        } else {
            $address_line_2 = '';
        }

        if(trim($request->postcode)!='') {
            $postcode = trim($request->postcode);
        } else {
            $postcode = '';
        }

         if(trim($request->country)!='') {
            $country = trim($request->country);
        } else {
            $country = '';
        }
         if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }
         if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }
         if(trim($request->username)!='') {
            $username = trim($request->username);
        } else {
            $username = '';
        }
         if(trim($request->password)!='') {
            $password = trim($request->password);
        } else {
            $password = '';
        }
         if(trim($request->security_question)!='') {
                    $security_question = trim($request->security_question);
                } else {
                    $security_question = '';
                }
         if(trim($request->security_answer)!='') {
                    $security_answer = trim($request->security_answer);
                } else {
                    $security_answer = '';
                }
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['first_name' => $first_name, 'last_name' => $last_name, 'organisation_name' => $organisation_name, 'website' => $website, 'address_line_1' => $address_line_1, 'address_line_2' => $address_line_2, 'postcode' => $postcode,'country' => $country, 'phone' => $phone, 'email' => $email, 'username' => $username, 'password' => $password, 'security_question' => $security_question, 'security_answer' => $security_answer, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        if($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/profileimages/'), $filename); 
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profile_picture' => $filename]);
        }
         $id = Auth::user()->id;
        $organiser = User::findOrFail($id);          

        return redirect()->route('organiserdashboard.index')

                        ->with('success','Organiser data updated successfully');

       
    }

    
}
