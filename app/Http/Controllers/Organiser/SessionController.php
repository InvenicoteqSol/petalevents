<?php

namespace App\Http\Controllers\Photographer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Eventgallery;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class SessionController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request){
         
        try {

        $id = Auth::user()->id;
        $customer = User::findOrFail($id); 

    $search_data =  trim($request->search_input); 

        $page = $request->input('page');
        $type = $request->type;

               if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '10';
               } 

             $query = DB::table('eventbookings');
             $query->where('booking_status', '=', 1);
             $query->where('payment_status', '=', 1);
             $query->where('status', '=', 1);;
             $query->where('deleted', '=', 0);
             $query->where('photographer_id', '=', $id);
             if($type=='complete'){
             $query->where('start_date', '<', date('Y-m-d'));
             } else {
             $query->where('start_date', '>', date('Y-m-d'));
             }
             $query->orderBy('id','DESC');
             $eventbookings = $query->paginate($perpage);

                
                return view('Photographer.Session.index',compact('customer', 'eventbookings','page','search_data', 'type' , 'perpage'))
                 ->with('i', ($request->input('page', 1) - 1) * $perpage);;

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
        //return view('Photographer.session');
    }
   public function show($id)
    {
       $userid = Auth::user()->id;
       $customer = User::findOrFail($userid); 

        $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $userid)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();

      $eventbooking = DB::table('eventbookings') ->where('id','=', $id)->where('status', '=', 1)->where('deleted', '=', 0) ->first(); 

        $session = DB::table('categories')->where('id', '=', $eventbooking->occassions)->first();
        $client_detail = DB::table('users')->where('id', '=', $eventbooking->user_id)->first();
        $userdetail = DB::table('users')->where('id', '=', $userid)->first();
        $eventgallery = DB::table('eventgallery')
                            ->where('eventid', '=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();
        $eventsinglimage = DB::table('eventgallery')
                            ->where('eventid', '=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->first();


        return view('Photographer.Session.show',compact('eventbooking','customer','session','userdetail', 'eventbookings','client_detail', 'eventgallery' ,'eventsinglimage')); 

       
    }

  /* public function store(Request $request)
     {
         
      $eventbooking = DB::table('eventbookings') ->where('id','=', $id)->where('status', '=', 1)->where('deleted', '=', 0) ->first(); 
      $evt_id=$eventbooking->id;

        if($request->hasfile('file'))
                 {
                    foreach($request->file('file') as $image )
                      {
                $filename = time() . '-' . $image->getClientOriginalName();
                $image->move(public_path('uploads/photoclassimages/'), $filename); 

        DB::table('eventgallery')->insert(
                    ['eventid' => $evt_id,'filename' => $filename]
                );
          }
        }
     }*/
}
