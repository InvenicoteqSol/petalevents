<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class CommunityController extends Controller
{    

    public function __construct()
    {

    }

      public function index(Request $request $id)
    {
       // $userid = Auth::user()->id;
       // $id = User::findOrFail($userid); 

      $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate(6);
        

             $categories = DB::table('categories')
              ->where('status', '=', 1)
             ->where('deleted', '=', 0)
             ->get();

             $galleryevent = DB::table('eventgallery')
                           ->where('id','=', $eventbookings->user_id)
                             ->where('eventid','=', $id)
                             ->where('status', '=', 1)
                             ->where('deleted', '=', 0)
                             ->orderBy('id','DESC')
                             ->get();         

       return view('community.index',compact('eventbookings', 'categories','galleryevent'));
    }
    
     public function clientalbum(Request $request)
    {

       return view('community.client-album');
    }
   
    public function clientview(Request $request)
    {

       return view('community.client-single');
    }

    public function volunteersession(Request $request)
    {

       return view('community.volunteer-session');
    }

    public function show($id)
    {   
         $event = DB::table('eventbookings')
                            ->where('id','=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first(); 

        $userinfo = DB::table('users')
                            // ->where('id','=', $class->user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
          $categories = DB::table('categories')
              ->where('status', '=', 1)
             ->where('deleted', '=', 0)
             ->first();                 

       
        $eventgalleryimages = DB::table('eventgallery')
                             ->where('eventid','=',$event->user_id)
                             ->where('status', '=', 1)
                             ->where('deleted', '=', 0)
                             ->orderBy('id','DESC')
                             ->first();   
                  
        // $BusinessInfo = DB::table('business_settings')->where('status', '1')->first(); 
                    
        return view('community.show',compact('event','userinfo','categories','$eventgalleryimages'));
    }

    
}

