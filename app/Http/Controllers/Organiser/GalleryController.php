<?php

namespace App\Http\Controllers\Photographer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use App\Pcgallery;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    { 
        $id = Auth::user()->id;
        $photographer = User::findOrFail($id);
        $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();
          return view('Photographer.Gallery.index',compact('photographer', 'eventbookings'));
    }

      public function store(Request $request)
    {
              $photogallery = new Pcgallery;
              $photogallery->user_id= Auth::id();

              if($request->hasfile('pcgallery_images'))
                 {
                    foreach($request->file('pcgallery_images') as $image )
                      {
                $filename = time() . '-' . $image->getClientOriginalName();
                $image->move(public_path('uploads/photoclassimages/'), $filename); 

                DB::table('pcgallery')->insert(
                    ['pc_id' => $photogallery->user_id,'pcglry_image' => $filename]
                );
            }
         }
          return redirect()->route('photographergallery.index')

                        ->with('success','Images upload successfully');



    }


public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('photoclasses')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('photographergallery.index')
                           ->with('success','Class deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('addons.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('addons.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    public static function delete_image(Request $request)
    {    
         $pcgalleryid = $request->pcgalleryid;
        $delquery = DB::table('pcgallery')->where('id', $pcgalleryid)->delete();
        if($delquery) {
           echo '1';
         } else {
            echo '0';
          }

         
    }


     
}


