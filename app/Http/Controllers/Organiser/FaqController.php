<?php

namespace App\Http\Controllers\Organiser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index(Request $request)
    {
    	

      return view('Organiser.FAQ.index');
    }

//     public function getDownload($filename)
//     {
//     // Define the path and the extension
//     $file = public_path() . '/uploads/'. $filename;
//     $ext = pathinfo($filename, PATHINFO_EXTENSION);

//     if($ext == 'png' || 'PNG'){
//       $headers = array(
//           'Content-Type' => 'image/png',
//         );
//     }

//     else if($ext == 'jpg' || 'jpeg' || 'JPEG' || 'JPG'){
//       $headers = array(
//           'Content-Type' => 'image/jpeg',
//         );
//       }

//       return response()->download($file, $filename, $headers);
// }
}