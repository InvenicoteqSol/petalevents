<?php

namespace App\Http\Controllers\Photographer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        try { 

          $id = Auth::user()->id;
         $customer = User::findOrFail($id);

         $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('photographer_id', '=', $id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();

        if(count($eventbookings) > 0){
            $userevents = array();
            foreach($eventbookings as $session){
                /*  if (!in_array($conceptresult['concept_name'], $conceptsname)){
                      array_push($conceptsname, $conceptresult['concept_name']);
                    }*/
                     if (!in_array($session->id, $userevents)){
                      array_push($userevents, $session->id);
                    }
            }
        } else {
            $userevents = array();
        }

        $query = DB::table('payments');
                    $query->where('deleted', '=', 0);
                    $query->WhereIn('event_id', $userevents);
                    $query->orderBy('id','DESC');
        $payments = $query->paginate(10);

        return view('Photographer.Payment.index',compact('customer', 'eventbookings', 'payments'));   

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }                
    }

    public function show($id)
    {

        try { 

        $user_id = Auth::user()->id;
        $customer = User::findOrFail($user_id);
        $eventbookings = DB::table('eventbookings')
                            ->where('booking_status', '=', 1)
                            ->where('payment_status', '=', 1)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('user_id', '=', $user_id)
                            ->where('start_date', '>', date('Y-m-d'))
                            ->get();  

        $payment = DB::table('payments')->where('id', '=', $id)->first();
        if($payment->event_id!='' && $payment->event_id!='null'){   
            $event = DB::table('eventbookings')->where('id', '=', $payment->event_id)->first();
            $category = DB::table('categories')->where('id', '=', $event->occassions)->first();
        } else {
            $event = array();
            $category = array();
        }
        if($payment->class_booking_id!='' && $payment->class_booking_id!='null'){ 
            $classbookings = DB::table('classbookings')->where('id', '=', $payment->class_booking_id)->first();
            $photoclass = DB::table('photoclasses')->where('id', '=', $classbookings->class_id)->first();
        } else {
            $classbookings = array();
            $photoclass = array();
        }

    return view('Photographer.Payment.show', compact('customer', 'eventbookings', 'payment', 'event', 'category', 'photoclass', 'classbookings')); 

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  

         
    }
}
