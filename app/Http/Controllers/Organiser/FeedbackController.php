<?php

namespace App\Http\Controllers\Photographer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class FeedbackController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request){
         
        try {

        $id = Auth::user()->id;
        $customer = User::findOrFail($id); 
        $page = $request->input('page');
        $type = $request->type;

               if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '1';
               } 

             $query = DB::table('eventbookings');
             $query->where('booking_status', '=', 1);
             $query->where('payment_status', '=', 1);
             $query->where('status', '=', 1);;
             $query->where('deleted', '=', 0);
             $query->where('photographer_id', '=', $id);
             if($type=='complete'){
             $query->where('start_date', '<', date('Y-m-d'));
             } else {
             $query->where('start_date', '>', date('Y-m-d'));
             }
             $query->orderBy('id','DESC');
             $eventbookings = $query->paginate(10);

        $feedbacks= DB::table('feedback')
        		->where('pc_id', '=', $id)
        		->get();
                return view('Photographer.Feedback.index',compact('customer', 'eventbookings','page', 'type' , 'perpage','feedbacks'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
        //return view('Photographer.session');
    }
 
}

