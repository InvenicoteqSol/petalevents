@extends('layouts.default')
@section('content')
<!--content-section-start-->
<main>
 <section class="innercontentpart">
  
<div class="innercontent_text">
       <div class="container">
      <h1>Real Clients Photo Album</h1>
        <div class="row">
          

          <?php if(count($eventbookings) > 0) { ?>
          

          <div class="col-sm-12 classblks_div">
            

            <?php 
            $eventcount = 1;
            foreach($eventbookings as $event){ 
              if($eventcount%3=='1'){
                echo '<div class="col-sm-12">';
              }
              ?>
           
            <?php $id= $event->id;
         $eventbookings = DB::table('eventbookings')
                ->where('id','=', $id)
                ->where('deleted', 0)
                ->first();
         $assignid= $eventbookings->photographer_id;
         $userinfo = DB::table('users')
                ->where('id','=', $assignid)
                ->where('deleted', 0)
                ->first();
                ?>            

        <div class="col-sm-4 classblk">
              <a href="{{ route('community.show',$event->id) }}">
              <div class="classblk_bg">

               <img src="{{ url('/public') }}/images/corporateevent.png" class="img-responsive">
              
              <div class="class_content">
                <div class="row">
                <div class="col-sm-6">
            <span class="postdate">{{ date('F d, Y', strtotime($event->created_at)) }}</span>
            </div>
             <div class="col-sm-6 hearticon text-right">
              <?php
                                if($event->occassions!='') {
                                     $catnameArray = array(); 
                                     $ocsioncatArr = explode(',', $event->occassions);
                                    foreach($ocsioncatArr as $ocsioncatid) {
                                      $catNames = DB::table('categories')->where('id', '=', $ocsioncatid)->get();
                                      foreach($catNames as $ctName) {
                                          $catnameArray[] = $ctName->cat_name;
                                         }
                                      }
                                       echo implode(', ', $catnameArray);  
                                      }
                                     ?>   
              
            </div>
          </div>
              <p>{{$userinfo->first_name}} </p>
             
                <p>{!! \Illuminate\Support\Str::words($event->additional_notes, 20,'')  !!}</p>
              
              <div class="row">
                <div class="col-sm-8">
             <p class="class_reviewstars"><i class="fa fa-star"></i>
              <i class="fa fa-star"></i><i class="fa fa-star"></i>
              <i class="fa fa-star"></i><i class="fa fa-star"></i></p>
            </div>
            <div class="col-sm-4 hearticon text-right"><i class="fa fa-heart"></i>
              
            </div>
          </div>
            </div>
          </div>
          </a>
            </div>
<?php 
            if($eventcount%3=='0'){
                echo '</div>';
              }
              $eventcount++;
              } 

              ?>
</div>
<?php } ?>
</div>
</div>
        <div class="photoidea_sec greybg">
          <div class="container">
            <a href="{{ route('community.blogshow',$posts->id) }}">
             <h1>Photography Idea</h1>
             <div class="col-sm-6 photoidea_leftsec">
                  <?php 
                     $query = DB::table('posts');
                     $query->where('deleted', 0);
                     $query->where('status', 1);
                     $query->orderBy('id','DESC');
                     $query->limit(1);
                     $querys = $query->get(); ?>

           @foreach($querys as $query)
             @if($query->feature_images!='')
              <img src="{{ url('public') }}/uploads/post/feature_post/{{ $query->feature_images }}" class="img-responsive">
              @else
              <img src="{{url('/public')}}/images/dummy.jpg" class="img-responsive">
              @endif

              <div class="photoidea_content">
                <h3>Best Places in Paris To Get The Best View of Eiffel Tower!</h3>
                <p>{{str_limit($query->description, 100)}}</p>
                <p class="postdates">
                  <span>BY: Admin</span>
                  <span>{{ date('F d, Y', strtotime($query->created_at)) }}</span>
                </p>
              </div>
              @endforeach
         
        </div>
                  <div class="col-sm-6 photoidea_rightsec">

                    <div class="row">
              @foreach($posts AS $post)
               <div class="col-sm-6 photosmall_sec">
                   @if($post->feature_images!='')
                  <img src="{{ url('public') }}/uploads/post/feature_post/{{ $post->feature_images }}" class="img-responsive">
                  @else
                  <img src="{{url('/public')}}/images/dummy.jpg" class="img-responsive">
                  @endif
                  <div class="photoidea_content">
                       <h3>Best Places in Paris To Get The Best 
                        View of Eiffel Tower!</h3>
                        <p class="postdates"> <span>{{ date('F d, Y', strtotime($post->created_at)) }}</span> </p>
                   </div>
                </div>
              @endforeach
              </div>

        </div>
        <div class="seemore_btn text-center">
          <a href="#">Read More</a>
        </div>
      <!--  -->
      


          </a>
      </div>
          </div

          <div class="society_sec">
              <div class="society_content">
                <h1>Help This Society</h1>
                <a href="{{ url('/volunteer-session') }}">
                <div class="society_imgsec">
                  <h1>Volunteer <br/> Photographar</h1>
                </div>
              </a>
            </div>
        </div>
 </section>
</main>
<!--content-section-end-->
@endsection