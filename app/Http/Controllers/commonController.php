<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
class CommunityController extends Controller
{    

    public function __construct()
    {

    }
    public function state($countryId)
    {
    	$state = DB::table('state')->pluck('id', 'country_name');
    	$state->where('countryid',countryId);
    	return $state;
    }
 }