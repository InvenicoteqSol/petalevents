<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class EventListingController extends Controller
{    
    public function __construct()
    {
    }
    public function index(Request $request)
    {
        try { 
            $location=$request->location;
            $start_date=$request->start_date;
            $end_date=$request->end_date;
            $location=$request->category;
            $showsearch="true";
            $startdate= date("Y-m-d", strtotime($start_date) );
            $enddate= date("Y-m-d", strtotime($end_date) );

             $search_data =  $request->search_input;
             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '10';
               }
                if($search_data!='') {
                $events = DB::table('eventbookings')->where('deleted', '=', 0)->where('status','=',1)->where(function($query) use ($search_data,$startdate,$enddate){
                                $query->where('event_title', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('venue_name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('venue_address', 'LIKE', '%'.$search_data.'%');
                                
                                 })

                            ->orderBy('id','DESC')
                            ->paginate($perpage);

                            if($request->start_date!='' || $request->end_date!='')
                            {
                                 $events=DB::select("SELECT * FROM eventbookings WHERE start_date BETWEEN '$startdate' AND '$enddate' AND deleted='0'");
                            }

                            if($request->location!='')
                            {
                                $events=$events->where('venue_name','=',$request->location);
                            }
                            if($request->category!='')
                            {
                                $events=$events->where('type_of_event','=',$request->category);
                            }
                            
             } else {
                
                 $events = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);

                            if($request->start_date!='' || $request->end_date!='')
                            {   
                                 $events=DB::select("SELECT * FROM eventbookings WHERE start_date BETWEEN '$startdate' AND '$enddate' AND deleted='0'");
                            }
                                                        
                             if($request->location!='')
                            {
                                $events=$events->where('venue_name','=',$request->location);
                            }
                            if($request->category!='')
                            {
                                $events=$events->where('type_of_event','=',$request->category);
                            }
            
             }

             return view('Eventslisting.listings',compact('events', 'search_data','perpage','showsearch'))

                         ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }
    
}
public function getEventDetails($eventName)
{
    $showsearch="false";
    if($eventName!=''){
        try{
     $events = DB::table('eventbookings')->where('event_title','=',$eventName)->get();
     $eventsId = DB::table('eventbookings')->where('event_title','=',$eventName)->first();
     $id=$eventsId->id;
     $tickets=DB::table('tickets')->where('event_id','=',$id)->where('deleted', '=', 0)->get();
     return view('Eventslisting.details',compact('events', 'eventName','tickets','showsearch'));
 }
 catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
                echo $e;
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
               echo $e;
 }

}
else
{
     return view('Eventslisting.details');
}
}
}