<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Config;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsletterRequest;
use Response;
use Mailchimp;

class NewsletterController extends Controller
{
    public $mailchimp;
     public $listId = 'f8fb3ac2e6';

    public function __construct()
    {
        // // $this->mailchimp = new config('services.mailchimp.api_key');
        // $this->mailchimp = $mailchimp;
    }

    public function subscribe(Request $request)
    {

       $email  = $request->email;
         $apiKey = '08f6ce1f07e03790c3093074d5532c2b-us18';
        $listID = 'f8fb3ac2e6';

        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;

        $json = json_encode([
            'email_address' => $email,
            'status'        => 'subscribed',
            'merge_fields'  => [
                'FNAME'     => '',
                'LNAME'     => ''
            ]
        ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpCode == 200) {
            echo '<div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                         <strong>Email Subscribed successfully.</strong>
                    </div>';
        } else if($httpCode == 214){
            echo '<div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>You are already subscribed.</strong>
                  </div>';
        } else {
            echo '<div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                       <strong>Some problem occurred, please try again.</strong>
                    </div>';
        }
        

    }

}



