<?php

namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use App\Tickets;
use Session;
use DB;
use \Cart;
use App\DiscountsObj;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Mail;

class NewCheckoutController extends Controller
{
  public function charge(Request $request) {
 
$key=DB::table('business_settings')->first();
                  $test=$key->stripe_mode;
                  $test_secret_key=$key->test_secret_key;
                  $live=$key->stripe_mode;
                  $live_secret_key=$key->live_secret_key;
                  if($test=="Test")
                  { 
      Stripe::setApiKey($test_secret_key);
    }
    else
    {
      Stripe::setApiKey($live_secret_key);
    }

      $stripeEmail=$request->stripeEmail;
      $data_amount= $request->data_amount;
      $stripeToken= $request->stripeToken;
      $bookingfeeamount=$request->totalbookingfee;
    $discount_given=$request->discount_given;
      $user_id= $request->user_id;
      $token=$request->stripeToken;
      
      
      $charge = Charge::create(array(
          'source' => $token,
          'amount' =>$data_amount,
          'currency' => 'gbp'
      ));
      
      $transaction_id=$charge->id;
      $seller_message=$charge->seller_message;
      $status=$charge->status;
     
      if($status=="succeeded")
      {
        $order_status_id=1;
      }
      else
      {
        $order_status_id=0;
      }
        $created_at  = date('Y-m-d H:m:i');
        $updated_at  = date('Y-m-d H:m:i');
        $data_amountdb=$data_amount/100;
        $order_number =uniqid();
      $id=$request->updateorder;
      $cartItems=Cart::content();
      $data = DB::table('orders')->insert(
               ['user_id' => $user_id,'order_status_id'=>$order_status_id,'amount' =>$data_amountdb,'order_number'=>$order_number,'totalbookingfeeamount'=>$bookingfeeamount,'discount'=>$discount_given,'transaction_id'=>$transaction_id,'created_at' => $created_at,'updated_at' => $updated_at]
                );
       $idorder =DB::getPdo()->lastInsertId();
      foreach ($cartItems as $value) {
         $ticket_id=$value->id;
         $tick=DB::table('tickets')
            ->where('id','=',$ticket_id)
            ->first();
         $event_id= $tick->event_id;
         $price=$value->price;
         $qty=$value->qty;
         $totalPrice=$value->options->discounted_amount;
         $discountAmount=$value->options->discount_given;
         $bookingfeeamount=$value->options->bookingfeeamount;
         $order_items = DB::table('order_items')->insert(
          ['order_id'=>$idorder,'order_ticket_number'=>'dsfds','ticket_id'=>$ticket_id,'event_id'=>$event_id,'netPrice'=>$price,'Quantity'=>$qty,'totalPrice'=>$totalPrice,'discountAmount'=>$discountAmount,'bookingFeeAmount'=>$bookingfeeamount]
        );
    
        
      
       
    }
        
     
        Cart::destroy();
    $ordernumber =DB::table('orders')->where('id','=',$idorder)->first();
    $data = array('idorder'=>$idorder,
                     'email'=>Auth::user()->email,
                     'subject'=>'Petal Events- Order Details For - '.$ordernumber->order_number.' !!!'
        );
    Mail::send(['text'=>'ordermail'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.replyinfo@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
      //return $charge;
        $showsearch="true";
        return view('pages.orderdetails',compact('showsearch', 'idorder'));
   }
}
