<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use App\Tickets;
use Session;
use DB;
use \Cart;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class CheckoutController extends Controller
{
   public function step1()
   {
       /*if (Auth::check()) {
            return view('pages.checkout');
       }
       return redirect('login');*/
   }
    public function shipping()
    {   
        if (Auth::check()) {
        $showsearch="true";
        return view('pages.checkout',compact('showsearch'));
    }
    return redirect('login');
    }
    public function payment()
    {
        return view('pages.payment');
    }
    public function storePayment(Request $request)
    {
        \Stripe\Stripe::setApiKey("sk_test_M8hfDnQwXx36Lm8qJ2zWjVDP");
        $token = $request->stripeToken;
        try {
            $charge = \Stripe\Charge::create(array(
                "amount" => Cart::total()*100, 
                "currency" => "usd",
                "source" => $token,
                "description" => "Example charge"
            ));
        } catch (\Stripe\Error\Card $e) {
            
        }
     
       Order::createOrder();
        return "Order completed";
    }

     public function shippingstore(Request $request, $id)
    {
         if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }

        if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }
        if(trim($request->postcode)!='') {
            $postcode = trim($request->postcode);
        } else {
            $postcode = '';
        }
         if(trim($request->country)!='') {
            $country = trim($request->country);
        } else {
            $country = '';
        }

        if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }
        if(trim($request->address_line_1)!='') {
            $address_line_1 = trim($request->address_line_1);
        } else {
            $address_line_1 = '';
        }
        if(trim($request->address_line_2)!='') {
            $address_line_2 = trim($request->address_line_2);
        } else {
            $address_line_2 = '';
        }

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['first_name' => $first_name, 'last_name' => $last_name, 'email' => $email,'postcode' => $postcode, 'country' => $country, 'phone' => $phone, 'address_line_1' => $address_line_1, 'address_line_2' => $address_line_2,'updated_by' => $updated_by, 'updated_at' => $updated_at]);
         
           
        } 
    
      
}