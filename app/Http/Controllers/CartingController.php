<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use App\Tickets;
use Session;
use DB;
use \Cart;
use App\DiscountsObj;

class CartingController extends Controller
{    
  public function index(Request $request)
    {
    $showsearch="true";
    $cartItems=Cart::content();
    $_discountobjlist=array();
    $key=DB::table('business_settings')->first();
    
                          foreach ($cartItems as $value) {
                                   $ticket_id=$value->id;
                                   $event_id=$value->options->size;
                                
                                    $qut=$value->qty;
                                    $price=$value->price;
                                    $Isbooking_fee=$value->options->Isbooking_fee;

                                  
                                    $pricequt=$price*$qut;
                                   $ticketcolabExist= DB::table('ticketcollabration')->where(function ($query) use($ticket_id,$event_id) {
    $query->where('ticket_id', '=',$ticket_id)
        ->where('event_id', '=', $event_id)->orwhere(function ($query1) use($ticket_id,$event_id) {
    $query1->where('ticket_id2','=', $ticket_id)
        ->where('event_id2', '=', $event_id);
});
})->get();

                                    
                                    
                                    $discountedPrice=array();
                                     $_discountobj = new DiscountsObj();
                                    if(count($ticketcolabExist)>0)
                                    {
                                      $ticketcolabdetails=$this->ticketcolab($ticket_id,$event_id,$ticketcolabExist);
                                      foreach ($ticketcolabdetails as $tck) {
                                        $existencecheck=$this->get_array_details_front($tck->TicketId,$_discountobjlist,'TicketId');
                                        
                                        if(count($existencecheck)>0){
                                               

                                        }
                                        else{
                                            array_push($_discountobjlist,$tck);

                                        }
                                        
                                      }
                                      

                                    }else{
                                       $discountType= array("earlyBird","bundle","ticketx");
                                       foreach($discountType as $disc)
                                    {

                                        array_push($discountedPrice,$this->getDiscount($event_id,$ticket_id,$price,$disc,$qut,$Isbooking_fee));

                                    }
                                    $discount=$this->max_attribute_in_array($discountedPrice,'discounted_amount');
                                   
                                    if($discount>0)
                                    {
                                         $_discountobj=$this->get_array_details($discount,$discountedPrice);
                                         $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         
                                          $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $_discountobj->discounted_amount;
                                        $options['discount_given'] = $_discountobj->discount_given;
                                        $options['subtotal_amount'] = $_discountobj->subtotal_amount;
                                        $options['bookingfeeamount']=$_discountobj->booking_fee;
                                        Cart::update($value->rowId, compact('options'));
                                         array_push($_discountobjlist,$_discountobj);

                                    }
                                    else{
                                       $totalPrice=$price*$qut;
                                       $_discountobj->discounted_amount=$totalPrice;
                                       $_discountobj->discount_given=0;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                      $_discountobj->event_id=$event_id;


                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                     $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $totalPrice;
                                        $options['discount_given'] = 0;
                                        $options['subtotal_amount'] = $totalPrice;
                                        if($Isbooking_fee==0)
        {
          $bookingfeepercent=$key->booking_discount;
          $_discountobj->booking_fee=($totalPrice*$bookingfeepercent)/100;
           $options['bookingfeeamount']=($totalPrice*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       
                                      Cart::update($value->rowId, compact('options'));
                                      array_push($_discountobjlist,$_discountobj);

                                    }

                                    }
                                    
                                 }
                              
                                $eventids=array_column($_discountobjlist, 'event_id');
                                $eventIdList=array_unique($eventids);
       if (Auth::check()) {
       return view('Cart.index',compact('cartItems','showsearch','_discountobjlist','eventIdList'));
         }
          return redirect('login?r_url=/cart');

        }
    public function max_attribute_in_array($array, $prop) {
    return max(array_column($array, $prop));
}
public function ticketcolab($ticket_id,$event_id,$colabdata)
{
  $cartItems=Cart::content();
  $_discountobj = new DiscountsObj();
  $_discountobjlist=array();
  
  foreach($colabdata as $data)
  {
                                    $ticket1=$data->ticket_id;
                                      $ticket2=$data->ticket_id2;
                                      $find=$cartItems->where('id','=',$ticket2);
                                        $find2=$cartItems->where('id','=',$ticket1);

                                       if((count($find)>0 && count($find2)>0) && $data->coll_type=="collticket")
                                       {
                                           $_discountobj=$this->calandfillDiscountObj($find,$data);
                                           $_discountobj1=$this->calandfillDiscountObj($find2,$data);
                                           $existencecheck=$this->get_array_details_front($_discountobj->TicketId,$_discountobjlist,'TicketId');
                                           $existencecheck2=$this->get_array_details_front($_discountobj->TicketId,$_discountobjlist,'TicketId');
                                           
                                           if(count($existencecheck)>0){
                                               

                                           }
                                           else{
                                            array_push($_discountobjlist,$_discountobj);

                                           }
                                           if(count($existencecheck2)>0){
                                               

                                        }
                                        else{
                                            array_push($_discountobjlist,$_discountobj1);

                                        }
                                        
                                        
                                          

                                       }
                                       elseif((count($find)>0 && count($find2)>0) && $data->coll_type=="self"){
                                        $_discountobj=$this->calandfillDiscountObj($find,$data);
                                        $_discountobj1=$this->calandfillDiscountObj($find2,$data);
                                        $existencecheck=$this->get_array_details_front($_discountobj->TicketId,$_discountobjlist,'TicketId');
                                        $existencecheck2=$this->get_array_details_front($_discountobj->TicketId,$_discountobjlist,'TicketId');
                                        
                                        if(count($existencecheck)>0){
                                            

                                        }
                                        else{
                                         array_push($_discountobjlist,$_discountobj);

                                        }
                                        if(count($existencecheck2)>0){
                                            

                                     }
                                     else{
                                         array_push($_discountobjlist,$_discountobj1);

                                     }
                                       }
                                       else{

                                           $findItem=$cartItems->where('id','=',$ticket_id);
                                           
                                           foreach($findItem as $f){
                                            $ticket_id=$f->id;
                                            $event_id=$f->options->size;
                                             $qut=$f->qty;
                                             $price=$f->price;
                                             $pricequt=$price*$qut;
                                             $discountedPrice= array();
                                             $Isbooking_fee=$value->options->Isbooking_fee;
                                        $discountType= array("earlyBird","bundle","ticketx");
                                        foreach($discountType as $disc)
                                     {
 
                                         array_push($discountedPrice,$this->getDiscount($event_id,$ticket_id,$price,$disc,$qut,$Isbooking_fee));
 
                                     }
                                     
                                     $discount=$this->max_attribute_in_array($discountedPrice,'discounted_amount');
                                    
                                     if($discount>0)
                                     {
                                          $_discountobj=$this->get_array_details($discount,$discountedPrice);
                                          $_discountobj->TicketTitle=$f->name;
                                          $_discountobj->Quantity=$f->qty;
                                          $_discountobj->TicketId=$f->id;
                                          $_discountobj->rowId=$f->rowId;
                                           $options = Cart::get($f->rowId)->options;
                                         $options['discounted_amount'] = $_discountobj->discounted_amount;
                                         $options['discount_given'] = $_discountobj->discount_given;
                                         $options['subtotal_amount'] = $_discountobj->subtotal_amount;
                                         $options['bookingfeeamount']=$_discountobj->booking_fee;
                                         Cart::update($f->rowId, compact('options'));
                                          array_push($_discountobjlist,$_discountobj);
 
                                     }
                                     else{
                                        $totalPrice=$price*$qut;
                                        $_discountobj->discounted_amount=$totalPrice;
                                        $_discountobj->discount_given=0;
                                        $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$f->options->size;

                                        $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                     ->where('id', '=', $event_id)->first();
                                      $_discountobj->TicketTitle=$f->name;
                                          $_discountobj->Quantity=$f->qty;
                                          $_discountobj->TicketId=$f->id;
                                          $_discountobj->rowId=$f->rowId;
                                          $options = Cart::get($f->rowId)->options;
                                         $options['discounted_amount'] = $totalPrice;
                                         $options['discount_given'] = 0;
                                         $options['subtotal_amount'] = $totalPrice;
                                         
                                          $options['bookingfeeamount']=$_discountobj->booking_fee;
                                         
                                         Cart::update($f->rowId, compact('options'));
                                       
                                        array_push($_discountobjlist,$_discountobj);
 
                                     }
                                    }
                                           
                                       }

                                      
                                      
                                     
  }
  
  return $_discountobjlist;

}
public function calandfillDiscountObj($find,$data)
{
    $_discountobj = new DiscountsObj();
    $key=DB::table('business_settings')->first();
    foreach($find as $f)
    {
        $price=$f->price;
        $qty=$f->qty;
        $totalPrice=$price*$qty;
        $dicountPerc=$data->discount;
        

        $discount_amount=$totalPrice-($totalPrice * ($dicountPerc/100));
        $_discountobj->discounted_amount=$discount_amount;
   $_discountobj->discount_given=$totalPrice-$discount_amount;
   $_discountobj->subtotal_amount=$totalPrice;
   $_discountobj->event_id=$f->options->size;
   $Isbooking_fee=$f->options->Isbooking_fee;
 
        
   $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
->where('id', '=', $f->options->size)->first();
 $_discountobj->TicketTitle=$f->name;
     $_discountobj->Quantity=$f->qty;
     $_discountobj->TicketId=$f->id;
     $_discountobj->rowId=$f->rowId;
     $options = Cart::get($f->rowId)->options;
    $options['discounted_amount'] = $discount_amount;
    $options['discount_given'] = $totalPrice-$discount_amount;
    $options['subtotal_amount'] = $totalPrice;
     if($Isbooking_fee==0)
        {
          $bookingfeepercent=$key->booking_discount;
          $_discountobj->booking_fee=($discount_amount*$bookingfeepercent)/100;
          $options['bookingfeeamount'] = ($discount_amount*$bookingfeepercent)/100;
        }

    Cart::update($f->rowId, compact('options'));
    }
    return $_discountobj;
}
public function get_array_details($amount,$arrayval)
{
     $result = new DiscountsObj();
     foreach ($arrayval as  $value) {
         $disamount=$value['discounted_amount'];
         if($disamount==$amount)
         {
            $result=$value;
         }
     }
     return $result;

}
public static function get_array_details_front($amount,$arrayval,$columnName)
{
    
     $result=array();
     foreach ($arrayval as  $value) {
         $disamount=$value[$columnName];
         if($disamount==$amount)
         {
            array_push($result, $value);

         }
     }
     return $result;

}
   public function getDiscount($event_id,$ticket_id,$price,$discount_type,$qut,$Isbooking_fee)
                        { 
                        
                          $_discountobj = new DiscountsObj();
                          $business_settings=DB::table('business_settings')->first();
                          $discount_amount = 0;
                          switch ($discount_type) {
                              case "earlyBird":
                              $discounts= DB::table('discounts')
                                    ->where('discounts.event_id', '=', $event_id)
                                    ->where('discounts.availability', '=', 'dc_two')
                                    ->where('discounts.ticket_type_id', '=', $ticket_id)
                                    ->where('discounts.is_deleted', '=', 0)
                                    ->first();
                              

                                    if(count($discounts)>0){
                                    $currentDate=date("Y-m-d");
                                    $startDate=$discounts->start_date;
                                    $endDate=$discounts->expiry_date;
                                    $dicountPerc=$discounts->discount;
                                    $totalPrice=$price*$qut;
                                    if($currentDate>=$startDate && $currentDate<=$endDate)
                                    {
                                      
                                       $discount_amount=$totalPrice-($totalPrice * ($dicountPerc/100));
                                       $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$totalPrice-$discount_amount;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$event_id;
  if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=($discount_amount*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }

                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                       

                                    }
                                    else{
                                        $_discountobj->discounted_amount=0;
                                       $_discountobj->discount_given=0;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$event_id;
                                       if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=($totalPrice*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                       
                                    }
                                  }
                              break;
                              case "bundle":
                                $discounts= DB::table('discounts')
                                    ->where('event_id', '=', $event_id)
                                    ->where('availability', '=', 'dc_three')
                                    ->where('ticket_type_id', '=', $ticket_id)
                                    ->where('is_deleted', '=', 0)
                                    ->first();

                                    if(count($discounts)>0){
                                      $bundle_offer_ticket=$discounts->bundle_offer_ticket;
                                      $priceforticket=$discounts->bundle_offer_price;
                                      if($qut>=$bundle_offer_ticket)
                                      {
                                        $discount_amount=$price*$priceforticket;
                                        $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=($price*$qut)-$discount_amount;
                                       $_discountobj->subtotal_amount=$price*$qut;
                                       $_discountobj->event_id=$event_id;
                                        if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=($discount_amount*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }

                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                      else
                                      {
                                        $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$discount_amount;
                                       $_discountobj->subtotal_amount=$price*$qut;
                                       $_discountobj->event_id=$event_id;
                                        if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=(($price*$qut)*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                    }
                                    else
                                      {
                                        $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$discount_amount;
                                       $_discountobj->subtotal_amount=$price*$qut;
                                       $_discountobj->event_id=$event_id;
                                       if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=(($price*$qut)*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                  break;
                              case "ticketx":
                                  $discounts= DB::table('discounts')
                                    ->where('event_id', '=', $event_id)
                                    ->where('availability', '=', 'dc_four')
                                    ->where('ticket_type_id', '=', $ticket_id)
                                    ->where('is_deleted', '=', 0)
                                    ->get();
                                    if(count($discounts)>0){
                                      $xval=$discounts->ticketx;
                                      $yval=$discounts->tickety;
                                      $difference=$xval-$yval;
                                      $totalPrice=$price*$qut;
                                      if($qut>=$xval)
                                      {
                                         $discount_amount=$price*$difference;
                                         $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$totalPrice-$discount_amount;
                                       $_discountobj->subtotal_amount=$discount_amount;
                                       $_discountobj->event_id=$event_id;
                                       if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=($discount_amount*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                      else
                                      {
                                        $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$discount_amount;
                                       $_discountobj->subtotal_amount=$price*$qut;
                                       $_discountobj->event_id=$event_id;
                                       if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=(($price*$qut)*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                    }
                                    else
                                      {
                                        $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$discount_amount;
                                       $_discountobj->subtotal_amount=$price*$qut;
                                       $_discountobj->event_id=$event_id;
                                       if($Isbooking_fee==0)
        {
          $bookingfeepercent=$business_settings->booking_discount;
          $_discountobj->booking_fee=(($price*$qut)*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                      }
                                  break;
                                  case "self":
                                  $colabFetch=DB::table('ticketcollabration')->where(function ($query) use($ticket_id,$event_id) {
    $query->where('ticket_id', '=',$ticket_id)
        ->where('event_id', '=', $event_id)->orwhere(function ($query1) use($ticket_id,$event_id) {
    $query1->where('ticket_id2','=', $ticket_id)
        ->where('event_id2', '=', $event_id);
});
})->where('coll_type','self')->first();
                                   $discountPercent=$colabFetch->discount;
                                   $totalPrice=$price*$qut;
                                   $discount_amount=$totalPrice-($totalPrice * ($discountPercent/100));
                                    $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$totalPrice-$discount_amount;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$event_id;
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();

                                  break;
                                  case "collab":
                                   $colabFetch=DB::table('ticketcollabration')->where(function ($query) use($ticket_id,$event_id) {
    $query->where('ticket_id', '=',$ticket_id)
        ->where('event_id', '=', $event_id)->orwhere(function ($query1) use($ticket_id,$event_id) {
    $query1->where('ticket_id2','=', $ticket_id)
        ->where('event_id2', '=', $event_id);
});
})->where('coll_type','collticket')->first();
                                   $discountPercent=$colabFetch->discount;
                                   $totalPrice=$price*$qut;
                                   $discount_amount=$totalPrice-($totalPrice * ($discountPercent/100));
                                    $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$totalPrice-$discount_amount;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$event_id;
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();

                                  break;
                               }
                          return $_discountobj;
                        }
                        function discount_amount_cal($type)
                        {
                           foreach ($discounts as $discount) {
                                          $discount= $discount->discount;
                                          $total = $price- ($price * ($discount/100));
                                          $discount_amount=$total;
                                       }

                        }

    public function addtocart(Request $request)
    { 
       $id=$request->tickettype;
       $quantity=$request->quantity;
       $tickets = Tickets::findOrFail($id);
       Cart::add($id, $tickets->ticket_type,$quantity,$tickets->prices,['size'=>$tickets->event_id,'discounted_amount'=>0,'discount_given'=>0,'subtotal_amount'=>0,'tick_flag'=>0,'colab_ticket'=>0,'Isbooking_fee'=>$tickets->Free_booking_fee,'bookingfeeamount'=>0]);
       return Cart::count();

    }
    public function applyCouponCode(Request $request)
    { 
     $code=$request->couponCode;
         $quantity=$request->quantity;
       $dicount = DB::table('discount');
       Cart::add($id, $tickets->ticket_type,$quantity,$tickets->prices,['size'=>$tickets->event_id,'discounted_amount'=>0,'discount_given'=>0,'subtotal_amount'=>0]);
       return Cart::count();

    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartcontent=Cart::content();
       
       
        $options = Cart::get($id)->options;

        
        if($options['colab_ticket']>0)
        {
        Cart::update($id,['qty'=>$request->qty]);
        $seconditem=$cartcontent->where('id','=',$options['colab_ticket']);
        foreach ($seconditem as $k) {
         $prowId=$k->rowId;
         Cart::update($prowId,['qty'=>$request->qty]);
        }

        
        }
        else {
           Cart::update($id,['qty'=>$request->qty]);
        }
        return back();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back();
    }
    public function minCart()
    {
        $cartItems=Cart::content();
    $_discountobjlist=array();
                          foreach ($cartItems as $value) {
                                   $ticket_id=$value->id;
                                   $event_id=$value->options->size;
                                
                                    $qut=$value->qty;
                                    $price=$value->price;
                                  $Isbooking_fee=$value->options->Isbooking_fee;
                                    $pricequt=$price*$qut;
                                    $discountedPrice=array();
                                    $discountType= array("earlyBird","bundle","ticketx");
                                    foreach($discountType as $disc)
                                    {

                                        array_push($discountedPrice,$this->getDiscount($event_id,$ticket_id,$price,$disc,$qut,$Isbooking_fee));

                                    }
                                    $discount=$this->max_attribute_in_array($discountedPrice,'discounted_amount');
                                    $_discountobj = new DiscountsObj();
                                    if($discount>0)
                                    {
                                         $_discountobj=$this->get_array_details($discount,$discountedPrice);
                                         $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                          $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $_discountobj->discounted_amount;
                                        $options['discount_given'] = $_discountobj->discount_given;
                                        $options['subtotal_amount'] = $_discountobj->subtotal_amount;
                                        Cart::update($value->rowId, compact('options'));
                                         array_push($_discountobjlist,$_discountobj);

                                    }
                                    else{
                                       $totalPrice=$price*$qut;
                                       $_discountobj->discounted_amount=$totalPrice;
                                       $_discountobj->discount_given=0;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                      $_discountobj->event_id=$event_id;
                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                     $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $totalPrice;
                                        $options['discount_given'] = 0;
                                        $options['subtotal_amount'] = $totalPrice;
                                        Cart::update($value->rowId, compact('options'));
                                      
                                       array_push($_discountobjlist,$_discountobj);

                                    }

                                    

                                }
                                $eventids=array_column($_discountobjlist, 'event_id');
                                $eventIdList=array_unique($eventids);

        return view('Cart.minCart',compact('cartItems','_discountobjlist'));

    }

     public function updateorder(Request $request)
    { 
      
      $total= Cart::total();
      $id=$request->updateorder;
      $cartItems=Cart::content();
      foreach ($cartItems as $value) {
         $ticket_id=$value->id;
         $tick=DB::table('tickets')
            ->where('id','=',$ticket_id)
            ->first();
         $event_id= $tick->event_id;
        
      $created_at  = date('Y-m-d H:m:i');
        $updated_at  = date('Y-m-d H:m:i');
        $order_number="LY9J101";

      $data = DB::table('orders')->insert(
               ['user_id' => $id,'amount' => $total,'order_number'=>$order_number,'ticket_id'=> $ticket_id,'event_id'=>$event_id,'created_at' => $created_at,'updated_at' => $updated_at]
                );
    }
        
    }

      public function ajaxdiscount(Request $request)
    { 
      $cartItems=Cart::content();
      foreach ($cartItems as $value) {
         $ticket_id=$value->id;
         $tick=DB::table('tickets')
            ->where('id','=',$ticket_id)
            ->first();
         $event_id= $tick->event_id;
     }
        
    }

    public function addtocartticket(Request $request)
    {  
       $id= $request->tickettype;
       $quantity=1;
       $tickets = Tickets::findOrFail($id);
       Cart::add($id, $tickets->ticket_type,$quantity,$tickets->prices,['size'=>$tickets->event_id,'discounted_amount'=>0,'discount_given'=>0,'subtotal_amount'=>0,'tick_flag'=>1,'colab_ticket'=>$request->tickettype2]);

       $id1= $request->tickettype2;
       $quantity=1;
       $tickets1 = Tickets::findOrFail($id1);
       Cart::add($id1, $tickets1->ticket_type,$quantity,$tickets1->prices,['size'=>$tickets1->event_id,'discounted_amount'=>0,'discount_given'=>0,'subtotal_amount'=>0,'tick_flag'=>1,'colab_ticket'=>$id]);
       return Cart::count();

    }
   public function applypromo(Request $request)
    {  
       try {
       $showsearch="true";
       $code_name=$request->couponCode;
       $ticket_type_id=$request->ticket_type_id;

       $dicount = DB::table('discounts')->where('code_name','=',$code_name)->where('ticket_type_id','=', $ticket_type_id)->first();   

       $discountpre=$dicount->discount;
       $discountTicket_id=$dicount->ticket_type_id;
       $cartItems=Cart::content();
       $_discountobjlist=array();
       $key=DB::table('business_settings')->first();
    
                          foreach ($cartItems as $value) {
                                   $ticket_id=$value->id;
                                   $event_id=$value->options->size;
                                
                                    $qut=$value->qty;
                                    $price=$value->price;
                                    $Isbooking_fee=$value->options->Isbooking_fee;
                                    $pricequt=$price*$qut;
                                    $discountedPrice=array();
                                     $_discountobj = new DiscountsObj();
                                   $ticketcolabExist= DB::table('ticketcollabration')->where(function ($query) use($ticket_id,$event_id) {
    $query->where('ticket_id', '=',$ticket_id)
        ->where('event_id', '=', $event_id)->orwhere(function ($query1) use($ticket_id,$event_id) {
    $query1->where('ticket_id2','=', $ticket_id)
        ->where('event_id2', '=', $event_id);
});
})->get();
                                   if($ticket_id==$discountTicket_id)
                                   {
                                      $totalPrice=$price*$qut;
        
                                      $discountpre=$dicount->discount;
                                       $discount_amount=$totalPrice-($totalPrice * ($discountpre/100));
                                       $_discountobj->discounted_amount=$discount_amount;
                                       $_discountobj->discount_given=$totalPrice-$discount_amount;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                       $_discountobj->event_id=$event_id;
                                  
                                      $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                     $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $discount_amount;
                                        $options['discount_given'] = $totalPrice-$discount_amount;
                                        $options['subtotal_amount'] = $totalPrice;
                                        if($Isbooking_fee==0)
                                   {
                                    $bookingfeepercent=$key->booking_discount;
                                    $_discountobj->booking_fee=($discount_amount*$bookingfeepercent)/100;
                                    $options['bookingfeeamount']=($discount_amount*$bookingfeepercent)/100;
                                   }
                                  else
                                  {
                                    $_discountobj->booking_fee=0;
                                  }
                                   Cart::update($value->rowId, compact('options'));
                                   array_push($_discountobjlist,$_discountobj);
                                   }
                                   else
                                   {
                                    if(count($ticketcolabExist)>0)
                                    {
                                      $ticketcolabdetails=$this->ticketcolab($ticket_id,$event_id,$ticketcolabExist);
                                      foreach ($ticketcolabdetails as $tck) {
                                        $existencecheck=$this->get_array_details_front($tck->TicketId,$_discountobjlist,'TicketId');
                                        
                                        if(count($existencecheck)>0){
                                               

                                        }
                                        else{
                                            array_push($_discountobjlist,$tck);

                                        }
                                        
                                      }
                                      

                                    }else{
                                       $discountType= array("earlyBird","bundle","ticketx");
                                       foreach($discountType as $disc)
                                    {

                                        array_push($discountedPrice,$this->getDiscount($event_id,$ticket_id,$price,$disc,$qut,$Isbooking_fee));

                                    }
                                    $discount=$this->max_attribute_in_array($discountedPrice,'discounted_amount');
                                   
                                    if($discount>0)
                                    {
                                         $_discountobj=$this->get_array_details($discount,$discountedPrice);
                                         $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         
                                          $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $_discountobj->discounted_amount;
                                        $options['discount_given'] = $_discountobj->discount_given;
                                        $options['subtotal_amount'] = $_discountobj->subtotal_amount;
                                        $options['bookingfeeamount']=$_discountobj->booking_fee;
                                        Cart::update($value->rowId, compact('options'));
                                         array_push($_discountobjlist,$_discountobj);

                                    }
                                    else{
                                       $totalPrice=$price*$qut;
                                       $_discountobj->discounted_amount=$totalPrice;
                                       $_discountobj->discount_given=0;
                                       $_discountobj->subtotal_amount=$totalPrice;
                                      $_discountobj->event_id=$event_id;


                                       $_discountobj->Event_Name=DB::table('eventbookings')->select('event_title')
                                    ->where('id', '=', $event_id)->first();
                                     $_discountobj->TicketTitle=$value->name;
                                         $_discountobj->Quantity=$value->qty;
                                         $_discountobj->TicketId=$value->id;
                                         $_discountobj->rowId=$value->rowId;
                                         $options = Cart::get($value->rowId)->options;
                                        $options['discounted_amount'] = $totalPrice;
                                        $options['discount_given'] = 0;
                                        $options['subtotal_amount'] = $totalPrice;
                                        if($Isbooking_fee==0)
        {
          $bookingfeepercent=$key->booking_discount;
          $_discountobj->booking_fee=($totalPrice*$bookingfeepercent)/100;
           $options['bookingfeeamount']=($totalPrice*$bookingfeepercent)/100;
        }
        else
        {
          $_discountobj->booking_fee=0;
        }
                                       
                                        Cart::update($value->rowId, compact('options'));
                                      
                                       array_push($_discountobjlist,$_discountobj);

                                    }

                                    }
                                }
                              }
                                

                                $eventids=array_column($_discountobjlist, 'event_id');
                                $eventIdList=array_unique($eventids);
                                
                        
        
    return view('Cart.index',compact('cartItems','showsearch','_discountobjlist','eventIdList'));
        
      } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            return redirect('/cart')->with('success','Incorrect Promo Code...');
  
       }  
    }   
 }