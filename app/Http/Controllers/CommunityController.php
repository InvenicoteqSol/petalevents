<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class CommunityController extends Controller
{    

    public function __construct()
    {

    }

      public function index(Request $request)
    {
      

      $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate(6);
              $categories = DB::table('categories')
              ->where('status', '=', 1)
              ->where('deleted', '=', 0)
              ->get();
        
        $query = DB::table('posts');
                     $query->where('deleted', 0);
                     $query->where('status', 1);
                     $query->orderBy('id','DESC');
                     $query->limit(4);
        $posts = $query->get();

       return view('community.index',compact('eventbookings', 'categories','posts'));
    }
    
     public function clientalbum(Request $request)
    {

       return view('community.client-album');
    }
   
    public function clientview(Request $request)
    {
      $posts = DB::table('posts')
                     ->where('id','=', $id)
                     ->where('status', '=', 1)
                     ->where('deleted', '=', 0)
                     ->orderBy('id','DESC')
                      ->get();


          $categories = DB::table('categories')
              ->where('status', '=', 1)
             ->where('deleted', '=', 0)
             ->first();  

       return view('community.client-single',compact('posts','categories'));
    }

    public function volunteersession(Request $request)
    {

       return view('community.volunteer-session');
    }

    public function show($id)
    {   
         $event = DB::table('eventbookings')
                            ->where('id','=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first(); 

        $userinfo = DB::table('users')
                             ->where('id','=', $event->user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
         

          $categories = DB::table('categories')
              ->where('status', '=', 1)
             ->where('deleted', '=', 0)
             ->first();                 

       
        $galleryevent = DB::table('eventgallery')
                             ->where('eventid','=',$id)
                             ->where('status', '=', 1)
                             ->where('deleted', '=', 0)
                             ->orderBy('id','DESC')
                            ->get();
                  
        // $BusinessInfo = DB::table('business_settings')->where('status', '1')->first(); 
                    
        return view('community.show',compact('event','userinfo','categories','galleryevent'));
    }

    

    
}

