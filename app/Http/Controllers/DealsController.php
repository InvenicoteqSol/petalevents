<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class DealsController extends Controller
{    

    

    

    public function __construct()
    {
    }

     public function index(Request $request)
    {
    	$showsearch="true";
    	$todaydate=date("Y-m-d h:i:s");
    	$fivedaysahead=date('Y-m-d', strtotime('+7 days'));
    	$events=DB::table('eventbookings')->where('is_featured','=','1')->where('start_date','>=',$fivedaysahead)->where('deleted','=','0')->get();
    	$featuredEventshappening=DB::table('eventbookings')->where('is_featured','=','0')->where('start_date','>=',$todaydate)->where('deleted','=','0')->orderBy('start_date', 'asc')->get();
    	return view('Deals.featureEvents',compact('events', 'featuredEventshappening','showsearch'));
    }
}