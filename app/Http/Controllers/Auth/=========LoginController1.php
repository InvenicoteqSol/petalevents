<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use Exception;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function index()
    {
       return view('auth.login');
    }

    protected function authenticated($request, $user,$rURL)
    {   
        
        if( $user->deleted==0 ) {

            if( $user->status==1 ) {

                  if($user->user_type==3) {
                    if($rURL!="")
                    {
                      return redirect()->intended($r_url);

                    }
                    else{
                    return redirect()->intended('/customerdashboard');  
                  }
                  } elseif ($user->user_type==4) {
				    if($user->prereqComplete>0)
					{
					return redirect()->intended('/organiserdashboard');
					
					}
					else{
					return redirect()->intended('/onboard-prereq');
					}
                      
                  } else {
                    return redirect()->intended('/admindashboard');  
                  }

            } else {
                Auth::logout();
                    return redirect()->intended('/login')->with('error','Your account is not active. Please contact with website admin to activate your account.');
            } 

       } else {
            Auth::logout();
                return redirect()->intended('/login')->with('error','Your account is deleted. Please contact with website admin to restore your account.');
       }      

    }

    public function redirectTOGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function Googlecallback(Request $request)
    {

       /* try {*/
            
        
            $googleUser = Socialite::driver('google')->user();

            $existUser = User::where('email',$googleUser->email)->first();

            if($existUser) {
                Auth::loginUsingId($existUser->id);
            }
            else {
              $password = rand(1,10000);
                $user = new User;
                $user->name = $googleUser->name;
                $user->email = $googleUser->email;
                $user->google_id = $googleUser->id;
                $user->password = bcrypt($password);
                $user->hdpwd = $password;
                $user->user_type = '3';
                $user->save();
                Auth::loginUsingId($user->id);
            }
            return redirect()->to('/home');
       /* } 
        catch (Exception $e) {
            return 'error';
        }*/
    }
}
