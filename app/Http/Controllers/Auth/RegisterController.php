<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/photographerdashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'country' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   

            if($data['first_name']!=''){
            $first_name = trim($data['first_name']);
            } else { $first_name=''; }

            if($data['last_name']!=''){
            $last_name = trim($data['last_name']);
            } else { $last_name=''; }

            if($data['last_name']!='' || $data['first_name']!=''){
            $name = trim($data['first_name']).' '.trim($data['last_name']);
            } else { $name=''; }

            if($data['email']!=''){
            $email = trim($data['email']);
            } else { $email=''; }

            if($data['username']!=''){
            $username = trim($data['username']);
            } else { $username=''; }

            if($data['website']!=''){
            $website = trim($data['website']);
            } else { $website=''; }

            if($data['phone']!=''){
            $phone = trim($data['phone']);
            } else { $phone=''; }

            if($data['country']!=''){
            $country = trim($data['country']);
            } else { $country='';}

            if($data['occassion']!=''){
            $occassion = trim($data['occassion']);
            } else { $occassion=''; }

            if($data['description']){
            $description = trim($data['description']);
            } else { $description=''; }

            if(count($data['photography_role']) > 0){
            $photography_role = implode(',', $data['photography_role']);
            } else { $photography_role=''; }

            if($data['password']){
            $password = trim($data['password']);
            } else { $password=''; }

            if($data['user_type']){
            $user_type = trim($data['user_type']);
            } else { $user_type=''; }
			if($data['user_type']==4)
			{
			  $user = User::create([
            'first_name' => ucfirst($first_name),
            'last_name' => ucfirst($last_name),
            'name' => ucfirst($first_name).' '.ucfirst($last_name),
            'email' => $email,
            'username' => $username,
            'password' => bcrypt($password),
            'hdpwd' => $password,
            'country' => $country,
            'phone' => $phone,
            'user_type' => $user_type,
            'photography_role' => $photography_role,
            'description' => $description,
            'website' => $website,
			'prereqComplete'=>0
        ]);
			
			}
			else{
				$user = User::create([
            'first_name' => ucfirst($first_name),
            'last_name' => ucfirst($last_name),
            'name' => ucfirst($first_name).' '.ucfirst($last_name),
            'email' => $email,
            'username' => $username,
            'password' => bcrypt($password),
            'hdpwd' => $password,
            'country' => $country,
            'phone' => $phone,
            'user_type' => $user_type,
            'photography_role' => $photography_role,
            'description' => $description,
            'website' => $website
			
        ]);
			}


        

                $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
                $business_name = $BusinessInfo->business_name;
                $business_email = $BusinessInfo->business_email;
                       
                        $to = $email;
                        $subject = "Registration successfull";
                        $message = '';
                        $message .= '<html><body>';
                        $message .= "Hello ".$name.",<br />";
                        $message .=  "<br />";
                        $message .= "<b> Greeting of the day.</b><br />";
                        $message .= "<b> Congratulations!</b><br />";
                        $message .=  "<br />";
                        $message .= "<b> Your registration has been done successfully";
                        $message .=  "<br />";
                        $message .= "<b> Here is your login details: </b> <br />";
                        $message .= "<b> Login Email: </b> ".$email."<br />";
                        $message .= "<b> Login Username: </b> ".$username."<br />";
                        $message .= "<b> Password: </b> ".$password."<br />";
                        $message .= "<br /><br />Thanks & best regards<br />";
                        $message .= "<b> "."MrsPortrait"."</b><br />";
                        $message .= "</font>";
                        $message .= "</body></html>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $headers .= 'From: '.'MrsPortrait'.'<'.$business_email.'>' . "\r\n";
                      //  mail($to,$subject,$message,$headers);
                            
                        $adminemail =  $business_email;
                        $adminsubject = "New user registration";
                        $adminmessage = '';
                        $adminmessage .= '<html><body>';
                        $adminmessage .= "Hello Admin,<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Greeting of the day.</b><br />";
                        $adminmessage .= "<b> Congratulations!</b><br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> ".$name." registred successfully";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<b> Here is user details: </b> <br />";
                        $adminmessage .= "<b> Name : </b> ".$name."<br />";
                        $adminmessage .= "<b> Email: </b> ".$email."<br />";
                        $adminmessage .= "<b> Phone: </b> ".$phone."<br />";
                        $adminmessage .= "<b> Country: </b> ".$country."<br />";
                        $adminmessage .=  "<br />";
                        $adminmessage .= "<br /><br />Thanks & best regards<br />";
                        $adminmessage .= "<b> "."MrsPortrait"."</b><br />";
                        $adminmessage .= "</font>";
                        $adminmessage .= "</body></html>";
                        $adminheaders = "MIME-Version: 1.0" . "\r\n";
                        $adminheaders .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                        $adminheaders .= 'From: '.'MrsPortrait'.'<'.$business_email.'>' . "\r\n";
                        //mail($adminemail,$adminsubject,$adminmessage,$adminheaders);

        return $user;
    }

    public function showregistrationform()
    {
    $countries = DB::table('countries')->pluck('id', 'country_name');
    $user_roles = DB::table('user_roles')->where('role_status', 1)->pluck('id', 'role_name');
    $categories = DB::table('categories')
                    ->where('status', '=', 1)
                    ->where('deleted', '=', 0)
                    ->orderBy('id','DESC')
                    ->get();
    $photographer_roles = DB::table('photographer_role')
                    ->where('role_status', '=', 1)
                    ->orderBy('id','ASC')
                    ->get();
    return view('auth.register', compact('countries', 'user_roles', 'categories', 'photographer_roles'));
    }

    
}
