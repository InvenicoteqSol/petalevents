<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

    	$id = Auth::user()->id;
        $customer = User::findOrFail($id);

      return view('Customer.Profile.index',compact('customer'));
    }

    public function edit($id)
    {
        
                 $id = Auth::user()->id;
                 $towns = DB::table('all_towns')->pluck('id', 'town_name');

                $customer = User::where('id', $id)
                                ->first();

                return view('Customer.Profile.edit',compact('customer','towns'));
    }

    public function update(Request $request)
    {

     try {
        
        $id = Auth::user()->id;

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }

        if(trim($request->first_name)!='' && trim($request->last_name)!='') {
            $name = ucfirst(trim($request->first_name)).' '.ucfirst(trim($request->last_name));
        } else {
            $name = '';
        }

        if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }

        if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }
        if(trim($request->country)!='') {
            $country = trim($request->country);
        } else {
            $country = '';
        }
        if(trim($request->dob)!='') {
            $dob = trim($request->dob);
            //$dob_Arr = explode('-',$dob_org);
            //$dob = $dob_Arr[2].'-'.$dob_Arr[1].'-'.$dob_Arr[0];
        } else {
            $dob = '';
        }

        if(trim($request->gender)!='') {
            $gender = trim($request->gender);
        } else {
            $gender = '';
        }

        if(trim($request->description)!='') {
            $description = trim($request->description);
        } else {
            $description = '';
        }

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['name' => $name, 'first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone,'country'=>$country, 'dob' => $dob, 'gender' => $gender, 'description' => $description, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        if($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/profileimages/'), $filename); 
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profile_picture' => $filename]);
        }         


         if($upQry){
            return redirect()->route('customerdashboard.index')

                          ->with('success','Customer information is updated successfully');
        }else{
            return redirect()->route('customerdashboard.index')
                   ->with('error','Some problem occured. Please try again.');
        }

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }   
    }

    public function show()
    {

    }


}
