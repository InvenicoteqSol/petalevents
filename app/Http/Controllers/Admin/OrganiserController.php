<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Business_settings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class OrganiserController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
         try { 
             
             $id = Auth::user()->id;
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '8';
               }

             if($search_data!='') {

                $organisers = DB::table('users')->where('user_type', '=', 4)->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('email', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('phone', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('gender', 'LIKE', '%'.$search_data.'%');
                            })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
             } else {

                $organisers = DB::table('users')
                               ->where('user_type', 4)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);
             }

             return view('Admin.Organiser.index',compact('organisers', 'search_data','perpage'))

                        ->with('i', ($request->input('page', 1) - 1) * $perpage);
                        
            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
               return view('Admin.Organiser.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->first_name = ucfirst(trim($request->first_name));
        $user->last_name =   trim($request->last_name);
        $user->organisation_name = $request->organisation_name; 
        $user->website = $request->website ; 
        $user->address_line_1 = $request->address_line_1; 
        $user->address_line_2 = $request->address_line_2; 
        $user->postcode = $request->postcode; 
        $user->country = $request->country;
        $user->phone = $request->phone;
        $user->email = $request->email; 
        $user->username = $request->username; 
        $user->password = bcrypt($request->password);
        $user->hdpwd = $request->password;
        $user->security_question = $request->security_question; 
        $user->security_answer = $request->security_answer; 
        $user->user_type = $request->user_type;
        $user->created_by = Auth::id();
        $user->save();
          return redirect()->route('adminorganiser.index')

                        ->with('success','Organiser created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organiser = User::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('Admin.Organiser.show',compact('organiser')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                /*$organiser = User::find($id);*/
                //$organiser = User::findOrFail($id);
                $organiser = User::where('id', $id)
                                ->first();
                $towns = DB::table('all_towns')->pluck('id', 'town_name');

                $organiser_role = DB::table('photographer_role')
                            ->where('role_status', '=', 1)
                             ->get();

                return view('Admin.Organiser.edit',compact('organiser', 'towns', 'organiser_role'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }
        if(trim($request->organisation_name)!='') {
            $organisation_name = trim($request->organisation_name);
        } else {
            $organisation_name = '';
        }

        if(trim($request->website)!='') {
            $website = trim($request->website);
        } else {
            $website = '';
        }

        if(trim($request->address_line_1)!='') {
            $address_line_1 = trim($request->address_line_1);
        } else {
            $address_line_1 = '';
        }

        if(trim($request->address_line_2)!='') {
            $address_line_2 = trim($request->address_line_2);
        } else {
            $address_line_2 = '';
        }

        if(trim($request->postcode)!='') {
            $postcode = trim($request->postcode);
        } else {
            $postcode = '';
        }

         if(trim($request->country)!='') {
            $country = trim($request->country);
        } else {
            $country = '';
        }
         if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }
         if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }
         if(trim($request->username)!='') {
            $username = trim($request->username);
        } else {
            $username = '';
        }
         if(trim($request->password)!='') {
            $password = bcrypt($request->password);
        } else {
            $password = '';
        }
        if(trim($request->hdpwd)!='') {
            $hdpwd = trim($request->password);
        } else {
            $hdpwd = '';
        }

         if(trim($request->security_question)!='') {
                    $security_question = trim($request->security_question);
                } else {
                    $security_question = '';
                }
         if(trim($request->security_answer)!='') {
                    $security_answer = trim($request->security_answer);
                } else {
                    $security_answer = '';
                }
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['first_name' => $first_name, 'last_name' => $last_name, 'organisation_name' => $organisation_name, 'website' => $website, 'address_line_1' => $address_line_1, 'address_line_2' => $address_line_2, 'postcode' => $postcode,'country' => $country, 'phone' => $phone, 'email' => $email, 'username' => $username, 'password' => $password, 'security_question' => $security_question, 'security_answer' => $security_answer, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        if($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/profileimages/'), $filename); 
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profile_picture' => $filename]);
        }
         $id = Auth::user()->id;
        $organiser = User::findOrFail($id);          

        return redirect()->route('adminorganiser.index')

                        ->with('success','Organiser data updated successfully');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyorganiser($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('users')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('adminorganiser.index')
                           ->with('success','organiser deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('adminorganiser.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('adminorganiser.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
		 
}

