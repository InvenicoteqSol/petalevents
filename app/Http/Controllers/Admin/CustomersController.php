<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Business_settings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
      
class CustomersController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
         try { 
             
              $id = Auth::user()->id;
               $search_data =  trim($request->search_input);

                if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '8';
               }

             if($search_data!='') {

                $customers = DB::table('users')->where('user_type', '=', 3)->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('email', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('phone', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('gender', 'LIKE', '%'.$search_data.'%');
                            })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
             } else {
                
                $customers = DB::table('users')
                               ->where('user_type', 3)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);
             }

             return view('Admin.Customer.index',compact('customers', 'search_data','perpage'))
                        ->with('i', ($request->input('page', 1) - 1) * $perpage);
                     

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         return view('Admin.Customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->first_name = ucfirst(trim($request->first_name));
        $user->last_name =   trim($request->last_name);
        $user->email = $request->email; 
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->hdpwd = $request->password;
        $user->gender = $request->gender;
        $user->status = $request->status;
        $user->user_type = $request->user_type;
        $user->created_by = Auth::id();
        $user->save();
          return redirect()->route('admincustomer.index')

                        ->with('success','Customers created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = User::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('Admin.Customer.show',compact('customer')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                $customer = User::where('id', $id)
                                ->first();
                $countries = DB::table('countries')->pluck('id', 'country_name');
                return view('Admin.Customer.edit',compact('customer', 'countries'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         if(Auth::check()){
            $user = Auth::user();    
            $user_id =  Auth::id();
         }

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }
        if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }

        if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }

        if(trim($request->gender)!='') {
            $gender = trim($request->gender);
        } else {
            $gender = '';
        }

          if(trim($request->password)!='') {
            $password = bcrypt($request->password);
        } else {
            $password = '';
        }
        if(trim($request->hdpwd)!='') {
            $hdpwd = trim($request->password);
        } else {
            $hdpwd = '';
        }

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone, 'gender' => $gender, 'password' =>$password, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);
            

        if($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/profileimages/'), $filename); 
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profile_picture' => $filename]);
        }         

        return redirect()->route('admincustomer.index')

                        ->with('success','Customer data updated successfully');

        } 
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('users')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('admincustomer.index')
                           ->with('success','Customer deleted successfully');    

               
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
}

