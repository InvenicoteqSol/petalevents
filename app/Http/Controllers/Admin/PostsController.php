<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Posts;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
     
class PostsController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
          try { 
             
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '10';
               }
                if($search_data!='') {


                $posts = DB::table('posts')->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('post_name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('post_des', 'LIKE', '%'.$search_data.'%');
                                
                            })

                            ->orderBy('id','DESC')
                            ->paginate($perpage);

                 $categories = DB::table('categories')
                                ->where('deleted', '=', 0)
                                ->where(function($query) use ($search_data){
                               $query->where('cat_name', 'LIKE', '%'.$search_data.'%');
                                })
                                ->orderBy('id','DESC')
                                ->paginate($perpage);


             } else {
                
                $posts = DB::table('posts')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->paginate($perpage);

             }

             return view('Admin.Posts.index',compact('posts', 'search_data','perpage','categories'))

                         ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {
       try {

        $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

        $posts = DB::table('posts')
                           ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get();                     


                return view('Admin.Posts.create',compact('categories', 'posts'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }



    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        
        
        $posts = new Posts;
        $posts->user_id = Auth::user()->name;
        $posts->post_name = ucfirst(trim($request->post_name));
        $posts->occassions = implode(',',$request->occassions);
        $posts->description = ucfirst(trim($request->description));
        $posts->created_by = Auth::id();

        if($request->hasFile('feature_images')) {
            $image = $request->file('feature_images');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/post/feature_post/'), $filename); 
            $posts->feature_images = $filename;
        } 

        $posts->save();

        
          return redirect()->route('adminposts.index')

                        ->with('success','Posts created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   {
        try {   
               
                $posts = Posts::where('id', $id)
                                ->first();

               
                                            
                

                 return view('Admin.Posts.show',compact('posts'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }
 // }  
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try { 
            $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->get(); 
                /*$addon = User::find($id);*/
                //$addon = User::findOrFail($id);
                $posts = Posts::where('id', $id)
                                ->first();
         return view('Admin.Posts.edit',compact('posts','categories'));
              
              
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    try {
        
        if(trim($request->post_name)!='') {
            $post_name = ucfirst(trim($request->post_name));
        } else {
            $post_name = '';
        }

        if(trim($request->description)!='') {
            $description = ucfirst(trim($request->description));
        } else {
            $description = '';
        }
         if($request->occassions!='') {
            $occassions = implode(',', $request->occassions);
        } else {
            $occassions = '';
        }

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');    

        $upQry = DB::table('posts')
                ->where('id', $id)
                ->update(['post_name' => $post_name, 'description' => $description ,'occassions'=> $occassions, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

    if($request->hasFile('feature_images')) {
            $image = $request->file('feature_images');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/post/feature_post/'), $filename); 
            $upImgQry = DB::table('posts')
            ->where('id', $id)
            ->update(['feature_images' => $filename]);
        }

        return redirect()->route('adminposts.index')

                        ->with('success','Blog data updated successfully');


        }  catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('posts')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                      return redirect()->route('adminposts.index')
                           ->with('success','post deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('addons.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('addons.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

     public function co_destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                
                $upQry = DB::table('comments')
                        ->where('blog_id', '=', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                      return redirect()->route('adminposts.index')
                           ->with('success','post deleted successfully');    

        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
   


}

