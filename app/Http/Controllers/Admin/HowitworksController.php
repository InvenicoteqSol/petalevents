<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Howitworks;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
     
class HowitworksController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
          try { 
             
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '10';
               }
                if($search_data!='') {


                $howitworks = DB::table('howitworks')->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('title', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('description', 'LIKE', '%'.$search_data.'%');
                                
                            })

                            ->orderBy('id','DESC')
                            ->paginate($perpage);

             } else {
                
                $howitworks = DB::table('howitworks')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);

             }

             return view('Admin.Howitworks.index',compact('howitworks', 'search_data','perpage')) ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
     {
       try {

                 $howitworks = DB::table('howitworks')
                           ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get();                     


                return view('Admin.Howitworks.create',compact('howitworks'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }



    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $howitworks = new Howitworks;
        $howitworks->heading = $request->heading;
        $howitworks->subheading = $request->subheading;
        $howitworks->title = $request->title;
        $howitworks->description = ucfirst(trim($request->description));
        $howitworks->created_by = Auth::id();

        if($request->hasFile('feature_images')) {
            $image = $request->file('feature_images');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/post/feature_post/'), $filename); 
            $howitworks->feature_images = $filename;
        } 

        $howitworks->save();

        
          return redirect()->route('adminhowitworks.index')

                        ->with('success','Data created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
   {
        try {   
               
                $howitworks = Howitworks::where('id', $id)
                                ->first();

          return view('Admin.Howitworks.show',compact('howitworks'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }
 // }  
   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try { 
            $howitworks = Howitworks::where('id', $id)->first();
         return view('Admin.Howitworks.edit',compact('howitworks'));
              
              
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    try {
        
        if(trim($request->heading)!='') {
            $heading = ucfirst(trim($request->heading));
        } else {
            $heading = '';
        }
        if(trim($request->subheading)!='') {
            $subheading = ucfirst(trim($request->subheading));
        } else {
            $subheading = '';
        }

        if(trim($request->title)!='') {
            $title = ucfirst(trim($request->title));
        } else {
            $title = '';
        }

        if(trim($request->description)!='') {
            $description = ucfirst(trim($request->description));
        } else {
            $description = '';
        }
      
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');    

        $upQry = DB::table('howitworks')
                ->where('id', $id)
                ->update(['heading' => $heading,'subheading' => $subheading,'title' => $title, 'description' => $description ,'updated_by' => $updated_by, 'updated_at' => $updated_at]);

    if($request->hasFile('feature_images')) {
            $image = $request->file('feature_images');
            $filename = time() . '-' . $image->getClientOriginalName();
            $image->move(public_path('uploads/post/feature_post/'), $filename); 
            $upImgQry = DB::table('howitworks')
            ->where('id', $id)
            ->update(['feature_images' => $filename]);
        }

        return redirect()->route('adminhowitworks.index')

                        ->with('success','Data updated successfully');


        }  catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('howitworks')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                      return redirect()->route('adminhowitworks.index')
                           ->with('success','Data deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('addons.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('addons.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

}

