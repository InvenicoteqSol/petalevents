<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\orders;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
     
class OrdersController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
      if(Auth::user()->user_type=='1'){
             $orders = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->get();
            
                            }
                              elseif(Auth::user()->user_type=='4')
                              {
                          $events=DB::table('eventbookings')->select('id')->where('user_id','=',Auth::user()->id)->get();
                          $orders = DB::table('users')
                          ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.order_date','orders.booking_fee','tickets.ticket_type','tickets.prices','orders.id','orders.order_status_id') 
                           ->join('orders', 'orders.user_id', '=', 'users.id')
                           ->join('order_items','orders.id','=','order_items.order_id')
                           ->join('tickets','order_items.ticket_id','=','tickets.id')->where('order_items.event_id','IN',$events)
                           ->get();
                              }
           return view('Admin.Orders.index',compact('orders'));
  }
   public function exportorders(Request $request){       
     if(Auth::user()->user_type=='1'){
      $orders = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->get();
                                    
    $tot_record_found=0;
    if(count($orders)>0){
        $tot_record_found=1;
         
        $CsvData=array('First Name,Last Name,Order Number,Amount,Order Date');          
        foreach($orders as $value){              
            $CsvData[]=$value->first_name.','.$value->last_name.','.$value->order_number.','.$value->amount.','.date('Y-M-d',strtotime($value->created_at));
        }
         
        $filename='orders-'.date('Y-m-d').".csv";
        $file_path=base_path().'/public/downloads/orders_csv/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
          fputcsv($file,explode(',',$exp_data));
        }   
        fclose($file);          
 
        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
      }
    }
      elseif(Auth::user()->user_type=='3'){
      $id = Auth::user()->id;
      $orders = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->where('users.id','=',$id) 
                             ->get(); 
                                    
    $tot_record_found=0;
    if(count($orders)>0){
        $tot_record_found=1;
         
        $CsvData=array('First Name,Last Name,Order Number,Amoun,Order Date');          
        foreach($orders as $value){              
            $CsvData[]=$value->first_name.','.$value->last_name.','.$value->order_number.','.$value->amount.','.date('Y-M-d',strtotime($value->created_at));
        }
         
        $filename='orders-'.date('Y-m-d').".csv";
        $file_path=base_path().'/public/downloads/orders_csv/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
          fputcsv($file,explode(',',$exp_data));
        }   
        fclose($file);          
 
        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
      }
    }
      elseif(Auth::user()->user_type=='4'){
      $id = Auth::user()->id; 
      $orders = DB::table('users')
                  ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->where('users.id','=',$id)
                             ->get();
                                    
    $tot_record_found=0;
    if(count($orders)>0){
        $tot_record_found=1;
         
        $CsvData=array('First Name,Last Name,Order Number,Amount,Order Date');          
        foreach($orders as $value){              
            $CsvData[]=$value->first_name.','.$value->last_name.','.$value->order_number.','.$value->amount.','.date('Y-M-d',strtotime($value->created_at));
        }
         
        $filename='orders-'.date('Y-m-d').".csv";
        $file_path=base_path().'/public/downloads/orders_csv/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
          fputcsv($file,explode(',',$exp_data));
        }   
        fclose($file);          
 
        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
      }
    }
    
    
   }
    public function cancelorder($id)
    { 
           $order_status_id ='1';
           $upQry = DB::table('orders')
                  ->where('id', $id)
                  ->update(['order_status_id' => $order_status_id]);
            
    }

  public function orderdetails()
  {
    $orders = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','tickets.ticket_type','tickets.prices','orders.id','orders.order_status_id','order_items.Quantity','order_items.totalPrice','order_items.netPrice')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->join('order_items','orders.id','=','order_items.order_id')
                             ->join('tickets','order_items.ticket_id','=','tickets.id')
                              ->get();  
  }
}