<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Mrs Portrait') }}</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('public') }}/fev.png" class="img-circle">


    <!-- Styles -->
    <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/style.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/full-slider.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/table-css.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amiri:400,700" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">

    <link href="{{ url('public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/calendar.css" type="text/css" rel="stylesheet" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
     <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>
   

</head>

<body>
    <div id="app">

      @include('layouts.dashboardheader')

      @yield('content')
      
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ url('public') }}/js/app.js"></script> -->
</body>
</html>