<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Contactus;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Mail;
     
class MailController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
         {
         try { 
             
             $id = Auth::user()->id;
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '5';
               }

             if($search_data!='') {
                $petalmail = DB::table('petalmail')->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('price', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('eventtitle', 'LIKE', '%'.$search_data.'%');
                              })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
                   } else {
                      $petalmail = DB::table('petalmail')
                                     ->where('deleted', 0)
                                     ->orderBy('id','DESC')
                                     ->paginate($perpage);
                   }

             return view('Admin.Mailorg.index',compact('petalmail', 'search_data','perpage'))

                        ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }
 }
  public function authPayment($id)
     {
        $businesssettings=DB::table('business_settings')->first();
        if($businesssettings->stripe_mode=="live")
        {
            Stripe::setApiKey($businesssettings->live_secret_key);
        }
        else {
            Stripe::setApiKey($businesssettings->test_secret_key);
        }
        
        $requestPaymentDetails=DB::table('petalmail')->where('id','=',$id)->first();
        $stripe_account=DB::table('users')->where('id','=',$requestPaymentDetails->requester_id)->first();

        \Stripe\Payout::create([
            "amount" => $requestPaymentDetails->price*100,
            "currency" => "gbp",
        ], ["stripe_account" => $stripe_account->stripeId]);
     }

 }