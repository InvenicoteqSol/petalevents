<?php
namespace App\Http\Controllers\Admin;
use App\Http\Admin\TicketsController\create;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tickets;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class TicketsController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index($id)
    {

             return view('Admin.Tickets.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
               return view('Admin.Tickets.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
        if(trim($request->end_time)!='') {
            $end_time=trim($request->end_time);
        }
        else
        {
            $end_time='';
        }
        if(trim($request->start_time)!='') {
            $start_time=trim($request->start_time);
        }
        else
        {
            $start_time='';
        }

        $tickets = new Tickets;
        $tickets->ticket_type = trim($request->ticket_type);
        $tickets->ticket_details = trim($request->ticket_detail);
        $tickets->prices = trim($request->prices);
        $tickets->stock = trim($request->stock);
        $tickets->Free_booking_fee=trim($request->Free_booking_fee);
        $tickets->start_date = $start_date;
        $tickets->start_time=$start_time;
        $tickets->end_date = $end_date;
        $tickets->end_time=$end_time;
        $tickets->event_id = trim($request->event_id);
        $tickets->stock_status=trim($request->stock_status);
        $tickets->created_by = Auth::id();
        $tickets->save();

        return redirect()->route('eventbookings.index')

                        ->with('success','Ticket created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        try {

        $tickets = Tickets::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('Admin.Tickets.show',compact('tickets')); 

         } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                $eventbooking = Tickets::find($id);
                //$eventbooking = Eventbookings::findOrFail($id);
                $tickets = Tickets::where('id', $id)
                                ->first();

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                $photographers = DB::table('users')
                            ->where('user_type', '=', 4)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                return view('Admin.Tickets.edit',compact('eventbooking', 'categories', 'photographers'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
            
        if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
        if(trim($request->start_time)!='') {
            $start_time=trim($request->start_time);
        }
        else
        {
            $start_time='';
        }
        if(trim($request->end_time)!='') {
            $end_time=trim($request->end_time);
        }
        else
        {
            $end_time='';
        }
            
        $ticket_type = trim($request->ticket_type);
        $prices = trim($request->prices);
        $stock = trim($request->stock);
        $ticket_detail= trim($request->ticket_detail);
        $stock_status=trim($request->stock_status);
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('tickets')
                ->where('id', $id)
                ->update(['start_date'=>$start_date, 'end_date'=>$end_date, 'ticket_type'=>$ticket_type, 'prices'=>$prices, 'stock'=>$stock,'stock_status'=>$stock_status,'ticket_details'=>$ticket_detail,'updated_by'=>$updated_by, 'updated_at'=>$updated_at,'start_time' => $start_time,'end_time' => $end_time]);

        return redirect()->route('eventbookings.index')

                        ->with('success','Ticket data updated successfully');

       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyticket($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('tickets')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('eventbookings.index')
                           ->with('success','Tickets deleted successfully');    

                
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
}

