<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
use Mail;
use \Cart;
use App\DiscountsObj;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;


class PosteventController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {   
            $Postevents = DB::table('requestdata')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->get();
              return view('Admin.Postevent.index',compact('Postevents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $eventid=$id;
                 return view('Admin.Postevent.show',compact('eventid'));

        
        }
  public function sendmail(Request $request) {
      $custID=$request->id;
      $variable =count($request->send_mail);
      $custID=$request->send_mail;
      $request_ID=$request->reqId;
      $status=1;

      foreach ($custID as $value) {
       
          $values = array('customer_id' => $value,'request_id' => $request_ID,'status'=>1);
          DB::table('customerrequestmapings')->insert($values);
           
        }
   
        $org_id = DB::table('requestdata')->pluck('org_id');
        $user = DB::table('users')->where('id','=', $org_id)->first();
        $email=$user->email;
        $amount= $variable*.10;
          $data = array('amount'=>$amount,
                         'email'=>$email,
                         'subject'=>'Petal Events- Customer Data Request- PETCUST'.$request_ID,
						 'requestId'=>$request_ID
        );

      Mail::send(['text'=>'postmail'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.reply@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
 }


   public function paynow(Request $request)
    {   
      if (Auth::check()) {
        $showsearch="true";
         $amount=$request->amount;
        return view('Admin.Postevent.paynow',compact('showsearch','amount'));
    }
    return redirect('login');
    }
     public function chargeorg(Request $request)
    {   

      Stripe::setApiKey("sk_test_18fcxQeJ56RdJ7iNrxwYlcYo");
      $stripeEmail=$request->stripeEmail;
      $data_amount= $request->data_amount;
      $stripeToken= $request->stripeToken;
      $user_id= $request->user_id;
      $token=$request->stripeToken;
	  $requestId=$request->requestId;
      
      
      $charge = Charge::create(array(
          'source' => $token,
          'amount' =>$data_amount,
          'currency' => 'gbp'
      ));
      
    }
      
}