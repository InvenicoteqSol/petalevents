<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Business_settings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class ProfileController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {

        
         try {   

                $adminprofile = DB::table('users')
                // $businesssetting = Businesssetting::where('id', $id)
                                ->first();
                // 
                 $countries = DB::table('countries')->pluck('id', 'country_name');               
                return view('Admin.Profile.index',compact('adminprofile', 'countries'));
                // $adminprofile = User::where('id', $id)
                //                 ->first();
                // $countries = DB::table('countries')->pluck('id', 'country_name');
                // return view('adminprofile.index',compact('adminprofile', 'countries'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adminprofile = User::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('adminprofile.show',compact('adminprofile')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                /*$customer = User::find($id);*/
                //$customer = User::findOrFail($id);
                $adminprofile = User::where('id', $id)
                                ->first();
                $countries = DB::table('countries')->pluck('id', 'country_name');
                return view('adminprofile.index',compact('adminprofile', 'countries'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

     try {

        $id = Auth::user()->id;

        if(trim($request->first_name)!='') {
            $first_name = ucfirst(trim($request->first_name));
        } else {
            $first_name = '';
        }

        if(trim($request->last_name)!='') {
            $last_name = ucfirst(trim($request->last_name));
        } else {
            $last_name = '';
        }

        if(trim($request->first_name)!='' && trim($request->last_name)!='') {
            $name = ucfirst(trim($request->first_name)).' '.ucfirst(trim($request->last_name));
        } else {
            $name = '';
        }

        if(trim($request->email)!='') {
            $email = trim($request->email);
        } else {
            $email = '';
        }

        if(trim($request->phone)!='') {
            $phone = trim($request->phone);
        } else {
            $phone = '';
        }

        if(trim($request->dob)!='') {
            $dob_org = trim($request->dob);
            $dob_Arr = explode('-',$dob_org);
            $dob = $dob_Arr[2].'-'.$dob_Arr[1].'-'.$dob_Arr[0];
        } else {
            $dob = '';
        }

        if(trim($request->gender)!='') {
            $gender = trim($request->gender);
        } else {
            $gender = '';
        }

        if($request->roles!='') {
            $roles = implode(',', $request->roles);
        } else {
            $roles = '';
        }

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');
      
        $upQry = DB::table('users')
                ->where('id', $id)
                ->update(['name' => $name, 'first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone, 'dob' => $dob, 'gender' => $gender, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);
        if($upQry){
        if($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
             $filename = time() . '-' . $image->getClientOriginalName();
            
            $image->move(public_path('uploads/profileimages/'), $filename); 
            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profile_picture' => $filename]);
        }         

        
            return redirect()->route('adminprofile.index', $id)
                   ->with('success','User information is updated successfully');
        }else{
            return redirect()->route('adminprofile.index', $id)
                   ->with('error','Some problem occured. Please try again.');
        }

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }   
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('users')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('adminprofile.index')
                           ->with('success','Customer deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('admincustomers.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('admincustomers.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
}

