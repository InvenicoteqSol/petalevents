<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Businesssetting;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;



class BusinesssettingController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {try {   


                $businesssetting = DB::table('business_settings')
                
                                ->first();
                return view('Admin.Businesssetting.edit',compact('businesssetting')); 

                
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)


    {

         $businesssetting = DB::table('business_settings')
              
                                ->first();
                return view('Admin.Businesssetting.edit',compact('businesssetting')); 
       // $businesssetting = Businesssetting::findOrFail($id);

        // $businesssetting = DB::table('business_settings')
        //                        ->where('status', 1)
        //                        ->where('id', $id)
        //                        ->first();
        // return view('businesssetting.show',compact('businesssetting')); 

    }                           
        /*$customer = User::where('id', $id)
                                
                                ->first();/
        return view('businesssetting.show',compact('businesssetting')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   


                $businesssetting = DB::table('business_settings')
                // $businesssetting = Businesssetting::where('id', $id)
                                ->first();
                return view('Admin.Businesssetting.edit',compact('businesssetting')); 

                
        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {

        if(Auth::check()){
            $businesssetting = Auth::user();    
            $businesssetting_id =  Auth::id();
         }

        if(trim($request->business_name)!='') {
            $business_name = ucfirst(trim($request->business_name));
        } else {
            $business_name = '';
        }

        if(trim($request->business_email)!='') {
            $business_email = trim($request->business_email);
        } else {
            $business_email = '';
        }

        if(trim($request->business_phone)!='') {
            $business_phone = trim($request->business_phone);
        } else {
            $business_phone = '';
        }

        if(trim($request->business_city)!='') {
            $business_city = trim($request->business_city);
        } else {
            $business_city = '';
        }

        if(trim($request->business_state)!='') {
            $business_state = trim($request->business_state);
        } else {
            $business_state = '';
        }

       if(trim($request->business_country)!='') {
            $business_country = ucfirst(trim($request->business_country));
        } else {
            $business_country = '';
        }
       
       if(trim($request->business_address)!='') {
            $business_address = trim($request->business_address);
        } else {
            $business_address = '';
        }
       
       if(trim($request->business_zipcode)!='') {
            $business_zipcode = trim($request->business_zipcode);
        } else {
            $business_zipcode = '';
        }
        if(trim($request->twitter_link)!='') {
            $twitter_link = trim($request->twitter_link);
        } else {
            $twitter_link = '';
        }
        if(trim($request->facebook_link)!='') {
            $facebook_link = trim($request->facebook_link);
        } else {
            $facebook_link = '';
        }
        if(trim($request->google_link)!='') {
            $google_link = trim($request->google_link);
        } else {
            $google_link = '';
        }
        if(trim($request->mailchip_keys)!='') {
            $mailchip_keys = trim($request->mailchip_keys);
        } else {
            $mailchip_keys = '';
        }
         if(trim($request->stripe_mode)!='') {
             $stripe_mode = trim($request->stripe_mode);
        } else {
             $stripe_mode = '';
        }
        if(trim($request->live_publish_key)!='') {
            $live_publish_key = trim($request->live_publish_key);
        } else {
            $live_publish_key = '';
        }
         if(trim($request->live_secret_key)!='') {
            $live_secret_key = trim($request->live_secret_key);
        } else {
            $live_secret_key = '';
        }
        if(trim($request->test_secret_key)!='') {
            $test_secret_key = trim($request->test_secret_key);
        } else {
            $test_secret_key = '';
        }
          if(trim($request->test_publish_key)!='') {
            $test_publish_key = trim($request->test_publish_key);
        } else {
            $test_publish_key = '';
        }
          if(trim($request->test_secret_key)!='') {  
            $test_secret_key = trim($request->test_secret_key);
        } else {
            $test_secret_key = '';
        }
           if(trim($request->paypal_mode)!='') {
            $paypal_mode = trim($request->paypal_mode);
        } else {
            $paypal_mode = '';
        }
           if(trim($request->paypal_live_email)!='') {
            $paypal_live_email = trim($request->paypal_live_email);
        } else {
            $paypal_live_email = '';
        }
          if(trim($request->paypal_test_email)!='') {
            $paypal_test_email = trim($request->paypal_test_email);
        } else {
            $paypal_test_email = '';
        }
         if(trim($request->booking_discount)!='') {
            $booking_discount = trim($request->booking_discount);
        } else {
            $booking_discount = '';
        }
       

        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');
        $businesssetting_id =  $id;

        $upQry = DB::table('business_settings')  
                ->where('id', $id)
                ->update(['business_name' => $business_name, 'business_email' => $business_email, 'business_phone' => $business_phone, 'business_city' => $business_city, 'business_state' => $business_state, 'business_country' => $business_country, 'business_address' => $business_address,'business_zipcode' => $business_zipcode,'twitter_link' =>$twitter_link,'facebook_link' =>$facebook_link,'google_link' =>$google_link,'mailchip_keys' =>$mailchip_keys,'stripe_mode' =>$stripe_mode,'live_publish_key' =>$live_publish_key,'live_secret_key' =>$live_secret_key,'test_secret_key' =>$test_secret_key,'test_publish_key' =>$test_publish_key,'paypal_mode' =>$paypal_mode,'paypal_live_email' =>$paypal_live_email,'paypal_test_email' =>$paypal_test_email,'booking_discount' =>$booking_discount,'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                return redirect()->route('adminbusinesssetting.edit', $id)
                    ->with('success','Business Setting data updated successfully');

       

         } catch (\Illuminate\Database\QueryException $e) {

             $errorClass = new ErrorsClass();
             $errors = $errorClass->saveErrors($e);

         } catch (\Exception $e) {
             $errorClass = new ErrorsClass();
             $errors = $errorClass->saveErrors($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('business_settings')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('Admin.Businesssetting.index')
                           ->with('success','Customer deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('adminphotographers.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('adminphotographers.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
}

