<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
use Stripe\Stripe;
use Stripe\Customer;
use Stripe\Charge;
use Mail;

class EventbookingsController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }


      public function index(Request $request)
    {
         try { 
             
            $search_data =  trim($request->search_input); 
             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '10';
               }

             $oc =  trim($request->oc);
             $ps =  trim($request->ps); 
             $bs =  trim($request->bs); 
             if($search_data!='' || $oc!='') {

                $eventbookings = DB::table('eventbookings')->where('deleted', '=', 0)->where(function($query) use ($search_data,$oc,$ps,$bs){
                                if ($search_data!='') {    
                                    $query->where('event_title', 'LIKE', '%'.$search_data.'%');
                                    $query->orWhere('type_of_event', 'LIKE', '%'.$search_data.'%');
                                }
                                if ($oc!='') {  
                                    $query->whereRaw("find_in_set('".$oc."', start_date)");
                                }
                                /*if ($ps!='') {  
                                    $query->where('payment_status', '=', $ps);
                                }
                                if ($bs!='') {  
                                    $query->where('booking_status', '=', $bs);
                                }*/
                            })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
             } else {
                
                $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);
             }

             $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get();

             return view('Admin.Eventbookings.index',compact('eventbookings', 'categories', 'search_data', 'oc', 'ps', 'bs','perpage'))

                        ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       try {
  $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

        // $isPreReqComplete = Auth::user()->prereqComplete; 
       
// if( $isPreReqComplete==1 ){

                return view('Admin.Eventbookings.create',compact('categories'));
            // }
  // else{

  //               return redirect()->route('eventbookings.index')

  //      ->with("success","Please connect with your stripe account first"."<a href='url('/')onboard-prereq'>Click here</a>");

  //           }

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        Stripe::setApiKey("sk_test_18fcxQeJ56RdJ7iNrxwYlcYo");
         if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }

        if( trim($request->start_date)!='' && trim($request->end_date)!='' ) {

            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];

            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];

            $start_end_date = $start_date.' to '.$end_date;
                } else {
                    $start_end_date = '';
                  }
            if(trim($request->end_time)!='') {
            $end_time=trim($request->end_time);
                  }
             else
                {
                 $end_time='';
                }
            if(trim($request->start_time)!='') {
            $start_time=trim($request->start_time);
                 }
               else
                 {
                     $start_time='';
                     }

        if(trim($request->waive_booking_fee)!='') {
            $waive_booking_fee = trim($request->waive_booking_fee);
          } else {
            $waive_booking_fee = '';
        }
        if(trim($request->stripe_token)!='')
        {
            $stripe="charge";
            $charge = Charge::create(array(
            'source' => trim($request->stripe_token),
            'amount' =>500,
            'currency' => 'gbp'
        ));
            

        }
        else
        {
            $stripe="";
        }
        $booking = new Eventbookings;
        $booking->user_id = Auth::user()->id;
        if(trim($request->event_title)!='') {
        $booking->event_title=trim($request->event_title);
        $emailcheck= DB::table('eventbookings')->where('event_title',$booking->event_title)->count();
        if($emailcheck > 0)
            {
            return redirect()->route('eventbookings.index')

                        ->with('success','Event already exists...');
            }
            else
            {
        $booking->supporting_art = trim($request->supporting_art);
        $booking->event_info = trim($request->event_info);
        $booking->type_of_event = trim($request->type_of_event);
        $booking->age_limit = trim($request->age_limit);
        $booking->venue_name = trim($request->venue_name);
        $booking->start_date = $start_date;
        $booking->end_date = $end_date;
        $booking->start_time = $start_time;
        $booking->end_time = $end_time;
        $booking->venue_address = trim($request->venue_address);
        $booking->details = trim($request->details);
        $booking->telephone = trim($request->telephone);
        $booking->website = trim($request->website);
        $booking->waive_booking_fee =$waive_booking_fee;
        if($stripe=="charge")
        {
            $booking->payment_status=$charge->status;
            $booking->is_featured=true;
            $booking->payment_transaction=$charge->id;


        }
        else
        {
            $booking->is_featured=false;
        }

        $booking->created_by = Auth::id();
        if($file = $request->hasFile('event_image')) {
        $file = $request->file('event_image');
        $extension = $file->getClientOriginalName();
        $thumb = Image::make($file->getRealPath())->fit(270, 151, function ($constraint) {
            $constraint->upsize(); 
        });
        $destinationPath = public_path('/uploads/profileimages/');
        $file->move($destinationPath, $extension);
        $thumb->save($destinationPath.'/thumb_'.$extension);
        $booking->event_image = $extension;
        $booking->image_thumb = 'thumb_' . $extension;
         }

        $booking->save(); 
        return redirect()->route('eventbookings.index')

                        ->with('success','Event created successfully');
            }
         }
         else
         {
            echo("Event title null");
            return redirect()->route('eventbookings.index')

                        ->with('success','Event Title Null');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        
        $eventbooking = Eventbookings::findOrFail($id);
        $eventTicketDetails = DB::table('users')
                 ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','tickets.ticket_type','tickets.prices','tickets.Free_booking_fee','orders.id','orders.order_status_id','order_items.Quantity','order_items.totalPrice','order_items.netPrice')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->join('order_items','orders.id','=','order_items.order_id')
                             ->join('tickets','order_items.ticket_id','=','tickets.id')
                             ->where('order_items.event_id','=',$eventbooking->id)
                             ->where('orders.order_status_id','=',1)
                             ->get();


        $amounttotal = DB::table('users')
                 ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','tickets.ticket_type','tickets.prices','tickets.Free_booking_fee','orders.id','orders.order_status_id','order_items.Quantity','order_items.totalPrice','order_items.netPrice')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->join('order_items','orders.id','=','order_items.order_id')
                             ->join('tickets','order_items.ticket_id','=','tickets.id')
                             ->where('order_items.event_id','=',$eventbooking->id)
                             ->where('orders.order_status_id','=',1)
                             ->select(DB::raw("SUM(totalPrice) as count"))
                             ->get();

         $totalquantity = DB::table('users')
                 ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','tickets.ticket_type','tickets.prices','tickets.Free_booking_fee','orders.id','orders.order_status_id','order_items.Quantity','order_items.totalPrice','order_items.netPrice')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->join('order_items','orders.id','=','order_items.order_id')
                             ->join('tickets','order_items.ticket_id','=','tickets.id')
                             ->where('order_items.event_id','=',$eventbooking->id)
                             ->where('orders.order_status_id','=',1)
                             ->select(DB::raw("SUM(Quantity) as count"))
                             ->get();

         /* $eventTicketDetails=DB::table('orders')->join('tickets','orders.ticket_id', '=', 'tickets.id')->where('orders.event_id','=',$id)->get();*/
        $totalTickets=DB::table('tickets')->where('event_id','=',$eventbooking->id)->sum('stock');
        $totalTickets1=DB::table('tickets')->where('event_id','=',$eventbooking->id)->get();
        $ticketsavailable=$totalTickets-count($eventTicketDetails);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('Admin.Eventbookings.show',compact('eventbooking','eventTicketDetails','ticketsavailable','totalTickets1','amounttotal','totalquantity')); 

         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                /*$eventbooking = Eventbookings::find($id);*/
                //$eventbooking = Eventbookings::findOrFail($id);
                $eventbooking = Eventbookings::where('id', $id)
                                ->first();

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                $photographers = DB::table('users')
                            ->where('user_type', '=', 4)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                return view('Admin.Eventbookings.edit',compact('eventbooking', 'categories', 'photographers'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
        if(trim($request->end_time)!='') {
            $end_time=trim($request->end_time);
        }
        else
        {
            $end_time='';
        }
        if(trim($request->start_time)!='') {
            $start_time=trim($request->start_time);
        }
        else
        {
            $start_time='';
        }
        $event_title = trim($request->event_title);
        $supporting_art = trim($request->supporting_art);
        $event_info = trim($request->event_info);
        $type_of_event = trim($request->type_of_event);
        $age_limit = trim($request->age_limit);
        /*if(trim($request->event_date)!='') {
         $event_date = trim($request->event_date);
          
        } else {
            $event_date = '';
        }  */
        $venue_name = trim($request->venue_name);
        $venue_address = trim($request->venue_address);
        $details = trim($request->details);
        $telephone = trim($request->telephone);
        $website = trim($request->website);
        $waive_booking_fee = trim($request->waive_booking_fee);
       // $user_id = trim($request->user_id);
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('eventbookings')
                ->where('id', $id)
                ->update(['start_date' => $start_date,'end_date' => $end_date, 'event_title' => $event_title, 'supporting_art' => $supporting_art, 'event_info' => $event_info, 'type_of_event' => $type_of_event, 'age_limit' => $age_limit, 'venue_name' => $venue_name, 'venue_address' =>$venue_address,'details' => $details, 'telephone' => $telephone, 'website' => $website,'waive_booking_fee'=> $waive_booking_fee, 'updated_by' => $updated_by, 'updated_at' => $updated_at,'start_time' => $start_time,'end_time' => $end_time]);
         
         
         if($file = $request->hasFile('event_image')) {
        $file = $request->file('event_image');
        $extension = $file->getClientOriginalName();
        $thumb = Image::make($file->getRealPath())->fit(270, 151, function ($constraint) {
            $constraint->upsize(); 
        });
        $destinationPath = public_path('/uploads/profileimages/');
        $file->move($destinationPath, $extension);
        $thumb->save($destinationPath.'/thumb_'.$extension);
        $upImgQry = DB::table('eventbookings')
                    ->where('id', $id)
                    ->update(['event_image' => $extension,'image_thumb' =>'thumb_' . $extension,]);

    }


        return redirect()->route('eventbookings.index')

                        ->with('success','Event data updated successfully');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyevent($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('eventbookings')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('eventbookings.index')
                           ->with('success','Event deleted successfully');    

                /*$user = User::findOrFail($id); 
                $deleted = $user->delete();
                if($deleted) {
                    return redirect()->route('eventbookings.index')
                           ->with('success','Customer deleted successfully');
                } else {
                    return redirect()->route('eventbookings.index')
                           ->with('error','Sorry fail to delete customer'); 
                }*/
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }
     public function sendmailpetal(Request $request)
     {
         $id = Auth::user()->id;
         $name = ucfirst(trim($request->username));
         $price = ucfirst(trim($request->totalamount));
         $eventtitle = ucfirst(trim($request->event_title));
    $values = array('name'=>$name,'price'=>$price,'eventtitle'=>$eventtitle,'requester_id'=>$id);

         DB::table('petalmail')->insert($values);
         $data = array('name'=>$name,
                       'email'=>'info@petalevents.co.uk',
                       'price'=>$price,
                       'eventtitle'=>$eventtitle,
                       'subject'=>'Petal Events- Welcome Aboard - '.$name.' !!!'
        );
      
      Mail::send(['text'=>'petalmail'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.reply@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
       return redirect()->route('eventbookings.index')
                           ->with('success','Payment request sent successfully'); 
     }
    
}

