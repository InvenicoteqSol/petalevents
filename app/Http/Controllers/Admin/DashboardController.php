<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Admindashboard;
use App\Adminphotographers;
use App\Eventbookings;
use App\Classbookings;
use App\Photoclasses;
use App\Categories;
use App\Addons;
use App\Posts;
use App\Faqs;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;


class DashboardController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index()
    {
         try { 
              $customers =  DB::table('users')
                               ->where('user_type', 3)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               
                               ->get();  
                              

               $photographers = DB::table('users')
                               ->where('user_type', 4)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               
                              ->get();

                $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                                
                                ->get();

           
             $data_request =  DB::table('requestdata')
                            ->where('deleted', 0)
                           ->orderBy('id','DESC')
                           ->get(); 
                $topFiveCustomers=$customers->take(5); 
                $totalCustomersCount=count($customers); 
                $topFiveOrganisers=  $photographers->take(5);
                $totalOrganisersCount=count($photographers  );
                $topFiveEvents=$eventbookings->take(5);
                $totalEventCount=count($eventbookings);
                $data_request_Count=count($data_request);

             
             return view('Admin.Dashboard.index',compact('topFiveCustomers','totalCustomersCount','topFiveOrganisers','totalOrganisersCount','topFiveEvents','totalEventCount','data_request_Count'));
                

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $customer = User::findOrFail($id);
        $photographer = User::findOrFail($id);
        $eventbooking = Eventbookings::findOrFail($id);
         $classbooking = DB::table('classbookings')
                            ->where('id','=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
        $categories = Categories::findOrFail($id);

    
        $catgalleryimages = DB::table('catgallery')
                            ->where('cat_id', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get(); 
        $photoclass = Photoclasses::findOrFail($id); 
                               
        $pcgalleryimages = DB::table('pcgallery')
                            ->where('pc_id', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                            ->get();  
              $countries = DB::table('countries')
              ->get();

         $addon = Addons::findOrFail($id);                      


        return view('Admin.Dashboard.Show',compact('customers','photographer','eventbooking','classbooking','categories','catgalleryimages','photoclass','pcgalleryimages','addon','countries'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

