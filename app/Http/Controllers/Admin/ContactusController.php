<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Contactus;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
     
class ContactusController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
         {
         try { 
             
             $id = Auth::user()->id;
             $search_data =  trim($request->search_input); 

             if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '5';
               }

             if($search_data!='') {

                $contactus = DB::table('contactus')->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('email', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('message', 'LIKE', '%'.$search_data.'%');
                              })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
             } else {
                
                $contactus = DB::table('contactus')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);
             }

             return view('Admin.Contactus.index',compact('contactus', 'search_data','perpage'))

                        ->with('i', ($request->input('page', 1) - 1) * $perpage);

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }
 }
  public function destroycontact($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('contactus')
                        ->where('id', $id)
                        ->update(['deleted' => '1', 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

                    return redirect()->route('admincontactus.index')
                           ->with('success','Message deleted successfully');    
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    public function show($id)
    {
        $contactus = DB::table('contactus')
                               ->where('deleted', 0)
                               ->where('id','=',$id)
                               ->first();
        return view('Admin.Contactus.show',compact('contactus')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 }