<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tickets;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class CustomerdataController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index($id)
    {
             $event_id=$id;
             return view('Admin.Customerdata.index',compact('event_id'));
     }
  public function exportcustomer($id){     
     $event_id=$id;  
     $customerdata = DB::table('users')
                  ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.order_date')
                  ->join('orders', 'orders.user_id', '=', 'users.id')
                 /* ->join('customerrequestmapings','orders.user_id','=','customerrequestmapings.customer_id')*/
                  ->where('orders.event_id','=', $event_id)
                   ->get();  
    $tot_record_found=0;
    if(count($customerdata)>0){
        $tot_record_found=1;
         
        $CsvData=array('First Name,Last Name,Email,Phone,Order Number,Amount,Order Date');          
        foreach($customerdata as $value){              
            $CsvData[]=$value->first_name.','.$value->last_name.','.$value->email.','.$value->phone.','.$value->order_number.','.date('Y-M-d',strtotime($value->order_date));
        }
         
        $filename='customerdata-'.date('Y-m-d').".csv";
        $file_path=base_path().'/public/downloads/customerdata_csv/'.$filename;   
        $file = fopen($file_path,"w+");
        foreach ($CsvData as $exp_data){
          fputcsv($file,explode(',',$exp_data));
        }   
        fclose($file);          
 
        $headers = ['Content-Type' => 'application/csv'];
        return response()->download($file_path,$filename,$headers );
    }
    
    
   }


}