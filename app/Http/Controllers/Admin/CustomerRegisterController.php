<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Business_settings;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
      
class CustomerRegisterController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {
         try { 
             
              $id = Auth::user()->id;
               $search_data =  trim($request->search_input);

                if($request->perpage!=''){
                    $perpage = $request->perpage;
               } else {
                    $perpage = '8';
               }

             if($search_data!='') {

                $customers = DB::table('users')->where('user_type', '=', 3)->where('deleted', '=', 0)->where(function($query) use ($search_data){
                                $query->where('name', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('email', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('phone', 'LIKE', '%'.$search_data.'%');
                                $query->orWhere('gender', 'LIKE', '%'.$search_data.'%');
                            })
                            ->orderBy('id','DESC')
                            ->paginate($perpage);
             } else {
                
                $customers = DB::table('users')
                               ->where('user_type', 3)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->paginate($perpage);
             }

             return view('Admin.Customers.index',compact('customers', 'search_data','perpage'))
                        ->with('i', ($request->input('page', 1) - 1) * $perpage);
                     

            } catch (\Illuminate\Database\QueryException $e) {
                $errorClass = new ErrorsClass();
                $errors = $errorClass->saveErrors($e);
            } catch (\Exception $e) {
               $errorClass = new ErrorsClass();
               $errors = $errorClass->saveErrors($e);
        }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
         return view('resources.views.createcustomer');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->first_name = ucfirst(trim($request->first_name));
        $user->last_name =   trim($request->last_name);
        $user->email = $request->email; 
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->hdpwd = $request->password;
        $user->gender = $request->gender;
        $user->status = $request->status;
        $user->user_type = $request->user_type;
        $user->created_by = Auth::id();
        $user->save();
          return redirect()->route('admincustomers.index')

                        ->with('success','Customers created successfully');
    }
}