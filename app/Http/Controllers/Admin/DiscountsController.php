<?php
namespace App\Http\Controllers\Admin;
use App\Http\Admin\TicketsController\create;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Discounts;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class DiscountsController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index($id)
    {
      $Tickets=DB::table('tickets')->where('event_id','=',$id)->get();

             return view('Admin.Discounts.index',compact('Tickets','id'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)

    {  $id;
               return view('Admin.Discounts.create',compact('id'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function discountsticket(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
       
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
          } else {
            $expiry_date = '';
        }
          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('discounts')->insertGetId([
                    'name' => $name,
                    'code_name' => $code_name,
                    'event_id' => $event_id,
                    'availability' => $availability,  
                    'discount_type' => $discount_type, 
                    'discount' => $discount,
                    'start_date' => $start_date,
                    'expiry_date' => $expiry_date,
                    'created_at' => $created_at,
                    'ticket_type_id'=>$ticketId,
                    'created_by'=>Auth::user()->id
                 ]);
       
        return redirect()->route('discounts.index',$id)

                        ->with('success','Discounts created successfully');
    }


      public function earlybirddiscounts(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
         if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
           if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('discounts')->insertGetId([
                    'name' => $name,
                    'code_name' => $code_name,
                    'event_id' => $event_id, 
                    'availability' => $availability, 
                    'discount_type' => $discount_type, 
                    'discount' => $discount,
                    'start_date' => $start_date,
                    'expiry_date' => $expiry_date,
                    'created_at' => $created_at,
                    'ticket_type_id'=>$ticketId
                 ]);
       
        return redirect()->route('discounts.index',$id)

                        ->with('success','Early bird discounts created successfully');
    }


      public function bundleofferdiscounts(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
          if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
         if(trim($request->bundle_offer_ticket)!='') {
            $bundle_offer_ticket = trim($request->bundle_offer_ticket);
          } else {
            $bundle_offer_ticket = '';
        }
        if(trim($request->bundle_offer_price)!='') {
            $bundle_offer_price = trim($request->bundle_offer_price);
          } else {
            $bundle_offer_price = '';
        }
          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('discounts')->insertGetId([
                    'name' => $name,
                    'code_name' => $code_name,
                    'event_id' => $event_id, 
                    'availability' => $availability,
                    'start_date' => $start_date,
                    'expiry_date' => $expiry_date,
                    'bundle_offer_ticket' => $bundle_offer_ticket,
                    'bundle_offer_price' => $bundle_offer_price,
                    'created_at' => $created_at,
                    'ticket_type_id'=>$ticketId
                 ]);
       
        return redirect()->route('discounts.index',$id)

                        ->with('success','bundle offer ticket discounts created successfully');
    }


    public function ticketxdiscounts(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
         if(trim($request->ticketx)!='') {
            $ticketx = trim($request->ticketx);
          } else {
            $ticketx = '';
        }
        if(trim($request->tickety)!='') {
            $tickety = trim($request->tickety);
          } else {
            $tickety = '';
        }
          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('discounts')->insertGetId([
                    'name' => $name,
                    'code_name' => $code_name,
                    'event_id' => $event_id, 
                    'availability' => $availability, 
                    'start_date' => $start_date,
                    'expiry_date' => $expiry_date,
                    'ticketx' => $ticketx,
                    'tickety' => $tickety,
                    'created_at' => $created_at,
                    'ticket_type_id'=>$ticketId
                 ]);
       
        return redirect()->route('discounts.index',$id)

                        ->with('success','Ticket X discounts created successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        try {

        $discounts = Discounts::findOrFail($id);
      
        return view('Admin.Discounts.show',compact('discounts')); 

         } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                $discount = Discounts::find($id);
                return view('Admin.Discounts.edit',compact('discount'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            
        if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
            
        $ticket_type = trim($request->ticket_type);
        $prices = trim($request->prices);
        $stock = trim($request->stock);
       
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('discount')
                ->where('id', $id)
                ->update(['start_date' => $start_date, 'end_date' => $end_date, 'ticket_type' => $ticket_type, 'prices' => $prices, 'stock' => $stock, 'max_ticket' => $max_ticket, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        return redirect()->route('discount.index')

                        ->with('success','Discount data updated successfully');

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroydiscount($id)
    {
        try {   
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('discounts')
                        ->where('discount_Id', $id)
                        ->update(['is_deleted' => '1']);

        return redirect()->route('discounts.index',$id)
                           ->with('success','Discount deleted successfully');    

                
        } catch (\Illuminate\Database\QueryException $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    public function discountsticketedit(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
        
        $updated_at = date('Y-m-d H:i:s');

          $upQry = DB::table('discounts')
                ->where('discount_Id', $id)
                ->update(['name' => $name, 'code_name' => $code_name, 'availability' => $availability,'event_id' => $event_id, 'discount_type' => $discount_type, 'discount' => $discount, 'start_date' => $start_date, 'expiry_date' => $expiry_date,'ticket_type_id'=>$ticketId, 'updated_at' => $updated_at]);
        
        return redirect()->route('discounts.index',$event_id)

                        ->with('success','Discounts updated successfully');
    }

      public function earlybirddiscountsedit(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
      
          if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
           if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
         if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }

        $updated_at = date('Y-m-d H:i:s');
      
           $upQry = DB::table('discounts')
                ->where('discount_Id', $id)
                ->update(['name' => $name, 'code_name' => $code_name, 'availability' => $availability,'event_id' => $event_id,'discount_type' => $discount_type, 'discount' => $discount,'start_date' => $start_date, 'expiry_date' => $expiry_date,'ticket_type_id'=>$ticketId, 'updated_at' => $updated_at]);
       
        return redirect()->route('discounts.index',$event_id)

                        ->with('success','Early bird discounts updated successfully');
    }
     public function bundleofferdiscountsedit(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
      
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
         if(trim($request->bundle_offer_ticket)!='') {
            $bundle_offer_ticket = trim($request->bundle_offer_ticket);
          } else {
            $bundle_offer_ticket = '';
        }
        if(trim($request->bundle_offer_price)!='') {
            $bundle_offer_price = trim($request->bundle_offer_price);
          } else {
            $bundle_offer_price = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
         if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
          
          $updated_at = date('Y-m-d H:i:s');
          $upQry = DB::table('discounts')
                ->where('discount_Id', $id)
                ->update(['name' => $name, 'code_name' => $code_name, 'availability' => $availability,'event_id' => $event_id,'start_date' => $start_date, 'expiry_date' => $expiry_date,'bundle_offer_ticket'=>$bundle_offer_ticket,'bundle_offer_price'=>$bundle_offer_price,'ticket_type_id'=>$ticketId, 'updated_at' => $updated_at]);
       
       
        return redirect()->route('discounts.index',$event_id)

                        ->with('success','bundle offer ticket discounts updated successfully');
    }


    public function ticketxdiscountsedit(Request $request,$id)
    {   

      
        if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->code_name)!='') {
            $code_name = trim($request->code_name);
          } else {
            $code_name = '';
        }
      
        if(trim($request->availability)!='') {
            $availability = trim($request->availability);
          } else {
            $availability = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->expiry_date)!='') {
            $end_date_org = trim($request->expiry_date);
            $end_date_Arr = explode('-',$end_date_org);
            $expiry_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $expiry_date = '';
        }
         if(trim($request->ticketx)!='') {
            $ticketx = trim($request->ticketx);
          } else {
            $ticketx = '';
        }
        if(trim($request->tickety)!='') {
            $tickety = trim($request->tickety);
          } else {
            $tickety = '';
        }
        if(trim($request->ticketId)!='') {
            $ticketId = trim($request->ticketId);
          } else {
            $ticketId = '';
        }
         if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
          
       $updated_at = date('Y-m-d H:i:s');
          $upQry = DB::table('discounts')
                ->where('discount_Id', $id)
                ->update(['name' => $name, 'code_name' => $code_name, 'availability' => $availability,'event_id' => $event_id,'start_date' => $start_date, 'expiry_date' => $expiry_date,'ticketx'=>$ticketx,'tickety'=>$tickety,'ticket_type_id'=>$ticketId, 'updated_at' => $updated_at]);
        return redirect()->route('discounts.index',$event_id)

                        ->with('success','Ticket X discounts updated successfully');
    }

}

