<?php
namespace App\Http\Controllers\Admin;
use App\Http\Admin\TicketcollabrationController\create;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticketcollabration;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use Image;
use Mail;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;

class TicketcollabrationController extends Controller
{    

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index()
  {
      if (isset($_GET['colabId'])==""|| isset($_GET['ans'])=="") {

             return view('Admin.Ticketcollabration.index');
           }
           else {
             $collabration=$_GET['colabId'];
             $ans=$_GET['ans'];
             if($ans=='yes'){
              $upQry = DB::table('ticketcollabration')
                ->where('id', $collabration)
                ->update(['status'=>1]);

              $ticketorg_id = DB::table('ticketcollabration')
                            ->where('id', $collabration)->first();
                            $org_id=$ticketorg_id->created_by;
                $user=DB::table('users')->where('id','=',$org_id)->first();
                $email=$user->email;
                $data = array(
                     'email'=>$email,
                     'subject'=>'Ticket collaboration request is accepted'
              );
              Mail::send(['text'=>'ticketcollabaccept'], $data, function($message) use($data) {
              $message->to($data['email'], 'Petal Events')->subject
              ($data['subject']);
              $message->from('no.replyinfo@petalevents.co.uk','Petal Events');
              $message->setContentType('text/html');
              });

              return view('Admin.Ticketcollabration.index')->with('success','Ticket collaboration request accepted successfully');
              }
              else
              {
                $upQry = DB::table('ticketcollabration')
                ->where('id', $collabration)
                ->update(['status'=>0]);
                $ticketorg_id = DB::table('ticketcollabration')
                            ->where('id', $collabration)->first();
                             $org_id=$ticketorg_id->created_by;
               $user=DB::table('users')->where('id','=',$org_id)->first();
               $email=$user->email;
               $data = array(
                     'email'=>$email,
                     'subject'=>'Ticket collaboration request is declined'
              );
              Mail::send(['text'=>'ticketcollabdecline'], $data, function($message) use($data) {
              $message->to($data['email'], 'Petal Events')->subject
              ($data['subject']);
              $message->from('no.replyinfo@petalevents.co.uk','Petal Events');
              $message->setContentType('text/html');
              });

              return view('Admin.Ticketcollabration.index')->with('success','Ticket collaboration request is declined');

              }
           }
        
    }
    public function collabration($id)
        {

            $eventbookings= DB::table('eventbookings')->where("id","$id")->first()->id;
            $tickets = DB::table("tickets")
                        ->where('deleted', 0)
                        ->where("event_id",$eventbookings)
                        
                        ->get();

           echo json_encode($tickets);
        }
    public function collabration_org($id)
        {

            $users= DB::table('users')->where("id","$id")->first()->id;
            $events = DB::table("eventbookings")
                        ->where('deleted', 0)
                        ->where("user_id",$users)
                        
                        ->get();

           echo json_encode($events);
        }
  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
               return view('Admin.Ticketcollabration.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function selfevents(Request $request)
    { 
      if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
        if(trim($request->ticket_id)!='') {
            $ticket_id = trim($request->ticket_id);
          } else {
            $ticket_id = '';
        }
        if(trim($request->event_id2)!='') {
            $event_id2 = trim($request->event_id2);
          } else {
            $event_id2= '';
        }
        if(trim($request->coll_type)!='') {
            $coll_type = trim($request->coll_type);
          } else {
            $coll_type= '';
        }
         if(trim($request->ticket_id2)!='') {
            $ticket_id2 = trim($request->ticket_id2);
          } else {
            $ticket_id2 = '';
        }
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('ticketcollabration')->insertGetId([
                    'name' => $name,
                    'event_id' => $event_id,
                    'ticket_id' => $ticket_id,
                    'coll_type'=>$coll_type,
                    'event_id2' => $event_id2,
                    'ticket_id2' => $ticket_id2,
                    'discount_type' => $discount_type, 
                    'discount' => $discount,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'created_at' => $created_at,
                    'created_by'=>Auth::user()->id
                 ]);
       
        return redirect()->route('ticketcollabration.index')

                        ->with('success','Ticket collaboration created successfully');
    }

  public function collabrationevent(Request $request)
    { 
      if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
         if(trim($request->org_id)!='') {
            $org_id = trim($request->org_id);
          } else {
            $org_id = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
         if(trim($request->coll_type)!='') {
            $coll_type = trim($request->coll_type);
          } else {
            $coll_type= '';
        }
        if(trim($request->ticket_id)!='') {
            $ticket_id = trim($request->ticket_id);
          } else {
            $ticket_id = '';
        }
        if(trim($request->event_id2)!='') {
            $event_id2 = trim($request->event_id2);
          } else {
            $event_id2= '';
        }
         if(trim($request->ticket_id2)!='') {
            $ticket_id2 = trim($request->ticket_id2);
          } else {
            $ticket_id2 = '';
        }
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }


          $created_at = date('Y-m-d H:i:s');
          $discounts = DB::table('ticketcollabration')->insertGetId([
                    'name' => $name,
                    'event_id' => $event_id,
                    'coll_type'=>$coll_type,
                    'ticket_id' => $ticket_id,
                    'org_id' => $org_id,
                    'event_id2' => $event_id2,
                    'ticket_id2' => $ticket_id2,
                    'discount_type' => $discount_type, 
                    'discount' => $discount,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'created_at' => $created_at,
                    'status'=>0,
                    'created_by'=>Auth::user()->id
                 ]);
          $user_id=Auth::user()->id;
          $user=DB::table('users')->where('id','=',$user_id)->first();
          $userorg=DB::table('users')->where('id','=',$org_id)->first();
          $event1=DB::table('eventbookings')->where('id','=',$event_id)->first();
          $event2=DB::table('eventbookings')->where('id','=',$event_id2)->first();
          $ticketName=DB::table('tickets')->where('id','=',$ticket_id)->first();
          $ticketName2=DB::table('tickets')->where('id','=',$ticket_id2)->first();

          $data = array('eventOrg'=>$user->first_name,
                        'email'=>$userorg->email,
                        'emailorg'=>$user->email,
                        'phone'=>$user->phone,
                        'event'=>$event2->event_title,
                        'eventColab'=>$event1->event_title,
                        'eventColab_Date'=>$event2->start_date,
                        'discount_offered'=>$discount.' '.$discount_type,
                        'colabticket'=>$ticketName->ticket_type,
                        'colabticket2'=>$ticketName2->ticket_type,
                        'collabID'=>$discounts,
                        'organisationname'=>$user->organisation_name,
                        'subject'=>'Petal Events - Ticket Collaboration Request From - '.$user->organisation_name.' !!!'
        );
           Mail::send(['text'=>'ticketcolabRequest'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.replyinfo@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
       
        return redirect()->route('ticketcollabration.index')

                        ->with('success','Ticket collaboration created successfully');
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        try {

        $Ticketcollabration = Ticketcollabration::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('Admin.Ticketcollabration.show',compact('Ticketcollabration')); 

         } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {   
                $eventbooking = Ticketcollabration::find($id);
                //$eventbooking = Eventbookings::findOrFail($id);
                $Ticketcollabration = Ticketcollabration::where('id', $id)
                                ->first();

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                $photographers = DB::table('users')
                            ->where('user_type', '=', 4)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                return view('Admin.Ticketcollabration.edit',compact('eventbooking', 'categories', 'photographers'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            
        if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
            
        $ticket_type = trim($request->ticket_type);
        $prices = trim($request->prices);
        $stock = trim($request->stock);
        
        $updated_by = Auth::id();
        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('Ticketcollabration')
                ->where('id', $id)
                ->update(['start_date' => $start_date, 'end_date' => $end_date, 'ticket_type' => $ticket_type, 'prices' => $prices, 'stock' => $stock, 'max_ticket' => $max_ticket, 'updated_by' => $updated_by, 'updated_at' => $updated_at]);

        return redirect()->route('eventbookings.index')

                        ->with('success','Ticket data updated successfully');

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyticketcoll($id)
    {
         
                $updated_by = Auth::id();
                $updated_at = date('Y-m-d H:i:s');
                $upQry = DB::table('ticketcollabration')
                        ->where('id', $id)
                        ->update(['deleted' => '1']);

                    return redirect()->route('ticketcollabration.index')
                           ->with('success','Ticket collaboration deleted successfully');    

                
        
    }

    public function selfeventsedit(Request $request, $id)
    { 
      if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
        if(trim($request->ticket_id)!='') {
            $ticket_id = trim($request->ticket_id);
          } else {
            $ticket_id = '';
        }
        if(trim($request->event_id2)!='') {
            $event_id2 = trim($request->event_id2);
          } else {
            $event_id2= '';
        }
         if(trim($request->coll_type)!='') {
            $coll_type = trim($request->coll_type);
          } else {
            $coll_type= '';
        }
         if(trim($request->ticket_id2)!='') {
            $ticket_id2 = trim($request->ticket_id2);
          } else {
            $ticket_id2 = '';
        }
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
         
          $updated_at = date('Y-m-d H:i:s');

          $upQry = DB::table('ticketcollabration')
                ->where('id', $id)
                ->update(['name' => $name, 'event_id' => $event_id, 'coll_type'=>$coll_type, 'ticket_id' => $ticket_id, 'event_id2' => $event_id2, 'ticket_id2' => $ticket_id2, 'discount' => $discount, 'start_date' => $start_date, 'end_date' => $end_date, 'updated_at' => $updated_at]);
       
        return redirect()->route('ticketcollabration.index')

                        ->with('success','Ticket collaboration updated successfully');
    }
 public function earlybirdedit(Request $request, $id)
    { 
      if(trim($request->name)!='') {
            $name = trim($request->name);
          } else {
            $name = '';
        }
        if(trim($request->event_id)!='') {
            $event_id = trim($request->event_id);
          } else {
            $event_id = '';
        }
        if(trim($request->ticket_id)!='') {
            $ticket_id = trim($request->ticket_id);
          } else {
            $ticket_id = '';
        }
        if(trim($request->event_id2)!='') {
            $event_id2 = trim($request->event_id2);
          } else {
            $event_id2= '';
        }
         if(trim($request->coll_type)!='') {
            $coll_type = trim($request->coll_type);
          } else {
            $coll_type= '';
        }
         if(trim($request->ticket_id2)!='') {
            $ticket_id2 = trim($request->ticket_id2);
          } else {
            $ticket_id2 = '';
        }
         if(trim($request->discount_type)!='') {
            $discount_type = trim($request->discount_type);
          } else {
            $discount_type = '';
        }
          if(trim($request->discount)!='') {
            $discount = trim($request->discount);
          } else {
            $discount = '';
        }
          if(trim($request->start_date)!='') {
            $start_date_org = trim($request->start_date);
            $start_date_Arr = explode('-',$start_date_org);
            $start_date = $start_date_Arr[2].'-'.$start_date_Arr[1].'-'.$start_date_Arr[0];
        } else {
            $start_date = '';
        }

        if(trim($request->end_date)!='') {
            $end_date_org = trim($request->end_date);
            $end_date_Arr = explode('-',$end_date_org);
            $end_date = $end_date_Arr[2].'-'.$end_date_Arr[1].'-'.$end_date_Arr[0];
        } else {
            $end_date = '';
        }
        if(trim($request->org_id)!='') {
            $org_id = trim($request->org_id);
          } else {
            $org_id = '';
        }
         
          $updated_at = date('Y-m-d H:i:s');

          $upQry = DB::table('ticketcollabration')
                ->where('id', $id)
                ->update(['name' => $name, 'event_id' => $event_id, 'org_id' => $org_id, 'coll_type'=>$coll_type, 'ticket_id' => $ticket_id, 'event_id2' => $event_id2, 'ticket_id2' => $ticket_id2, 'discount' => $discount, 'start_date' => $start_date, 'end_date' => $end_date, 'updated_at' => $updated_at]);
       
        return redirect()->route('ticketcollabration.index')

                        ->with('success','Ticket collaboration updated successfully');
    }
}


