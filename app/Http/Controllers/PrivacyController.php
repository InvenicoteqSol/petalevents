<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use Session;
use DB;
use Hash;
use App\Classes\ErrorsClass;

class PrivacyController extends Controller
{    

    public function __construct()
    {
    }

     public function index(Request $request)
    {
        $showsearch="true";
        return view('pages.privacy-policy',compact('showsearch'));

    }
}

