<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Classbooking;
use App\Contactlists;
use Session;
use DB;
use Hash;
use Image;
use Mail;


class CustomersregistrationController extends Controller
{    
    public function __construct()
    {
    }

     public function index(Request $request)
    { 
         $showsearch="true";
        return view("pages.customersregistration",compact("showsearch"));

    }

    public function insert(Request $request)
    {   
        $showsearch="true";
        $userExists= DB::table('users')->where('email','=',$request->email)->get();
        if(count($userExists)>0)
        {
            return redirect()->route('customersregistration',compact('showsearch'))

                        ->with('error','User with same credentials already exists. Please Login.');

        }
        else{
        $user = new User;
        $user->first_name = ucfirst(trim($request->first_name));
        $user->last_name =   trim($request->last_name);
        $user->email = $request->email; 
        $user->address_line_1 = $request->address_line_1; 
        $user->address_line_2 = $request->address_line_2; 
        $user->postcode = $request->postcode; 
        $user->country = $request->country;
        $user->phone = $request->phone;
        $user->password = bcrypt($request->password);
        $user->hdpwd = $request->password;
        $user->gender = $request->gender;
        $user->user_type = $request->user_type;
        $user->created_by = Auth::id();
        $user->save();

        $data = array('first_name'=>$user->first_name,
                     'email'=>$user->email,
                     'phone'=>$user->phone,
                     'subject'=>'Petal Events- Welcome Aboard- '.$user->first_name.' !!!'
        );
      
      Mail::send(['text'=>'mail'], $data, function($message) use($data) {
         $message->to($data['email'], 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.reply@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
       
      return redirect()->route('customersregistration',compact('showsearch'))

                        ->with('success','Registration Successful.');
                    
         }

}


}