<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Classbooking;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class BlogController extends Controller
{    

    public function __construct()
    {
    }

      public function index(Request $request)
    {
        try {

            $query = DB::table('posts');
                     $query->where('deleted', 0);
                     $query->where('status', 1);
                     $query->orderBy('id','DESC');
                     $query->limit(6);
            $posts1 = $query->get(0);
            print_r($posts1); 
            /*$posts2 = $query->get(1);
            print_r($posts2);
            $posts3 = $query->get(2);
            print_r($posts3);
            $posts4 = $query->get(3);
            print_r($posts4);*/


            return view('blog.index',compact('posts'));

            } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
   
    }


    public function create()
    {
 
    }


    public function store(Request $request)
    {   

    }

    public function show($id)
    {   
         $post = DB::table('posts')
                            ->where('id','=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first(); 
   
        return view('blog.show',compact('post'));
    }

    public function edit($id)
    {
            
    }

    public function update(Request $request)
    {

    }


    public function destroy($id)
    {
        
    }

}

