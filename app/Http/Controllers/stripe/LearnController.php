<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Classbooking;
use App\Photoclasses;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;

class LearnController extends Controller
{    

    public function __construct()
    {
    }

      public function index(Request $request)
    {
        try {

            $query = DB::table('photoclasses');
                   $query->where('deleted', 0);
                   $query->where('status', 1);
                   $query->orderBy('id','DESC');
            $classes = $query->paginate(6);
            return view('learn.index',compact('classes'));

            } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       }
   
    }


    public function create()
    {
 
    }


    public function store(Request $request)
    {   

    }

    public function show($id)
    {   
         $class = DB::table('photoclasses')
                            ->where('id','=', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first(); 

        $user = DB::table('users')
                            ->where('id','=', $class->user_id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
       
        

        $pcgalleryimages = DB::table('pcgallery')
                             ->where('pc_id', $id)
                             ->where('status', '=', 1)
                             ->where('deleted', '=', 0)
                             ->orderBy('id','DESC')
                             ->first();                        
        
        return view('learn.show',compact('class','user','photoclass','pcgalleryimages'));
    }

    public function edit($id)
    {
            
    }

    public function update(Request $request)
    {

    }


    public function destroy($id)
    {
        
    }
}

