<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Eventbookings;
use Session;
use DB;
use Hash;
use Image;
use App\Eventgallery;
use App\Classes\ErrorsClass;
use App\Classes\CalendarClass;
use Mail;

class UserbookingController extends Controller
{    

    use RegistersUsers;

    protected $redirectTo = '/customerdashboard';

    public function __construct()
    {
        $this->middleware('auth');
    }

      public function index(Request $request)
    {

        try { 

                //$eventid = $request->eventid;

                if($request->eventid){
                    $eventdata = DB::table('eventbookings')->where('id', '=', $request->eventid)->first();
                } else {
                    $json = '{ "id": "", "start_date": "", "occassions": "", "shooting_locations": "", "take_all_photos": "", "booking_amount": "", "additional_hours": "", "additional_notes":""}';
                    $eventdata = json_decode($json);
                }

                if($eventdata->start_end_date!=''){
                    $eventdates = explode(',', $eventdata->start_end_date);
                } else {
                    $eventdates = array();
                }


                $calendar = new CalendarClass();
                $calendardata = $calendar->show();

                $categories = DB::table('categories')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                             ->get(); 

                return view('booking.index',compact('categories','calendardata', 'eventdata'));

        } catch (\Illuminate\Database\QueryException $e) {

            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);

        } catch (\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
       } 
         
    }

    public function bookingdata(Request $request){

            $created_at = date('Y-m-d H:i:s');
            $additional_hours = $request->additional_hours;

            if($request->event_dates!=''){
                   $start_date = date('Y-m-d H:i:s', strtotime($request->event_dates));

                   $eventhour = $additional_hours + 2;

                   $end_date = date('Y-m-d H:i:s',strtotime('+'.$eventhour.' hours',strtotime($start_date)));
            } else {
                $start_date = '';
                $end_date ='';
            }


        $eventamount = '3600';

        if($request->occassions!=''){
            $occassions = $request->occassions;
        } else {
            $occassions = '';
        }

        $shooting_locations = $request->shooting_locations;

        if(count($request->take_all_photos) > 0){
        $allphotos = implode(',', $request->take_all_photos);
        } else {
             $allphotos = '';
        }


        $addontotal = $request->addontotal;


        if($additional_hours!='0'){
            $eventamount = $eventamount + ($additional_hours * 1800);
        }

        $eventamount = $eventamount + $addontotal;        

        $event_id = $request->eventid;
        $additional_notes =  $request->additional_notes;

        if($event_id==''){
        $event_id = DB::table('eventbookings')->insertGetId([
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'occassions' => $occassions, 
                    'shooting_locations' => $shooting_locations, 
                    'additional_hours' => $additional_hours,
                    'take_all_photos' => $allphotos,
                    'booking_amount' => $eventamount,
                    'additional_notes' => $additional_notes,
                    'created_at' => $created_at
                 ]);
    } else {

        $updated_at = date('Y-m-d H:i:s');

        $upQry = DB::table('eventbookings')
                ->where('id', $event_id)
                ->update(['start_date' => $start_date, 'end_date' => $end_date, 'occassions' => $occassions, 'shooting_locations' => $shooting_locations, 'additional_hours' => $additional_hours, 'take_all_photos' => $allphotos, 'booking_amount' => $eventamount, 'additional_notes' => $additional_notes, 'updated_at' => $updated_at]);
        }

        //return view('booking.payment');

        return redirect()->action(
    'BookingController@payment', ['id' => $event_id]
);
    }

    public function payment(Request $request){
        $eventid = $request->id;
        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();

        return view('booking.payment', compact('eventid', 'BusinessInfo'));
    }

    function stripepayment(Request $request){

        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
        $business_name = $BusinessInfo->business_name;
        $business_email = $BusinessInfo->business_email;


         if(!empty($request->stripeToken)){

    $token  = $request->stripeToken;
    $amount = $request->amount;
    $email = $request->stripeEmail;
    $eventid = $request->eventid;
    $payment_date = date('Y-m-d');
    $userid = Auth::user()->id;
    $created_at = date('Y-m-d H:i:s');
    
    //include Stripe PHP library
    require_once('stripe/init.php');

    if($BusinessInfo->stripe_mode=='Live'){
            $publish_key =  $BusinessInfo->live_publish_key;
            $secret_key =  $BusinessInfo->live_secret_key;
      } else {
            $publish_key =  $BusinessInfo->test_publish_key;
            $secret_key =  $BusinessInfo->test_secret_key;
      }
    
    //set api key
    $stripe = array(
      "secret_key"      => $secret_key,
      "publishable_key" => $publish_key
    );
    
    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    //add customer to stripe
    $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
    ));
    
    //item information
    $itemName = "Mrs.Portrait Event Payment";
    $currency = "HKD";
    $orderID = "Mrsportrait_".$eventid;
    
    //charge a credit or a debit card
    $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $amount.'00',
        'currency' => $currency,
        'description' => $itemName,
        'metadata' => array(
        'order_id' => $orderID
        )
    ));
    
    //retrieve charge details
    $chargeJson = $charge->jsonSerialize();

    //check whether the charge is successful
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
        //order details 
        //$amount = $chargeJson['amount'];
        $balance_transaction = $chargeJson['balance_transaction'];
        $currency = $chargeJson['currency'];
        $status = $chargeJson['status'];
        $date = date("Y-m-d H:i:s");

        $paymentid = DB::table('payments')->insertGetId(['user_id'=> $userid, 'event_id'=> $eventid, 'payment_type'=> 'eventbooking', 'payment_method'=> 'Stripe','amount' =>$amount, 'transaction_id'=>$customer->id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at, 'created_by'=> $userid]);

        $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['user_id'=> $userid, 'payment_status' => '1', 'booking_status' => '1', 'updated_at' => $date, 'created_by'=> $userid, 'updated_by'=> $userid]);

        //if order inserted successfully
                if($paymentid && $status == 'succeeded'){

            return redirect('customerdashboard')->with('success','Payment Transaction done successfully');
    
        }else{
            return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Transaction has been failed');
          //  $statusMsg = "Transaction has been failed";
        }
    }else{
       return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Transaction has been failed');
    }
}else{

    return redirect()->action(
    'BookingController@payment', ['id' => $eventid]
)->with('error','Form submission error.......');

}

    }


    function paypalpayment(Request $request){
        
        $amount = $request->amt;
        $eventid = $request->item_number;
        $transaction_id = $request->tx;
        $date = date("Y-m-d H:i:s");
        $userid = Auth::user()->id;
        $payment_date = date('Y-m-d');

        $paymentid = DB::table('payments')->insertGetId(['user_id'=> $userid, 'event_id'=> $eventid, 'payment_type'=> 'eventbooking', 'payment_method'=> 'Paypal','amount' =>$amount, 'transaction_id'=>$transaction_id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $date, 'created_by'=> $userid]);

        $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['user_id'=> $userid, 'payment_status' => '1', 'booking_status' => '1', 'updated_at' => $date, 'created_by'=> $userid, 'updated_by'=> $userid]);

        if($paymentid ){

            return redirect('customerdashboard')->with('success','Payment Transaction done successfully');
    
        } else {

            return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Transaction has been failed');

        }
    }


     function classstripepayment(Request $request){

        $BusinessInfo = DB::table('business_settings')->where('status', '1')->first();
        $business_name = $BusinessInfo->business_name;
        $business_email = $BusinessInfo->business_email;


         if(!empty($request->stripeToken)){

     $token  = $request->stripeToken;
     $amount = $request->amount;
     $email = $request->stripeEmail;
     $classid = $request->classid;
     $tutor_id = $request->tutor_id;
     $course = $request->course;
     $userid = Auth::user()->id;
     $payment_date = date('Y-m-d');
     $created_at = date('Y-m-d H:i:s');

    
    //include Stripe PHP library
    require_once('stripe/init.php');

    if($BusinessInfo->stripe_mode=='Live'){
            $publish_key =  $BusinessInfo->live_publish_key;
            $secret_key =  $BusinessInfo->live_secret_key;
      } else {
            $publish_key =  $BusinessInfo->test_publish_key;
            $secret_key =  $BusinessInfo->test_secret_key;
      }
    
    //set api key
    $stripe = array(
      "secret_key"      => $secret_key,
      "publishable_key" => $publish_key
    );
    
    \Stripe\Stripe::setApiKey($stripe['secret_key']);
    
    //add customer to stripe
    $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
    ));
    
    //item information
    $itemName = "Mrs.Portrait Class Booking Payment";
    $currency = "HKD";
    $orderID = "Mrsportrait_".$classid;
    
    //charge a credit or a debit card
    $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $amount.'00',
        'currency' => $currency,
        'description' => $itemName,
        'metadata' => array(
        'order_id' => $orderID
        )
    ));
    
    //retrieve charge details
    $chargeJson = $charge->jsonSerialize();

    //check whether the charge is successful
    if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1){
        //order details 
        //$amount = $chargeJson['amount'];
        //$balance_transaction = $chargeJson['balance_transaction'];
        //$currency = $chargeJson['currency'];
        $status = $chargeJson['status'];
        //$date = date("Y-m-d H:i:s");

        $classbookings_id = DB::table('classbookings')->insertGetId([
                    'user_id' => $userid,
                    'class_id' => $classid,
                    'tutor_id' => $tutor_id,
                    'course' => $course,
                    'booking_amount' => $amount, 
                    'payment_status' => '1', 
                    'booking_status' => '1',
                    'created_by' => $userid,
                    'created_at' => $created_at
                 ]);

        $paymentid = DB::table('payments')->insertGetId(['user_id'=> $userid, 'class_booking_id'=> $classbookings_id, 'payment_type'=> 'classbooking', 'payment_method'=> 'Stripe','amount' =>$amount, 'transaction_id'=>$customer->id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at, 'created_by'=> $userid]);

        //if order inserted successfully
                if($paymentid && $status == 'succeeded'){

            return redirect('customerdashboard')->with('success','Payment Transaction done successfully');
    
        }else{
            return redirect()->route('learn.show', $classid)
                ->with('error','Transaction has been failed');
        }
    }else{
        return redirect()->route('learn.show', $classid)
            ->with('error','Transaction has been failed');
    }
}else{

    return redirect()->route('learn.show', $classid)
            ->with('error','Form submission error.......');

}

    }

    function classpaypalpayment(Request $request){
        
         $amount = $request->amt;
         $class_id = $request->item_number;
         $course = $request->item_name;
         $tutor_id = $request->cm;
         $transaction_id = $request->tx;
         $userid = Auth::user()->id;
         $created_at = date("Y-m-d H:i:s");
         $payment_date = date('Y-m-d');

         $classbookings_id = DB::table('classbookings')->insertGetId([
                    'user_id' => $userid,
                    'class_id' => $class_id,
                    'tutor_id' => $tutor_id,
                    'course' => $course,
                    'booking_amount' => $amount, 
                    'payment_status' => '1', 
                    'booking_status' => '1',
                    'created_by' => $userid,
                    'created_at' => $created_at
                 ]);

         $paymentid = DB::table('payments')->insertGetId(['user_id'=> $userid, 'class_booking_id'=> $classbookings_id, 'payment_type'=> 'classbooking', 'payment_method'=> 'Paypal','amount' =>$amount, 'transaction_id'=>$transaction_id, 'payment_status'=>'1', 'payment_date' => $payment_date, 'created_at'=> $created_at, 'created_by'=> $userid]);

       

        if($paymentid ){
            return redirect('customerdashboard')->with('success','Payment Transaction done successfully');
    
        } else {

            return redirect()->route('learn.show', $class_id)
                ->with('error','Transaction has been failed');

        }
    }


    public function fileStore(Request $request)
    {  
        $image = $request->file('file');
        $evt_id=$request->eventid;
        $imageName = $image->getClientOriginalName();
        //$image->move(public_path('images'),$imageName);
        $image->move(public_path('uploads/photoclassimages/'), $imageName); 
        $data = DB::table('eventgallery')->insert(
                    ['eventid' => $evt_id,'filename' => $imageName]
                );

        if($data){
            echo 'yes';
        } else {
            echo 'no';
        }
        
        /*$imageUpload = new Eventgallery();
        $imageUpload->filename = $imageName;
        $imageUpload->save();*/
        
    }
    public static function pc_delete(Request $request)
    {    
         $pgalleryid = $request->pgalleryid;
         $delquery = DB::table('eventgallery')->where('id', $pgalleryid)->delete();
        if($delquery) {
           echo '1';
         } else {
            echo '0';
          }

         
    }

/*    public function loadDataAjax(Request $request)
    {   
        $id = $request->id;
        
        $eventgallery = DB::table('eventgallery')
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->where('id', '<', $id)
                            ->orderBy('id','DESC')
                            ->get();
        
        if(count($eventgallery) > 0 )
        {
           
            foreach($eventgallery as $gallery){
                $url = url('public/uploads/photoclassimages/'.$gallery->filename);
                
                    echo  '<div class="col-sm-3 gallaryimg">';
            echo '<a href="javascript:void(0)" class="delete_btn" id="'.$gallery->id.'" title="Delete"><i class="fa fa-trash"></i></a>';
            echo '<a href="'.$url.'" data-toggle="lightbox" data-gallery="example-gallery" data-title="&nbsp;" data-footer="&nbsp;">';
            echo '<img src="'.$url.'" class="session_gallery"></a>';
            echo '</div>';
            }

           echo  '<div id="remove-row">
                            <button id="btn-more" data-id="'.$eventgallery[2]->id.'" class="nounderline btn-block mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" > Load More </button>
                        </div>';
            
             
            
        }
    }*/
    
     public function notification(Request $request ,$id)
    {  
               $pc_id =$request->pc_id;
               $client_id =$request->client_id;
               $message =$request->message;
               $created_at            = date('Y-m-d H:m:i');
               $updated_at            = date('Y-m-d H:m:i');
               $data = DB::table('notification')->insert(
                    ['pc_id' => $pc_id,'client_id' => $client_id,'message' => $message,'created_at' => $created_at,'updated_at' => $updated_at]
                );

     return redirect('photographersession')->with('success','Notification sent successfully');
     }
    
        public function updatenoti(Request $request)
    {    
           $id= $request->noti;
          /*$pc_id              = $request->pc_id;
          $client_id          = $request->client_id;
          $message            = $request->message;*/
          $created_at         = date('Y-m-d H:m:i');
          $updated_at         = date('Y-m-d H:m:i');

         $bnk= DB::table('notification')
             ->where('id', $id)
             ->update(['status' => '0', 'deleted' =>'0', 'created_at' => $created_at, 'updated_at' => $updated_at]);
    }

     public function feedback(Request $request ,$id)
    {  
               $pc_id =$request->pc_id;
               $client_id =$request->client_id;
               $feedback_description =$request->feedback_description;
               $feedback_rating =$request->feedback_rating;
               $created_at            = date('Y-m-d H:m:i');
               $updated_at            = date('Y-m-d H:m:i');
               $data = DB::table('feedback')->insert(
                    ['pc_id' => $pc_id,'client_id' => $client_id,'feedback_description' => $feedback_description,'feedback_rating' => $feedback_rating,'created_at' => $created_at,'updated_at' => $updated_at]
                );

     return redirect('customerevents')->with('success','Notification sent successfully');
     }

          public function updatephotonotify(Request $request)
    {    
           $id= $request->photo;
           $created_at         = date('Y-m-d H:m:i');
           $updated_at         = date('Y-m-d H:m:i');

         $bnk= DB::table('feedback')
             ->where('id', $id)
             ->update(['status' => '0', 'deleted' =>'0', 'created_at' => $created_at, 'updated_at' => $updated_at]);
    }

    function discountpayment(Request $request){
            $couponcode = $request->couponcode;
            $eventid = $request->eventid;
            $today = date('Y-m-d');
            $created_at = date("Y-m-d H:i:s");
            $userid = Auth::user()->id;

            $coupondata = DB::table('coupens')
                ->where('coupen_code', '=', $couponcode)
                ->where('vaild_start_date', '<', $today)
                ->where('vaild_end_date', '>=', $today)
                ->where('status', '=', '1')
                ->where('deleted', '=', '0')->first();

            if($coupondata){
                $reedem_id = DB::table('payments')->insertGetId([
                    'event_id' => $eventid,
                    'user_id' => $userid,
                    'payment_type' => 'eventbooking', 
                    'payment_method' => 'Coupon', 
                    'amount' => $coupondata->coupen_prices,
                    'transaction_id' => $coupondata->coupen_code,
                    'payment_status' => '1',
                    'payment_date' => $today,
                    'created_at' => $created_at
                 ]);

                if($reedem_id){

                     $upQry = DB::table('eventbookings')
                ->where('id', $eventid)
                ->update(['coupon_id' => $coupondata->id]);

                    return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('success','Coupon discount apply successfully');

                } else {
                    return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Coupon discount not apply');
                }
            } else {
                return redirect()->action(
                'BookingController@payment', ['id' => $eventid]
                    )->with('error','Coupon code is not valid');
            }
     }

    public function adminupdatenotiy(Request $request)

    {    
          $id= $request->userdata;
          $userrole= $request->userrole;
          $created_at         = date('Y-m-d H:m:i');
          $updated_at         = date('Y-m-d H:m:i');

         $bnk= DB::table('users')
             ->where('id', $id)
             ->where('user_type', $userrole)
             ->update(['notify_id' => '0','deleted' =>'0', 'created_at' => $created_at, 'updated_at' => $updated_at]);

        if($bnk){
            echo 'suucess';
        } else {echo 'error';}

    }

    function classfeedback(Request $request){
        $rating = $request->rating;
        $class_id = $request->class_id;
        $user_id = $request->user_id;
        $comment = $request->comment;
        $photographer_id = $request->photographer_id;
        $created_at = date("Y-m-d H:i:s");
        $created_by = Auth::id();

        $feedbackid = DB::table('classfeedback')->insertGetId(['user_id'=> $user_id, 'photographer_id'=> $photographer_id, 'class_id'=> $class_id, 'rating'=> $rating,'comments' =>$comment, 'created_at'=>$created_at, 'created_by'=>$created_by]);

        if($feedbackid){
            return redirect()->action(
                'LearnController@show', ['id' => $class_id]
                    )->with('success','Feedback has saved successfully');
        } else {
            return redirect()->action(
                'LearnController@show', ['id' => $class_id]
                    )->with('error','Feedback not saved');

        }
     }


 function requestdata(Request $request){
        $event_id = $request->event_id;
        $org_id = $request->org_id;
        $created_at = date("Y-m-d H:i:s");
        $created_by = Auth::id();
        $rquestdata = DB::table('requestdata')->insertGetId(['event_id'=> $event_id, 'org_id'=> $org_id,'created_at'=>$created_at, 'created_by'=>$created_by]);


        if($rquestdata!='')
        {  
            $email= "info@petalevents.co.uk";
            $data = array('event_id'=>$event_id,
                          'org_id'=>$org_id,
                          'email'=>$email,
                          'subject'=>'Petal Events- Welcome Aboard !!!'
                );
              
              Mail::send(['text'=>'requestdata'], $data, function($message) use($data) {
                 $message->to($data['email'], 'Petal Events')->subject
                    ($data['subject']);
                 $message->from('no.reply@petalevents.co.uk','Petal Events');
                 $message->setContentType('text/html');
              });
                    
           
         }
         return redirect('eventbookings');

         }
}

