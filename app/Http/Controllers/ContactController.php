<?php
namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Auth;
use App\Classbooking;
use App\Contactus;
use Session;
use DB;
use Hash;
use Image;
use App\Classes\ErrorsClass;
use Mail;

class ContactController extends Controller
{    

    public function __construct()
    {
    }

      public function index(Request $request)
    {
        $showsearch="true";
        return view('contact.index',compact('showsearch'));

    }


    public function create()
    {
 
    }
    public function show($id)
    {
        $contact = User::findOrFail($id);
        /*$customer = User::where('id', $id)
                                ->first();*/
        return view('contact.show',compact('contact')); 
    }

    


    public function store(Request $request)
    {  
         $showsearch="true";      
         $name = $request->name;
         $email = $request->email;
         $message = $request->message;
         $contactus = DB::table('contactus')->insertGetId(['name'=> $name,'email' =>$email, 'message'=>$message]);


       $data = array('name'=>$request->name,
                     'customeremail'=>$request->email,
                     'customermessage'=>$request->message,
                     'subject'=>'Petal Events- New Enquiry From - '.$request->name.' !!!'
        );
      
      Mail::send(['text'=>'enquirymail'], $data, function($message) use($data) {
         $message->to('info@petalevents.co.uk', 'Petal Events')->subject
            ($data['subject']);
         $message->from('no.replyinfo@petalevents.co.uk','Petal Events');
         $message->setContentType('text/html');
      });
        
        return redirect()->route('contact.index',compact('showsearch'))

                        ->with('success','Thank you! Your message has been sent successfully.');

    }
      
}

        


