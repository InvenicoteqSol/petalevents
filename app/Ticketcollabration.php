<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketcollabration extends Model
{
 				public $fillable = [
 					'ticket_coll_name','event_id','ticket_id','org_id','event_id2','ticket_id2','discount_type','discount','start_date', 'end_date','status','deleted','created_by','updated_by','created_at','updated_at','coll_type'];
}
