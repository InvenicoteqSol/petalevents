@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-10 dashboard_right">
            @include('layouts.flash-message') 

        <div class="col-sm-5">
        <h3>Classes</h3>
      </div>
      <div class="col-sm-7 searchform">
          <div class="col-sm-8 searchbar">
        <form class="navbar-form navbar-left" name="searchfrm" id="searchfrm" method="GET" action="{{ route('photographerclasses.index') }}">
        <div class="form-group">
          <label>Search</label>
          <input type="text" class="form-control" placeholder="Search here" name="search_input" id="search_input" value="{{ $search_data }}" placeholder="Search here">
        </div>
      </form>
    </div>
  <div class="col-sm-4 selectdiv topbar_selectiv text-right createbtn">
 <a class="editbtn" href="{{ route('photographerclasses.create') }}"> Add New Class </a>
  </div>

       </div>
      <div class="col-sm-12 tablediv spacertop">
<div class="tablewrapper">
<table class="table-bordered table-striped">
        <thead>
          <tr class="headings">
            <th class="column2">Image</th>
            <th class="column3">Class Name</th>
            <th class="column6">Description</th>
            <th class="column3">Price</th>
            <th class="column3">Date</th>
            <th class="column3">Action</th>
          </tr>
        </thead>
        <tbody>
          @if(count($photoclasses)>0)
              @foreach ($photoclasses as $photoclass)
          <tr class="tabledata classtable">
            <td class="column2 user_img">
            @if ($photoclass->pc_image!='' && $photoclass->pc_image!='NULL')
            <img src="{{ url('public') }}/uploads/photoclassimages/{{$photoclass->pc_image}}" class="img-rounded">
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" class="img-rounded">
            @endif
            </td>
            <td class="column3">{{$photoclass->class_title}}</td>
            <td class="column6"><!-- {{$photoclass->pc_description}}  -->
            {!! \Illuminate\Support\Str::words($photoclass->pc_description, 20,'')  !!}</td>
            </td>
            <td class="column3">${{$photoclass->pc_price}}</td>
            <td class="column3">{{ date('d M Y', strtotime($photoclass->created_at)) }}</td>
             <td class="column3 action_btns"> 
                      <a class="tablebtn" href="{{ route('photographerclasses.show',$photoclass->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                     <a class="tablebtn" href="{{ route('photographerclasses.edit',$photoclass->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>
                     {!! Form::open(['method' => 'DELETE','route' => ['photographerclasses.destroy', $photoclass->id],'style'=>'display:inline']) !!}
                     {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']) !!}

                       {!! Form::close() !!}
                    </td>
          </tr>
           @endforeach
        @else
          <tr class="tablesection">
            <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
          </tr>
        @endif                               
        </tbody>
      </table>
          
          
</div>
 @if(count($photoclasses) > 0)
<div class="pagination_section">
<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

  {!! $photoclasses->render() !!}
</nav>

  </div>
  
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="{{ url('/photographerclasses') }}" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" @if($perpage=='10'){{'selected'}}@endif>10</option>
      <option value="20" @if($perpage=='20'){{'selected'}}@endif>20</option>
      <option value="50" @if($perpage=='50'){{'selected'}}@endif>50</option>
      <option value="100" @if($perpage=='100'){{'selected'}}@endif>100</option>
    </select>
    </form>
  </div>

</div>
@endif

 <!--  -->
</div>
            <div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
      </div>

    </div>
<!--booking-sec-end-->    
<!--booking-sec-end-->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
@endsection