<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/terms', function () {
    return view('terms-conditions');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/forgotpassword', 'BookingController@forgotpassword');
Route::get('/booking', 'BookingController@index');
Route::get('/payment/{id}', 'BookingController@payment');
Route::get('/usersignup/{id}', 'BookingController@usersignup');
Route::get('/calendardates', 'BookingController@calendardates');
Route::POST('/bookingdata', 'BookingController@bookingdata');
Route::POST('/storeeventdata', 'BookingController@storeeventdata');
Route::POST('/stripepayment', 'BookingController@stripepayment');
Route::get('/paypalpayment', 'BookingController@paypalpayment');
Route::get('/checkuseremail', 'BookingController@checkuseremail');
Route::get('/checkedituseremail', 'BookingController@checkedituseremail');
Route::get('/community', 'CommunityController@index');
Route::get('/client-album', 'CommunityController@clientalbum');
Route::get('/client-view', 'CommunityController@clientview');
Route::get('/volunteer-session', 'CommunityController@volunteersession');
Route::get('auth/google', 'Auth\LoginController@redirectTOGoogle');
Route::get('googlecallback', 'Auth\LoginController@Googlecallback');
Route::resource('/learn', 'LearnController');
Route::resource('/blog', 'BlogController');
Route::get('classpaypalpayment', 'BookingController@classpaypalpayment');
Route::post('classstripepayment', 'BookingController@classstripepayment');
Route::get('userclasspaypalpayment', 'UserbookingController@classpaypalpayment');
Route::post('userclassstripepayment', 'UserbookingController@classstripepayment');
Route::get('usereventpaypalpayment', 'UserbookingController@paypalpayment');
Route::post('usereventstripepayment', 'UserbookingController@stripepayment');
Route::resource('/customerdashboard', 'Customer\ProfileController');
Route::resource('/customerevents', 'Customer\EventController');
Route::resource('/customerpayments', 'Customer\PaymentController');
Route::resource('/customerclasses', 'Customer\ClassesController');
Route::resource('/customerfeedback','Customer\FeedbackController');
Route::resource('/photographerdashboard', 'Photographer\ProfileController');
Route::resource('/photographersession', 'Photographer\SessionController');
Route::resource('/photographerclasses', 'Photographer\ClassesController');
Route::resource('/photgrapherfeedback','Photographer\FeedbackController');
Route::resource('/photographerpayments', 'Photographer\PaymentController');
Route::resource('/photographergallery', 'Photographer\GalleryController');

Route::get('/photographergallery/{id}/index/delete_image/{pcgalleryid}','Photographer\GalleryController@delete_image');

Route::resource('/admindashboard', 'Admin\DashboardController');
Route::resource('/adminfaq', 'Admin\FaqController');
Route::resource('/adminprofiles', 'Admin\ProfilesController');
Route::resource('/adminbusinesssetting', 'Admin\BusinesssettingController');
Route::resource('/admincategory','Admin\CategoryController');
Route::resource('/adminposts', 'Admin\PostsController');
Route::resource('/admincustomers', 'Admin\CustomersController');
Route::resource('/admincategory','Admin\CategoryController');
Route::get('/admincategory/{id}/edit/delete_image/{catgalleryid}','CategoryController@delete_image');
Route::resource('/adminclasses','Admin\ClassesController');
Route::get('/adminclasses/{id}/edit/delete_image/{pcgalleryid}','CategoryController@delete_image');
Route::resource('/adminphotographers','Admin\PhotographersController');
Route::resource('/adminaddons','Admin\AddonsController');
Route::resource('/admineventbookings','Admin\EventbookingsController');
Route::resource('/adminclassbooking','Admin\ClassbookingController');
Route::resource('/admincoupens','Admin\CoupensController');





