       <?php
    if(Auth::user()->photography_role!='' && Auth::user()->photography_role!='NULL'){
        $photographerroles = explode(',', Auth::user()->photography_role); 
    } else { 
        $photographerroles = array();
    }

  ?>
     @if(Auth::user()->user_type=='1')
        <div class="sidebar">
          <div class="profileimg">
            @if (Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL')
            <img src="{{ url('public') }}/images/uploads/profileimages/profileimg.png" class="img-circle">
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" class="img-circle">
            @endif
            <p>{{ Auth::user()->name}} <br/><span>Admin</span></p>
          </div>
          <h6>Menus</h6>
          <ul class="list-sidenav">
            <li><a href="{{ url('/adminposts') }}"><i class="fa fa-user"></i><span>Blog</span></a></li>
            <li><a href="{{ url('/adminfaq') }}"><i class="fa fa-user"></i><span>Faq</span></a></li>
            <li><a href="{{ url('/adminbusinesssetting') }}"><i class="fa fa-user"></i><span>Business Setting</span>
            <li><a href="{{ url('/adminprofiles') }}"><i class="fa fa-user"></i><span>Admin Profile</span></a></li>
            <li><a href="{{ url('/admincategory') }}"><i class="fa fa-user"></i><span>Occassions</span></a></li>
            <li><a href="{{ url('/adminclasses') }}"><i class="fa fa-user"></i><span>Classes</span></a></li>
            <li><a href="{{ url('/admincustomers') }}"><i class="fa fa-user"></i><span>Customers</span></a></li>
             <li><a href="{{ url('/adminphotographers') }}"><i class="fa fa-user"></i><span>Photographers</span></a>
            <li><a href="{{ url('/adminaddons') }}"><i class="fa fa-user"></i><span>Addons</span></a>
              <li><a href="{{ url('/admineventbookings') }}"><i class="fa fa-user"></i><span>Booking</span></a>
             </li>

            
            <li><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <span>Logout</span> </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
          </ul>
        </div>
      @endif


      @if(Auth::user()->user_type=='3')
        <div class="sidebar">
          <div class="profileimg">
            @if (Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL')
            <img src="{{ url('public') }}/uploads/profileimages/{{Auth::user()->profile_picture}}" class="img-circle">
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" class="img-circle">
            @endif
            <p>{{ Auth::user()->name}} <br/><span>Customer</span></p>
          </div>
          <h6>Menus</h6>
          <ul class="list-sidenav">
            <li><a href="{{ url('/customerdashboard') }}"><i class="fa fa-user"></i><span>Profile</span></a></li>
            <li><a href="{{ url('/customerevents') }}"><i class="fa fa-calendar"></i> <span>Events</span><span class="sessions_counter">{{count($eventbookings)}}</span></a></li>
            <li><a href="{{ url('/customerfeedback') }}"><i class="fa fa-comment"> </i> <span>Feedback</span><span class="feedback_counter">4</span></a></li>
            <li><a href="{{ url('/customerpayments') }}"><i class="fa fa-money"> </i> <span>Payment</span></a></li>
            <li><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <span>Logout</span> </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
          </ul>
        </div>
      @endif

      @if(Auth::user()->user_type=='4')
        <div class="sidebar">
          <div class="profileimg">
            @if (Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL')
            <img src="{{ url('public') }}/uploads/profileimages/{{Auth::user()->profile_picture}}" class="img-circle">
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" class="img-circle">
            @endif
            <p>{{ Auth::user()->name}} <br/><span>Photographer</span></p>
          </div>
          <h6>Menus</h6>
          <ul class="list-sidenav">
            <li><a href="{{ url('/photographerdashboard') }}"><i class="fa fa-user"></i><span>Profile</span></a></li>
            <li><a href="{{ url('/photographersession') }}"><i class="fa fa-calendar"></i> <span>Session</span><span class="sessions_counter">{{count($eventbookings)}}</span></a></li>
            @if(in_array('2', $photographerroles))
            <li><a href="{{ url('/photographerclasses') }}"><i class="fa fa-users"> </i><span>Classes</span><span class="classes_counter">3</span></a></li>
            @endif
            <li><a href="{{ url('/photgrapherfeedback') }}"><i class="fa fa-comment"> </i> <span>Feedback</span><span class="feedback_counter">4</span></a></li>
            <li><a href="{{ url('/photographerpayments') }}"><i class="fa fa-money"> </i> <span>Payment</span></a></li>
            <li><a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <span>Logout</span> </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
          </ul>
        </div>
      @endif