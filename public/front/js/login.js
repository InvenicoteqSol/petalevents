 $(document).ready( function() {

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $("#loginfrm").validate({
        rules: {
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 16
                }
            },
        messages: {
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                password: {
                  required: "This is required field.", 
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 16 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
     $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: false
                  },
                  1000: {
                    items: 5,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              });

 });
