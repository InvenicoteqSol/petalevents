@extends('layouts.userdefault')
@section('content')

      <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-12 dashboard_right">
        @include('layouts.flash-message')
        <div class="col-sm-12 nopadding">
  <div class="row">
        
        <div class="col-sm-12 selectdiv topbar_selectiv text-right createbtn">
  
        <h3><a class="btn btn-primary" href="{{ url('exportorders') }}" class="btn_expt">EXPORT CSV</a></h3>
    </div>

  </div>
      </div>

           <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Event Orders</h4>
                  <p class="card-category"></p>
                </div>
      <div class="col-sm-12 tablediv spacertop">
        @if(Auth::user()->user_type=='1')
<div class="card-body table-responsive">
<table class="table table-hover ">
        	<thead class=" text-primary">

                    <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Name</th>
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if(count($orders)>0)
                     @foreach ($orders as $order)
                    <tr class="tabledata odd">
                      <td class="column3">{{date('Y-M-d',strtotime($order->created_at))}}</td>
                      <td class="column3">{{ $order->order_number }}</td>
                      <td class="column3">{{ $order->first_name }}</td>
                      <td class="column3">{{ $order->amount }}</td>
                      @if ($order->order_status_id == '0') <td class="column3">{{'Completed'}}</td>@elseif ($order->order_status_id == '1') <td class="column3">{{' Awaiting Payment '}}</td>@else  @endif 
                      
                         <td class="action_btns column3"> 
                          
<div class="modal" id="linkmodal_{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="linkmodal_{{$order->id}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : {{ $order->order_number }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> {{ $order->first_name }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> {{ $order->last_name }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> {{ $order->amount }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  {{ $order->order_number }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br> {{date('Y-M-d',strtotime($order->created_at))}}
                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr><th>
                                Ticket
                            </th>
                            
                            <th>
                                Price
                            </th>
                            
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <tr>
                                    <td>
                                   
                                    </td>
                                    
                                    <td>
                                     {{ $order->amount }}
                                    </td>
                                     <td>
                                     <?php  echo $total=$order->amount+$order->booking_fee;
                                     ?>
                                    </td>
                                </tr>
                                 
                            </tbody>
                        </table>

                    </div>
                </div>

              
            </div> <!-- /end modal body-->

            <div class="modal-footer">
               <button class="btn modal-close btn-danger" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
</div>
</div>
</div>
                  <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="dropdown-item" href="{{ url('/cancelorder') }}/{{$order->id}}">Cancel</a>
                     <a class="dropdown-item" data-toggle="modal" data-target="#linkmodal_{{$order->id}}" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Details</a>  
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>
  </div>
 @else (Auth::user()->user_type=='3')
 <?php  $id = Auth::user()->id; 
   $orderscustomer = DB::table('users')
                          ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.order_date','orders.booking_fee','tickets.ticket_type','tickets.prices','orders.id','orders.order_status_id')
                           ->join('orders', 'orders.user_id', '=', 'users.id')
                          // ->join('tickets','orders.ticket_id','=','tickets.id')
                           ->where('users.id','=',$id)
                           ->get();
                        
                           ?>

  <div class="card-body table-responsive">
<table class="table table-hover ">
          <thead class=" text-primary">

                    <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Name</th>
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @if(count($orderscustomer)>0)
                     @foreach ($orderscustomer as $order)
                    <tr class="tabledata odd">
                      <td class="column3">{{date('Y-M-d',strtotime($order->order_date))}}</td>
                      <td class="column3">{{ $order->order_number }}</td>
                      <td class="column3">{{ $order->first_name }}</td>
                      <td class="column3">{{ $order->amount }}</td>
                      @if ($order->order_status_id == '0') <td class="column3">{{'Completed'}}</td>@elseif ($order->order_status_id == '1') <td class="column3">{{' Awaiting Payment '}}</td>@else  @endif 
                      
                         <td class="action_btns column3"> 
                          
<div class="modal" id="linkmodal_{{$order->id}}" tabindex="-1" role="dialog" aria-labelledby="linkmodal_{{$order->id}}">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : {{ $order->order_number }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> {{ $order->first_name }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> {{ $order->last_name }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> {{ $order->amount }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  {{ $order->order_number }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br>  {{ $order->order_date }}
                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr><th>
                                Ticket
                            </th>
                            <th>
                                Quantity
                            </th>
                            <th>
                                Price
                            </th>
                            
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <tr>
                                    <td>
                                    {{ $order->ticket_type }}
                                    </td>
                                    <td>
                                     

                                    </td>
                                    <td>
                                     {{ $order->amount }}
                                    </td>
                                    
                                  
                                    <td>
                                     <?php  echo $total=$order->amount+$order->booking_fee;
                                     ?>
                                    </td>
                                </tr>
                                 
                            </tbody>
                        </table>

                    </div>
                </div>

              
            </div> <!-- /end modal body-->

            <div class="modal-footer">
               <button class="btn modal-close btn-danger" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
</div>
</div>
</div>
                  <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="dropdown-item" href="{{ url('/cancelorder') }}/{{$order->id}}">Cancel</a>
                     <a class="dropdown-item" data-toggle="modal" data-target="#linkmodal_{{$order->id}}" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Details</a>  
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>
  </div>
 @endif
    </div>
    </div>
  </div>
  <style type="text/css">
    .tablebtn {
    background: #666;
    color: #fff !important;
    padding: 5px 10px !important;
}
.modal-content {
    margin-left: 25%;
    margin-right: 25%;
    margin-right: 25%;
}

.modal-header.text-center {
    margin-top: 7%;
}
.btn.btn-primary.btn-sm.markPaymentReceived {
    color: #fff;
    padding: 5px 10px;
}
</style>

@endsection
