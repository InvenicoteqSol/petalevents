<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventbookings extends Model
{
 				public $fillable = [
 					'user_id', 'start_date', 'end_date', 'start_end_date', 'occassions', 'additional_hours','description', 'shooting_locations', 'take_all_photos', 'additional_notes', 'booking_amount', 'photographer_id', 'payment_status', 'booking_status', 'status', 'deleted','event_title','supporting_art','event_info','event_image','type_of_event',' 	age_limit','venue_name','venue_address','details','telephone','website','event_date','waive_booking_fee','payment_status','is_featured','payment_transaction',
 				];
}

