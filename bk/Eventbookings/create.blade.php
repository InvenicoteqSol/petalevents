@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->

<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
        
      <div class="content">
      <div class="container-fluid">
      <div class="row">
      <div class="col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-header card-header-primary">
          <h4 class="com_busin_sec card-title">New Event</h4>
          <p class="card-category">Create your Booking</p>
        </div>
   
    
            {!! Form::open(array('route' => 'eventbookings.store', 'method'=>'POST', 'id' => 'edtbookingfrm',  'class' => 'edtbookingfrm formarea', 'files' => true)) !!}
   <div class="card-body">
        <form>
  <div class="row">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('event_title') ? ' has-error' : '' }}">
            <label for="event_title" class="control-label bmd-label-floating"> Event Title *</label>
            <input type="text" class="form-control event_title" id="event_title" name="event_title" value="{{ @old(event_title) }}" required>
            @if ($errors->has('event_title'))
              <span class="help-block">
                <strong>{{ $errors->first('event_title') }}</strong>
              </span>
            @endif
        </div>
    </div>
    
  </div>
<div class="row topcls">
    <div class="col-md-12">
    <div class="form-group{{ $errors->has('event_info') ? ' has-error' : '' }}">
    <label for="event_info" class="control-label bmd-label-floating">Event Info *</label>
         <textarea id="event_info" name="event_info" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ @old(event_info) }}</textarea>

        @if ($errors->has('event_info'))
            <span class="help-block">
                <strong>{{ $errors->first('event_info') }}</strong>
            </span>
        @endif
    </div>
    </div>
  </div>
<div class="row topcls">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('type_of_event') ? ' has-error' : '' }}">
       
                    <select class ="form-control" id="type_of_event" name="type_of_event">
                    <option>Type of event</option>
                    <option> Arts & Culture</option>
                    <option> Bar & Pub</option>
                    <option> Burlesque </option>
                    <option> Cabaret </option>
                    <option> Celebrity</option>
                    <option> Charity </option>
                    <option> Children </option>
                    <option> Cinema </option>
                    <option> Clubbing </option>
                    <option> Comedy</option>
                    <option> Concert </option>
                    <option> Conference </option>
                    <option>Cosplay </option>
                    <option> Exhibition</option>
                    <option> Festival</option>
                    <option> Gentlemen's Club</option>
                    <option> Hotel</option>
                    <option> Karaoke</option>
                    <option> Latin Dance</option>
                    <option> Live Music</option>
                    <option> Magic</option>
                    <option> Museum</option>
                    <option> Pantomime</option>
                    <option> Poetry</option>
                    <option> Restaurant</option>
                    <option> Speaker</option>
                    <option> Sport</option>
                    <option> Theatre</option>


                    </select>
                      @if ($errors->has('type_of_event'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('type_of_event') }}</strong>
                          </span>
                      @endif  
    </div>
  </div>
        <div class="col-md-6">
        <div class="form-group{{ $errors->has('age_limit') ? ' has-error' : '' }}">
       
                    <select class ="form-control" id="age_limit" name="age_limit">
                       <option> Age Limit </option>
                       <option>  All ages </option>
                       <option> Under 18 </option>
                       <option> 18 and over  </option>                              
                             
                    </select>
                      @if ($errors->has('age_limit'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('age_limit') }}</strong>
                          </span>
                      @endif     
    </div>
  </div>
</div>
  <div class="row">
      <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <label for="start_date" class="control-label bmd-label-floating"> Start Date*</label>
            <div class="input-group">
            <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
            <input type="text" class="form-control time" id="start_time" name="start_time" value="{{ @old(start_time) }}" required>
          </div>
             
            @if ($errors->has('start_date'))
              <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
              </span>
            @endif
        </div>

    </div>

<div class="col-md-6">
   <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <label for="end_date" class="control-label bmd-label-floating"> End Date *</label>
            <input type="text" class="form-control end_date" id="end_date" name="end_date" value="{{ @old(end_date) }}" required>
            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
        </div>
     </div>
  </div>
  <div class="row">
 <div class="col-md-12 line_div">
            <h3 class="com_busin_sec">Venue Details </h3>
        </div>
      </div>
<div class="row topcls">
   <div class="col-md-4">
     <div class="form-group{{ $errors->has('venue_name') ? ' has-error' : '' }}">
    <label for="venue_name" class="control-label bmd-label-floating">Venue Name*</label>
        <input type="text" class="form-control" id="venue_name" name="venue_name" value="{{ @old(venue_name) }}" required>
        @if ($errors->has('venue_name'))
            <span class="help-block">
                <strong>{{ $errors->first('venue_name') }}</strong>
            </span>
        @endif
    </div>
</div> 
    <div class="col-md-4">
          <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
    <label for="telephone" class="control-label bmd-label-floating">Telephone*</label>
        <input type="number" class="form-control" id="telephone" name="telephone" value="{{ @old(telephone) }}" maxlength="10" required>
        @if ($errors->has('telephone'))
            <span class="help-block">
                <strong>{{ $errors->first('telephone') }}</strong>
            </span>
        @endif
    </div>
  </div> 
                          <div class="col-md-4">
                   <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                    <label for="website" class="control-label bmd-label-floating"> Website </label>
                    <input type="text" class="form-control" id="website" name="website" value="{{ @old(website) }}" >
                              @if ($errors->has('website'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('website') }}</strong>
                                </span>
                              @endif
                          </div>
                    </div>
              </div>
<div class="row topcls">              
    <div class="col-md-6 edit_radiobtn">
          <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
    <label for="details" class="control-label bmd-label-floating">Venue Details </label>
         <textarea id="details" name="details" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ @old(details) }}</textarea>

        @if ($errors->has('details'))
            <span class="help-block">
                <strong>{{ $errors->first('details') }}</strong>
            </span>
        @endif
    </div>  
 </div>
<div class="col-md-6 edit_radiobtn">
      <div class="form-group{{ $errors->has('venue_address') ? ' has-error' : '' }}">
    <label for="venue_address" class="control-label bmd-label-floating">Venue Address *</label>
         <textarea id="venue_address" name="venue_address" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ @old(venue_address) }}</textarea>

        @if ($errors->has('venue_address'))
            <span class="help-block">
                <strong>{{ $errors->first('venue_address') }}</strong>
            </span>
        @endif
    </div>
  </div>
 </div>
                             <div class="row">
                              <div class="col-md-12 col-xs-12">  
                              <div class="col-featured-img">   
                         
                              <label for="event_image" >Event Image</label>
                                <div class="col-md-9">

                                 <div class="col-md-6" id="ftrd_browse_img" style="padding-top: 10px;">
                                 <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" style="width: 100px; height: 100px;" class="img-responsive">     
                                 </div>

                                <div class="col-md-6">
                                <input id="event_image" type="file" name="event_image" accept="image/*">
                              </div>

                            </div>
                            <div class="col-md-3">

      <div class="form-group">
        <label for="end_date" class="control-label"> Display In Featured Events</label>
      <input type="checkbox" id="makeFeatured" data-toggle="toggle">
      <input type="hidden" id="stripe_tokens">

    </div>
    
                            </div>
                        </div>
                    </div>
                    </div>
                  <div class="col-md-12">
                  <div class=" topcls">
                  <div class="form-group">
                    <div class="buttondiv">
                        <button id="createEvent" class="btn btn-primary pull-right">
                            Save
                        </button>
                        <div class="clearfix"></div>
                     </form>
                   </div>
                 </div>
               </div>
             </div>
  {{ Form::close() }}
</div>
</div>
</div>
</div>
<script src="https://checkout.stripe.com/checkout.js"></script>
<script>
 $(document).ready( function() {
  $("#event_image").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });


        var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px; width:100px; height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };
  var someDate = new Date();
  var numberOfDaysToAdd = 2;
  someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
  
  $(".start_date").datepicker({
          startDate: someDate,
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                event_date: {
                    required: true
                   },
                event_title: {
                    required: true,
                    unique: true
                }
            },
        messages: {
                event_date: {
                required: " * required: You must select Event"
                },
                event_title: {
                  required: "This is required field." 
                }
            }
        });
});


</script>
<script type="text/javascript">
  var handler = StripeCheckout.configure({
  key: 'pk_test_6020SfAmkxN79Hq31s8W0WHZ',
  image: '',
  locale: 'auto',
  token: function(token) {
    // You can access the token ID with `token.id`.
    // Get the token ID to your server-side code for use.
   
    
     $("#stripe_tokens").val(token.id);
     
    $("#edtbookingfrm").append('<input type="hidden" name="stripe_token" value="'+token.id+'" />').submit();
  
  }
});

document.getElementById('createEvent').addEventListener('click', function(e) {
  // Open Checkout with further options:
  var value= $('#makeFeatured').prop('checked');

  
  if(value==true){
  handler.open({
    currency:'gbp',
    amount: 500
  });
}
else{
   
   
  $("#edtbookingfrm").submit();
    }

  
  e.preventDefault();
});

// Close Checkout on page navigation:
window.addEventListener('popstate', function() {
  handler.close();
});
</script>

@endsection
