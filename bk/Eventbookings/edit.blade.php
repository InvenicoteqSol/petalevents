@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>     
     <div class="content">
        <div class="container-fluid">
          <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">Update Booking</h4>
              <p class="card-category">Update your Booking</p>
        </div>

          <div class="card-body">
               {{ Form::model($eventbooking, array('route' => array('eventbookings.update', $eventbooking->id), 'id' => 'edtbookingfrm', 'class' => 'edt_booking_frm', 'method' => 'PUT', 'files' => true)) }}
      <div class="row">

      <div class="col-md-6">
        <div class="form-group{{ $errors->has('event_title') ? ' has-error' : '' }}">
            <label for="event_title" class="bmd-label-floating"> Event title *</label>
            <input type="text" class="form-control" id="event_title" name="event_title" value="{{ $eventbooking->event_title }}" required>
            @if ($errors->has('event_title'))
              <span class="help-block">
                <strong>{{ $errors->first('event_title') }}</strong>
              </span>
            @endif
        </div>
    </div>
    
  </div>

<div class="row">
 
  <div class="col-md-12">
    <div class="form-group{{ $errors->has('event_info') ? ' has-error' : '' }}">
    <label for="event_info" class="bmd-label-floating">Event info *</label>
         <textarea id="event_info" name="event_info" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ $eventbooking->event_info }}</textarea>

        @if ($errors->has('event_info'))
            <span class="help-block">
                <strong>{{ $errors->first('event_info') }}</strong>
            </span>
        @endif
    </div>
    </div>
  </div>

<div class="row">
    
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('type_of_event') ? ' has-error' : '' }}">
        
                    <select class ="form-control" id="type_of_event" name="type_of_event" value="{{$eventbooking->type_of_event}}">
                    <option>Type of event</option>
                    <option> Arts &amp; Culture</option>
                    <option> Bar &amp; Pub</option>
                    <option> Burlesque </option>
                    <option> Cabaret </option>
                    <option> Celebrity</option>
                    <option> Charity </option>
                    <option> Children </option>
                    <option> Cinema </option>
                    <option> Clubbing </option>
                    <option> Comedy</option>
                    <option> Concert </option>
                    <option> Conference </option>
                    <option>Cosplay </option>
                    <option> Exhibition</option>
                    <option> Festival</option>
                    <option> Gentlemen's Club</option>
                    <option> Hotel</option>
                    <option> Karaoke</option>
                    <option> Latin Dance</option>
                    <option> Live Music</option>
                    <option> Magic</option>
                    <option> Museum</option>
                    <option> Pantomime</option>
                    <option> Poetry</option>
                    <option> Restaurant</option>
                    <option> Speaker</option>
                    <option> Sport</option>
                    <option> Theatre</option>
                    </select>
                      @if ($errors->has('type_of_event'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('type_of_event') }}</strong>
                          </span>
                      @endif  
    </div>
  </div>
        <div class="col-md-6">
        <div class="form-group{{ $errors->has('age_limit') ? ' has-error' : '' }}">
        
                    <select class ="form-control" id="age_limit" name="age_limit" value="{{$eventbooking->age_limit}}">
                      <option> Age Limit </option>
                      <option>  All ages </option>
                       <option> Under 18 </option>
                       <option> 18 and over  </option>         
                    </select>
                      @if ($errors->has('age_limit'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('age_limit') }}</strong>
                          </span>
                      @endif     
    </div>
</div>
</div>
    <div class="col-md-12">
<div class="row">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <label for="start_date" class="control-label"> Start date*</label>
                <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->start_date)->format('d-m-Y') }}<?php } ?>" required >
            @if ($errors->has('start_date'))
              <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
              </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <label for="end_date" class="control-label"> End date *</label>
            <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($eventbooking->end_date!='' || $eventbooking->end_date!='0000-00-00'  || $eventbooking->end_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->end_date)->format('d-m-Y') }}<?php } ?>" required >

            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
        </div>
    </div>
  </div>
</div>
    <div class="row">
    <div class="col-md-12">
      <label for="waive_booking_fee" class="control-label lblcls">Waive Booking Fee</label>
       <input type="checkbox" name="waive_booking_fee" id="waive_booking_fee" value="1" <?php if($eventbooking->waive_booking_fee== '1') echo " checked "?>>
     </div>
   </div>

      <div class="row">
      <div class="col-md-12 line_div">
            <h3 class="com_busin_sec">Venue details </h3>
          </div>
        </div>
        <div class="col-md-12">
    <div class="row">
   <div class="col-md-4">
     <div class="form-group{{ $errors->has('venue_name') ? ' has-error' : '' }}">
    <label for="venue_name" class="bmd-label-floating">Venue name*</label>
        <input type="text" class="form-control" id="venue_name" name="venue_name" value="{{ $eventbooking->venue_name }}" maxlength="10" required>
        @if ($errors->has('venue_name'))
            <span class="help-block">
                <strong>{{ $errors->first('venue_name') }}</strong>
            </span>
        @endif
    </div>
</div> 
    <div class="col-md-4">
          <div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
    <label for="telephone" class="bmd-label-floating">Telephone*</label>
        <input type="number" class="form-control" id="telephone" name="telephone" value="{{ $eventbooking->telephone }}" maxlength="10" required>
        @if ($errors->has('telephone'))
            <span class="help-block">
                <strong>{{ $errors->first('telephone') }}</strong>
            </span>
        @endif
    </div>
  </div> 

   <div class="col-md-4">
        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
            <label for="website" class="bmd-label-floating"> Website *</label>
            <input type="text" class="form-control" id="website" name="website" value="{{ $eventbooking->website }}" required>
            @if ($errors->has('website'))
              <span class="help-block">
                <strong>{{ $errors->first('website') }}</strong>
              </span>
            @endif
        </div>
  </div>
</div>
</div>
<div class="row">
 <div class="col-md-6 col-xs-12">                         
          <div class="form-group{{ $errors->has('details') ? ' has-error' : '' }}">
    <label for="details" class="bmd-label-floating">Details *</label>
         <textarea id="details" name="details" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ $eventbooking->details }}</textarea>

        @if ($errors->has('details'))
            <span class="help-block">
                <strong>{{ $errors->first('details') }}</strong>
            </span>
        @endif
    </div>  
 </div>
<div class="col-md-6 col-xs-12">
      <div class="form-group{{ $errors->has('venue_address') ? ' has-error' : '' }}">
    <label for="venue_address" class="bmd-label-floating">Venue address *</label>
         <textarea id="venue_address" name="venue_address" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ $eventbooking->venue_address }}</textarea>

        @if ($errors->has('venue_address'))
            <span class="help-block">
                <strong>{{ $errors->first('venue_address') }}</strong>
            </span>
        @endif
    </div>
  </div>
</div>
                      <div class="col-md-12">
                             <div class="row topcls"> 
                             <div class="col-md-6">
                              <label for="event_image" >Event Image</label>
                            <div class="col-md-3 nopadding" id="ftrd_browse_img">
                                @if($eventbooking->event_image!='') 
                                 <img src="{{ url('public') }}/uploads/profileimages/{{ $eventbooking->event_image }}" class="img-responsive" style="width: 100px; height: 100px;">
                                     @else
                                    <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   @endif
                                    
                                </div>
                                <div class="col-md-3 prflinput">
                                <input id="event_image" type="file" name="event_image" accept="image/*">
                              </div>
                        </div>
                    </div>
                </div>
 
            <button type="submit" class="btn btn-primary pull-right">
                Update 
            </button>
            <div class="clearfix"></div>
               {{ Form::close() }}

        </div>
    </div>
   </div>
</div>

    
  </div>
</div>

<script>
 $(document).ready( function() {
  $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


        $("#event_image").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });


        var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px; width:100px; height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
    $('.datepicker').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });


 });
</script>

@endsection
