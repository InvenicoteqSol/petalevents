<?php use \App\Http\Controllers\CartingController;?>
@extends('layouts.innerdefault')
@section('content')
   <?php
   $subtotal_amount=0;
   $discount_given=0;
   $discounted_amount=0;
   $totalbookingfee=0;
 
   ?>
  <!-- CONTACT SECTION -->
<section class="section-checkout">
   
<div class="container">
  <!-- row -->
  @if(count($_discountobjlist)>0)
@foreach($eventIdList as $Items)
<!-- row -->
<?php

$thisEventItems=CartingController::get_array_details_front($Items,$_discountobjlist,'event_id');
$EventSubTotal=0;
 ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
         <thead>
            <tr>
              <?php
              $eventname=DB::table('eventbookings')->select('event_title')->where('id','=',$Items)->first();

              ?>
               <th colspan="5" class="e-name">{{$eventname->event_title}}</th>
            </tr>
            <tr>
              <th>Ticket Type</th>
              <th>Net Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @if(count($thisEventItems)>0)
              @foreach($thisEventItems as $cartItems)
              
              <tr>
              <td>{{$cartItems->TicketTitle}} </td>
              <td><i class="fa fa-gbp"></i> <!-- {{$cartItems->subtotal_amount}} -->
                 <?php
              $ticket_type=$cartItems->TicketTitle;
              $ticketid=DB::table('tickets')->select('prices')->where('ticket_type','=',$ticket_type)->first();
              ?>
               {{$ticketid->prices}}

              </td>
              <td>  {!! Form::open(['route' => ['cart.update',$cartItems->rowId], 'method' => 'PUT']) !!}
                        <input name="qty" type="number" class="form-control cartupdate" value="{{$cartItems->Quantity}}">
                        <input type="hidden" id="size" value="{{$cartItems->event_id}}">
                        {!! Form::close() !!}</td>
              <td><i class="fa fa-gbp"></i> {{$cartItems->discounted_amount}}</td>
              <th>
                <form action="{{route('cart.destroy',$cartItems->rowId)}}"  method="POST">
                           {{csrf_field()}}
                           {{method_field('DELETE')}}
                           <button class="btn btn-primary" type="submit"><i class="fa fa-trash"></i></button>
                         </form>
              </th>
              <?php
              $EventSubTotal=$EventSubTotal+$cartItems->discounted_amount;
              $subtotal_amount=$subtotal_amount+$cartItems->subtotal_amount;
              $discount_given=$discount_given+$cartItems->discount_given;
              $totalbookingfee=$totalbookingfee+$cartItems->booking_fee;
              $discounted_amount=$discounted_amount+$cartItems->discounted_amount;
              

              ?>
              </tr>
              @endforeach
              @endif
              
            
            
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="total-amt">Sub-total: <i class="fa fa-gbp"></i>{{$EventSubTotal}}</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  <!--// row -->

@endforeach
@endif
  
 </div>
  <div class="container">
      <div class="row row-promo">
    <div class="col-md-12 col-xs-12">
   <div class="card">
          <h4 class="sub-heading">Apply Promo Codes</h4>
      <div class="card-body">
       @include('layouts.flash-message')
      <form id="contact-form" class="settingform" action="{{ url('/applypromo') }}" method="GET" enctype="multipart/form-data">
       {{ csrf_field() }}  
  
      <div class="input-group">
      <input type="text" class="form-control" aria-label="..." name="couponCode" required="">

@if(count($_discountobjlist)>0)
@foreach($eventIdList as $Items)
<!-- row -->
<?php
$thisEventItems=CartingController::get_array_details_front($Items,$_discountobjlist,'event_id');
 ?>
       @if(count($thisEventItems)>0)
              @foreach($thisEventItems as $cartItems)
            <?php 
              $ticket_type=$cartItems->TicketTitle;
              $ticketid=DB::table('tickets')->where('ticket_type','=',$ticket_type)->first();
            ?>
      <input type="hidden" class="form-control" aria-label="..." name="ticket_type_id" value="{{$ticketid->id}}" required="">
       @endforeach
        @endif

@endforeach
@endif
      <div class="input-group-btn">
        <button type="submit" class="btn btn-default">Apply</button>
      </div><!-- /btn-group -->
      </div><!-- /input-group -->
      
      </form>
          </div>
        </div>
      </div>
    </div>

   <!--// row -->
        <!-- row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
          <tbody>
            <tr>
               <th class="e-name text-left">Total Amount To Be Paid</th>
              <th  class="total-amt text-right"> 
                        <p>SubTotal: <i class="fa fa-gbp"></i> {{$subtotal_amount}}</p>
                        <p>Discount Given:<i class="fa fa-gbp"></i> {{$discount_given}}</p>
                        <p>Booking Fee: <i class="fa fa-gbp"></i>{{$totalbookingfee}}</p>
                        <p>Total: <i class="fa fa-gbp"></i>{{$discounted_amount+$totalbookingfee}}</p>
                    
                    </th>
            </tr>
          </tbody>
        </table>
      </div><!--end of .table-responsive-->
     <?php   $user_id = Auth::user()->id ;
     $user_email=Auth::user()->email;

     ?>

        <form action="{{url('/charge')}}" method="POST">
            {{ csrf_field() }}
            <?php $key=DB::table('business_settings')->first();
                  $test=$key->stripe_mode;
                  $test_publish_key=$key->test_publish_key;
                  $live=$key->stripe_mode;
                  $live_publish_key=$key->live_publish_key;
                    
           

             if($test=='Test'){ ?>
                                  <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="{{$test_publish_key}}"
                    data-amount="{{($discounted_amount+$totalbookingfee)*100}}"
                    data-name=""
                    data-description=""
                    data-image=""
                    data-email={{$user_email}}
                    data-locale="auto"
                    data-currency="gbp">
                     </script>
                 <?php } 

                  else { ?>
                      <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="{{$live_publish_key}}"
                    data-amount="{{($discounted_amount+$totalbookingfee)*100}}"
                    data-name=""
                    data-description=""
                    data-image=""
                    data-email={{$user_email}}
                    data-locale="auto"
                    data-currency="gbp">
                      </script>
                 <?php } ?>

              <input type="hidden" name="data_amount" value="{{($discounted_amount+$totalbookingfee)*100}}">
              <input type="hidden" name="user_id" value="{{$user_id}}">
              <input type="hidden" name="totalbookingfee" value="{{$totalbookingfee}}">
              <input type="hidden" name="discount_given" value="{{$discount_given}}">
        </form>
     <!--  <div class="col-pay-now">
          <a href="#" id="updateorder" updateorder="" class="btn btn-primary"> Pay Now! </a>
      </div> -->
    </div>
  </div>
   <!--// row -->
</div>


</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
   
   $(".cartupdate").change(function() {
     this.form.submit();
});
 });
</script>

@endsection
