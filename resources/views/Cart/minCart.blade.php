<?php use \App\Http\Controllers\CartingController;?>
<li class="c_head"><label>My cart</label></li>
 <li class="item-list item-head">
                <div class="item-name">
                <label>Ticket / Event</label>
             
                </div>
                 <div class="item-quantity">
                   <label>Quantity</label>
                 </div>
                 <div class="item-price">
                   <label>Price</label>
                   
                   </div>
                   <div class="item-action">
                   <label>Action</label>
                   </div>
                  </li>
                  
                  <?php 
                  $EventSubTotal=0;?>

                  @foreach($_discountobjlist as $cartItem)         

              <!--  //item-list -->
          <li class="item-list ">
                <div class="item-name">
                
<!--   <span class="e-media"><img src="images/e2.jpg"></span>-->             
    <span class="t-type">
                  <span class="t-name">{{$cartItem->TicketTitle." tickets for ".$cartItem->Event_Name->event_title}}</span>
                </span>
                </div>
                 <div class="item-quantity">
                  
                   <span class="t-qty">
                    <div class="input-group">
              <span class="form-control input-number" >{{$cartItem->Quantity}}</span>

              </div>
               </span>
                </div>
                 <div class="item-price">
                 
                   <span class="t-price"><i class="fa fa-gbp"></i>{{$cartItem->subtotal_amount}}</span>
                   </div>
                     <div class="item-action">
                 
                 
                    <form action="{{route('cart.destroy',$cartItem->rowId)}}"  method="POST">
                           {{csrf_field()}}
                           {{method_field('DELETE')}}
                            <button class="btn btn-primary" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                         </form>
                     
                   </div>
                  </li>
              <?php
              $EventSubTotal=$EventSubTotal+$cartItem->subtotal_amount;
            

              ?>
              <!--  //item-list -->
              @endforeach
                   <!--  item-list -->
              <li class="item-list">
                <span class="total-price">
                  Total Price: <i class="fa fa-gbp"></i> {{$EventSubTotal}}


                  </span>
              </li>
                  <!--  //item-list -->
                   <!--  item-list -->
                 <li class="item-checkout">
               <a href="{{url('/cart')}}" class="btn btn-primary">
                CHECKOUT
               </a>
                </li>
                 <!--  //item-list -->


