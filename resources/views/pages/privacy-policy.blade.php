@extends('layouts.frontdefault')
@section('content')
<!--content-section-start-->
<main>
	<section class="p-policy">
   <div class="container">
    <div class="row">
      <div class="col-md-12 col-xs-12 policy-content">
          <h2 class="main-s-heading">Privacy Policy</h2>
          <p>THIS DOCUMENT SETS OUT HOW INFORMATION PROVIDED BY USERS OF THIS WEBSITE, www.petalevents.co.uk, WILL BE PROTECTED AND USED BY COMMERCE GLOBAL VENTURES LTD, THE OWNER AND OPERATOR OF THIS WEBSITE. THIS DOCUMENT TOGETHER WITH THE EVENT PROMOTER TERMS AND CONDITIONS, THE GENERAL TERMS AND CONDITIONS AND ANY OTHER DOCUMENTS REFERRED TO IN ANY OF THE FORGOING DOCUMENTS SETS OUT THE TERMS AND CONDITIONS THAT GOVERN USE OF THIS WEBSITE BY ANY PERSON WHO VISITS OR USES IT.<br><br>
On 25th May 2018, a new data privacy law known as the EU General Data Protection Regulation (or the "GDPR") becomes effective. The following privacy policy details how we process your personal data.
</p>
      </div>
      <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">1. General</h4>
          	<p>1.1 This website is owned and operated by Commerce Global Ventures Ltd trading as www.petalevents.co.uk (the "Site"), a company registered in England and Wales and whose registered address and registration number is as follows: Kemp House 160 City Road London EC1V 2NX; 10521270 ("CGV"). The Site provides box office and social networking services (collectively the "CGV Service").<br><br>
1.2 Any person who visits or uses the Site agrees to be bound to this Privacy Policy (as amended from time to time). This Privacy Policy applies to all "personal data" as defined in the Data Protection Act 1998 and the Data Controller for the purpose of the same legislation shall be CGV.<br><br>
1.3 This Privacy Policy only applies to information collected by CGV on the Site.<br><br>
1.4 Any transmission of information to the Site is at the risk of the person doing so. Although CGV shall endeavour to protect information provided by users, it cannot guarantee that the information supplied will be protected against unauthorised access.<br><br>
1.5 From time to time, CGV may contain links to other websites and services that are not owned or controlled by CGV. In these circumstances, CGV shall not be responsible for the privacy practices of those websites.<br><br>
1.6 Any capitalised words used but not defined in this Privacy Policy shall have the same meaning as defined in either the Event Promoter Terms and Conditions or the General Terms and Conditions.
</p>
      </div>
      <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">2. Information Collected</h4>
          <p>2.1 In order to use the CGV Service, a person may be required to provide certain information including but not limited to information which may be used to identify that person such as names, telephone numbers, residential/postal addresses and email addresses as well as financial information. This information may be provided to CGV and our Event Promoters using the platform to run an event. <br><br>
2.2 In addition to the above, certain information is automatically transferred to the Site when the Site is used or visited. This includes but is not limited to Internet Protocol (IP) addresses, cookies and other connection information.
</p></div>
		 <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">3. Use and Disclosure of Information</h4>
          <p>3.1 All information collected is used to improve and maintain the CGV Service. Some of examples include but are not limited to: <br><br>
3.1.1 Using personally identifiable information (Personal Information) in order to prevent abuse of the Site; <br><br>
3.1.2 Using financial information (Financial Information) to process orders and detect and prevent fraud; <br><br>
3.1.3 Providing users with information regarding updates, promotions and recommendations; <br><br>
3.1.4 Using automatic information to maintain and improve the Site and notifying users of any changes to the Site. <br><br>
3.2 None of the information on the Site collected is sold to third parties. It is only used in the manner set out in this document, namely; <br><br>
3.2.1 Information is always used in a manner which CGV believes is necessary to protect the integrity of the Site and the CGV; <br><br>
3.2.2 Financial and Personal Information is never released to any third party unless it is necessary to do so in order to comply with the law, protect the Site, the CGV Service or other users of the Site;
 <br><br>
3.2.3 Without prejudice to the forgoing or the remainder of this document, CGV in the event of a transfer of ownership of the Site, reserves the right to transfer any information it holds if so required as part of the transfer of ownership. <br><br>
3.2.4 A person who provides Financial Information when using the CGV Service will be deemed to have given CGV express consent to use that Financial Information in any manner necessary in order to facilitate the delivery of the goods/services being purchased. This includes but is not limited to granting any relevant financial institutions access to the Financial Information for the purpose of facilitating payment.     <br><br>
3.2.5 The CGV Service includes a box office facility where CGV sells Tickets to Events on behalf of Event Promoters. The CGV Service also provides a platform from which the Event Promoter may manage the Event. In order to facilitate this, information collected on the Site may be shared between CGV and the Event Promoter.   <br><br>
3.2.6 In addition to paragraph 3.2.5 above, with consent, registered Users of the Site may be sent information about events, goods and/or services which may be of interest to them. This information may be supplied by CGV. Any person who does not wish to receive information of this nature may leave the checkbox un-ticked prompted during registration or by sending an email with the word "unsubscribe" in the subject field to CGV at the following address:unsubscribe@petalevents.co.uk <br><br>
3.2.7 Automatic Information refers to information such as IP addresses, information stored on cookies and any other information which can be used to determine how the Site and the CGV Service is being used and which is generated automatically. This is used to develop statistics and aggregate information about the number of people who visit the Site and use the CGV Service. This information is then used to customise the content and layout of the Site in order to improve the CGV Service. One of the ways this is achieved is by way of advertising. CGV may transfer Automatic Information to its advertising partners and other third parties. This information may be used to provide advertising, promotions and other products and services that may be of particular interest to registered Users. These advertising partners and third parties cannot access the CGV servers to obtain any Personal Information from there.   <br><br> 
3.2.8 With your consent, we will contact you with information regarding our upcoming events, products or services. This contact may be through email, push and web notifications, SMS, or social media platforms. <br><br>
3.2.9 We allow Event Promoters to contact users regarding event updates. This may be done by us on the behalf of the Event Promoter. If you have opted in to receive communications directly from the Event Promoter during the event booking process, your email address will be available to that Event Promoter. In some cases, Event Promoters may work with a third party to create an event on the platform which will enable them to see relevant information. To unsubscribe from an Event Promoter's database, please contact them directly. We are not responsible for the actions the Event Promoters or their third party affiliations. <br><br>
3.2.10 Your information will be provided to Event Promoters, for the purpose of event management and customer analytics and as further described in their Privacy Policy.
</p></div>
			 <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">4. Cookies</h4>
          <p>4.1 When a computer accesses the Site, small files known as cookies, may be downloaded onto that computer automatically. These files contain information about how that computer is being used so that where it is used to access the Site frequently or where a particular feature of the Site is accessed frequently, CGV can obtain useful information on how to improve the Site and the CGV Service.<br><br>
4.2 Cookies can be disabled. Users are advised to consult the "help" function on this matter.
</p></div>
			 <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">5. Security and Confidentiality</h4>
          <p>CGV has installed secure-server software which encrypts all Personal Information and Financial Information including credit and debit card numbers. The encryption process takes the characters entered on the Site and converts them into bits of code that are then securely transmitted over the Internet. This is done using 128-bit Secure Socket Layer certificates. As a result, no cardholder information is passed unencrypted. Messages sent to the CGV servers are signed using MD5 hashing to further protect the information</p></div>
          <div class="col-md-12 col-xs-12 policy-content">
          <h4 class="policy-sub-heading">6. Transfer of Data</h4>
          <p>In order to fulfil our services, CGV may need to transfer your Personal Data outside of the country from which it was originally provided. This may be third parties (e.g such as server providers) that we work with who may be located in jurisdictions outside the EEA, Switzerland and the UK which have no data protection laws or laws that are less strict compared with those in Europe. <br>
Whenever we transfer Personal Data outside of the EEA, Switzerland or the UK, we take legally required steps to make sure that appropriate safeguards are in place to protect your Personal Data. 
</p></div>
  </div>
</div>
</section>
	</main>
<!--content-section-end-->
@endsection