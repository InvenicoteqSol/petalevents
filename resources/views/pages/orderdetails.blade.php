@extends('layouts.innerdefault')
@section('content')
<?php  $id = Auth::user()->id; 
         $orders = DB::table('orders')
                 ->where('id','=',$idorder)
                 ->where('user_id','=',$id)
                 ->first();
         $order_items=DB::table('order_items')->where('order_id','=',$idorder)->get();
      ?>
      <section class="section-overview">
  <div class="container">
    <div class="row">
        <!--  col-oview -->
      <div class="col-md-12 col-oview">
         <div class="o-head">
          <h5>Order : {{$orders->order_number}}</h5>
         </div>
      </div>
        <!--  //col-oview -->
         <!--  col-oview -->
      <div class="col-md-12 col-sm-12 col-xs-12 col-oview">
     
      <div class="o-body">
         <div class="o-head">
        <h4>Order Overview</h4>
      </div>
        <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br>{{Auth::user()->first_name}}
       
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> {{Auth::user()->last_name}}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> {{$orders->amount}}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  {{$orders->order_number}}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b> <br>  {{$orders->created_at}}
                        </div>
                        
                   </div>
               </div>
          </div>
            <!--  //col-oview -->
                <!--  col-oview -->
      <div class="col-md-12 col-sm-12 col-xs-12 col-oview">
     
      <div class="o-body">
         <div class="o-head">
        <h4>Order Items</h4>
      </div>
     
        <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>
                                Ticket
                            </th>
                            
                            <th>
                                Price
                            </th>
                            <th>
                              Quantity
                            </th>
                           
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                            @if(count($order_items)>0)
                              @foreach($order_items as $orderItem)
                              <tr>  
                                
                                    <td>
                                   <?php $ticket_name=DB::table('tickets')->select('ticket_type')->where('id','=',$orderItem->ticket_id)->first();
                                   ?>
                                   {{$ticket_name->ticket_type}}
                                    </td>
                                    
                                    <td>
                                   {{$orderItem->netPrice }}
                                    </td>
                                    <td> {{$orderItem->Quantity }}</td>
                                    <td>
                                      {{$orderItem->totalPrice }} 
                                    </td>
                                </tr>
                                @endforeach
                                 <tr> <td><strong> Discount Given : </strong>{{$orders->discount }} </td> </tr>
                                  <tr> <td><strong> Booking Fee : </strong> {{$orders->totalbookingfeeamount }} </td>
                                  </tr>
                                  <tr>  <td><strong> Total Amount : </strong>  {{$orders->amount }} </td>
                                  </tr>
                                @else
                                <tr><td>No Records found!!</td></tr>
                                @endif
                     
                                                                                                 
                            </tbody>
                        </table>

                    </div>                
       
               </div>
          </div>
            <!--  //col-oview -->
      </div>
  </div>
 </section>
@endsection
