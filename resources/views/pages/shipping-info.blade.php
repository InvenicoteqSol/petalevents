@extends('layouts.innerdefault')
@section('content')


  <!-- CONTACT SECTION -->
<section class="section-checkout">
   

<div class="container">
  <!-- row -->
<div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 col-head-area">
        <h2 class="main-s-heading">Event Checkout heading here</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      </div>

    </div>
     <!--// row --> 
     <!-- row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
         <thead>
            <tr>
              <th colspan="5" class="e-name">Data retrieved from</th>
            </tr>
            <tr>
              <th>Ticket Type</th>
              <th>Ends date</th>
              <th>Net Price</th>
              <th>Quantity</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Standard Ticket </th>
              <td>  22nd Dec</td>
              <td>$ 20.00</td>
              <td>  2</td>
              <td>$40.00</td>
            </tr>
              <tr>
              <th>Standard Ticket </th>
              <td>  22nd Dec</td>
              <td>$ 20.00</td>
              <td>  2</td>
              <td>$40.00</td>
            </tr>
            <tr>
              <td>Booking Fees</td>
              <td>-</td>
              <td>-</td>
              <td>-</td>
              <td>$9.00</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="total-amt">Sub-total: $89</th>
            </tr>
          </tfoot>
        </table>
      </div><!--end of .table-responsive-->
    </div>
  </div>
   <!--// row -->
        <!-- row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
         <thead>
            <tr>
              <th colspan="5" class="e-name">Data retrieved from</th>
            </tr>
            <tr>
              <th>Ticket Type</th>
              <th>Ends date</th>
              <th>Net Price</th>
              <th>Quantity</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Standard Ticket </th>
              <td>  22nd Dec</td>
              <td>$ 20.00</td>
              <td>  2</td>
              <td>$40.00</td>
            </tr>
              <tr>
              <th>Standard Ticket </th>
              <td>  22nd Dec</td>
              <td>$ 20.00</td>
              <td>  2</td>
              <td>$40.00</td>
            </tr>
            <tr>
              <td>Booking Fees</td>
              <td>-</td>
              <td>-</td>
              <td>-</td>
              <td>$9.00</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="total-amt">Sub-total: $89</th>
            </tr>
          </tfoot>
        </table>
      </div><!--end of .table-responsive-->
    </div>
  </div>
   <!--// row -->
 </div>
   <div class="container">
   <div class="row row-promo">
    <div class="col-md-12 col-xs-12">
   <div class="card">
          <h4 class="sub-heading">Apply Promo Codes</h4>
          <div class="card-body">
            <P>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</P>
            <div class="input-group">
      <input type="text" class="form-control" aria-label="...">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default">Apply</button>
       
      </div><!-- /btn-group -->
    </div><!-- /input-group -->
        
          </div>
        </div>
      </div>
   </div>
   <!--// row -->
        <!-- row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
          <tbody>
            <tr>
               <th class="e-name text-left">Total Paid Amount</th>
              <th  class="total-amt text-right">Total:$178</th>
            </tr>
          </tbody>
        </table>
      </div><!--end of .table-responsive-->
      <div class="col-pay-now">
         <button type="button" class="btn btn-default">Pay Now!</button>
      </div>
    </div>
  </div>
   <!--// row -->
</div>

</section>
<?php
                    $id = Auth::user()->id;
                     $customer = DB::table('users')
                               ->where('id','=',$id)
                               ->first();          
                    $countries = DB::table('countries')->pluck('id', 'country_name');
                    ?>
<!--  <section id="organiser-register">
     <div class="container">
          <div class="row">
<div class="row">
    <div class="small-6 small-centered columns">
        <h3>Shipping Info</h3>
                 
               
         <form id="contact-form" class="settingform" action="{{ url('/shippingstore') }}/{{$customer->id}}" method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}
                    <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                             <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                             <label for="first_name"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $customer->first_name }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                        <div class="col-md-6">
                                   <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name">Last Name*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $customer->last_name }}" minlength="2" maxlength="91" required>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                </div>

                     <div class="col-md-12">
                        <div class="row topcls">
                          <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-Mail Address*</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ $customer->email }}" maxlength="191" required readonly="readonly">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country">Country</label>
                                <select class="form-control" id="country" name="country" required>
                                    @foreach ($countries as $countryName=>$countryId)
                                        <option value="{{ $countryId }}" {{ ( $customer->country == $countryId ) ? 'selected' : '' }}>{{ $countryName }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>



                 <div class="col-md-12">
                        <div class="row topcls">
                          <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
                            <label for="email">Address line 1</label>
                            <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="{{ $customer->address_line_1}}" minlength="2" maxlength="91" required>
                                @if ($errors->has('address_line_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
                            <label for="country">Address line 2 *</label>
                               <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="{{ $customer->address_line_2}}" minlength="2" maxlength="91" required>

                                @if ($errors->has('address_line_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
       
                 <div class="col-md-12">
                        <div class="row topcls">
                          <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Postcode</label>
                            <input id="postcode" type="text" class="form-control" name="postcode" value="{{ $customer->postcode}}" minlength="2" maxlength="91" required>

                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                     <div class="col-md-6">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone">Phone*</label>
                            <input id="phone" type="text" class="form-control" name="phone" value="{{ $customer->phone }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                </div>
                <div class="col-md-12">
                        <div class="topcls">
                     <div class="form-group">
                            <div class="buttondiv">
                               <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                               
                               

                            </div>
                        </div>
                    </div>
                </div>
                    </form>
                   
    </div>
</div>
</div>
</div>
</section> -->
 <a href="#" id="updateorder" updateorder="{{$customer->id}}" class="btn btn-primary"> Proceed to Payment </a>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
   $("#updateorder").click(function(){
    var updateorder = $("#updateorder").attr("updateorder");
    jQuery.ajax({
            method : 'GET',
            url  : "{{ url('/updateorder') }}",
            data :{'updateorder': updateorder},
            success :  function(resp) {

             }
       });
 
   });
 });
</script>


@endsection