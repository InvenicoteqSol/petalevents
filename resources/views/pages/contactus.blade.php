@extends('layouts.default')
@section('content')

<!--content-section-start-->
<main>
 <section class="innercontentpart">
    <div class="innercontent_text">
      <div class="contact_bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 contactdiv">
            <div class="col-sm-4 contactinfo contact_title">
              <h2>Address</h2>
              <ul>
                <li><i class="fa fa-map-marker"></i><span>58911 Lepzig Hore,  85000 Vienna, 
Hong kong</span></li>
<li><i class="fa fa-phone"></i><span>+1 777 55-32-21</span></li>
<li><i class="fa fa-envelope"></i><span>contacts@potrate.com</span></li>
              </ul>
              <div class="contact_socialmedia">
                <h3>Follow us:</h3>
                <ul>
                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>
            </div>
          </div>
             <div class="col-sm-8 contactform contact_title">
              <h2>GET IN TOUCH WITH US</h2>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
                    <form class="contact_form formarea" id="contact_form">
        <div class="row">
  <div class="col-sm-12 form-group forminput">
    <input type="text" class="form-control" id="name" placeholder="Your Name" value="">
  </div>
</div>
<div class="row">
  <div class="col-sm-12 form-group forminput">
   <input type="email" class="form-control" id="email" placeholder="Your Email Address" value="">
  </div>
</div>
<div class="row">
  <div class="col-sm-2 tel_code">
    <span>+852</span>
  </div>
  <div class="col-sm-10 form-group forminput">
   <input type="text" class="form-control" id="phonenumber" placeholder="Phone number" value="">
  </div>
</div>
<div class="row">
  <div class="col-sm-12 form-group forminput">
   <input type="text" class="form-control" id="subject" placeholder="Subject" value="">
  </div>
</div>
<div class="row">
  <div class="col-sm-12 form-group forminput">
   <textarea class="form-control" rows="6" placeholder="Your Message"></textarea>
  </div>
</div>

  <div class="submitbtn">
  <button type="submit" class="btn btn-default">Send Request</button>
</div>

</form>
          </div>
        </div>
      </div>
</div>
</div>
 </section>
</main>
<!--content-section-end-->

@endsection