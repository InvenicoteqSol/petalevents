@extends('layouts.default')
@section('content')
  
<!--content-section-start-->
          <?php 
            $categories = DB::table('categories')
                        ->where('status', '=', 1)
                        ->where('deleted', '=', 0)
                        ->orderBy('id','ASC')
                        ->get();
              ?>
<main>
  <section class="section1 section_mdiv">
    <div class="container">
    <div class="section_content text-center">
      <h2>Occasions</h2>
      <span><img src="{{ url('/public') }}/images/border-icon.png"></span>
      <p>It is a long established fact that a reader will</p>
      <div class="occasion_sec">
        <div class="row">
        @foreach($categories as $categoriey) 
          <div class="col-sm-3 occasion_div">
             <div class="occasion_img">
        @if($categoriey->cat_featur_image!='' && $categoriey->cat_featur_image!='NULL')
            <img src="{{ url('public') }}/uploads/category/featureimages/{{$categoriey->cat_featur_image}}" class="phot_home">
            @else
            <img src="{{url('/public')}}/images/dummy.jpg" class="phot_home">
            @endif
             <h3>{{$categoriey->cat_name}}</h3>
          </div>
        </div>
        @endforeach
        </div>
 
      </div>
    </div>
  </div>
  </section>

</main>

<!--content-section-end-->

@endsection