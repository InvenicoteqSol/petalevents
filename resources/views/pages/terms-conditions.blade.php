@extends('layouts.frontdefault')
@section('content')
<!--content-section-start-->
<main>
 <section class="signup_section form_section innercontentpart p-policy">
   <div class="container">
    <div class="innercontent_text row">
      <div class="col-md-12 col-xs-12 policy-content">
      <h1>General Terms and Conditions</h1>
      <p style="pa">Please read the following terms and conditions carefully. By using this site and by registering to buy tickets via it you will be deemed to have accepted all relevant terms and conditions.<br><br>
Welcome to the terms and conditions for Commerce Global Ventures Ltd. Commerce Global Ventures Ltd acts as an agent facilitating the sale of Tickets to Events on behalf of Event Organisers (all as defined below). We therefore enter this agreement with you in our own right and on behalf of the Event Organisers.<br><br>
These terms and conditions are divided into 2 parts:<br>
1 Those relating to Ticket purchases made by you from the Event Organiser(s) of the Event(s) which you have selected via our Website (the "Ticket Purchase Terms"); and<br>
2 Those relating to your use of this Website (the "Website Terms")
The expression "terms and conditions" is used to describe the combined provisions of the Ticket Purchase Terms and of the Website Terms.<br>
In these terms and conditions;<br>
• "we" and "us" means Commerce Global Ventures Ltd (registered as a company in England and Wales, number 10521270), whose registered office is at Kemp House, 160 City Road, London, EC1V 2NX.<br>
• "Event" means the individual event or events advertised on our Website for which Tickets are available.<br>
• "Event Organiser" means a third party supplier or suppliers of Tickets for the Events which may include a venue, performer, promoter or event organiser.<br>
• "Tickets" or "Bookings" means rights to admission in the form of unique Booking references.<br>
• "Website" means the website on which we make Tickets available and from which we promote Events.<br>
• "you" means an individual purchasing tickets using this Website.<br>
If there is any conflict between the Ticket Purchase Terms and the Website Terms, then the latter shall prevail

      </p>
    </div>
      <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">1.Ticket Purchase Terms</h4><br>
        <h4 class="policy-sub-heading">Sale of Tickets</h4>
        <p>Rights to admission for Events are sold in the form of unique Booking references which are delivered electronically via this Website.<br><br>
Please note we do not purchase the Tickets, set the Ticket prices, or determine seating locations at the Event. We collect the Ticket money on behalf of the Event Organiser and supply details of your booking to them. Ownership of the Tickets and rights to grant entry to the Event remains with the Event Organiser and does not pass to us at any time.<br><br>
A request by you to purchase Tickets using the booking process on this Website is an offer made subject to these Ticket Purchase Terms. The offer is made when you click the "proceed" button on the Basket Page of the Website. Acceptance by us of your offer on behalf of the Event Organiser shall be deemed to be effective when your payment has been cleared and we have confirmed your order.
</p>
      </div>
           <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Making Your Booking</h4>
        <p>To buy Tickets, you must register and set up an account with us (or log in to an account you have previously set up). Please see our Privacy Policy for further information about how we will use your personal data. You must not use our Website or any services available via it if you do not accept this Privacy Policy and our terms and conditions.<br><br>
You must be over 16 years old to buy Tickets using this Website and must also comply with any age restrictions which apply to the venue where the Event is being held (as set out below).<br><br>
Throughout the Website and particularly when you are logged into this Website you may choose the Tickets you wish to purchase and add them to your basket. Please check your purchase carefully before you confirm your purchase and check out by clicking on "proceed". At this stage an offer has been made by you to purchase the Tickets selected.<br><br>
Once you have offered to purchase the Tickets then, subject to their availability at advertised prices and checking your payment details, we will accept your offer and confirm your Booking by displaying your unique Booking reference.
When we accept your offer to buy a Ticket we will charge you via your chosen payment method with the face value of the Ticket plus a Booking fee where applicable. The face value and booking fee will be clearly itemised in your basket before you confirm your purchase and check out. We forward the face value of the Ticket to the Event Organiser. The booking fee is the separate charge we make to you for facilitating the Booking (and is inclusive of VAT). If you require a VAT receipt for the face value of the Ticket you must contact the Event Organiser.
Until your payment is processed and accepted by us the unique Booking reference will not be valid.
You are advised to check your purchase on receipt of confirmation from us.
Please keep your unique Booking reference in a safe place as you may be refused entry to the Event without it. (See "Gaining admission to the Event").
</p>
      </div>
                 <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Gaining Admission to the Event</h4>
        <p>The unique Booking reference for each Booking is issued subject to these terms and conditions. We advise that you familiarise yourself with the following terms and conditions before attending the Event.<br>
As the person making the Booking, you must attend the Event yourself, and must accompany all other persons for whom the Booking is made unless part or all of the Booking is reallocated using our reallocation system, read more here. Please note if you are an adult purchasing Tickets for children, you must either attend the Event yourself, or reallocate the Booking to the name of one or more of the children using our reallocation system. Some of the Events and Ticket types have age restrictions. Whilst we use reasonable efforts to provide details of any age restrictions, it is your responsibility to find out if there are any age restrictions which apply.<br>
When you arrive at the Event you have booked, you (and/or any persons to whom you have reallocated your Booking) must be able to provide your unique Booking reference, your name, your email address, and either photographic or signature identification. Only the person in whose name the Booking has been made/reallocated needs to provide this information; persons in their party arriving with them do not; though each person will have to provide proof of age or concessionary status where these are required (see "Age restrictions, concessionary rates, right to admission"). You are responsible for making people to whom you reallocate the tickets aware of the relevant parts of these terms and conditions.
</p>
      </div>
                       <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">The Reallocation System</h4>
        <p>For most Events, you may reallocate some or all of your Tickets to other people using our reallocation system. This means that those other people can gain admission to the Event without you being with them. To reallocate Tickets after purchase, log in to My Account, use the My Tickets link to display your list of Tickets and then click on the reallocation button next to the Tickets you wish to reallocate. You will retain the original unique Booking reference and it will be valid only for the tickets that you have not reallocated. A new unique Booking reference will be displayed for the tickets you have reallocated. The people to whom you reallocate the tickets must provide their unique Booking reference, identification and all other information detailed above in "Gaining admission to the Event". You will continue to be bound by these terms and conditions regardless of the reallocation of some or all of your Tickets.<br>
Reallocation may be unrestricted, restricted or unavailable.<br>
We will use reasonable efforts to ensure that it is clearly stated during Booking if reallocation for a particular Event is restricted or unavailable.<br>
Reallocation is usually unrestricted (available at any time up to midnight on the day before the Event, or up to midnight on Thursday for weekend Events).<br>
For certain Events reallocation may be restricted (available up to two hours after the Booking), or unavailable.

</p>
      </div>
                             <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Venue Rules and Regulations</h4>
        <p>Tickets are issued subject to the Rules and Regulations of the Event Organiser issued at the venue. Full details are available from the Event Organiser on request. You and all other Ticket holders in your party must comply with all relevant statutes, safety announcements and venue regulations whilst attending the Event. The following points apply in general: the Event Organiser, venue management, and we accept no responsibility for any personal property taken to an Event. At the Event Organiser's option there may be no pass-outs or re-admissions of any kind to an Event. Ticket holders are advised that official merchandise is usually only available inside the venue.<br><br>
The Event Organiser has entered into an agreement with us to honour the unique Booking references and to treat them as conferring the same rights as printed tickets or any other form of booking available for the same Event.
All details about Events advertised by us are provided on behalf of the Event Organiser and are supplied by the Event Organisers themselves. You are strongly advised to check Event details with the venue at which it is to be held and/or the Event Organiser before travelling to an Event. We make the best efforts to keep all information as up to date as possible but we cannot be held responsible for any errors.<br><br>
We offer you the opportunity to buy Tickets from a number of Event Organisers through this Website. We exclude liability for any Tickets or other goods provided by Event Organisers or other third party suppliers to the fullest extent permitted by law. In no circumstance shall we be liable for any indirect loss, consequential loss, loss of profit, data, revenue, business opportunity, anticipated savings, goodwill or reputation whether in contract, tort or otherwise arising out of or in connection with this agreement or purchases made through this Website save where such liability cannot be excluded by law.<br>
Any dispute regarding the content or quality of the Event, or any actions of the Event Organiser, venue, performers or their representatives is deemed to be between you and the Event Organiser.
Nothing in these terms and conditions shall be taken to limit or exclude our liability for death, personal injury or fraud caused by our or our employees or directors' negligence.
Subject to the above terms and conditions our maximum liability arising in connection with these terms and conditions and the Website shall be £1,000 per claim.
</p>
      </div>
                                   <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Refunds, Cancellations, Rescheduling of Events</h4>
        <p>Purchased Tickets cannot be refunded unless:<br>
a an Event is cancelled;<br>
b an Event is moved to another date and you decide not to retain your Booking for the rescheduled Event; or<br>
c details of an Event are significantly changed after an order is placed (significant changes being a change of headline act or venue).<br>
Only the face value of the Ticket will be refunded if (a), (b) or (c) occurs. Ticket booking fees are non-refundable.
If an Event is cancelled, rescheduled, or the details are significantly changed, and we have been informed of the change by the Event Organiser, we will contact you as soon as possible by email using the details you have supplied in the My Account section of the Website. If you do not receive this notification because you have not updated the email address that we hold in your account, we cannot be held liable. We also cannot be held liable should the email address we hold in your account be configured in such a way to block or divert into spam or junk folders any emails we send to your email address.
If an Event is cancelled, and we have been informed by the Event Organiser, we will automatically refund the face value of your Ticket: you don't need to notify us or claim your refund.<br>
If an Event is cancelled, and we have NOT been informed by the Event Organiser (and so we have not been able to inform you of the cancellation), you must notify us no later than 48 hours after the Event that you wish to claim a refund.
If we have been informed by the Event Organiser that the Event is to be rescheduled (as set out in (b) above), or the details significantly changed (as set out in (c) above), and we have informed you of the change, we will offer you the choice of retaining your Booking for the rescheduled or changed Event, or obtaining a refund of the face value of the Ticket.<br>
If you wish to receive a refund you must notify us of your choice as soon as possible, and no later than 4pm on the working day before the original date of the Event, or by 4pm on the Thursday before the original date of the Event in the case of Events where the original date was a Saturday, Sunday or a Bank Holiday Monday. If however we have only notified you in the 48 hours preceding the original date of the Event, you must notify us as soon as possible, and no later than 4pm on the day of the original date of the Event. If we have only notified you on the original date of the day of the Event, you must notify us as soon as possible, and no later than the opening time on the original date of the Event.<br><br>
If you don't notify us that you wish to obtain a refund, or you do not notify us within the times set out above, you will be deemed to have chosen to retain your Booking and you will not be entitled to a refund.<br><br>
If an Event is rescheduled, or the details are significantly changed and we have NOT been informed by the Event Organiser (and so have not informed you of the change), you must notify us as soon as possible, and no later than 48 hours after the original date of the Event, if you wish to claim a refund.<br><br>
If an Event is moved to another date (as set out in (b) above) or the details are significantly changed (as set out in (c) above) and you decide to retain your Booking, you shall not be entitled to any refund of the face value of the Ticket, or the booking fee or to any other compensation whatsoever.<br>
In the case of a refund the face value of the Ticket will be credited to you via the payment method used for the original transaction. If this process fails then we will issue a cheque made payable to the name listed in the My Account section of the website. We will send this cheque via standard Royal Mail post to the address in the Address field registered in the My Account section of the website.
</p>
      </div>
                                         <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Transfer of Tickets</h4>
         <p>By using this site to buy Tickets, you agree that:<br>
• they are for the personal use of you and your party only,<br>
• you will not attempt to transfer them (other than by the permitted use of the reallocation system),<br>
• you will not attempt to resell them in any way (including by using the reallocation system).<br>
Use of the reallocation system is only permitted for customers who wish to buy on behalf of their friends or children, its use is not permitted for the reselling of Tickets.<br>
• If you attempt to transfer your Ticket, (other than by the permitted use of the reallocation system) it will be invalidated and the holder will be refused entry to the venue.<br>
• If you attempt to resell the Ticket, it will be invalidated and the holder will be refused entry to the venue.<br>
• If we suspect you are attempting to use the reallocation system for resale of Tickets, we will invalidate your Booking.
We reserve the right to cancel Tickets which we reasonably suspect to have been acquired fraudulently.
</p>
      </div>
                                              <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Age Restrictions, Concessionary Rates, Right to Admission</h4>
        <p>Some Events and Ticket types have age restrictions and we will take reasonable efforts to ensure these are detailed in the Event information. Please read all the information that applies to the Event you are booking, and bring a proof of age if appropriate.<br><br>
Some Tickets are offered at concessionary rates to eligible persons, for example, students, under-18s. If you buy concessionary Tickets, you must take the necessary proof that each Ticket holder is eligible for that concession to the Event. If you cannot prove eligibility for each person holding a concessionary Ticket, you may be asked to pay the difference on the door or refused entry completely. If you are refused entry, we cannot refund you.<br><br>
The right to admission to an Event is reserved by the Event Organiser and Event venue, who may take health and safety, environmental and security concerns into account, and may carry out security searches. We are unable to offer you a refund if you are refused entry to or ejected from a venue on account of being under age (or appearing to be underage and failing to provide proof of age), declining to be searched, abusive, threatening, drunken, or other antisocial behaviour (including smoking in no smoking areas), carrying offensive weapons or illegal substances, or making unauthorised audio, video or photographic recordings. In cases of Events where prompt arrival is required, late arrival may also result in refusal of entry.<br>
Laser pens, mobile phones, animals and Ticket holder's own food may also be prohibited from the venue.
</p>
      </div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Availability of Tickets</h4>
        <p>Some Events and Ticket types have restricted availability or allow a maximum number of Tickets per purchaser. These restrictions are indicated in the Event information or during booking. If you order Tickets in breach of these restrictions or in excess of the maximum allowed we reserve the right to cancel your order.<br>
Tickets for Events may be available through other outlets and box offices. The number of Tickets available on this Website only reflects our remaining allocation, which is controlled by the Event Organiser, and not the total tickets remaining for the Event itself.
</p>
      </div>
       <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Website Terms</h4>
        <p>If you use this Website you agree with us to be bound by the following terms and conditions. You are responsible for ensuring that anyone using the Website via your login details complies with these terms and conditions.</p>
      </div>
             <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Access to the Website and Content</h4>
        <p> We will endeavour to allow uninterrupted access to the Website, but access to the Website may be suspended, restricted or terminated at any time.<br>
We reserve the right to change, modify, substitute or remove without notice any information on the Website from time to time.<br>
We assume no responsibility for the contents of any other websites to which the Website has links.
</p>
      </div>
                  <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4 class="policy-sub-heading">Intellectual Property</h4>
        <p>The copyright, trademarks and all other intellectual property rights in the material contained in the Website, together with the Website design, text and graphics, and their selection and arrangement, and all software compilations, underlying source code and software (including applets) belongs to us or is currently licensed to us, our subsidiaries or the providers of such information. All rights are reserved. None of this material may be reproduced or redistributed without our express written permission. You may, however, download or print a single copy for your own non-commercial off-line viewing.<br>
You shall retain ownership of all copyright in data you submit to the Website. You grant us a worldwide exclusive, royalty-free, non-terminable licence to use, copy, distribute, publish and transmit such data in any manner subject to our obligations as set out in our Privacy Policy.
</p>
      </div>
             <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Exclusions of Liability</h4>
        <p>We use reasonable endeavours to ensure that the data on the Website is accurate and to correct any errors or omissions as soon as practicable after being notified of them. We do not monitor, verify or endorse information submitted by third parties including Event Organisers for posting on the Website. To the extent permitted by applicable law, we disclaim all warranties and representations (whether express or implied) as to the accuracy of any information contained on the Website. We do not guarantee that the Website will be fault free and do not accept liability for any errors or omissions.<br><br>
Due to the nature of electronic transmission of data over the Internet, any liability we may have for any losses or claims arising from an inability to access the Website, or from any use of the Website or reliance on the data transmitted using the Website, is excluded to the fullest extent permissible by law. In no event shall we be liable for any indirect loss, consequential loss, loss of profit, data, revenue, business opportunity, anticipated savings, goodwill or reputation whether in contract, tort or otherwise arising out of or in connection with this agreement or use of the Website save where such liability cannot be excluded by law.<br>
We do not give any warranty that the Website is free from viruses or anything else which may have a harmful effect on any technology and you should take your own safeguards in this area.
</p>
      </div>
      <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Email Address and Password</h4>
        <p>On registering with us, you provide your email address and a password which must be used in order to access certain restricted parts of the Website. This is personal to you and is not transferable.<br>
Your email address and password are the methods used by us to identify you and so are very important. You are responsible for all information posted on the Website by anyone using your email address and password and any payments due for services accessed through the Website by anyone using your email address and password. Any breach of security of an email address and password should be notified to us immediately.<br>
You may not adapt or circumvent the systems in place in connection with the Website, nor access the Website other than through normal operations.
</p></div>
           <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Data Submitted by Users</h4>
        <p>We accept no liability for data supplied by any user for display on the Website and the limitations in the section headed "Exclusions of liability" apply.
If you submit data for display on the Website you are responsible for ensuring that the data is accurate, complete and up to date and for updating that data where necessary.
If you submit data for display on the Website you are responsible for ensuring that no data is uploaded or submitted which is untrue, defamatory, obscene or abusive or otherwise objectionable or in breach of any applicable laws or rights of third parties.
You warrant that you have taken all reasonable precautions to ensure that any data you upload or otherwise submit to the Website is free from viruses and anything else which may have a contaminating or destructive effect on any part of the Website or any other technology.
We reserve the right (without limiting our rights to seek other remedies) to remove offending material placed on the Website that we consider to constitute a misuse of the Website or which is otherwise harmful to other users of the Website.
You will indemnify us for any claim or loss (including without limitation, economic loss) suffered by us arising out of your failure to observe any of the terms of this condition headed "Data submitted by users".
</p></div>
              <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Termination</h4>
        <p>We reserve the right to terminate your access to the Website and/or your Booking immediately and without notice and invalidate your Ticket so that you will be refused entry at the venue if you breach any provision of these terms and conditions.<br>
If any provision of any of the terms and conditions on the Website is held to be unlawful, invalid or unenforceable, that provision shall be deemed severed and the validity and enforceability of the remaining provisions of the terms and conditions shall not be affected.<br><br>
We may modify any of the Website Terms at any time by publishing the modified terms and conditions on the Website. Any modifications shall take effect 3 (three) days after posting on the Website. We may modify any of the Ticket Purchase Terms at any time by providing the modified terms each time you make a Booking. By using this Website and its services you agree to these terms and conditions at the time of use or of Booking, therefore you should check these terms each time you access the Website and make a purchase.<br><br>
All disclaimers, indemnities and exclusions in these terms and conditions shall survive termination of the agreement between us for any reason.<br>
English Law governs all of these terms and conditions and each party submits to the exclusive jurisdiction of the English courts.<br>
Neither party shall be liable to the other to the extent that any liability relates to an event over which that party has no control.<br>
The parties shall not assign this agreement to any other party without the other's consent. However, you are deemed to consent to an assignment if there is a trade sale or group reorganisation of Commerce Global Ventures Ltd.
No part of this agreement is enforceable by anyone who is not a party to it, pursuant to the Contracts (Rights of Third Parties) Act 1999 except the following terms and conditions may be enforced by the Event Organiser in their own rights; "Gaining admission to the Event", "Venue rules and regulations and Age restrictions", "Concessionary rates, right to admission".
</p></div>
        <div class="col-md-12 col-xs-12 policy-content">
      <h1>Event Organiser Terms and Conditions</h1>
      <p>Commerce Global Ventures Ltd and the Event Organiser agree that Commerce Global Ventures Ltd is appointed to act as a non-exclusive (save where we agree exclusivity in respect of an Event with you) agent to facilitate the sale of rights to admission to Events on behalf of the Event Organiser in the form of our unique Booking references, which are delivered electronically.<br><br>
By setting up an account with us you agree that you are the Event Organiser according to the definition below, and you will be deemed to have accepted and agreed to all these terms and conditions and obligations of the Event Organiser.<br><br>
You are responsible and liable for ensuring that anyone using the Website via your Event Organiser login details complies with these terms and conditions.<br><br>
In these terms and conditions;<br>
3 "you" or the "Event Organiser" means a third party supplier or suppliers of Tickets for the Events which may include a venue, performer, promoter or event organiser.<br>
4 "we" and "us" means Commerce Global Ventures Ltd (registered as a company in England and Wales, number 10521270), whose registered office is at Kemp House, 160 City Road, London, EC1V 2NX.<br>
5 "Customer" means an individual or individuals who purchase Tickets via our Website and any person on whose behalf a Booking has been made via our Website.<br>
6 "Event" means the individual event or events listed on our Website for which Tickets are available.<br>
7 "Tickets" or "Bookings" means rights to admission in the form of unique Booking references.<br>
8 "Website" means the website or websites on which we make Tickets available and from which we promote Events and the Event Organiser administration area or areas of these websites.<br>
9 "Ticket list" means the final list of all Tickets purchased through Commerce Global Ventures Ltd for an Event.
</p></div>
     <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading"> Event Organiser Obligations</h4>
    <p>You confirm that you are authorised to appoint us as your agent in respect of the Events and that all rights and permissions have been obtained to enable us to promote the Events.<br><br>
You, the Event Organiser, agree in a timely and efficient manner<br>
• To provide accurate, complete and up-to-date information about all Events for which you make Tickets available for sale via the Website, and to keep that information up to date. Such information includes but is not limited to the accurate description of the Event, Event date and start time, venue, location, venue opening times, age restrictions and any other relevant restrictions.<br>
• To bring to the attention of the Customers any additional restrictions and/or terms and conditions applicable to the Event which are not expressly stated in our Customer Terms and Conditions. You also authorise us to enter into the Customer Terms and Conditions on your behalf with the Customer.<br>
• To provide the accurate face value price information for all Tickets you make available for sale through the Website.<br>
• To make available a proportion of your Tickets for sale via the Website ("our allocation") and not to sell those Tickets by another method unless unsold Tickets are first removed from our allocation and the Website.<br>
• Not to make available through the Website and all other methods of sale more Tickets than the capacity of the Event.<br>
• Not to print the Ticket List before the Event has been taken off sale from the Website and not to print the Ticket List before the day of the Event (and not before Friday in the case of an Event held on Saturday or Sunday).<br>
• To ensure that competent personnel are in possession of the Ticket List at the entrance to the Event.<br>
• To check that all persons presenting themselves at the Event with our unique Booking references are named on the Ticket List and to confirm their identity to your satisfaction.
• To permit entry to all persons named on the Ticket List along with the specified number of accompanying persons. The named person must accompany all unnamed persons in the party for whom they have made a Booking.<br>
• To honour our unique Booking references and to treat them as conferring the same rights as printed tickets or any other form of booking available for the same Event.<br>
• Not to admit any person presenting themselves with our unique Booking reference who is not named on the Ticket List, or who cannot confirm to your satisfaction that they are the named person, including any person who has bought or otherwise obtained our unique Booking reference from any source other than ourselves.<br>
• Not to admit any person who we inform you has obtained a unique Booking reference fraudulently or in contravention of our Customer terms and conditions.<br>
• To accept our standard payment terms (as set out below), and to provide bank details so that payment can be made electronically.<br>
• To add links to the relevant pages on the Website and include these links in all relevant email marketing you do for your Events unless we expressly request otherwise of you.<br>
• To promote and advertise the availability of Tickets via the Website.<br>
• Not to upload or submit information to the Website which is untrue, defamatory, obscene or abusive or otherwise objectionable or in breach of any applicable laws or rights of third parties including, but not limited to, any copyright held by third parties in the Event images you upload for display on the Website.<br>
• That you have taken all reasonable precautions to ensure that any data you upload or otherwise submit to the Website is free from viruses and anything else which may have a contaminating or destructive effect on any part of the Website or any other technology.<br>
• Not to use the name, address, URL or any other details of the Website or any details of Commerce Global Ventures Ltd on illegal or unauthorised flyposting, or in any other publicity activities that may be illegal or contravene local by-laws or planning restrictions.
</p></div>
<div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading"> Ownership of Tickets</h4>
    <p>Commerce Global Ventures Ltd does not buy Tickets to the Event Organiser's Events, and title to the Tickets always remains with the Event Organiser.</p></div>
    <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Obligations of Commerce Global Ventures Ltd</h4>
    <p> Commerce Global Ventures Ltd will display the details of your Tickets on the Website but we do not guarantee that any or all of your Tickets will be purchased. We retain sole and complete discretion to decide the look and feel of the Website and the manner and length of time for which Events are publicised.<br>
 Commerce Global Ventures Ltd will obtain payment from the Customer of the face value of the Ticket as per the information provided by the Event Organiser and will forward the full face value of all sold Tickets to the Event Organiser according to our standard payment terms.<br>
 Commerce Global Ventures Ltd will not charge the Event Organiser for the service provided in accordance with these terms and conditions unless otherwise agreed.<br>
 Subject to the other provisions of these terms and conditions Commerce Global Ventures Ltd will not sell Tickets for more than the agreed face value.<br>
 Commerce Global Ventures Ltd charges the Customer a Booking fee as payment for the Booking services rendered. The Booking fee will be clearly itemised as a separate charge from the face value of the Ticket.<br>
 The Event Organiser will not be entitled to all or any part of the Booking fee.
</p></div>
<div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Refunds, cancellation, rescheduling of Events</h4>
    <p>If;<br>
• an Event is cancelled;<br>
• an Event is moved to another date and/or time; or<br>
• there is a material change to the acts scheduled to appear at an Event or to the venue (i.e. the Event is "significantly changed")<br>
you must inform us immediately with full, accurate and up-to-date details of that change.<br>
If an Event is cancelled, we will facilitate the refunding of the face value of the Tickets on your behalf, and deduct this from any monies payable to you. You agree to pay promptly to us sums to refund us such face values if we request you to.<br>
If an Event is moved to another date, or if details of the Event are significantly changed, we will attempt to advise the Customer by email that the Event has been rescheduled or significantly changed and that their Tickets are still valid for the rescheduled or significantly changed Event (which you hereby confirm that they will be). If the Customer chooses not to retain the Booking for the rescheduled or significantly changed Event, we will facilitate the refunding of the face value of the Tickets on your behalf, and deduct this from any monies payable to you. You agree to pay promptly to us sums to refund us such face values if we request you to.<br>
If a Customer decides to retain the Booking for the rescheduled or significantly changed Event then their Tickets shall still be valid and you shall still admit them to the Event in accordance with these terms and conditions.
</p></div>
    <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Standard Payment Terms</h4>
    <p>After the event has taken place, you will initiate an invoice for the total face value of your Tickets sold by us on the Website for the Event as shown on the final Ticket List. We process payments on Friday for the events that have taken place between the Monday and Sunday of the previous week. So, for example, if your event takes place between Monday and Sunday of this week, your payment will be processed on Friday of next week,<br><br>
We pay by Stripe payment direct to your nominated bank account. It is your responsibility to ensure that your correct bank details are provided. We accept no liability for payments that are lost as a result of you submitting incorrect bank details.<br><br>
We will not add VAT to, or deduct VAT from, the face value of the Tickets. Accounting for and payment of any VAT due on the Event Organiser’s Ticket sales through the Website is the obligation of the Event Organiser. We will not issue VAT receipts for the Ticket face value. The Event Organiser agrees to provide a VAT receipt to Customers who request one, if the Event Organiser is registered for VAT. The Booking fee will be stated as inclusive of VAT where appropriate, and we will account for and pay any VAT due on the Booking fee.<br><br>
We will pay all payment processing charges incurred in the Customer transactions, and will not charge the Event Organiser for these unless otherwise agreed.
<br><br>
Payment by us is without prejudice to any claims or rights which we may have against you and shall not constitute any admission by us to the performance by you of your obligations under these terms and conditions. Prior to making such payment, we shall be entitled to make deductions or deferments in respect of any disputes or claims whatsoever with or against you.
</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Breach of these Terms and Conditions</h4>
    <p>Without prejudice to any right or remedy we may have against you for breach or non-performance of these terms and conditions, we may, with immediate effect, terminate the display of your Events on the Website on or at any time after the happening of any of the following circumstances:<br>
• if you make any voluntary arrangement with your creditors or become bankrupt or become subject to an administration order or go into liquidation or we reasonably apprehend that any of the circumstances mentioned above is about to occur;<br>
• if you for any reason whatsoever are substantially prevented from performing or become unable to perform your obligations; or<br>
• if you or third parties connected to you place offending material on the Website or otherwise misuse the Website.
</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Access to the Website and Content</h4>
    <p>We will endeavour to allow uninterrupted access to the Website, but access to it may be suspended, restricted or terminated at any time.
We reserve the right to change, modify, substitute or remove without notice any information on the Website from time to time.
</p></div>
          <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Intellectual Property</h4>
    <p>The copyright, trademarks and all other intellectual property rights in the material contained in the Website, together with the Website design, text and graphics, and their selection and arrangement, and all software compilations, underlying source code and software (including applets) and in the Ticket List belongs to us or is currently licensed to us, our subsidiaries or the providers of such information. All rights are reserved. None of this material may be reproduced or redistributed without our express written permission. You may, however, download or print a single copy for your own non-commercial off-line viewing.<br>
You shall retain ownership of all copyright in data you submit to the Website. You grant us a worldwide exclusive, royalty-free, non-terminable licence to use, copy, distribute, publish and transmit such data in any manner.
</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Confidentiality</h4>
    <p>You undertake to keep confidential all information about our business which is disclosed under an explicit or implied duty of confidentiality, including (without limitation) that which is stamped confidential and/or which relates to our Customers, other partners, accounts and technology. This provision shall not apply in respect of information which comes into the public domain other than as a result of an obligation of confidentiality or disclosure of which is expressly required by law provided that you have notified the same to us in advance.</p></div>

    <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Data Protection</h4>
    <p>For the purposes of this clause, references to the "Data Protection Legislation" shall refer to all applicable laws and regulations relating to the processing of personal data and privacy including (but not limited to) the General Data Protection Regulation 2016/679/EU and the Privacy and Electronic Communications (EC Directive) Regulations 2003 (SI 2426/2003), together with all codes of practice and other guidance on the foregoing issued by the Information Commissioner's office, all as amended, updated or re-enacted from time to time. The terms 'personal data', 'data subject', 'processor', 'controller', 'processing' and 'personal data breach' have the meanings set out in the Data Protection Legislation.<br>
Both we and you are data controllers with respect to the personal data included in any Ticket List we send you. This sharing of personal data is necessary to permit the identification of Customers at Events, and to permit the Event Organiser to send marketing communications to Customers where they have opted into receiving such communications. We will indicate whether or not a Customer has opted in.<br>
Both we and you shall comply with our obligations under the Data Protection Legislation. Both you and we shall:<br>
a take appropriate technical and organisational measures to keep the relevant personal data safe;
b respond promptly to any requests by data subjects to exercise their rights, or provide reasonable assistance to the other party to do the same;<br>
c not transfer relevant personal data to an unconnected third party, save where the transfer is carried out in accordance with a written contract which imposes suitable obligations of security and confidentiality on the third party; and<br>
d inform the other party without undue delay in the event of a personal data breach.
We shall:<br>
a ensure that the personal data we share with you is not irrelevant or excessive with respect to the purposes for which we are sharing it with you; and<br>
b ensure that (to the extent of our reasonable knowledge and belief) the personal data we share with you is accurate.<br>
You shall:<br>
a only process the personal data we share with you for the purposes listed above; and<br>
b not send marketing communications to those Customers who have not specifically opted into receiving them.
</p></div>
<div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Liability</h4>
    <p>We use reasonable endeavours to ensure that the data on the Website is accurate and to correct any errors or omissions as soon as practicable after being notified of them. We do not monitor, verify or endorse information submitted by third parties including Event Organisers for posting on the Website. To the extent permitted by applicable law, we disclaim all warranties and representations (whether express or implied) as to the accuracy of any information contained on the Website. We do not guarantee that the Website will be fault free and do not accept liability for any errors or omissions.<br>
Due to the nature of electronic transmission of data over the Internet, any liability we may have for any losses or claims arising from an inability to access the Website, or from any use of the Website or reliance on the data transmitted using the Website, is excluded to the fullest extent permissible by law. In no event shall we be liable for any indirect loss, consequential loss, loss of profit, data, revenue, business opportunity, anticipated savings, goodwill or reputation whether in contract, tort or otherwise arising out of or in connection with these terms and conditions or use of the Website save where such liability cannot be excluded by law. In respect of all other loss or damage arising under or in connection with these terms and conditions our liability is (to the maximum extent permitted by law) limited to £25,000 per claim.<br>
We do not give any warranty that the Website is free from viruses or anything else which may have a harmful effect on any technology and you should take your own safeguards in this area.
</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Username and Password</h4>
    <p>On registering with us, you will choose your unique username and an associated password, which must be used in order to access certain restricted parts of the Website. The username and password are personal to you and are not transferable.<br>
Your username and password are the methods used by us to identify you and so are very important. You are responsible for all information posted on the Website by anyone using your username and password. Any breach of security of a username and password should be notified to us immediately.<br>
You may not adapt or circumvent the systems in place in connection with the Website, nor access the Website other than through normal operations.
</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Data Submitted by Event Organisers</h4>
    <p>We reserve the right (without limiting our rights to seek other remedies) to remove offending material placed on the Website that (after it has been bought to our attention) we consider to constitute a misuse of the Website or which is otherwise harmful to other users of the Website.</p></div>
          <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
    <h4  class="policy-sub-heading">Indemnity</h4>
    <p>You undertake to indemnify us and keep us indemnified fully at all times against all claims, demands, actions, proceedings, damages, losses (including without limitation, economic loss), costs and expenses made or brought against or incurred by us which are attributable either directly or indirectly to any act default or omission on your part in carrying out your obligations under these terms and conditions, including but not limited to failing to provide the correct Ticket pricing and Event information, failure to inform the Customer of additional conditions applicable to the Event, use of Our details in any illegal publicity and in respect of any third party claim for breach of their rights arising as a result of our appointment under these terms and conditions. We reserve the right to deduct from money due to you the costs and expenses of any such claims, demands, actions, proceedings that may be made against us.</p></div>
    <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Variations</h4>
        <p>We may modify any of these terms and conditions at any time by publishing the modified terms and conditions on the Website. Any modifications shall be effective unless you object to them within 5 (five) days of the modified terms and conditions being published on the Website.</p></div>
        <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">General</h4>
        <p>The rights, powers and remedies conferred on any party by these terms and conditions and remedies available to any party are cumulative and are additional to any right, power or remedy which it may have under general law or otherwise.<br>
Either party may, in whole or in part, release, compound, compromise, waive, or postpone, in its absolute discretion, any liability owed to it or right granted to it in these terms and conditions by the other party without in any way prejudicing or affecting its rights in respect of that or any other liability or right not so released, compounded, compromised, waived or postponed.<br>
No single or partial exercise, or failure or delay in exercising any right, power or remedy by any party shall constitute a waiver by that party of, or impair or preclude any further exercise of, that or any right, power or remedy arising under these terms and conditions or otherwise.<br>
If any provision of any of the terms and conditions on the Website is held to be unlawful, invalid or unenforceable, that provision shall be deemed severed and the validity and enforceability of the remaining provisions of the terms and conditions shall not be affected.<br>
These terms and conditions set out the entire agreement and understanding between the parties in respect of the subject matter of the terms and conditions.<br>
English Law governs all of these terms and conditions and each party submits to the exclusive jurisdiction of the English courts in respect of any dispute arising under these terms and conditions.
All disclaimers, indemnities and exclusions in these terms and conditions shall survive termination of the agreement between us for any reason.<br>
Neither party shall be liable to the other to the extent that any liability relates to an event over which that party has no control.<br>
The parties shall not assign these terms and conditions to any other party without the other's consent. However, you are deemed to consent to an assignment if there is a trade sale or group reorganisation of Commerce Global Ventures Ltd.<br>
No part of these terms and conditions is enforceable by anyone who is not a party to it, pursuant to the Contracts (Rights of Third Parties) Act 1999 except the following terms and conditions may be enforced by the Customer in their own right; "Event Organiser's Obligations" and "Refunds, cancellation, rescheduling of Events".
</p></div>
         <div class="terms-conditionsdiv col-md-12 col-xs-12 policy-content">
        <h4  class="policy-sub-heading">Contact details</h4>
        <p>We can be contacted via our registered office:<br>
Commerce Global Ventures Ltd<br>
Kemp House<br>
160 City Road<br>
London<br>
EC1V 2NX<br>
UK<br>
Company Registration Number: 10521270<br>
Email: info@petalevents.co.uk
</p></div>
    </div>
   </div>
 </section>
</main>
<!--content-section-end-->
@endsection