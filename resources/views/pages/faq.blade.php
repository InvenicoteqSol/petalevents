@extends('layouts.frontdefault')
@section('content')
<!--content-section-start-->
<main>
	<section class="section-faq">
   <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12 col-head-area">
        <h2 class="main-s-heading">Frequently Asked Questions</h2>
    </div>
</div>
		 <!-- row -->
    <div class="row">
     <div class="col-md-12 col-sm-12 col-xs-12">

<!--  panel-group -->

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <!--  panel panel-default panel-faq -->
  <div class="panel panel-default panel-faq">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        	How do I receive my ticket?
        	 </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      	We provide e-tickets therefore you will receive your ticket to your email address. This will be fine to show on your phone at the event venue, however you may print your ticket if you wish.
      	</div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
     <!--  panel panel-default panel-faq -->
  <div class="panel panel-default panel-faq">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        	I haven’t received my ticket?
        	</a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
      	Please allow up to 10 minutes for your tickets to arrive into your inbox. If you still haven’t received your tickets after this period, please check that the correct email address has been associated to your Petal Events account.Also, check your junk mail. If there is still no sign of the ticket, please check your bank account to see if the transaction went through and then check 'My Tickets'.
      	 </div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
    <!--  panel panel-default panel-faq -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        	Where can I find my tickets?
        	</a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
 <div class="panel-body">
 	You can find your tickets in 'My Tickets'. Alternatively, you will have been sent your tickets to the email address associated with your Petal Events account.
 	 </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->

  <!--  panel panel-default panel-faq -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading4">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
         How do I get a free booking fee?
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
 <div class="panel-body">
        Event organisers have the option to make their tickets free of the standard 10% booking fee. In these cases, the Event Organisers will promote this to their potential customers.
      </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
      <!--  panel panel-default panel-faq -->
      	 <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading5">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
        	What Ticket Discounts are available?
        	 </a>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
      <div class="panel-body">
      	Event organisers have the ability to offer the following discounts to their event tickets:<br>
      	<br><h4>Discount Codes</h4><br>
Some tickets may have discount codes providing percentages or amounts off the standard ticket price.
<br><br>
<h4>Early Bird Discounts</h4><br>
Some tickets may offer discounted prices for early purchases.<br><br>

<h4>Free Ticket Bundles</h4><br>
Some tickets may provide the free tickets on top of those purchased (for example, buy 5 tickets and receive a further 1 ticket free).<br><br>

In all of these cases, the event organisers will notify their customers of the available discount offers.

      	</div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
     <!--  panel panel-default panel-faq -->
     <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading6">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
        	What are Ticket Collaborations?
        	</a>
      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
 <div class="panel-body">
 	Event organisers have the ability to offer discounts if tickets are purchased for more than one of their events.<br><br>
Event organsiers are also able to to collaborate with other organisers to offer discounts if tickets are purchased for both of their events.<br><br>
In both of these cases, the event organisers will notify their customers of the available discount offers.
 	 </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading7">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
        	How do I get a refund?
        	</a>
      </h4>
    </div>
    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
 <div class="panel-body">
 	Refund policy: All sales are final unless the event has been cancelled. It is your responsibility to ensure that you comply with all restrictions and terms and conditions as outlined.<br><br>
For refund requests to do with events, especially if you are refused entry for no fault of your own, you must email info@petalevents.co.uk by 5pm on the next working day to request a refund, with a full explanation as to why you were refused entry.<br><br>
Requests received after this time will not be considered. Where possible, please make every attempt to ensure that your entry refusal has been acknowledged and recorded by the event organiser as evidence before leaving the event.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading8">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
        	Where’s my refund?
        	</a>
      </h4>
    </div>
    <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
 <div class="panel-body">
 	When your refund is processed you will receive a refund receipt, it can take up to 5-10 working days for your bank account to receive the funds, this is dependent on your bank provider. Please be aware that bank holidays/weekends are not considered working days.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading9">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
        	I've heard an event is cancelled what do I do?
        	</a>
      </h4>
    </div>
    <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
 <div class="panel-body">
 	Please be aware that if an event is cancelled on PetalEvents.co.uk you should always receive a direct email from notifications@petalevents.co.uk with a title starting 'Event Cancellation Notice...'.<br><br>
Please check the inbox or junk mail folder of the email address associated with your Petal Events account.<br><br>
If you have not received any emails from Petal Events, please contact us immediately at info@petalevents.co.uk so that we can investigate this matter.<br><br>
If you have received any contact from the event organiser (eg, text, email, social media post) advising of a cancellation, please contact us at info@petalevents.co.uk immediately so that we can investigate this matter, as events must always be cancelled online.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading10">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
        	The venue/date/time of an event I bought a ticket to has changed. What can I do?
        	</a>
      </h4>
    </div>
    <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
 <div class="panel-body">
 	Under these conditions, you may be entitled to a refund. You would have received an email with this information via the email address you used to book the tickets. Please reply to that email if you need to contact us. If not, you can contact us on info@petalevents.co.uk.
 	 </div>
 </div>
  </div>
  <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading11">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
        	I tried to make a booking but it was declined/expired.
        	</a>
      </h4>
    </div>
    <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
 <div class="panel-body">
 	If a booking expires it means it was not successful, so no tickets are sent. If monies look like they have left your account, this is in fact a banking error and the amount is 'pending' and has not left your account. This will rectify itself in a few hours.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading12">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
        	I tried to make a booking but was redirected back to the 'Confirm Booking for..' page. Did my booking go through?
        	</a>
      </h4>
    </div>
    <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
 <div class="panel-body">
 	No, if you are taken back to the 'Confirm Booking for...' page where you have to re-enter your name and email address unfortunately you booking was not successful, as no tickets have been sent. You can always check 'My Tickets' for tickets, as if your booking is successful tickets will show here to be downloaded. If monies look like they have left your account, this is in fact a banking error and the amount is 'pending' and has not left your account. This will rectify itself in a few hours.
 	 </div>
 </div>
  </div>
 <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading13">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="false" aria-controls="collapse13">
        	I saw a 'Card Declined' pop up when I tried to complete my booking. Did my payment go through?
        	</a>
      </h4>
    </div>
    <div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
 <div class="panel-body">
 	No, if you saw this message your booking did not go through and tickets have not been sent. If monies look like they have left your account, this is in fact an error, the amount is 'pending' and has not left your account. This will rectify itself in a few hours. You can always check 'My Tickets' for tickets, as if your booking is successful tickets will show here to be downloaded.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading14">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="false" aria-controls="collapse14">
        	Do I have to collect tickets from the Box Office?
        	</a>
      </h4>
    </div>
    <div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
 <div class="panel-body">
 	For certain events i.e. concerts you may have to collect hard copy tickets at the venue's Box Office - an email would have been sent to the email address you used to book tickets. In these cases your e-ticket acts as a receipt which must be exchanged for hard copy tickets at the box office.<br><br>
Please take the e-ticket to the box office to get your hard copy tickets, as you are only permitted entry with hard copy tickets.<br><br>
Please ensure that you bring relevant forms of personal ID - must be photo I.D. that is a full/provisional driving licence or passport. Other forms of I.D. may be rejected.<br><br>
The main ticket buyer must be present to collect the tickets, and their full name must match their I.D. must be photo I.D. that is a full/provisional driving licence or passport. Other forms of I.D. may be rejected.<br><br>
Depending on the venue please double check the Box Office opening/close times as you must arrive during this time. Failure to do so can result in non-entry.
 	 </div>
 </div>
  </div>
  <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading15">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="false" aria-controls="collapse15">
        	I received a 'Payment Declined' email but the monies have left my account.
        	</a>
      </h4>
    </div>
    <div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
 <div class="panel-body">
 	If you receive a 'Payment Declined' notice, we can assure you that the payment is certainly declined. Please note that although your transaction has been declined, your bank may see this as a "pending charge". However, this will automatically rectify itself in a few hours and the pending charge will disappear.
 	 </div>
 </div>
  </div>
 <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading16">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse16" aria-expanded="false" aria-controls="collapse16">
        	My Password isn’t working.
        	</a>
      </h4>
    </div>
    <div id="collapse16" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
 <div class="panel-body">
 	If you have forgotten your password, please use the ‘Forgot your password?’ option on the sign in page. You will be required to enter your registered email address in order to retrieve/reset your password.<br>
 	 </div>
 </div>
  </div>
<!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading17">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse17" aria-expanded="false" aria-controls="collapse17">
        	I’m still having trouble, how can I contact support?
        	</a>
      </h4>
    </div>
    <div id="collapse17" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
 <div class="panel-body">
 	If the FAQs have been unable to answer your question, you can contact us via the Contact Us enquiry form or by emailing is at <a href = "mailto: info@petalevents.co.uk.">info@petalevents.co.uk.</a>
 	 </div>
 </div>
  </div>
 <!--  ///panel panel-default panel-faq -->
</div>
<!--End panel-group -->
</div>
</div>
</div>
</section>
	</main>
<!--content-section-end-->
@endsection