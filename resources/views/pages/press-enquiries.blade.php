@extends('layouts.frontdefault')
@section('content')
<!--content-section-start-->
<main>
 <section class="signup_section form_section innercontentpart p-policy">
   <div class="container">
    <div class="innercontent_text row">
      <div class="col-md-12 col-xs-12 policy-content">
      	<h1 class="main-s-heading">Press Enquiries</h1>
      	<p>For all press enquiries, please email us at <a href="mailto:Press@PetalEvents.co.uk.">Press@PetalEvents.co.uk.</a></p>


      </div>
      	</div>
</div>
</section>
	</main>
<!--content-section-end-->
@endsection