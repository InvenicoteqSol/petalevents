@extends('layouts.innerdefault')
@section('content')

 <!-- CONTACT SECTION -->
<section id="organiser-register">
     <div class="container">
          <div class="row">
      <p style="text-align: center;">@include('layouts.flash-message') </p>

             <div class="col-md-12 col-sm-12 col-xs-12 col-head-area">
        <h2 class="main-s-heading">Organiser Registration</h2>
        <p>Please enter the below details to register for the promotion of your events and tickets.</p>
      </div>
             </div>
             <div class="row">

               <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- CONTACT FORM HERE -->
                              <form id="contact-form" class="settingform" action="{{ url('organiserregistration/insert') }}" method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}

                              <div class="col-md-6 col-sm-6">
                               <!--  form-group -->
                                <div class="form-group">
                                   <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" placeholder="First name*" required>
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                   <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" placeholder="Last name*" required="">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="organisation_name" value="{{ old('organisation_name') }}" placeholder="Organisation Name*" required="">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="website" value="{{ old('website') }}" placeholder="Organisation website" >
                                 </div> 
                                 </div>                                 
                                 <!--  //form-group -->                                
                                <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" value="" maxlength="191" placeholder="Email*" required="">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                  <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" value="" minlength="11" maxlength="13" placeholder="Phone*" required="" pattern="[0-9]{11}">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                   <input type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1') }}" placeholder="Address line 1*" required="">
                                 </div> 
                                 </div>                                 
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                   <input type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2') }}" placeholder="Address line 2" >
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="col-md-6 col-sm-6">
                                 <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                   <select class="form-control" id="country" name="country" required="" aria-required="true">
                                    <?php  $countries = DB::table('all_towns')->orderBy('town_name','asc')->pluck('id', 'town_name'); ?>
                                    <option value="">Select City/Town*</option>
                                    @foreach ($countries as $countryName=>$countryId)
                                        <option value="{{ $countryId }}" {{ ( old('country') == $countryId ) ? 'selected' : '' }}>{{ $countryName }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                                 </div>   
                                 </div>                               
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                  <input type="text" class="form-control" name="postcode" value="{{ old('postcode') }}"  placeholder="Postcode*" required="" pattern="^([A-PR-UWYZ0-9][A-HK-Y0-9][AEHMNPRTVXY0-9]?[ABEHMNPRVWXY0-9]? {1,2}[0-9][ABD-HJLN-UW-Z]{2}|GIR 0AA)$">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                  <input id="username" type="text" class="form-control" name="username" minlength="6" maxlength="17" placeholder="Username*" required="">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                 <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" placeholder="Password*" required="">
                                 </div>
                               </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                 <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                 <input id="security_question" type="text" class="form-control" name="security_question" minlength="6" maxlength="17" placeholder="Security question*" required="">
                                 </div>  
                                 </div>                                
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="col-md-6 col-sm-6">
                                  <div class="form-group">
                                  <input id="security_answer" type="text" class="form-control" name="security_answer" minlength="6" maxlength="17" placeholder="Security answer*" required="">

                                  <input type="hidden" value="4" name="user_type" id="user_type"> 
                                 </div> 
                                 <!--  //form-group -->
                                </div>
                              
                              <div class="col-md-12 col-sm-12 form-action">
                              
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="{{ url('/login') }}" class="btn btn-primary">Sign In</a>
                              
                              </div>
                        </form>
                   
               </div>


          </div>
     </div>
</section>
@endsection
