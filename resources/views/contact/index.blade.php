@extends('layouts.innerdefault')
@section('content')
<!-- CONTACT SECTION -->
<section id="contact" class="section-contact">
     <div class="container">
          <div class="row">

               <div class="col-md-12 col-sm-12">
                
                @include('layouts.flash-message')

                    <!-- SECTION TITLE -->
                    <div class="contact-heading">
                         <h2>Get In Touch With Us</h2>
                         <p>Please refer to our FAQs for any questions or issues.
If you need further help, contact us via the form below, we will contact you as soon
as possible.</p>
                    </div>
               </div>
             </div>
             <div class="row">

               <div class="col-md-7 col-sm-10">
                    <!-- CONTACT FORM HERE -->
                    <div class="wow fadeInUp" data-wow-delay="0.4s">
                      {!! Form::open(array('route' => 'contact.store','method'=>'POST','class' =>'contact-form','id' =>'contact-form','files' => true)) !!}

                              <div class="col-md-6 col-sm-6">
                                   <input type="text" class="form-control" name="name" placeholder="Name" required="">
                              </div>
                              <div class="col-md-6 col-sm-6">
                                   <input type="email" class="form-control" name="email" placeholder="Email" required="">
                              </div>
                              <div class="col-md-12 col-sm-12">
                                   <textarea class="form-control" rows="5" name="message" placeholder="Message" required=""></textarea>
                              </div>
                              <div class="col-md-12 col-sm-12">
                                   <button id="submit" type="submit" class="form-control" name="submit">Send Message</button>
                              </div>
                        
                    {!! Form::close() !!}
                    </div>
               </div>

               <div class="col-md-5 col-sm-8">
                    <!-- CONTACT INFO -->
                    <div class="contac-info">
                         <div class="section-title">
                              <h2>Contact Info</h2>
                              <p>Do you have an enquiry?</p>
                            
                         </div>
                        
                           <!--  col-address -->
                         <div class="col-address">
                          <span class="a-icon"><i class="fa fa-comment"></i> </span>
                          <span class="a-content">
                              <label>Send Us An Email</label>
                             <p><a href="mailto:info@petalevents.co.uk">info@petalevents.co.uk</a></p>
                          </span>
                         </div>
                         <!--  //col-address -->
                          <!--  col-address -->
                         <div class="col-address col-smedia">
                         
                          <span class="a-content">
                              <label>Join Us On Social Media</label>
                              <ul>
                              <li><a href="http://facebook.com/PetalEventsUK" target="_blank"><span class="fa fa-facebook"></span></a></li>
                              <li><a href="http://twitter.com/petalevents" target="_blank"><span class="fa fa-twitter"></span></a></li>
                              <li><a href="http://instagram.com/petal.events" target="_blank"><span class="fa fa-instagram"></span></a></li>
                              <li><a href="https://www.youtube.com/channel/UCMX8YneayHaK3YjPcvKbDOQ" target="_blank"><span class="fa fa-youtube-play" target="_blank"></span></a></li>
                            </ul>
                          </span>
                         </div>
                         <!--  //col-address -->
                      </div>
               </div>

          </div>
     </div>
</section>


@endsection
