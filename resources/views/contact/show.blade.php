@extends('layouts.default')
@section('content')
@include('layouts.flash-message')
<!--content-section-start-->
<main>

 <section class="innercontentpart"> 
    <div class="innercontent_text">
       <div class="container">
        <div class="singleview_content">
      <h1>{{ $post->post_name }}</h1>
        <h3><?php
                                if($post->occassions!='') {
                                     $catnameArray = array(); 
                                     $ocsioncatArr = explode(',', $post->occassions);
                                    foreach($ocsioncatArr as $ocsioncatid) {
                                      $catNames = DB::table('categories')->where('id', '=', $ocsioncatid)->get();
                                      foreach($catNames as $ctName) {
                                          $catnameArray[] = $ctName->cat_name;
                                         }
                                      }
                                       echo implode(', ', $catnameArray);  
                                      }
                                     ?></h3>
        <p>{{ $post->description }}</p>

<div class="album_carousel">
  <div class="container">

    @if($post->feature_images!='') 
                                    <img src="{{ url('public') }}/uploads/post/feature_post/{{ $post->feature_images }}" class="img-responsive>
                                     @else
                                    <img src="{{ url('public') }}/uploads/photoclassimages/photoclass_default_image.png" class="img-responsive img-circle" style="width: 100px; height: 100px;">
                                 @endif
  
   <div class="outside">
  <!-- <span id="slider-prev"><i class="fa fa-angle-left"></i></span> <span id="slider-next"><i class="fa fa-angle-right"></i></span>  -->
</div>
        <div class="sharediv">
          <p><span>Share</span><i class="fa fa-facebook"></i><i class="fa fa-skype"></i></p>

        </div>

    <?php $id= $post->id;
    $comments = DB::table('comments')
                        ->where('deleted', 0)
                        ->where('status', 1)
                        ->where('blog_id', '=', $id)
                        ->get();
                   ?>
    <div class="col-md-12">

        @foreach($comments as $cmt)
        <div class=" ">
            
            <textarea id="co_comments" name="co_comments" class="form-control" rows="2" cols="50" readonly="">{{ $cmt->co_comments }}</textarea>
           <span class="postdate">By:-{{ $cmt->co_name }} , Date:-{{ date('F d, Y', strtotime($cmt->created_at)) }}</span>
           
          </div>
       
       
      
      @endforeach
      
    </div>

     










        <div class="message_sec">
 {!! Form::open(array('route' => 'blog.store', 'method'=>'POST', 'id' => 'blogcfrm',  'class' => 'edt_blog_frm formarea', 'files' => true)) !!}


 <div class="col-md-12 nopadding">
  <div class="form-group nopadding">
    <label>Leave a Comment</label>
   

         <div class="form-group{{ $errors->has('co_comments') ? ' has-error' : '' }}">
                         
                         
                                
                            <div class="col-md-12">
                                 <textarea id="co_comments" name="co_comments" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ @old(co_comments) }}</textarea>

                                @if ($errors->has('co_comments'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('co_comments') }}</strong>
                                    </span>
                                @endif
                            </div>
                       

                        <div class="col-md-12 nopadding">


                     <div class="form-group{{ $errors->has('co_name') ? ' has-error' : '' }}">
                            <div class="col-md-4 ">
                                <input type="text" class="form-control" id="co_name" name="co_name" value="{{ old('co_name') }}" minlength="2" maxlength="191" placeholder="Name" required autofocus>
                                @if ($errors->has('co_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('co_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('co_email') ? ' has-error' : '' }}">
                            
                            <div class="col-md-4">
                                <input id="co_email" type="co_email" class="form-control" name="co_email" value="{{ old('co_email') }}" maxlength="191" placeholder="E-Mail" required>

                                @if ($errors->has('co_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('co_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                     <div class="form-group{{ $errors->has('co_websiite') ? ' has-error' : '' }}">
                           
                            <div class="col-md-4 inputspace">
                                <input type="text" class="form-control" id="co_websiite" name="co_websiite" value="{{ old('co_websiite') }}" minlength="2" maxlength="191" placeholder="Website" required autofocus>
                                @if ($errors->has('co_websiite'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('co_websiite') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('blog_id') ? ' has-error' : '' }}">
                            <div class="col-md-4 ">
                                <input type="hidden" class="form-control" id="blog_id" name="blog_id" value="{{$post->id}}" minlength="2" maxlength="191" placeholder="Name" required autofocus>
                                @if ($errors->has('blog_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       </div>

                       <div class="form-group">
                            <div class="col-md-6 buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Submit Comments
                                </button>
                            </div>
                        </div>
                        

                       {{ Form::close() }}
   
  </div>
</div>
</form>
        </div>

             <?php $posts = DB::table('posts')
                        ->where('deleted', 0)
                        ->where('status', 1)
                        ->orderBy('id','DESC')
                        ->take(3)
                        ->get();
                   ?>
<div class="related_posts">
  <div class="container">
  <div class="row">
    <div class="col-sm-12 relatedpost_div">
      <h1>Suggested Post</h1>
        @foreach($posts as $query)
        <div class="col-sm-4 relatedpost_blk">
        <a href="{{ route('blog.show',$query->id) }}" target="_blank">
        @if($query->feature_images!='' && $query->feature_images!='NULL')
         <img src="{{ url('public') }}/uploads/post/feature_post/{{$query->feature_images}}" class="img-responsive" style="width: 300px; height: 200px;" >
        @else
         <img src="{{ url('public') }}/images/classesdummy.jpg" class="img-responsive" style="width: 300px; height: 300px;">
        @endif
      </a>
        <div class="col-sm-12 nopadding">
           <div class="col-sm-6 pricesec nopadding text-right">
            <p class="poname">{{ $query->post_name }}</p>
            <span class="postdate">{{ date('F d, Y', strtotime($query->created_at)) }}</span>
          </div>
        </div>
       
      </div>
      @endforeach
      
    </div>

</div>
</div>
  </div>

    </div>
</div>

</div>
 </section>
</main>
<!--content-section-end-->
<script type="text/javascript">
  jQuery(function($){
  
var slider = $('.bxslider').bxSlider({
 minSlides: 1,
  moveSlides:1,
  maxSlides: 3,
   pager:false,
  controls:false,
  slideWidth: 280,
  slideMargin: 0,
    onSlideBefore : function($slideElement, oldIndex, newIndex){ // your code here 

$(".bxslider li").removeClass("active-slide");
        $slideElement.next().addClass("active-slide");
},
    onSliderLoad:function(index){
       
$(".bxslider li").eq(1+$(".bxslider .bx-clone").length/2).addClass("active-slide");
    }
});
   
    
$('#slider-next').click(function(){
  slider.goToNextSlide();
    
  return false;
});
$('#slider-prev').click(function(){
  slider.goToPrevSlide();
  
  return false;
});

});
</script>
<style>
p.poname {
    padding-left: 112px;

}
input#co_name {
   
    margin-top: 15px;
}
label {
    padding-left: 16px;
}
</style>
@endsection