<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>PETAL EVENTS</title>

    <!-- Styles -->
    <link href="{{ url('/public') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/style.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/full-slider.css" rel="stylesheet">
    <link href="{{ url('public') }}/css/table-css.css" rel="stylesheet">
    <link href="http://bxslider.com/lib/jquery.bxslider.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">
    <link href="{{ url('public') }}/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
    <link href="{{ url('public') }}/css/mobiscroll.javascript.min.css" type="text/css" rel="stylesheet" />
    <script src="{{ url('/public') }}/js/mobiscroll.javascript.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ url('/public') }}/js/bootstrap.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery.bxslider.js"></script>
    <script src="{{ url('/public') }}/js/jquery.validate.min.js"></script>
    <script src="{{ url('/public') }}/js/jquery-validate.bootstrap-tooltip.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Karla%7CPT+Serif" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Serif:400,400i,700,700i" rel="stylesheet">

     

</head>
<body>
    <div id="app">

      @include('layouts.header')

      @yield('content')

      @include('layouts.footer2')
      
    </div>

    <!-- Scripts -->
    <!-- <script src="{{ url('public') }}/js/app.js"></script> -->
</body>
</html>

