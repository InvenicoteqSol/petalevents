<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
     <title>Petal Events</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
    <script>
      addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
        window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <link href="{{ url('/public') }}/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <link rel="stylesheet" href="{{ url('/public') }}/front/css/owl.carousel.css">
    <link rel="stylesheet" href="{{ url('/public') }}/front/css/owl.theme.default.css">
    <link href="{{ url('/public') }}/front/css/jquery-sakura.css" rel="stylesheet" type="text/css">
    <!-- font-awesome icons -->
   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{ url('/public') }}/front/css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
   <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!--js working-->
  
    
    
</head>
<body>
    

      @include('layouts.header')

      @yield('content')

      @include('layouts.footer')
      
    

    <!-- Scripts -->
    <!-- <script src="{{ url('public') }}/js/app.js"></script> -->
</body>
</html>

