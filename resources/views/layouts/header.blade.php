<div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <!--//navigation section -->
          <nav class="navbar navbar-expand-lg navbar-light">
            <div class="hedder-up " >
              <h1><a class="navbar-brand" href="{{url('/')}}">
                <img class="imglogo" src="{{ url('/public') }}/front/images/logo.png">
              </a>
              </h1>
            </div>
            <div class="seventyper">
              <div class="logos">
              <ul>

                @if( Auth::user() !=null)
                   @if(Auth::user()->user_type=='1')
                <a href="{{ url('/admindashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->name}}</a>
                @endif
                @if(Auth::user()->user_type=='3')
                  <a href="{{ url('/customerdashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->first_name}}</a>
                @endif
                @if(Auth::user()->user_type=='4')
                  <a href="{{ url('/organiserdashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->first_name}}</a>
                @endif
                @else
                <a href="{{ url('/login') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> Customer Login</a>
                @endif
                <li class="dropdown dropdown-cart">
                 <a href="#" class="dropdown-toggle"id="cartdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Cart</a>
                 <a href="{{route('cart.index')}}"><span class="alert badge" id="cartCount">{{Cart::count()}}</span></a>
                <ul class="dropdown-menu" aria-labelledby="cartdropdown" id="cartbody">
                
              </ul>
              </li>
             </ul>
              </div>
              <div class="tgl">
                <img class="imglogo1"src="{{ url('/public') }}/front/images/logo.png">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
               <ul class="navbar-nav ">
               
               
                <li class="nav-item">
                  <a href="{{url('/featured-events')}}" class="nav-link">Featured Events</a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('/ticket-deals') }}" class="nav-link">Ticket Deals</a>
                </li>
                 <li class="nav-item">
                  @if( Auth::user() !=null)
                   @if(Auth::user()->user_type=='1')
                <a href="{{ url('/eventbookings/create') }}"> <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                @if(Auth::user()->user_type=='3')
                  <a href="{{ url('/eventbookings/create') }}"> <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                @if(Auth::user()->user_type=='4')
                  <a href="{{ url('/eventbookings/create') }}"><button type="button" class="btn btn-danger">Create Event</button> </a>
                @endif
                @else
                <a href="{{ url('/organiserregistration') }}">
                  <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                </li>
              </ul>
            </div>
          </div>
          </div>
          </nav>
          <div class="clearfix"> </div>
        </div>
      </div>
      <!--//navigation section -->
      <div class="banner-slide-img text-center">
        <div class="banner-bride-name">
          <h4>Event Tickets Online & Mobile</h4>
        </div>
        <div class="banner-groom-name">
          <h5>The Petal Events Ticket Platform</h5>
        </div>
        <div class="searchitem">
          <div class="row megh">
        <div class="col">
            <div id="imaginary_container"> 
              <form name="search" id="eventsearch" method="POST" action="{{ url('event-listings') }}">
                {{ csrf_field() }}
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control" id="search_input" name="search_input"  placeholder="EVENT NAME, LOCATION" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <!-- <span class="glyphicon glyphicon-search"></span> -->
                            Find Event
                        </button>  
                    </span>
                </div>
              </form>
            </div>
        </div>
  </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>