<div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <!--//navigation section -->
          <nav class="navbar navbar-expand-lg navbar-light">
            <div class="hedder-up " >
              <h1><a class="navbar-brand" href="{{ url('/') }}">
                <img class="imglogo" src="{{ url('/public') }}/front/images/logo.png">
              </a>
              </h1>
            </div>
            <div class="seventyper">
               <div class="logos">
              <ul>

                @if( Auth::user() !=null)
                   @if(Auth::user()->user_type=='1')
                <a href="{{ url('/admindashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->name}}</a>
                @endif
                @if(Auth::user()->user_type=='3')
                  <a href="{{ url('/customerdashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->first_name}}</a>
                @endif
                @if(Auth::user()->user_type=='4')
                  <a href="{{ url('/organiserdashboard') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> {{Auth::user()->first_name}}</a>
                @endif
                @else
                <a href="{{ url('/login') }}"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> Customer Login</a>
                @endif
                <li class="dropdown dropdown-cart">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Cart</a>
                 <a href="{{route('cart.index')}}"><span class="alert badge" id="cartCount">{{Cart::count()}}</span></a>
                <ul class="dropdown-menu" id="cartbody">
                
              </ul>
              </li>
             </ul>
              </div>
              <div class="tgl">
                <img class="imglogo1"src="{{ url('/public') }}/front/images/logo.png">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
               <ul class="navbar-nav ">
                
               
                <li class="nav-item">
                  <a href="{{url('/featured-events')}}" class="nav-link">Featured Events</a>
                </li>
                <li class="nav-item">
                  <a href="{{ url('/ticket-deals') }}" class="nav-link">Ticket Deals</a>
                </li>
                 <li class="nav-item">
                  @if( Auth::user() !=null)
                   @if(Auth::user()->user_type=='1')
                <a href="{{ url('/eventbookings/create') }}"> <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                @if(Auth::user()->user_type=='3')
                  <a href="{{ url('/eventbookings/create') }}"> <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                @if(Auth::user()->user_type=='4')
                  <a href="{{ url('/eventbookings/create') }}"><button type="button" class="btn btn-danger">Create Event</button> </a>
                @endif
                @else
                <a href="{{ url('/organiserregistration') }}">
                  <button type="button" class="btn btn-danger">Create Event</button></a>
                @endif
                </li>
              </ul>
            </div>
          </div>
          </div>
          </nav>
          <div class="clearfix"> </div>
        </div>
      </div>
      <?php $showsearch; ?>
      @if($showsearch=="true")
      <!--//navigation section -->
      <div class="banner-slide-img text-center">
        <div class="banner-bride-name">
          <h4>Event Tickets Online & Mobile</h4>
        </div>
        <div class="banner-groom-name">
          <h5>The Petal Events Ticket Platform</h5>
        </div>
        <div class="searchitem">
          <div class="row megh">
             <form name="search form row" id="eventsearch" method="POST" action="{{ url('event-listings') }}">
                {{ csrf_field() }}
        <div class="col">
            <div id="imaginary_container"> 
                
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control" id="search_input" name="search_input"  placeholder="EVENT NAME, LOCATION" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <!-- <span class="glyphicon glyphicon-search"></span> -->
                            Find Event
                        </button>  
                    </span>
                </div>
             
            </div>
        </div>
        <!--  col-adwance-search -->
            <div class="col-adwance-search">
              <a class="btn btn-primary btn-as" role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
              Advanced Search <i class="fa fa-angle-down" aria-hidden="true"></i>
               </a>
              <div class="collapse" id="collapseExample">
             <div class="well ">
             
              <div class="form-group col-md-6 col-xs-12">
               <input type="text" class="form-control" id="start_date" name="start_date" placeholder="Start date" data-provide="datepicker-inline">
              </div>
              <div class="form-group col-md-6 col-xs-12">
                <input type="text" class="form-control" id="end_date"  name="end_date" placeholder="End Date" data-provide="datepicker-inline">
              </div>
              <div class="form-group col-md-6 col-xs-12">
                <input type="text" class="form-control" id="location" name="location" placeholder="Enter location">
              </div>
               <div class="form-group col-md-6 col-xs-12">
                <select class="form-control" id="category" name="category">
                    <option value=""> Any </option>
                    <option> Arts & Culture</option>
                    <option> Bar & Pub</option>
                    <option> Burlesque </option>
                    <option> Cabaret </option>
                    <option> Celebrity</option>
                    <option> Charity </option>
                    <option> Children </option>
                    <option> Cinema </option>
                    <option> Clubbing </option>
                    <option> Comedy</option>
                    <option> Concert </option>
                    <option> Conference </option>
                    <option>Cosplay </option>
                    <option> Exhibition</option>
                    <option> Festival</option>
                    <option> Food & Drink</option>
                    <option> Gentlemen's Club</option>
                    <option> Hotel</option>
                    <option> Karaoke</option>
                    <option> Latin Dance</option>
                    <option> Live Music</option>
                    <option> Magic</option>
                    <option> Museum</option>
                    <option> Pantomime</option>
                    <option> Poetry</option>
                    <option> Restaurant</option>
                    <option> Speaker</option>
                    <option> Sport</option>
                    <option> Theatre</option>
                    <option> Tours</option>
              </select>
              </div>
            
              </div>
              </div>
             </div>
            </form>
        <!--  //col-adwance-search -->
  </div>
        </div>
      </div>
      @else
      <section class="event-area">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="event-detail">
            <div class="category-overlay">
         {{ $events[0]->type_of_event}}
        </div>
       <span class="card-title">{{ $events[0]->event_title}}</span>
      <div class="date-and-place">
       <div class="venue-details">
             <div class="col-action">
         <span class="e-date-time"><?php if($events[0]->start_date!='' || $events[0]->start_date!='0000-00-00'  || $events[0]->start_date!=NULL){?>{{ \Carbon\Carbon::parse($events[0]->start_date)->format('d M, Y') }}<?php } ?></span>
         </div>
     <span class="e-loc">
      
      <span class="l-con"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
      <span class="l-name"><label>{{ $events[0]->venue_name}}</label><br>{{ $events[0]->venue_address}}</span>
    </span>
       </div>
       </div>
    
     </div>
          </div>
        </div>
      </div>
      
    </section>
      @endif
    </div>
     