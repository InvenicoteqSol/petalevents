       <?php
    if(Auth::user()->photography_role!='' && Auth::user()->photography_role!='NULL'){
        $photographerroles = explode(',', Auth::user()->photography_role); 
    } else { 
        $photographerroles = array();
    }

  ?>
    <div class="sidebar" data-color="purple" data-background-color="black" data-image="{{ url('/public') }}/assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="{{ url('') }}" class="simple-text logo-normal">
          <img class="imglogo" src="{{ url('/public') }}/front/images/logo.png">
        </a>
      </div>
      
      <div class="sidebar-wrapper">
        @if(Auth::user()->user_type=='1')
        <ul class="nav">
          <li class="nav-item {{ Request::is('admindashboard*') ? 'active' : '' }}" id="dash">
            <a class="nav-link" href="{{ url('/admindashboard') }}"><i class="material-icons">dashboard</i><p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('adminbusinesssetting*') ? 'active' : '' }}" id="settings">
            <a class="nav-link" href="{{ url('/adminbusinesssetting') }}">
              <i class="material-icons ">settings</i>
              <p>Settings</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('adminprofiles*') ? 'active' : '' }}" id="profile">
            <a class="nav-link" href="{{ url('/adminprofiles') }}">
              <i class="material-icons">perm_identity</i>
              <p>Profile</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('adminorganiser*') ? 'active' : '' }}" id="organiser">
            <a class="nav-link" href="{{ url('/adminorganiser') }}">
              <i class="fa fa-sitemap"></i>
              <p>Organisers</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('admincustomer*') ? 'active' : '' }}" id="customer">
            <a class="nav-link" href="{{ url('/admincustomer') }}">
              <i class="material-icons">bubble_chart</i>
              <p>Customers</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('eventbookings*') ? 'active' : '' }}" id="events">
            <a class="nav-link" href="{{ url('/eventbookings') }}">
              <i class="material-icons">location_ons</i>
              <p>Events</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}" id="orders">
            <a class="nav-link" href="{{ url('/orders') }}">
              <i class="material-icons">notifications</i>
              <p>Orders</p>
            </a>
          </li>
             <li class="nav-item {{ Request::is('ticketcollabration*') ? 'active' : '' }}" id="collabration">
            <a class="nav-link" href="{{ url('/ticketcollabration') }}">
              <i class="material-icons">library_books</i>
              <p>Ticket Collaborations</p>
            </a>
          </li>
           <li class="nav-item {{ Request::is('admincontactus*') ? 'active' : '' }}" id="contactus">
            <a class="nav-link" href="{{ url('/admincontactus') }}">
              <i class="material-icons">notifications</i>
              <p>Enquiries</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('adminmarketingmaterial*') ? 'active' : '' }}" id="materials">
            <a class="nav-link" href="{{ url('/adminmarketingmaterial') }}">
              <i class="material-icons">equalizer</i>
              <p>Marketing Materials</p>
            </a>
          </li>
             <li class="nav-item {{ Request::is('adminpayment*') ? 'active' : '' }}" id="contactus">
            <a class="nav-link" href="{{ url('/adminpayment') }}">
              <i class="material-icons">payment</i>
              <p>Payments</p>
            </a>
          </li>
           <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
          </form>
          </li>
          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
         @endif
        
       @if(Auth::user()->user_type=='3')

        <ul class="nav">
          <li class="nav-item {{ Request::is('customerdashboard*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/customerdashboard') }}"><i class="fa fa-user"></i><p>Profile</p>
            </a>
          </li> 
            <li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/orders') }}">
              <i class="material-icons">notifications</i>
              <p>My Tickets</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
          </form>
          </li>
           </ul>
           
      @endif

      @if(Auth::user()->user_type=='4')
        <ul class="nav">
          <li class="nav-item {{ Request::is('organiserdashboard*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/organiserdashboard') }}"><i class="fa fa-user"></i><p>Profile</p>
            </a>
          </li> 
           <li class="nav-item {{ Request::is('eventbookings*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/eventbookings') }}"><i class="fa fa-calendar"></i>
              <p>My Events</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('ticketcollabration*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/ticketcollabration') }}">
              <i class="material-icons">library_books</i>
              <p>Ticket Collaborations</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('orders*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/orders') }}">
              <i class="material-icons">notifications</i>
              <p>My Event Orders</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('adminmarketingmaterial*') ? 'active' : '' }}" id="materials">
            <a class="nav-link" href="{{ url('/adminmarketingmaterial') }}">
              <i class="material-icons">equalizer</i>
              <p>Marketing Materials</p>
            </a>
          </li>
          <li class="nav-item {{ Request::is('faq*') ? 'active' : '' }}" id="materials">
            <a class="nav-link" href="{{ url('/faq') }}">
              <i class="material-icons">expand_more</i>
              <p>FAQ</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
          </form>
          </li>
           </ul>
            
      @endif

      </div>
    </div>
     
     
  