@extends('layouts.default')

@section('content')

    <section class="section-m-event crazy-move-pic py-lg-4 py-md-3 py-sm-3 py-3">
      <div class="container py-lg-5 py-md-4 py-sm-4 py-3">
        <h5 class="title text-center mb-lg-5 mb-md-4 mb-sm-4 mb-3">Create, Sell & Manage Any Event</h5>
        
        <div class="createmanase">
 

        </div>
 <div class="row second">
 <!-- <div class="col-md-12"> -->
        <div class="col-full">
            <div class="col-md-4 sellk">
              <div class="imaa">
              <div class="imagg">
                <h5>CREATE</h5>
              </div>  
            </div>
              <p>Set up event listings with our free, simple-to-use ticketing system. Whether a commercial event or a charity/non-profit event including auctions, you have complete event-management.
              </p>
            </div>
             
            <div class="col-md-4 sellm">
              <!-- <img src="images/sell.png" > -->
              <div class="bord"></div>
            </div>
             </div>
            <!--  //col-full -->
             <div class="col-full-1">
            <div class="col-md-4 sellk">
              <div class="imaa">
              <div class="imagg">
                <h5>MANAGE</h5>
              </div>  
            </div>
               <p>We provide simple event management and reporting functions to enable you to track the progress and success of your events.
          </p>
            </div>
             
<!--           </div> -->
          <div class="ros"></div>

            <div class="w-100"></div>
          
      <!--        <div class="col-md-12 mepad"> -->
            <div class="col-md-4 sellms">
               <div class="bords"></div>
              <!-- <img src="images/create.png" > -->
                
            </div>
             </div>
            <!--  //col-full -->
             <div class="col-full">
            
            <div class="col-md-4 sellk">
              <div class="imaa">
              <div class="imagg">
                <h5>SELL</h5>
              </div>  
            </div>
               <p>Petal Events enables you to promote and sell your event tickets with a simple customer purchase experience, suitable for all events and organisations.
          </p>
            </div>
            
            <div class="col-md-4 sellmss">
               <div class="bords"></div>
              <!-- <img src="images/manage.png" > -->

          </div>
           </div>
            <!--  //col-full -->
             <div class="col-full">

   <!-- </div> -->
     
        <div class="col-md-12 bt">
          <a class="btn btn-primary" href="{{url('/organiserregistration')}}">EVENT REGISTRATION</a>
        </div>
       
      </div>
    </section>

@endsection