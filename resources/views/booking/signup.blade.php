@extends('layouts.default2')
@section('content')
@include('layouts.flash-message')
<div class="signup_section form_section">
  <div class="container">
    <div class="signup_bg form_bg">
     <div class="col-sm-12 signup_rightside">
      <p>Please Create and Account to Complete your Booking</p>
      <form class="signupform formarea" id="signup_form" method="post" name="signup_form" action="{{ url('/storeeventdata') }}">
        {{ csrf_field() }}
<div class="row inputspace">
  <div class="col-sm-6 form-group forminput{{ $errors->has('first_name') ? ' has-error' : '' }}">
    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="">
    @if ($errors->has('first_name'))
        <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
        </span>
    @endif
  </div>
  <div class="col-sm-6 form-group forminput{{ $errors->has('last_name') ? ' has-error' : '' }}">
    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="">
    @if ($errors->has('last_name'))
        <span class="help-block">
            <strong>{{ $errors->first('last_name') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-6 form-group forminput{{ $errors->has('email') ? ' has-error' : '' }}">
   <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="">
   @if ($errors->has('email'))
      <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
      </span>
  @endif
  </div>
  <div class="col-sm-6 form-group forminput{{ $errors->has('username') ? ' has-error' : '' }}">
    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="">
    @if ($errors->has('username'))
        <span class="help-block">
            <strong>{{ $errors->first('username') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-6 nopadding">
    <div class="row">
  <div class="col-sm-3 col-xs-4 tel_code">
    <span>+852</span>
  </div>
  <div class="col-sm-9 col-xs-8 form-group forminput{{ $errors->has('phone') ? ' has-error' : '' }}">
   <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number" value="">
   @if ($errors->has('phone'))
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif
  </div>
</div>
  </div>

  <div class="col-sm-6 form-group forminput{{ $errors->has('country') ? ' has-error' : '' }}">
   <input type="text" class="form-control" id="country" name="country" value="Hong Kong">
   @if ($errors->has('country'))
        <span class="help-block">
            <strong>{{ $errors->first('country') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-6 form-group forminput{{ $errors->has('password') ? ' has-error' : '' }}">
   <input type="password" class="form-control" id="regpassword" name="password" placeholder="Password" value="">
   @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
  </div>
  <div class="col-sm-6 form-group forminput{{ $errors->has('confirmpassword') ? ' has-error' : '' }}">
   <input type="password" class="form-control" id="regconfirmpassword" name="confirmpassword" placeholder="Confirm Password" value="">
   @if ($errors->has('confirmpassword'))
        <span class="help-block">
            <strong>{{ $errors->first('confirmpassword') }}</strong>
        </span>
    @endif
  </div>
</div>
<input type="hidden" name="user_type" id="user_type" value="3">
<input type="hidden" name="eventid" id="eventid" value="{{$eventid}}">
<input type="hidden" name="type" id="type" value="{{$type}}">
<input type="hidden" name="paymentid" id="paymentid" value="{{$paymentid}}">
<input type="hidden" name="userid" id="userid" value="@guest{{''}}@else {{ Auth::user()->id }} @endguest">
<div class="col-sm-12 text-center signupbtn_div">
  <div class="submitbtn">
  <button type="submit" class="btn btn-default">Sign Up</button>
  <p> Already sign up ?<a href="{{ url('/login') }}">Login here</a></p>
</div>
</div>
</form>
     </div>
    </div>
  </div>
</div>

<script>
 $(document).ready( function() {

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#signup_form").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190,
                    remote: {
                        url: "{{ url('checkuseremail') }}",
                        type: "GET"
                    }
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 16
                },
                confirmpassword: {
                    equalTo: '#regpassword'
                },
                country: {
                    required: true
                },
                username: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed.",
                  remote: "Email already exists."
                },
                password: {
                  required: "This is required field.", 
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 16 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                username: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });

</script>
@endsection