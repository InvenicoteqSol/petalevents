@extends('layouts.default2')
@section('content')
@include('layouts.flash-message')
<main>
 <section class="innercontentpart">
    <div class="innercontent_text">
<?php 

        $eventdata = DB::table('eventbookings')->where('id', '=', $eventid)->first();

        $redeemdata = DB::table('payments')
              ->where('event_id', '=', $eventid)
              ->where('payment_method', '=', 'Coupon')
              ->where('payment_status', '=', '1')
              ->where('deleted', '=', '0')
              ->first();

        if($redeemdata){
            $eventtotal = $eventdata->booking_amount - $redeemdata->amount;
        } else {
          $eventtotal = $eventdata->booking_amount;
        }

        $category = DB::table('categories')
              ->where('id', '=', $eventdata->occassions)
              ->where('status', '=', '1')
              ->where('deleted', '=', '0')->first();

        $totalphotos = ($eventdata->additional_hours * 20) + 60;

       
?>
      <div class="paymentsection" id="paymentsection" >
        <div class="container">
          <div class="col-sm-12 payment-heading">
            <h1>{{$category->cat_name}}</h1>
            <div class="col-sm-8 payment_left">
              <div class="event_paymentsec">
                <img src="{{ url('public') }}/images/gallaryimg1.jpg">
                <div class="col-sm-12 payment_datediv nopadding">
                  <div class="col-sm-6 paymentdate">
                    Session Date <br/>
                    <span>{{$eventdata->start_date}}</span>
                  </div>
                  <div class="col-sm-6 editorder_btn text-right">
                    <form id="editform" method="GET" name="editform" action="{{ url('/booking') }}">
                      {{ csrf_field() }}
                      <input type="hidden" name="eventid" value="{{$eventid}}">
                    </form>

                    <a style="cursor:pointer;" id="editorder" onclick="document.getElementById('editform').submit();">Edit Order</a>
                  </div>
                </div>
                <div class="col-sm-12 paymentbtns nopadding">
                  <div class="col-sm-6 payment_btn">
                    <a href="#">Total Session: <span id="sessions"> 1 </span></a>
                  </div>
 <div class="col-sm-6 payment_btn">
                    <a href="#">Total Photo : <span id="photos"> {{ $totalphotos}}</span></a>
                  </div>
                </div>

                <div class="col-sm-12 paymentbtns nopadding">
                  <div class="col-sm-6 payment_btn">
                    <a href="#">All Photos Back: <span id="photos_bkup">@if($eventdata->take_all_photos=='0'){{'No'}} @else {{'Yes'}} @endif</span></a>
                  </div>
 <div class="col-sm-6 payment_btn">
                    <a href="#">Additional Hour: <span id="addition_hour">{{$eventdata->additional_hours}}</span></a>
                  </div>
                </div>

                 <div class="col-sm-12 subtotal_div nopadding">
                SUBTOTAL : ${{$eventdata->booking_amount}}
                </div>
              </div>
            </div>
            <div class="col-sm-4 payment_right">
              <h1>Summary</h1>
               @guest
                <form id="discount" method="POST" name="discount" action="{{ url('/discountpayment') }}">
                @else
                <form id="discount" method="POST" name="discount" action="{{ url('/userdiscountpayment') }}">
                @endguest
                      {{ csrf_field() }}
              <div class="summarydiv">
                <p>Subtotal <span>${{$eventdata->booking_amount}}</span></p>
                 <p class="editorder_btn">Voucher
                  @if($redeemdata)
                  <label>Applied Coupon <br><span>{{$redeemdata->transaction_id}}</span></label>

                  @else
                  <input placeholder="Use Voucher" type="text" name="couponcode" required>
                  <input type="hidden" name="eventid" value="{{$eventdata->id}}">
                  <button type="submit" style="float: left;margin-top: 20px;">Redeem</button>
                  @endif
                </p>
              </div>
              </form>
              <div class="totalprice_div">
                <p>Total Price <span>${{$eventtotal}}</span></p>
<label class="radiodiv">I Agree to mrs.portrait <span style="color: #444547;"><a href="{{ url('/terms') }}" target="_blank">Term & Condition</a></span>
  <input id="terms" type="radio" name="terms" required value="yes"> 
  <span class="checkmark"></span>
</label>

<div class="summarydiv" id="paymentdiv" style="display: none;">
                <div class="col-sm-6">
                @guest
                <form id="editform" method="POST" name="editform" action="{{ url('/stripepayment') }}">
                @else
                <form id="editform" method="POST" name="editform" action="{{ url('/usereventstripepayment') }}">
                @endguest
                      {{ csrf_field() }}
                  <input type="hidden" name="eventid" value="{{$eventdata->id}}">
                  <input type="hidden" name="amount" value="{{$eventtotal}}">
  <?php 
    if($BusinessInfo->stripe_mode=='Live'){
          $publish_key =  $BusinessInfo->live_publish_key;
    } else {
          $publish_key =  $BusinessInfo->test_publish_key;
    }
  ?>
  <script
    src="https://checkout.stripe.com/checkout.js"
    class="stripe-button"
    data-label="Pay With Stripe"
    data-panel-label="Pay With Stripe"
    data-key="{{$publish_key}}"
    data-name="Mrs.Portrait"
    data-description="Event Total (${{$eventtotal}})"
    data-amount="{{$eventtotal.'00'}}">
  </script>
</form>
</div>

<?php

if($BusinessInfo->paypal_mode=='Live'){
    $paypalemail =  $BusinessInfo->paypal_live_email;
    $action_url = 'https://www.paypal.com/cgi-bin/webscr';
  } else {
    $paypalemail =  $BusinessInfo->paypal_test_email;
    $action_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
  }
?>

<div class="col-sm-6">
<form name="paymentForm" id="paymentForm" method="post" action="{{$action_url}}" >
           <input type="hidden" name="business" value="{{$paypalemail}}">
           <input type="hidden" name="cmd" id="cmd" value="_xclick">
           <input type="hidden" name="item_name" id="item_name" value="Event Payment">
           <input type="hidden" name="item_number" id="item_number" value="{{$eventdata->id}}">
           <input type="hidden" name="quantity" value="1">
           <input type="hidden" name="tax" id="tax" value="0.00">
           <input type="hidden" id="amount" name="amount" value="{{$eventtotal}}" />
           <input type="hidden" name="currency_code" value="HKD">
           <input type="hidden" name="rm" value="2">
           <input type="hidden" name="notify_url" value="">
           <input type="hidden" name="no_note" value="1">
           <input type="hidden" name="no_shipping" value="1">
           <input name="first_name" id="first_name" type="hidden" value="" />
           <input name="last_name"  id="last_name" type="hidden" value="" />
           <input name="email"  id="email" type="hidden"  value="" />
           <input type='hidden' name='cancel_return' value="{{ url('/payment', $eventdata->id) }}">
           @guest
           <input type='hidden' name='return' value="{{ url('/paypalpayment/') }}?status=success">
           @else
           <input type='hidden' name='return' value="{{ url('/usereventpaypalpayment') }}">
           @endguest
           
           <input type="image" name="submit" border="0"
  src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/PP_logo_h_100x26.png"
  alt="Pay With Paypal"> 
         
           </form>
         </div>

              </div>

              </div>
            </div>
        </div>
      </div>
</div>
</div>
 </section>
</main>
<!--content-section-end-->
<script>
 $(document).ready( function() {

  $("#terms").click(function() {
       if($(this).is(':checked')){
          $('#paymentdiv').show();
       } else {
         $('#paymentdiv').hide();
       }
        
    });

   });
</script>
@endsection