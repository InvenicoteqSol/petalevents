@extends('layouts.default2')
@section('content')
@include('layouts.flash-message')
<!--booking-sec-start-->
<?php
if($eventdata->occassions!=''){
  $occassions = explode(',', $eventdata->occassions);
} else {
  $occassions = array();
}
if($eventdata->start_date!=''){
$start_date = date('m/d/Y g:i A', strtotime($eventdata->start_date));
} else {
$start_date ='';
}
?>
    
<form name="eventbooking" id="eventbooking" method="POST" action="{{ url('/bookingdata') }}">

     {{csrf_field()}}
<div class="booking_section" id="bookingsection">
  <div class="container">
    <div class="booking_content">
      <h1>Book Your Photo Session <br><span style="font-style: italic;">anytime. anywhere. any occasions.</span></h1> 
<div id="part-1" class="col-sm-12 booking_form nopadding"> 
  <h3 class="bookingform_title">Is your shoot for personal or business? *</h3>
<div class="col-sm-6 col-xs-12">
  <div class="step1div" id="businessdiv">
   <i class="fa fa-briefcase"></i> Business
  </div>
</div>
<div class="col-sm-6 col-xs-12">
<div class="step1div" id="personaldiv">
 <i class="fa fa-user"></i> Personal
</div>
</div>
</div>
<div id="part-2" style="display:none;">
  <span id="step1back"><i class="fa fa-arrow-left"></i> &nbsp; Back </span>
        <div class="col-sm-12 booking_form nopadding">
          <div class="col-sm-4 col-xs-12 bookingform_div">
            <h3 class="bookingform_title">Pick a Date*</h3>
             <div class="form-group bookform_input">
                <label>Event Date*</label>
                <input type="text" class="form-control" name="event_dates" id="event_dates" value="{{$start_date}}">
              </div>
          </div>
    <div class="col-sm-4 col-xs-12">
            <h3 class="bookingform_title"> Select occasion *</h3>
        
    <div class="occasion_div bookingform_div " id="personalcat" style="display: none;">
    <ul>  <?php 
                $personal_cat=DB::table('categories')
                              ->where('cat_type', 'LIKE', '%' .'Personal'. '%')
                              ->orderBy('cat_name', 'asc')
                              ->get();
                ?>
                @if(count($personal_cat)>0)
                  @foreach ($personal_cat as $category)
                 <li>
                <label><input type="radio" name="occassions" value="{{ $category->id }}" @if($eventdata->occassions==$category->id){{'checked'}} @endif> {{ $category->cat_name }}</label>
              </li>
                  @endforeach
                @endif
              </ul>
          </div>
           <div class="occasion_div bookingform_div " id="businesscat" style="display: none;">
              <ul>
                <?php 
                $busniess_cat=DB::table('categories')
                              ->where('cat_type', 'LIKE', '%' .'Business'. '%')
                              ->orderBy('cat_name', 'asc')
                              ->get();
                ?>
                @if(count($busniess_cat)>0)
                  @foreach ($busniess_cat as $category)
                 <li>
  
                <label><input type="radio" name="occassions" value="{{ $category->id }}"  @if($eventdata->occassions==$category->id){{'checked'}} @endif> {{ $category->cat_name }}</label>
              </li>
                  @endforeach
                @endif
              </ul>
          </div>
        </div>
            <div class="col-sm-4 col-xs-12 additional_div bookingform_div">
            <h3 class="bookingform_title">Additional Information</h3>
            <div class="form-group bookform_input">
    <label>Shooting Locations*</label>
    <input type="text" class="form-control" id="shootinglocation" placeholder="" name="shooting_locations" value="{{$eventdata->shooting_locations}}">
  </div>
   <div class="form-group bookform_input">
       <label>Additional Hour</label>
  <a href="#" data-toggle="tooltip" title="HK $1,800/Hour/20 Photos"><i class="fa fa-info-circle" aria-hidden="true"></i>
</a>
    <div class="quantity borderdiv">
  <label>Quantity</label>
  <input type="number" min="0" max="24" step="1" readonly="" name="additional_hours" id="additional_hours" value="@if($eventdata->additional_hours!=''){{$eventdata->additional_hours}}@else{{'0'}}@endif">
</div>
  </div>

  <?php 
    $addons=DB::table('addons')
      ->where('status', '=', '1')
      ->where('deleted', '=', '0')
      ->get();

      if(count($addons) > 0){
  ?>
     <div class="form-group bookform_input photosdiv">
    <label>Do you want all photos back? </label>
    <div class="borderdiv">
      <?php 
        if($eventdata->take_all_photos!=''){
          $addonsarr = explode(',', $eventdata->take_all_photos);

          $addonamount = 0;
          foreach($addonsarr as $singleaddon){
              $addondata = DB::table('addons')->where('id', '=', $singleaddon)->first();
              $addonamount = $addondata->adon_price;
          }
        } else {
          $addonsarr = array();
          $addonamount = 0;
        }
      ?>
      <?php foreach($addons as $addon){ ?>

                <label class="checkboxdiv">{{$addon->adon_title}}
                  <input type="checkbox" name="take_all_photos[]" data-value="{{$addon->adon_price}}" value="{{$addon->id}}"  class="addons" required @if(in_array($addon->id, $addonsarr)) {{'checked'}}@endif>
                  <a href="#" <?php if($addon->adon_description!=''){echo  'data-toggle="tooltip" title="'.$addon->adon_description.'"'; } ?>>
                    <?php if($addon->adon_description!=''){ ?>
                      <i class="fa fa-info-circle" aria-hidden="true"></i>
                    <?php } ?>
                </a>
                  <span class="checkmark"></span>
                </label>

<?php } ?>

</div>
  </div>
<?php } ?>
</div>
</div>

<div class="col-sm-12 col-xs-12  bookingform_div">
            <h3 class="bookingform_title">Photo Shoot Brief</h3>
              <div class="form-group bookform_input">
   <textarea class="form-control" id="additional_notes" name="additional_notes" rows="6" cols="50">{{$eventdata->additional_notes}}</textarea>
  </div>
          </div>

        <div class="col-sm-12 col-xs-12  bookingform_div">
          <h3 class="bookingform_title">Summary</h3>
          <div class="col-sm-9 total_price">
              <div class="col-sm-3 totalamt_div">
              <p>Total Price : $<span id="amount">@if($eventdata->booking_amount){{$eventdata->booking_amount}} @else {{'3600'}} @endif</span></p>
              <input type="hidden" id="eventamount" value="3600">
            </div>
            <div class="col-sm-3 totalamt_div">
              <?php 
                if($eventdata->additional_hours!='' && $eventdata->additional_hours!='0'){
                $total_photos = 60 + ($eventdata->additional_hours * 20);
                } else {
                  $total_photos = 60;
                }
              ?>
              <p>Total Photo : <span id="photo_div">{{ $total_photos}}</span></p>
            </div>
            
            <div class="col-sm-3 totalamt_div">
              <p></p>
            </div>
          </div>
          
          <input type="hidden" name="eventid" id="eventid" value="{{$eventdata->id}}">
          <input type="hidden" name="addontotal" id="addontotal" value="{{$addonamount}}">
          <div class="col-sm-3 checkout_btn">
            <input type="submit" class="btn" value="Check Out" id="booking">
          </div>
        </div>
        </div>
        </div>
    </div>
  </div>
<input type="hidden" id="today" value="{{date('Y-m-d')}}">
    </form>
<script>
 $(document).ready( function() {
var now = new Date();
mobiscroll.settings = {
        theme: 'ios',         
        lang: 'en'            
    };
    var now = new Date();
    mobiscroll.datetime('#event_dates', {
        dateWheels: '|dd MM yy|', 
        display: 'center', 
        min: now,
    });
    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });
     $("#eventbooking").validate({
        rules: {
                occassions: {
                    required: true
                },
                terms: {
                    required: true
                },
                shooting_locations: {
                    required: true
                },
                additional_hours: {
                    required: true
                },
                take_all_photos: {
                    required: true
                },   
            },
        messages: {
                occassions: {
                  required: "This is required field." 
                },
                shooting_locations: {
                  required: "This is required field."  
                },
                additional_hours: {
                  required: "This is required field." 
                },
                take_all_photos: {
                  required: "This is required field."
                },     
            },
        submitHandler: function(form) {
              var eventdates = $('#event_dates').val();
              if(eventdates!=''){
                form.submit();
              } else {
                  alert('Please select event date');
              }
          }
        });
     $('.step1div').click(function(){

                var shootdiv = $(this).attr('id');

                if(shootdiv=='businessdiv'){
                  $('#businesscat').show();
                } else {
                  $('#personalcat').show();
                }
                  $('#part-2').show();
                  $('#part-1').hide();
             });

             $('#step1back').click(function(){
                  $('#part-1').show();
                  $('#part-2').hide();
                  $('.occasion_div').hide();
             });
     $(".addons").change(function(){
          var actualamount = '3600';
          var addontotal = $('#addontotal').val();
          var addonvalue = $(this).attr('data-value');
         if ($(this).is(":checked")) {
             var newaddontotal = parseInt(addontotal) + parseInt(addonvalue);
         } else {
             var newaddontotal = parseInt(addontotal) - parseInt(addonvalue);
         }
         if($('#additional_hours').val() > '0'){
              var addhour = $('#additional_hours').val();
              var actualamount = parseInt(actualamount) + (parseInt(1800) * parseInt(addhour));
            }
         var actualamount = parseInt(actualamount) + parseInt(newaddontotal);
         document.getElementById('addontotal').value = newaddontotal;
          jQuery('#eventamount').val(actualamount);
            jQuery('#amount').html(actualamount);
     });
 });
</script>
<script>
      jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');
      btnUp.click(function() {
         //var eventamount = jQuery('#eventamount').val();
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");

          var actualamount = '3600';
          var addontotal = $('#addontotal').val();
          actualamount = parseInt(actualamount) + parseInt(addontotal);
            
            if(newVal > '0'){
              actualamount = parseInt(actualamount) + (parseInt(1800) * parseInt(newVal)) ;
            }
        var actualhour = parseInt(newVal) + parseInt(2) ;
        jQuery('#amount').html(actualamount);
         jQuery('#eventamount').val(actualamount);
         
          jQuery('#hour_div').html(actualhour);
          var photo_sec=parseInt(60) + (parseInt(20) * parseInt(newVal));
         jQuery('#photo_div').html(photo_sec);

      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");

        var actualamount = '3600';
        var addontotal = $('#addontotal').val();
          actualamount = parseInt(actualamount) + parseInt(addontotal);
            

            if(newVal > '0'){
              actualamount = parseInt(actualamount) + (parseInt(1800) * parseInt(newVal)) ;
            }

             var actualhour = parseInt(newVal) + parseInt(2) ;

        jQuery('#amount').html(actualamount);
         jQuery('#eventamount').val(actualamount);
         jQuery('#hour_div').html(actualhour);
         var photo_sec=parseInt(60) + (parseInt(20) * parseInt(newVal));
         jQuery('#photo_div').html(photo_sec);
      });
    });
</script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<script>
    window._token = '{{ csrf_token() }}';
</script>
<style type="text/css">
  .step1div {
    border: 1px solid #ccc;
    padding: 20px;
    border-radius: 5px;
    font-size: 18px;
    cursor: pointer;
}
.step1div .fa {
    color: #74b5f9;
    margin-right: 10px;
    font-size: 25px;
    vertical-align: middle;
}
#step1back{
  cursor: pointer;
}
.checkboxdiv{font-size: 16px;}
</style>
@endsection