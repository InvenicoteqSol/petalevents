@extends('layouts.default2')
@section('title', 'Petal Events | Register')
@section('content')
<div class="signup_section form_section">
  <div class="container">
    <div class="signup_bg form_bg">
     <div class="col-sm-6 signup_leftside">
      <img src="{{ url('/public') }}/images/photographer_img.png" class="img-responsive">
        <div class="signup_content">
          <div class="col-sm-12 signup_text"><img src="{{ url('/public') }}/images/cameraicon1.png">
            <div class="textsection">
          <h2>Friendly and outgoing Personality</h2>
              <span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,</span>
            </div>
          </div>
             <div class="col-sm-12 signup_text"><img src="{{ url('/public') }}/images/cameraicon2.png">
           <div class="textsection"><h2>Excellent photography skill</h2>
              <span>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters,</span>
            </div>
            </div>
        </div>
     </div>
     <div class="col-sm-6 signup_rightside">
      <p class="reg_sec_pge">If these sound like you,we are more than happy to welcome you in Mrs Portrait. Hope to connect With you soon</p>
      <form name="regfrm" id="regfrm" class="login_form formarea" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

<div class="row inputspace">

  <div class="col-sm-6 form-group forminput{{ $errors->has('first_name') ? ' has-error' : '' }}">

   <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First name" value="{{ old('first_name') }}" required>
   @if ($errors->has('first_name'))
        <span class="help-block">
            <strong>{{ $errors->first('first_name') }}</strong>
        </span>
    @endif
  </div>

  <div class="col-sm-6 form-group forminput{{ $errors->has('last_name') ? ' has-error' : '' }}">
   <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last name" value="" required>
   @if ($errors->has('last_name'))
        <span class="help-block">
            <strong>{{ $errors->first('last_name') }}</strong>
        </span>
    @endif
  </div>

</div>

                          <div class="row inputspace">
                        <div class="forminput col-sm-12 form-group{{ $errors->has('username') ? ' has-error' : '' }}">

                                <input id="username" type="text" class="form-control" placeholder="Username" name="username" value="{{ old('username') }}" maxlength="191" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                        </div>
                      </div>

                      <div class="row inputspace">
                        <div class="forminput col-sm-12 form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                <input id="email" type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" maxlength="191" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                      </div>

                        <div class="row inputspace">
  <div class="col-sm-12 form-group forminput{{ $errors->has('website') ? ' has-error' : '' }}">
   <input type="url" class="form-control" id="website" name="website" placeholder="Your Website" value="{{ old('website') }}" onkeyup="checkURL(this)">
   @if ($errors->has('website'))
        <span class="help-block">
            <strong>{{ $errors->first('website') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-2 tel_code">
    <span>+852</span>
  </div>
  <div class="col-sm-10 form-group forminput{{ $errors->has('website') ? ' has-error' : '' }}">
   <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone number" name="phone" value="{{ old('phone') }}" minlength="10" maxlength="13" required>
   @if ($errors->has('phone'))
        <span class="help-block">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-12 form-group forminput{{ $errors->has('country') ? ' has-error' : '' }}">
   <input type="text" class="form-control" id="country" name="country" value="Hong kong" minlength="2" maxlength="40" required readonly="readonly">
   @if ($errors->has('country'))
        <span class="help-block">
            <strong>{{ $errors->first('country') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  <div class="col-sm-12 form-group forminput{{ $errors->has('occassion') ? ' has-error' : '' }}">
   <select class="form-control" name="occassion" id="occassion" >
    <option value=""> Please select occassion </option>
    @if(count($categories)>0)
    @foreach ($categories as $categ)
    <option value="{{ $categ->id }}"> {{ $categ->cat_name }} </option>
    @endforeach
    @endif
   </select>
   @if ($errors->has('occassion'))
        <span class="help-block">
            <strong>{{ $errors->first('occassion') }}</strong>
        </span>
    @endif
  </div>
</div>



<div class="row inputspace">
  <div class="col-sm-12 form-group forminput{{ $errors->has('description') ? ' has-error' : '' }}">
   <textarea class="form-control" rows="6" placeholder="About yourself" name="description" id="description" required>{{ old('description') }}</textarea>
   @if ($errors->has('description'))
        <span class="help-block">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
  </div>
</div>

<div class="row inputspace">
  @if(count($photographer_roles)>0)
  @foreach ($photographer_roles as $role)
  <div class="col-sm-6 form-group forminput">
  <label class="checkboxdiv">Be a {{ $role->role_name }}
  <input type="checkbox"  name="photography_role[]" id="role{{ $role->id }}" value="{{ $role->id }}" required>
  <span class="checkmark"></span>
  </label>
  </div>
  @endforeach
  @endif

  @if ($errors->has('photography_role'))
        <span class="help-block">
            <strong>{{ $errors->first('photography_role') }}</strong>
        </span>
    @endif
</div>

<div class="row inputspace">
  <div class="col-sm-6 form-group forminput{{ $errors->has('password') ? ' has-error' : '' }}"> <i class="fa fa-eye icon" onclick="myPassword()"></i>
   <input type="password" class="form-control" id="regpassword" name="password" placeholder="Your password" value="{{ old('password') }}" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
   @if ($errors->has('password'))
        <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
  </div>
  <div class="col-sm-6 form-group forminput{{ $errors->has('confirmpassword') ? ' has-error' : '' }}"><i class="fa fa-eye icon" onclick="myRepassword()"></i>
   <input type="password" class="form-control" id="regconfirmpassword" name="confirmpassword" placeholder="Confirm password" value="" required>
   @if ($errors->has('confirmpassword'))
        <span class="help-block">
            <strong>{{ $errors->first('confirmpassword') }}</strong>
        </span>
    @endif
  </div>
</div>
<input type="hidden" name="user_type" id="user_type" value="4">
<div class="submitbtn">
  <button type="submit" class="btn btn-default">Join Us</button>
</div>
                    </form>
     </div>
    </div>
  </div>
</div>

<script>
 $(document).ready( function() {

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

     $.validator.addMethod("PSSVLD", function(value, element) {
                return this.optional(element) || /^(?=.*[a-zA-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{7,19}$/i.test(value);
            }, "Password should have a minimum length of 8, a maximum length 16, should contain alphabets, at least 1 special character, 1 number and should not start with a special character.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#regfrm").validate({
        rules: {
                fullname: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
               
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },

                username: {
                    required: true,
                    maxlength: 190
                },

                password: {
                    required: true,
                    PSSVLD:true,
                    minlength: 6,
                    maxlength: 16
                },
                confirmpassword: {
                    PSSVLD:true,
                    equalTo: '#regpassword'
                },
                country: {
                    required: true
                },
                occassion: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                fullname: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
              
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                username: {
                  required: "This is required field.",   
                  maxlength: "Maximum 190 characters allowed."
                },

                password: {
                  required: "This is required field.", 
                  minlength: "Minimum 6 characters required.",
                  maxlength: "Maximum 16 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });

 function checkURL (abc) {
  var string = abc.value;
  if (!~string.indexOf("http")) {
    string = "http://" + string;
  }
  abc.value = string;
  return abc
}
</script>
<script>
function myPassword() { 
    var x = document.getElementById("regpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>
<script>
function myRepassword() { 
    var x = document.getElementById("regconfirmpassword");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
</script>

@endsection
