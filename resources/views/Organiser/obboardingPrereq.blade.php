@extends('layouts.innerdefault')
@section('content')
<section class="section-contact">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="contact-heading">
                         <h2>Connect With Stripe</h2>
                         <p>Please connect your already created stripe account / create a new one by clicking on the button below .</p>
                    </div>
					<div class="col-md-6">
					<a href="https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_E987szW8QVTu4entAIEU22HTHxN1OEKf&scope=read_write"><img src="{{url('public/front/images/blue-on-light.png')}}"/></a>
					</div>
</div>
</div>
</div>
</section>
@endsection