@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
</div>

  {{ Form::model($organiser, array('route' => array('organiserdashboard.update', $organiser->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT', 'files' => true)) }}

   <div class="content">
        <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
        <h4 class="card-title">Update Organiser </h4>
        <!-- <p class="card-category">Update your Booking</p> -->
        </div>

        <div class="card-body">
            <form>
      <div class="row">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">           
                            <label for="first_name" class="bmd-label-floating"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $organiser->first_name }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="bmd-label-floating">Surname*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $organiser->last_name }}" minlength="2" maxlength="91" required>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                        <div class="row">
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('organisation_name') ? ' has-error' : '' }}">
                            <label for="organisation_name" class="bmd-label-floating">Organisation name *</label>

                                <input id="organisation_name" type="text" class="form-control" name="organisation_name" value="{{ $organiser->organisation_name }}" minlength="2" maxlength="91" required>

                                @if ($errors->has('organisation_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('organisation_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                             <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="bmd-label-floating">Organisation website </label>

                                <input id="website" type="text" class="form-control" name="website" value="{{ $organiser->website}}" minlength="2" maxlength="91" placeholder="" >

                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                             <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                             <label for="email" class="bmd-label-floating">E-Mail Address* </label>
                      
                                <input id="email" type="email" class="form-control" name="email" value="{{ $organiser->email }}" maxlength="191" required >
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                        <div class="col-md-6">
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="bmd-label-floating">Phone*</label>
                        
                                <input id="phone" type="text" class="form-control numberinput" name="phone" value="{{ $organiser->phone }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                       <div class="row">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
                            <label for="address_line_1" class="bmd-label-floating">Address line 1 *</label>

                                <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="{{ $organiser->address_line_1}}" minlength="2" maxlength="91" required>

                                @if ($errors->has('address_line_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                             <div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
                            <label for="address_line_2" class="bmd-label-floating">Address line 2 </label>

                                <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="{{ $organiser->address_line_2}}" minlength="2" maxlength="91">

                                @if ($errors->has('address_line_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                    <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                              <label for="country" class="bmd-label-floating">City/Town *</label>
                            
                                <select class="form-control" id="country" name="country" required>
                                    @foreach ($towns as $townName=>$townId)
                                        <option value="{{ $townId }}" {{ ( $organiser->country == $townId ) ? 'selected' : '' }}>{{ $townName }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('country'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                              <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                            <label for="postcode" class="bmd-label-floating">Postcode </label>
                     
                                <input id="postcode" type="text" class="form-control numberinput" name="postcode" value="{{ $organiser->postcode}}" minlength="2" maxlength="91" required>

                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         </div>
                         <div class="row">
                               <div class="col-md-6">    
                <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="bmd-label-floating"> Username *</label>

                                <input id="username" type="text" class="form-control" name="username" minlength="6" value="{{ $organiser->username }}" maxlength="17" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            <div class="col-md-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="bmd-label-floating">Password*</label>


                                <input id="password" type="password" class="form-control" name="password" value="{{ $organiser->password }}" minlength="6" maxlength="17" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="row">
                               <div class="col-md-6">
                        <div class="form-group{{ $errors->has('security_question') ? ' has-error' : '' }}">
                            <label for="security_question" class="bmd-label-floating">Security question*</label>

                                <input id="security_question" type="text" class="form-control" name="security_question"  value="{{ $organiser->security_question }}" minlength="6" maxlength="17" required>

                                @if ($errors->has('security_question'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('security_question') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                         <div class="form-group{{ $errors->has('security_answer') ? ' has-error' : '' }}">
                            <label for="security_answer" class="bmd-label-floating">Security answer*</label> 
                            <input id="security_answer" type="text" class="form-control" name="security_answer"  value="{{ $organiser->security_answer }}" minlength="6" maxlength="17" required>

                                @if ($errors->has('security_answer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('security_answer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                        <div class="col-md-12">
                                  <div class="row topcls"> 
                                 <div class="col-md-6">
                            <div class="col-md-3 nopadding" id="ftrd_browse_img">
                                @if($organiser->profile_picture!='') 
                                 <img src="{{ url('public') }}/uploads/profileimages/{{ $organiser->profile_picture }}" class="img-responsive" style="width: 100px; height: 100px;">
                                     @else
                                    <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   @endif
                                    
                                </div>
                                <div class="col-md-3 prflinput">
                                <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
                              </div>
                        </div>
                    </div>
                </div>



                    <!-- <div class="row">
                       <div class="col-md-12 col-xs-12">
                         <div class="col-featured-img">
                            <label class="bmd-label-floating">Featured Image</label>
                            <img id="blah" src="../assets/img/background.png" alt="your image" />
                            <input type='file' class="form-control" onchange="readURL(this);" />
           
                  <div class="col-md-12">
                         <div class="row topcls">
                          <div class="col-md-6">
                            <label for="roles">Featured Image</label>
                            <div class="form-group">
                                  <div class="col-md-3 nopadding" id="ftrd_browse_img" style="padding-top: 10px;">

                                  @if($organiser->profile_picture!='') 
                                    <img src="{{ url('public') }}/uploads/profileimages/{{ $organiser->profile_picture }}" class="img-responsive" style="width: 100px; height: 100px;">
                                     @else
                                    <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   @endif
                                    
                                </div>

                                <div class="col-md-3 prflinput">
                                  <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
                              </div>

                            </div>
                        </div>
                         <div class="col-md-6">
                         </div>
                    </div>
                </div>


                       </div>                   
                      </div>
                    </div> -->
                    
                  
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                     {{ Form::close() }}
                                
         </div>
    </div>
</div>
    
<script>
 $(document).ready( function() {

    /*  var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })*/

    $("#profile_picture").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });    

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                },
                dob:{
                   required: true
                },
                 gender:{
                   required: true
                },
                 status:{
                   required: true
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                },
                 dob: {
                  required: "This is required field."
                },
                 gender: {
                  required: "This is required field."
                },
                 status: {
                  required: "This is required field."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
    
       $('.datepicker').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });
 });
</script>
<script type="text/javascript">
 $(document).ready(function () {
     $(".numberinput").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
@endsection
