@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      
  </div>

   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
                  <h4 class="card-title">FAQ</h4>              
                </div>
    <div class="row">
     <div class="col-md-12 col-sm-12 col-xs-12">

<!--  panel-group -->

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <!--  panel panel-default panel-faq -->
  <div class="panel panel-default panel-faq">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        	How do I create an event?
        	 </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      	Select My Events>CREATE NEW EVENT and enter your event and venue details. If you wish for your event to appear in Featured Events for a £5 fee, set the ‘Display In Featured Events’ button to ON.
      	</div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
     <!--  panel panel-default panel-faq -->
  <div class="panel panel-default panel-faq">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        	How do I create my event tickets?
        	</a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
      	Select My Events and under the ‘Action’ button for your event select Ticket>Create and enter your ticket details.
      	 </div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
    <!--  panel panel-default panel-faq -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
        	Where can I find my tickets?
        	</a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
 <div class="panel-body">
 	You can find your tickets in 'My Tickets'. Alternatively, you will have been sent your tickets to the email address associated with your Petal Events account.<br><br>
  If you want to remove the ticket’s customer booking fee, set the ‘Free Booking Fee’ button to ON. If the free customer booking fee is applied, the booking fee will be deducted from your ticket sales payment after the event.
 	 </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->

  <!--  panel panel-default panel-faq -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading4">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
        How do I create Discounts?
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
 <div class="panel-body">
        Select My Events and under the ‘Action’ button for your event select Discount>CREATE NEW DISCOUNT.<br><br>
Discount - This enables you to create a discount code which either gives a percentage or amount off your ticket price.<br><br>
Early Bird Discount - This enables you to create an early purchase discount code which either gives a percentage or amount off your ticket price.<br><br>
Bundle Offer Ticket Discount - This enables you to create a ticket bundle offer where you can set a Bundle offer price if the customer buys a set amount of tickets.<br><br>
Ticket X Discount - This enables you to create a discount code where if the customer buys X number of tickets they receive Y number of tickets free.
      </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
      <!--  panel panel-default panel-faq -->
      	 <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading5">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
        	How do I create a Ticket Collaboration?
        	 </a>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
      <div class="panel-body">
      	Select Ticket Collaborations>CREATE NEW COLLABORATION.<br><br>
My Event Collaborations - This enables you to offer a discount percentage or amount if the customer buys tickets for two of your events at the same time.<br><br>
Set a clear Name for the ticket collaboration, eg “Buy tickets for both events for a 20% discount on each ticket!”.<br><br>
Your Ticket Collaborations will appear in the Ticket Deals section of the website for customers to purchase.<br><br>
Organiser Collaborations- This enables you to offer a discount percentage or amount if the customer buys tickets for one of your events and for another event organiser’s event at the same time.<br><br>
A request will be sent to the other event organiser to accept or decline the ticket collaboration, therefore always ensure that you have agreed the collaboration before creating it.<br><br>
Set a clear Name for the ticket collaboration, eg “Buy tickets for both events for a 20% discount on each ticket!”.<br><br>
Once accepted, your Ticket Collaborations will appear in the Ticket Deals section of the website for customers to purchase.<br><br>
      	</div>
    </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
     <!--  panel panel-default panel-faq -->
     <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading6">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
        	How do I print my ticket list?
        	</a>
      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
 <div class="panel-body">
 	Select My Events and under the ‘Action’ button for your event select Event>View. Select the ‘PRINT MY TICKETS’ button for a list of customers and tickets purchased.
 	 </div>
 </div>
  </div>
    <!--  ///panel panel-default panel-faq -->
  <!--  panel panel-default panel-faq -->
   <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading7">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
        	How do I receive payment for my event ticket sales?
        	</a>
      </h4>
    </div>
    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
 <div class="panel-body">
 	Once your event has taken place, select My Events and under the ‘Action’ button for your event select Event>View. Select the ‘SEND PAYMENT REQUEST’ button. Payment will be made on the Friday of the following week of your event to your event organiser Stripe account. Please ensure that you have connected with Stripe as prompted at login to ensure that your payment is received.
 	 </div>
 </div>
  </div>
   <!--  ///panel panel-default panel-faq -->
</div>
<!--End panel-group -->
</div>
</div>
</div>
  
<!--content-section-end-->
</div>
</div>
</div>
</div>
<style type="text/css">
  #accordion {
    padding-left: 30px;
    margin-top: 3%;
    margin-bottom: 5%;
}
</style>
@endsection