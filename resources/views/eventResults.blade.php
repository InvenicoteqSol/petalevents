@extends('layouts.innerdefault')
@section('content')
<section class="section-trending">
    <div class="container">
      <div class="row">
        <div class="col-md-12 cil-sm-12 col-xs-12">
          <h3 class="section-heading">Search Results For " <?php $search_data ?> "</h3>
    </div>
   </div>
    <!-- row -->
    <div class="row">
    	@if(count($events)>0)
                     @foreach ($events as $event)
      <!-- card-event -->
      <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
          Live Music
        </div>
        <a href="">
        <img src="images/DAVIDO_socials_INSTA-min.JPG">
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title">{{ $event->event_title  }}</span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc">Europa School</span>
       <span class="e-date-time"><?php if($event->event_date!='' || $event->event_date!='0000-00-00'  || $event->event_date!=NULL){?>{{ \Carbon\Carbon::parse($event->event_date)->format('d M, Y') }}<?php } ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="#" class="btn btn-primary">Buy Tickets</a>
    </div>
     </div>
    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <i class="fa fa-usd" aria-hidden="true"></i> 29.25
       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
        </div>
      </div>
       <!-- END card-event -->
     @endforeach
                     @else
                      @endif

    </div>
    <!-- row -->
   </div>
 </section>
 
@endsection