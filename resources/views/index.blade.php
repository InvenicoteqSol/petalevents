@extends('layouts.default')
@section('content')
@include('layouts.flash-message')
<!--content-section-start-->

 
<?php 
                $id= $categ->id;
                $catgalleryimages = DB::table('catgallery')
                            ->where('cat_id', $id)
                            ->where('status', '=', 1)
                            ->where('deleted', '=', 0)
                            ->orderBy('id','DESC')
                           ->get(); 
                           ?>   

<main>


 <section class="section1 section_mdiv">
    <div class="container">
    <div class="section_content text-center">
      <h2>Occasions</h2>
      <span><img src="{{ url('/public') }}/images/border-icon.png"></span>
      <p>It is a long established fact that a reader will</p>
      <div class="occasion_sec">
        <div class="row">
       @if(count($categories)>0)
        @foreach ($categories as $categ)
          <div class="col-sm-3 occasion_div">
             <div class="occasion_img">
              @if($categ->cat_featur_image!='') 
      
           <img src="{{ url('public') }}/uploads/category/featureimages/{{ $categ->cat_featur_image }}" class="img-rounded">
                         @else
                        <img src="{{ url('public') }}/images/avtar.jpg" class="img-rounded">
                       @endif
             <h3>{{$categ->cat_name}}</h3>
          </div>
        </div>
         @endforeach
                     @else
                    
                    @endif
        </div>
        
      </div>
    </div>
  </div>
  </section>







</main>
<!--content-section-end-->
@endsection