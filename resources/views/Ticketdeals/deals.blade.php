@extends('layouts.innerdefault')
@section('content')

<section class="evnt-slide">
  <div class="container">
  <!-- row -->
  <div class="row">
  <!-- slider -->
  <?php $counter=0;?>
  @foreach($Ticketdeals as $Ticketdeal)
  @if($counter<=1)
  <div class="col-md-6 col-sm-6 col-xs-12" id="tickets">
  <h3 class="section-heading">Tickets</h3>
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <!--    item -->
 <div class="item active">
  <div class="row">
    <?php $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();?>
    @foreach($events as $event)
     <?php $ticket1=$Ticketdeal->ticket_id;
           $ticket2=$Ticketdeal->ticket_id2;
        $discount=$Ticketdeal->discount;
        $ticket=DB::table('tickets')->where('id','=',$ticket1)->first();
        $price =$ticket->prices;
        $total = $price-($price*($discount/100));
         ?>
        <div class="col-md-6 col-xs-12">
        <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
        {{$event->type_of_event}}
        </div>
        <a href="">
        @if($event->event_image!="")
        <img src="{{ url('public') }}/uploads/profileimages/{{ $event->image_thumb }}">
        @else
        <img src="{{ url('public') }}/front/images/image_thumbnail.jpg">
        @endif
        </a>
        </div>
        <!--  //card-body -->
        <div class="card-body">
        <div class="event-detail">
        <span class="card-title">{{$event->event_title}}</span>
        <div class="date-and-place">
        <div class="venue-details">
        <span class="e-loc">{{$Ticketdeal->name}}</span>
       <?php                
          /*$eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];*/
          ?>
       <span class="e-date-time"><?php if($event->start_date!='' || $event->start_date!='0000-00-00'  || $event->start_date!=NULL){?>{{ \Carbon\Carbon::parse($event->start_date)->format('d M, Y') }}<?php } ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" class="btn btn-primary mycartnew" id="mycart" data-ticket="{{$ticket1}}" data-ticket2="{{$ticket2}}">Buy Tickets</a>
        
       </div>
       </div>
       <!-- ///event-detail -->
       <div class="price">
       <i class="fa fa-ticket" aria-hidden="true"> </i>
       <div class="value ticke_value1">
      
         <i class="fa fa-gbp" aria-hidden="true">{{ $total }}</i> 
        

       </div>
       </div>
       <!--  //price -->
       </div>
       <!-- // card-body -->
       </div>
       </div>
        @endforeach
        <?php  $event_id2=$Ticketdeal->event_id2;
        $events=DB::table('eventbookings')->where('id','=',$event_id2)->get();?>
        @foreach($events as $event)
        <?php
            $ticket1=$Ticketdeal->ticket_id;
            $ticket2=$Ticketdeal->ticket_id2;
            $discount=$Ticketdeal->discount;
            $ticket=DB::table('tickets')->where('id','=',$ticket2)->first();
            $price =$ticket->prices;
            $total = $price-($price*($discount/100));
           ?>
          <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
          <div class="category-overlay">
        {{$event->type_of_event}}
        </div>
        <a href="">
        @if($event->event_image!="")
        <img src="{{ url('public') }}/uploads/profileimages/{{ $event->image_thumb }}">
        @else
        <img src="{{ url('public') }}/front/images/image_thumbnail.jpg">
        @endif
        </a>
       </div>
       <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
          <span class="card-title">{{$event->event_title}}</span>
          <div class="date-and-place">
          <div class="venue-details">
          <span class="e-loc">{{$Ticketdeal->name}}</span>
          <?php                
          /*$eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];*/
          ?>
       <span class="e-date-time"><?php if($event->start_date!='' || $event->start_date!='0000-00-00'  || $event->start_date!=NULL){?>{{ \Carbon\Carbon::parse($event->start_date)->format('d M, Y') }}<?php } ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" id="mycart" class="btn btn-primary mycartnew" data-ticket="{{$ticket1}}" data-ticket2="{{$ticket2}}">Buy Tickets</a>
       
       </div>
       </div>
      <!-- ///event-detail -->
      <div class="price">
      <i class="fa fa-ticket" aria-hidden="true"> </i>
      <div class="value ticke_value2">
       <i class="fa fa-gbp" aria-hidden="true">{{ $total }}</i> 
      </div>
      </div>
     <!--  //price -->
     </div>
     <!-- // card-body -->
     </div>
     </div>
      </div>
     <!--     row -->


    </div>
    @endforeach
    <!--item End -->

 </div></div> </div>
  <?php $counter=$counter+1;?>
  @else
  <div  class="clearfix"></div>
  <div class="col-md-6 col-sm-6 col-xs-12" id="tickets">
  <h3 class="section-heading">Tickets</h3>
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <!--    item -->
 <div class="item active">
  <div class="row">
    <?php $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();?>
    @foreach($events as $event)
     <?php $ticket1=$Ticketdeal->ticket_id;
           $ticket2=$Ticketdeal->ticket_id2;
        $discount=$Ticketdeal->discount;
        $ticket=DB::table('tickets')->where('id','=',$ticket1)->first();
        $price =$ticket->prices;
        $total = $price-($price*($discount/100));
         ?>
        <div class="col-md-6 col-xs-12">
        <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
        {{$event->type_of_event}}
        </div>
        <a href="">
        @if($event->event_image!="")
        <img src="{{ url('public') }}/uploads/profileimages/{{ $event->image_thumb }}">
        @else
        <img src="{{ url('public') }}/front/images/image_thumbnail.jpg">
        @endif
        </a>
        </div>
        <!--  //card-body -->
        <div class="card-body">
        <div class="event-detail">
        <span class="card-title">{{$event->event_title}}</span>
        <div class="date-and-place">
        <div class="venue-details">
        <span class="e-loc">{{$Ticketdeal->name}}</span>
       <?php                
        $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
        $datesplit=explode('-', $eventdate);
        $month=$datesplit[0];
        $date=$datesplit[1];
        $year=$datesplit[2];
        ?>
       <span class="e-date-time">{{$month}} <label>{{$date}}</label> {{$year}}</span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" class="btn btn-primary mycartnew" id="mycart" data-ticket="{{$ticket1}}" data-ticket2="{{$ticket2}}">Buy Tickets</a>
        
       </div>
       </div>
       <!-- ///event-detail -->
       <div class="price">
       <i class="fa fa-ticket" aria-hidden="true"> </i>
       <div class="value ticke_value1">
      
         <i class="fa fa-gbp" aria-hidden="true">{{ $total }}</i> 
        

       </div>
       </div>
       <!--  //price -->
       </div>
       <!-- // card-body -->
       </div>
       </div>
        @endforeach
        <?php  $event_id2=$Ticketdeal->event_id2;
        $events=DB::table('eventbookings')->where('id','=',$event_id2)->get();?>
        @foreach($events as $event)
        <?php
            $ticket1=$Ticketdeal->ticket_id;
            $ticket2=$Ticketdeal->ticket_id2;
            $discount=$Ticketdeal->discount;
            $ticket=DB::table('tickets')->where('id','=',$ticket2)->first();
            $price =$ticket->prices;
            $total = $price-($price*($discount/100));
           ?>
          <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
          <div class="category-overlay">
        {{$event->type_of_event}}
        </div>
        <a href="">
        @if($event->event_image!="")
        <img src="{{ url('public') }}/uploads/profileimages/{{ $event->image_thumb }}">
        @else
        <img src="{{ url('public') }}/front/images/image_thumbnail.jpg">
        @endif
        </a>
       </div>
       <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
          <span class="card-title">{{$event->event_title}}</span>
          <div class="date-and-place">
          <div class="venue-details">
          <span class="e-loc">{{$Ticketdeal->name}}</span>
          <?php                
          $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];
          ?>
       <span class="e-date-time">{{$month}} <label>{{$date}}</label> {{$year}}</span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" id="mycart" class="btn btn-primary mycartnew" data-ticket="{{$ticket1}}" data-ticket2="{{$ticket2}}">Buy Tickets</a>
       
       </div>
       </div>
      <!-- ///event-detail -->
      <div class="price">
      <i class="fa fa-ticket" aria-hidden="true"> </i>
      <div class="value ticke_value2">
       <i class="fa fa-gbp" aria-hidden="true">{{ $total }}</i> 
      </div>
      </div>
     <!--  //price -->
     </div>
     <!-- // card-body -->
     </div>
     </div>
      </div>
     <!--     row -->


    </div>
    @endforeach
    <!--item End -->

 </div></div> </div>
  <?php $counter=0;?>

 @endif
 @endforeach

</div>
 </div>
<!--  endslider -->
<!-- slider -->
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
 <script type="text/javascript">
   $(document).ready(function() {
   $(".mycartnew").click(function(){
      tickettype=$(this).data('ticket');
      tickettype2=$(this).data('ticket2');
           
            jQuery.ajax({
              method : 'GET',
              url  : "{{ url('/addtocartticket') }}",
              data :{'tickettype': tickettype,'tickettype2': tickettype2},
              success :  function(resp) {
              $.notify({  
              message: 'Basket Updated Redirecting To Cart...' 
                },{
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
              window.location.href = "{{ url('/cart') }}";
             }
            });
         
       });
   });
</script>
<script>
$(document).ready(function() {
$('.owl-carousel').owlCarousel({
loop: true,
margin: 10,
responsiveClass: true,
responsive: {
0: {
items: 1,
nav: true
},
600: {
items: 3,
nav: false
},
1000: {
items: 5,
nav: true,
loop: false,
margin: 20
}
}
});
} );
</script>
<style type="text/css">
h3.section-heading {
margin-top: 50px;
}
</style>
@endsection