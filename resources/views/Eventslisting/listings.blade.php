@extends('layouts.innerdefault')
@section('content')
<section class="section-trending">
    <div class="container">
      <div class="row">
        <div class="col-md-12 cil-sm-12 col-xs-12">
          <h3 class="section-heading">Search Results For "{{ $search_data }}"</h3>
    </div>
   </div>
    <!-- row -->
    <div class="row">
      <?php 
$rowCount=0; ?>
      @if(count($events)>0)
                     @foreach ($events as $event)
      <!-- card-event -->
      <?php 
       $rowCount=$rowCount+1;
       
       ?>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
          {{ $event->type_of_event  }}
        </div>
        <a href="{{ url('event-details') }}/{{$event->event_title}}" target="_blank">
          @if($event->event_image!="")
        <img src="{{ url('public') }}/uploads/profileimages/{{ $event->image_thumb }}">
        @else
        <img src="{{ url('public') }}/front/images/image_thumbnail.jpg">
        @endif
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title" title="{{$event->event_title}}">
      {!! \Illuminate\Support\Str::words($event->event_title, 3,'..')  !!}</span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc" title="{{$event->venue_name}}">
     {!! \Illuminate\Support\Str::words($event->venue_name, 4,'..')  !!}</span>
       <span class="e-date-time"><?php if($event->start_date!='' || $event->start_date!='0000-00-00'  || $event->start_date!=NULL){?>{{ \Carbon\Carbon::parse($event->start_date)->format('d M, Y') }}<?php } ?></22span>
    </div>
       </div>
       <div class="btn-act">
      <a href="{{ url('event-details') }}/{{$event->event_title}}" target="_blank" class="btn btn-primary">Buy Tickets</a>
    </div>
     </div>

    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <?php 
       $ticketLowest=DB::table('tickets')->where('event_id', '=',$event->id)->where('deleted', '=', 0)->orderBy('prices')->first();
       $countcheck=count($ticketLowest);
       $priceval=0;
       if($countcheck>0)
       {
         $priceval=$ticketLowest->prices;
       }
       ?>
       <i class="fa fa-gbp" aria-hidden="true"></i>{{$priceval}}
       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
        </div>
      </div>
      @if($rowCount==4)
      
      
      <div class="clearfix"></div>
      <?php 
       $rowCount=0;
       
       ?>
     
       @endif
       

       <!-- END card-event -->
      
     @endforeach
                     @else
                     <p>No Records Found</p>
                      @endif

    </div>
    <!-- row -->
   </div>
 </section>
@endsection