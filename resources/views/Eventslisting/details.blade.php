@extends('layouts.innerdefault')
@section('content')
<div class="clearfix"> </div>
<!-- --------------------//banner-slide------------------ -->

    <!-- section-upcoming-events -->
  <section class="section-event-details">
    <div class="container">
       <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12">
           <div class="event-info">
           <span class="card-title">{{ $events[0]->event_title}}</span>
           <p class="event-summry">{!! \Illuminate\Support\Str::words($events[0]->event_info)  !!}</p>
          </div>
          </div>
           <div class="col-md-5 col-sm-5 col-xs-12">
           <div class="event-info">
           <span class="card-title">Venue Information</span>
           <span class="e-loc">
      <span class="e-loc">
      
      <span class="l-con"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
      <span class="l-name"><label>{{ $events[0]->venue_name}}</label><br>{{ $events[0]->venue_address}}</span>
      <span class="contat-col">{{ $events[0]->telephone}}</span>
    </span>
          </div>
        </div>
       </div>
       <!-- //row end --> 
     </div>

          <?php
        $d= strtotime("today");
        $date = date("Y-m-d", $d);
        $date1=  date("h:i:sa",$d);  
           ?>

      <div class="container buy-tickets">
         <div class="row ">
          <div class="col-md-12 col-xs-12">
            <div class="row-heading">
              <span>Buy Tickets</span>
            </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-12">
            <table class="table table-hover" id="tickets">
<thead>
<tr>
<th>Ticket Type</th>
<th class="">Ticket Details</th>
<th>Price</th>
<th class="quantity">Quantity</th>
</tr>
</thead>
<tbody>
 @if(count($tickets)>0)
 
 @foreach ($tickets as $ticket)
<tr>
<td class="ticket-type">
<h3>{{$ticket->ticket_type}}</h3>
<input type="hidden" value="{{$ticket->id}}" id="ticketid">
</td>
<td class="date-cell">
<nobr>{!! \Illuminate\Support\Str::words($ticket->ticket_details)  !!}</nobr>
</td>
<td class="price-cell">

                   <!--  @if ($ticket->prices> '0') 
                        <td class="column3">
                        <?php $amount=$ticket->prices;
                        $total=$amount+($amount*(10/100));
                        ?> 
                       {{$total}}</td>
                       @else
                       <td class="column3"> {{$ticket->amount}} </td> 
                      
                       @endif  -->

<span class="c_value">{{$ticket->prices}}<span>
</td>
<td class="quantity-cell">
  <?php 
  $stockAvailable=DB::table('orders')->where('event_id','=',$events[0]->id)->where('ticket_id','=',$ticket->id);
   $stockcount=($ticket->stock-$stockAvailable->count());
  ?>
    @if((($date>=$ticket->start_date)||($date1>=$ticket->start_time))&&(($date<=$ticket->end_date)||($date1<=$ticket->end_time)))
    @if($ticket->stock_status=='Available')
    @if($stockcount>0)
    <select name="" >
    @for ($i = 0; $i <= $stockcount; $i++)
    <option value="{{ $i }}"> {{ $i }} </option>
    @endfor
    </select>
    @else
    <h3>Sold Out</h3>
    @endif
    @else
     <h3>Unavailable</h3>
    @endif
    @else
    <h3>Sale of tickets closed.</h3>
    @endif
</td>
</tr>
  @endforeach
  @else
  @endif

</tbody>
</table>
<div class="t_action">
  <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>-->
<div class="btn-group" role="group">
  <a href="#" id="mybasket" class="btn btn-primary"> Add To My Basket!</a>
<a href="#" id="mycart" class="btn btn-primary"> Buy Ticket Now!</a>
</div>

</div>

</div>
          <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="event-card">
            	@if($events[0]->event_image!="" )
              <img class="" src="{{ url('public') }}/uploads/profileimages/{{ $events[0]->event_image }}">@else
              <img class="" src="{{ url('public') }}/front/images/login-vector.png">
              @endif
            </div>
          </div>
         </div>
         <!-- //row end --> 
    </div>
 </section>
  <!-- //section-upcoming-events -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {

$("#mybasket").click(function(){
  var table = $("#tickets tbody");

    table.find('tr').each(function (i) {
        var $tds = $(this).find('td'),
            tickettype = $tds.find("#ticketid").val(),
           
            Quantity = $tds.find("select").val();
            
            console.log('tickettype'+tickettype+', quantity'+Quantity);
            if(Quantity>0)
            {
             jQuery.ajax({
              method : 'GET',
            url  : "{{ url('/addtocart') }}",
            data :{'tickettype': tickettype,'quantity':Quantity},
            success :  function(resp) {
              $("#cartCount").text(resp);
              $("#cartbody").html("");
              $("#cartbody").append($('<div>').load("{{ url('/minCart') }}"));
              $.notify({  // options
             message: 'Basket Updated' 
                },{
                  // settings
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
             }
            });
            }
            
        
       
    });
  });



   $("#mycart").click(function(){
  var table = $("#tickets tbody");

    table.find('tr').each(function (i) {
        var $tds = $(this).find('td'),
            tickettype = $tds.find("#ticketid").val(),
           
            Quantity = $tds.find("select").val();
            if(Quantity>0)
            {
             jQuery.ajax({
              method : 'GET',
            url  : "{{ url('/addtocart') }}",
            data :{'tickettype': tickettype,'quantity':Quantity},
            success :  function(resp) {
              
              $.notify({  // options
             message: 'Basket Updated Redirecting To Cart...' 
                },{
                  // settings
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
              window.location.href = "{{ url('/cart') }}";
             }
            });
            }
            
        
       
    });
  });
   });
</script>


@endsection