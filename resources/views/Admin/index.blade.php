@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-10 dashboard_right">
        <div class="row">
       <!--  <div class="pagehead col-sm-12">
          <div class="col-sm-6">
            <div class="pagdiv"><p class="pagheding">Welcome TO Mrs Portrait <span>2000</span></p>
              <p class="pagedes">Number of Views<span>10</span></p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="">
              <div class="pagdiv"><p class="pagheding">Active Sessions</p>
              <p class="pagedes">Booking</p>
            </div>
          </div>
        </div>
      </div> -->

        <div class="col-sm-12 totalnumsec">
          <div class="col-sm-3 totalnumdiv one">
            <div class="sectionbox_one">
            <?php  $count_customers =  DB::table('users')
                               ->where('user_type', 3)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               ->get();
                               ?>
         <p class="dashboardicon_div"> {{count($count_customers)}}<span class="icon"><i class="fa fa-users"></i></span></p>
           <p class="numbersofcst">Customers</p>
          </div> 
          </div>
          <div class="col-sm-3 totalnumdiv two">
            <div class="sectionbox_two">
            <?php  $count_photographers = DB::table('users')
                               ->where('user_type', 4)
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                               
                              ->get();  ?>
                              <p class="dashboardicon_div">{{count($count_photographers)}}<span class="icon"><i class="fa fa-camera"></i></span></p>
           <p class="numbersofcst">Photographers </p>
          </div>
        </div>
          <div class="col-sm-3 totalnumdiv three">
            <div class="sectionbox_one">
             <?php $count_eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->orderBy('id','DESC')
                                
                                ->get();
                               ?>
          <p class="dashboardicon_div">{{count($count_eventbookings)}}<span class="icon"><i class="fa fa-calendar"></i></span></p>
          <p class="numbersofcst">Session Bookings</p>
        </div>
          </div>
          <div class="col-sm-3 totalnumdiv four">
            <div class="sectionbox_two">
             <?php  $count_classbooking = DB::table('classbookings')
                               ->where('status', '=', 1)
                               ->where('deleted', '=', 0)
                                
                                ->get();
                               ?>
          <p class="dashboardicon_div">{{count($count_classbooking)}}<span class="icon"><i class="fa fa-book"></i></span></p>
          <p class="numbersofcst">Class Bookings</p>
          </div>
        </div>
        </div>
<div class="row">         
<div class="col-sm-12">
   <div class="col-sm-6 tablediv spacertop">
<div class="row"> 
     <div class="col-sm-12 hedti nopadding">
    <div class="row">  
      <div class="col-sm-6 nopadding"><p class="titleofborad">Event Booking</p></div>
      <div class="col-sm-6 selectdiv  topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View Event Booking" href="{{ route('admineventbookings.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
      </div>
     </div>
   </div>
    <div class="tablewrapper">
    <table class="table-bordered table-striped dask">
               
                <thead>
                    <tr class="headings">
                        <th class="">Booking Date</th>
                       
                        <th class="">Occassions</th>
                        <th class="">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @if(count($eventbookings)>0)
                     @foreach ($eventbookings as $eventbooking)
                   <tr class="tabledata">

                        <td class=""><?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->start_date)->format('d M, Y') }}<?php } ?> 

                       

                        <td class="">
                          <?php
                                if($eventbooking->occassions!='') {
                                     $catnameArray = array(); 
                                     $ocsioncatArr = explode(',', $eventbooking->occassions);
                                    foreach($ocsioncatArr as $ocsioncatid) {
                                      $catNames = DB::table('categories')->where('id', '=', $ocsioncatid)->get();
                                      foreach($catNames as $ctName) {
                                          $catnameArray[] = $ctName->cat_name;
                                         }
                                      }
                                       echo implode(', ', $catnameArray);  
                                      }
                                     ?>   

                        </td>
                       
   
                        

                    <td class="action_btns"> 
                    
                     <a class="tablebtn" href="{{ route('admineventbookings.show',$eventbooking->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>

                    </tr>
                     @endforeach
                     @else
                     
                    @endif

        </tbody>
      </table>

    </div>
  </div>
    <div class="col-sm-6 tablediv spacertop">
       <div class="row">
     <div class="col-sm-12 hedti nopadding">
     <div class="row">
      <div class="col-sm-6  nopadding"><p class="titleofborad">Customers</p></div>
      <div class="col-sm-6 selectdiv topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Customers" href="{{ route('admincustomers.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
    </div>
     </div>
   </div>
    <div class="tablewrapper">
    <table class="table-bordered table-striped">
               
                <thead>
                    <tr class="headings">
                        <th class="3">Image</th>
                        <th class="11">Name</th>
                        <th class="4">Registered</th>
                        <th class="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($customers)>0)
                     @foreach ($customers as $customer)
                    <tr class="tabledata">

                        <td class="3 imgdiv">
                        @if($customer->profile_picture!='') 
                        <img src="{{ url('public') }}/uploads/profileimages/{{ $customer->profile_picture }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>
                       
                        <td class="11">{{ $customer->name }}</td>
                    
                        <td class="4">{{ \Carbon\Carbon::parse($customer->created_at)->format('d M Y')}}</td>
  
                      
                    <td class="action_btns 3"> 
                     <a class="tablebtn" href="{{ route('admincustomers.show',$customer->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>  </td>                     
                    </tr>
                     @endforeach
                     @else
                    
                    @endif

        </tbody>
      </table>

    </div>
  </div>


  </div>
     </div>
 <div class="col-sm-12">
  <div class="col-sm-6 tablediv spacertop">
     <div class="row">
     <div class="col-sm-12  hedti nopadding">
     <div class="row">
      <div class="col-sm-6  nopadding"><p class="titleofborad">Occassions</p></div>
      <div class="col-sm-6 selectdiv topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Occassions" href="{{ route('admincategory.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
    </div>
     </div>

   </div>
    <div class="tablewrapper">
    <table class="table-bordered table-striped dask">
               
                <thead>
                    <tr class="headings">
                        <th class="">Image</th>
                        <th class="">Category Name</th>
                        <th class="">Tag line</th>
                        <th class="">Action</th>
                    </tr>
                </thead>
               <tbody>
                    @if(count($categories)>0)
                     @foreach ($categories as $categ)
                    <tr class="tabledata">
                       
                       <td class="">
                        @if($categ->cat_featur_image!='') 
                        <img src="{{ url('public') }}/uploads/category/featureimages/{{ $categ->cat_featur_image }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/uploads/category/featureimages/photoclass_default_image.png" class="img-responsive" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>

                        <td class="">{{ $categ->cat_name }}</td>
                       
                        <td class="">{{ $categ->cat_tagline }}</td>
                       
                        
                       
                        

                    <td class="action_btns"> 
                    
                     <a class="tablebtn" href="{{ route('admincategory.show',$categ->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                    
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     
                    @endif

        </tbody>
      </table>

    </div>
  </div>

    <div class="col-sm-6 tablediv spacertop">
 <div class="row">
    <div class="col-sm-12 hedti nopadding">  
       <div class="row">
      <div class="col-sm-6 nopadding"><p class="titleofborad">Photographers</p></div>
      <div class="col-sm-6 selectdiv topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Photographers" href="{{ route('adminphotographers.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
    </div>
     </div>
   </div>
    <div class=" tablewrapper">
<table class="table-bordered table-striped dask">
                <thead>
                    <tr class="headings">
                       <th class="3">Image</th>
                        <th class="11">Name</th>
                       
                        <th class="4">Registered</th>
                       
                        <th class="2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($photographers)>0)
                     @foreach ($photographers as $photographer)
                    <tr class="tabledata">

                      <td class="3">
                        @if($photographer->profile_picture!='') 
                        <img src="{{ url('public') }}/uploads/profileimages/{{ $photographer->profile_picture }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>
                        
                        <td class="11">{{ $photographer->name }}</td>
                       
                       
                       
                        <td class="4">{{ \Carbon\Carbon::parse($photographer->created_at)->format('d M Y')}}</td>
                       
                        

                    <td class="action_btns 2"> 
                    
                     <a class="tablebtn" href="{{ route('adminphotographers.show',$photographer->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                    
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     
                    @endif

        </tbody>
      </table>
    </div> 

  
  </div>
</div>
   <div class="col-sm-12">
     <div class="col-sm-6 tablediv spacertop">
       <div class="row">
    <div class="col-sm-12 hedti nopadding">  
       <div class="row">
      <div class="col-sm-6 nopadding"><p class="titleofborad">Class Booking</p></div>
      <div class="col-sm-6 selectdiv  topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Class Booking" href="{{ route('adminclassbooking.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
       </div>
     </div>
   </div>
    <div class=" tablewrapper">
<table class="table-bordered table-striped dask">
                <thead>
                    <tr class="headings">
                       <th class="">Booking Date</th>
                        <th class="">Tutor ID</th>
                       
                        <th class="">Booking Amount</th>
                       
                        <th class="">Action</th>
                    </tr>
                </thead>
                 <tbody>
                    @if(count($classbooking)>0)
                     @foreach ($classbooking as $classbookings)
                    <tr class="tabledata">

                        <td class=""><?php if($classbookings->course!='' || $classbookings->course!='0000-00-00'  || $classbookings->course!=NULL){?>{{ \Carbon\Carbon::parse($classbookings->course)->format('d M, Y') }}<?php } ?> </td>

                        <td class="">{{ $classbookings-> tutor_id }}</td>

                        <td class="">{{ $classbookings->booking_amount }}</td>


                         <td class="action_btns"> 
 
                     <a class="tablebtn" href="{{ route('adminclassbooking.show',$classbookings->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                     
                    </td>


                       
                    </tr>
                     @endforeach
                     @else
                    
                    @endif

        </tbody>
      </table>
    </div>
  </div> 
    

  <div class="col-sm-6 tablediv spacertop">
     <div class="row">
    <div class="col-sm-12 hedti nopadding">  
       <div class="row">
      <div class="col-sm-6 nopadding"><p class="titleofborad">Classes</p></div>
      <div class="col-sm-6 selectdiv topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Classes"  href="{{ route('adminclasses.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
      </div>
     </div>
   </div>
    <div class=" tablewrapper">
<table class="table-bordered table-striped dask">
                <thead>
                    <tr class="headings">
                       <th class="">Image</th>
                        <th class="">Class Name</th>
                       
                        <th class="">Price</th>
                       
                        <th class="">Action</th>
                    </tr>
                </thead>
                 <tbody>
          @if(count($photoclasses)>0)
              @foreach ($photoclasses as $photoclass)
           <tr class="tabledata">
            <td class=" user_img">
            @if ($photoclass->pc_image!='' && $photoclass->pc_image!='NULL')
            <img src="{{ url('public') }}/uploads/photoclassimages/{{$photoclass->pc_image}}" style="width: 40px; height: 30px;" >
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" style="width: 40px; height: 30px;">
            @endif
            </td>
            <td class="">{{$photoclass->class_title}}</td>
            
            <td class="">${{$photoclass->pc_price}}</td>
           
            <td class=""><a href="{{ route('adminclasses.show',$photoclass->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
          </tr>
           @endforeach
        @else
          <tr class="tabledata">
            <td colspan="7" style="text-align: center;"><b>No record found</b></td>
          </tr>
        @endif                               
        </tbody>
      </table>
    </div>
  </div>
     </div> 



      <div class="col-sm-12">
    <div class="col-sm-6 tablediv spacertop">
       <div class="row">
     <div class="col-sm-12 hedti nopadding">
     <div class="row">
      <div class="col-sm-6 nopadding"><p class="titleofborad">Addon</p></div>
      <div class="col-sm-6 selectdiv  topbar_selectiv text-right "><a class="" data-toggle="tooltip" title="View More Addon" href="{{ route('adminaddons.index') }}"><button type="button" class="btn btn-default"><i class="fa fa-align-left"></i></button></a></div>
    </div>
     </div>
   </div>
    <div class="tablewrapper">
    <table class="table-bordered table-striped dask">
               
                <thead>
                    <tr class="headings">
                        <th class="">Image</th>
                        <th class="">Title</th>
                       
                        <th class="">Price</th>
                        <th class="">Action</th>
                    </tr>
                </thead>
               <tbody>
                    @if(count($addons)>0)
                     @foreach ($addons as $addon)
                    <tr class="tabledata">
                       
                       <td class="">
                        @if($addon->adon_image!='') 
                        <img src="{{ url('public') }}/uploads/addonsimages/{{ $addon->adon_image }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/uploads/addonsimages/addon_default_image.png" class="img-responsive" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>

                        <td class="">{{ $addon->adon_title }}</td>
                       
                       
                       
                        <td class="">{{ $addon->adon_price }}</td>
                       

                    <td class="action_btns"> 
                   
                     <a class="tablebtn" href="{{ route('adminaddons.show',$addon->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                   
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                    
                    @endif

        </tbody>
      </table>

    </div>
  </div>

 
  
  <div class="col-sm-6 tablediv spacertop">
     <div class="row">
     <div class="col-sm-12 hedti nopadding">
     <div class="row">
      <div class="col-sm-6 nopadding"><p class="titleofborad">Social Media</p></div>
      <div class="col-sm-6 selectdiv  topbar_selectiv text-right "></div>
    </div>
     </div>
   </div>
   <div class="row">
    <div class="col-sm-12 socical_media_div">
      <div class="col-sm-12 nopadding">
      <div class="col-sm-6">
      
          <div class="socialmedia_icon mrs_facbook">
   

         <img src="{{ url('public') }}/images/face.png" class="img-responsive" style="width:85%; height: 85px;">
         <p class="social_text">15k likes</p>

         
        </div>
      </div>
      <div class="col-sm-6">
        <div class="socialmedia_icon mrs_twitter">
          <img src="{{ url('public') }}/images/twitter.png" class="img-responsive" style="width:85%; height: 85px;">
        <p class="social_text">20M Followers</p>
        </div>
      </div>
    </div>
      <div class="col-sm-12 nopadding">
      <div class="col-sm-6">
        <div class="socialmedia_icon mrs_insta">
          <img src="{{ url('public') }}/images/instagram.png" class="img-responsive" style="width:85%; height: 85px;">
          <p class="social_text">15k likes</p>
        </div>
      </div>
      <div class="col-sm-6">  
        <div class="socialmedia_icon mrs_linkdin">
           <img src="{{ url('public') }}/images/linkdin.png" class="img-responsive" style="width:85%; height: 85px;">
           <p class="social_text">20M Followers</p>
        </div>
      </div>
    </div>
     </div>
    </div>

  </div>
  </div>
     
            <div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
      </div>
    </div>
    </div>
                       

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style>


.pagheding {
    padding-left: 17px;
    font-size: 17px;
}

.pagedes {
    padding-left: 17px;
}

.mrs_facbook {
    background: url();
}


a.editbtnn {
    border: solid #434547 2px;
    color: #434547;
    padding: 8px;
    font-size: 15px;
}
p.social_text {
    text-align: center;
}
p.social_text {
    background: #fff;
    padding-top: 5px;
    padding-bottom: 7px;
    width: 85%;
    font-size: 15px;box-shadow: 0 2px 23px 6px #efeded;
}
.dashboardicon_div > span {
    float: right;
    font-size: 40px;
    padding-right: 10px;
}
.totalnumdiv {
    padding: 0 6px;
}

.dashboardicon_div {
    text-align: left !important;
    font-size: 39px !important;
    font-weight: bold !important;
    width: 87%;
  }

.totalnumsec {
    padding: 0 25px;
}
.tablesection td{padding: 10px;text-align: left;}
.user_img img{width: 50px;height: 50px;}


.totalnumdiv.one .sectionbox_one {
    background: #00c0ef none repeat scroll 0 0;
    color: #fff;
    font-size: 18px;
    line-height: 25px;
    padding-bottom: 30px;
    padding-left: 14px;
    padding-top: 30px;
    text-transform: uppercase;}

.totalnumdiv.two  .sectionbox_two {

    background: #00a65a;
    color: #fff;
    font-size: 18px;
    line-height: 25px;
    padding-bottom: 30px;
    padding-left: 14px;
    padding-top: 30px;
    text-transform: uppercase;
  }

.totalnumdiv.three .sectionbox_one {
    background: #f39c12 none repeat scroll 0 0;
    color: #fff;
    font-size: 18px;
    line-height: 25px;
    padding-bottom: 30px;
    padding-left: 14px;
    padding-top: 30px;
    text-transform: uppercase;}

 .totalnumdiv.four  .sectionbox_two {
    background: #dd4b39;
    color: #fff;
    font-size: 18px;
    line-height: 25px;
    padding-bottom: 30px;
    padding-left: 14px;
    padding-top: 30px;
    text-transform: uppercase;
    }
.numbersofcst {
    font-size: 15px;
    padding-top: 17px;
}
.icon .fa {
 color: rgba(0,0,0,0.15);
}
.icon .fa:hover {
    transform: scale(1.2);
}
.icon .fa {
    transition: .4s ease;
    font-size: 48px;
}
.tablediv .tabledata td {
    border: 0 none;
    padding: 5px 10px;
    text-align: left;
}
.action_btns.\33 {
    text-align: center;
    text-align: center !important;
}
.tablediv.dask table {
    height: 230px !important;
}
.table-bordered.table-striped.dask {
    height: 246px;
}
tr.tabledata:hover {
    background: #dddddd !important;
}
p.titleofborad {
    font-size: 18px;
    margin-top: 17px;
    padding-bottom: 0px;
    margin-bottom: 0px;
}
.hedti {
    margin-bottom: 7px;
    margin-top: 0px;
}
.socical_media_div {
    margin-top: 6px;
}
.pagdiv {
    background: #fff none repeat scroll 0 0;
    box-shadow: -1px 2px 20px 8px #efeded;
    margin-bottom: 22px;
    padding-bottom: 22px;
    padding-top: 22px;
    border-top: solid;
}
</style>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

@endsection
