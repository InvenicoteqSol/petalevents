@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
  <!--booking-sec-start-->
  <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-users" aria-hidden="true"></i>

                  </div>
                  <p class="card-category">Customers</p>
                  <h3 class="card-title">{{$totalCustomersCount}}
                    
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                    <a href="{{ url('/admincustomer')}}">View All</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                   <i class="fa fa-sitemap" aria-hidden="true"></i>

                  </div>
                  <p class="card-category">Organisers</p>
                  <h3 class="card-title">{{$totalOrganisersCount}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                  </div>
                  <p class="card-category">Total Events</p>
                  <h3 class="card-title">{{$totalEventCount}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-database" aria-hidden="true"></i>



                  </div>
                  <p class="card-category">Data Requests</p>
                  <h3 class="card-title">{{$data_request_Count}}</h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                  
                    <a href="{{ url('/postsevent')}}">View All</a>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Customers</h4>
                  
                </div>
                <div class="card-body">
                 <table class="table table-hover">
               
                <thead>
                    <tr class="text-warning">
                        <th >Image</th>
                        <th >Name</th>
                        <th >Registered</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($topFiveCustomers)>0)
                     @foreach ($topFiveCustomers as $customer)
                    <tr class="tabledata">

                        <td >
                        @if($customer->profile_picture!='') 
                        <img src="{{ url('public') }}/uploads/profileimages/{{ $customer->profile_picture }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                         <img src="{{ url('public') }}/images/avtar.jpg" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>
                       
                        <td >{{ $customer->first_name }}</td>
                    
                        <td >{{ \Carbon\Carbon::parse($customer->created_at)->format('d M Y')}}</td>
  
                      
                    <td class="action_btns 3"> 
                     <a class="tablebtn" href="{{ route('admincustomer.show',$customer->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>  </td>                     
                    </tr>
                     @endforeach
                     @else
                    
                    @endif

        </tbody>
      </table>

                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Organisers</h4>
                  
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                <thead >
                    <tr class="text-warning">
                       <th> Image</th>
                        <th >Name</th>
                       
                        <th >Registered</th>
                       
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($topFiveOrganisers)>0)
                     @foreach ($topFiveOrganisers as $photographer)
                    <tr class="tabledata">

                      <td >
                        @if($photographer->profile_picture!='') 
                        <img src="{{ url('public') }}/uploads/profileimages/{{ $photographer->profile_picture }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/images/avtar.jpg" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>
                        
                        <td >{{ $photographer->first_name }}</td>
                       
                       
                       
                        <td >{{ \Carbon\Carbon::parse($photographer->created_at)->format('d M Y')}}</td>
                       
                        

                    <td class="action_btns 2"> 
                    
                     <a class="tablebtn" href="{{ route('adminorganiser.show',$photographer->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                    
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     
                    @endif

        </tbody>
      </table>
                </div>
              </div>
            </div>
          </div>

       

 
@endsection
