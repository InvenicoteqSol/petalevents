@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>

         @include('layouts.flash-message') 
         <div class="content">
        <div class="container-fluid">
          <div class="row">
      <div class="col-sm-12 dashboard_right">
        <div class="card">
          <div class="card-header card-header-primary">
         <div class="line_div"><h4 class="com_busin_sec card-title">Business Settings</h4>
          <p class="card-category">Your Settings</p>
        </div>
         </div>


                    {{ Form::model($businesssetting, array('route' => array('adminbusinesssetting.update', $businesssetting->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT')) }}
                       <div class="card-body">
                            <form>
                              <div class="col-md-12">
                       <div class="row">
                            <div class="col-md-6">
                              <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                                <label for="first_name" class="bmd-label-floating"> Business Name*</label>
                                <input id="business_name" type="text" class="form-control" name="business_name" value="{{ $businesssetting->business_name }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_name') }}</strong>
                                    </span>
                                @endif
                                  </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('business_email') ? ' has-error' : '' }}">
                                 <label for="business_email" class="bmd-label-floating">E-Mail Address*</label>
                                 <input id="business_email" type="business_email" class="form-control" name="business_email" value="{{ $businesssetting->business_email }}" maxlength="191" required >
                                @if ($errors->has('business_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_email') }}</strong>
                                    </span>
                                @endif
                                  </div>
                                </div>
                           </div>
                          </div>
                        <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('business_phone') ? ' has-error' : '' }}">
                                <label for="business_phone" class="bmd-label-floating">Phone*</label>
                                <input id="business_phone" type="text" class="form-control numberinput" name="business_phone" value="{{ $businesssetting->business_phone }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('business_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                         <div class="form-group{{ $errors->has('business_city') ? ' has-error' : '' }}">
                                <label for="business_city" class="bmd-label-floating"> Business City*</label>
                                <input id="business_city" type="text" class="form-control" name="business_city" value="{{ $businesssetting->business_city }}"  required autofocus>
                                @if ($errors->has('business_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-md-12">
                          <div class="row topcls">
                           <div class="col-md-6">
                            <div class="form-group{{ $errors->has('business_state') ? ' has-error' : '' }}">
                            <label for="business_state" class="bmd-label-floating"> Business State*</label>
                            <input id="business_state" type="text" class="form-control" name="business_state" value="{{ $businesssetting->business_state }}" minlength="2" maxlength="91" required autofocus>
                            @if ($errors->has('business_state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            <div class="col-md-6">
                             <div class="form-group{{ $errors->has('business_country') ? ' has-error' : '' }}">
                            <label for="business_country" class="bmd-label-floating"> Business Country *</label>
                             <input id="business_country" type="text" class="form-control" name="business_country" value="{{ $businesssetting->business_country }}" minlength="2" maxlength="91" required autofocus>
                             @if ($errors->has('business_country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('business_address') ? ' has-error' : '' }}">
                            <label for="business_address" class="bmd-label-floating"> Business Address *</label>
                            <input id="business_address" type="text" class="form-control" name="business_address" value="{{ $businesssetting->business_address }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                              <div class="form-group{{ $errors->has('business_zipcode') ? ' has-error' : '' }}">
                              <label for="business_zipcode" class="bmd-label-floating"> Business Postcode *</label>
                                <input id="business_zipcode" type="text" class="form-control" name="business_zipcode" value="{{ $businesssetting->business_zipcode }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
               <!--   <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('twitter_link') ? ' has-error' : '' }}">
                                <label for="twitter_link"> Twitter Link </label>
                                <input id="twitter_link" type="text" class="form-control" name="twitter_link" value="{{ $businesssetting->twitter_link }}" minlength="2" maxlength="255"  autofocus>
                                @if ($errors->has('twitter_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('twitter_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group{{ $errors->has('facebook_link') ? ' has-error' : '' }}">
                            <label for="facebook_link"> Facebook Link </label>
                                <input id="facebook_link" type="text" class="form-control" name="facebook_link" value="{{ $businesssetting->facebook_link }}"  maxlength="91"  autofocus>
                                @if ($errors->has('facebook_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('facebook_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                     </div>
                    </div> -->
                <!--      <div class="col-md-12">
                        <div class="row topcls">
                       <div class="col-md-6">
                            <div class="form-group{{ $errors->has('google_link') ? ' has-error' : '' }}">
                            <label for="google_link"> Google Link </label>
                                <input id="google_link" type="text" class="form-control" name="google_link" value="{{ $businesssetting->google_link }}"  maxlength="91"  autofocus>
                                @if ($errors->has('google_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('google_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                           <div class="col-md-6">
                            <div class="form-group{{ $errors->has('mailchip_keys') ? ' has-error' : '' }}">
                            <label for="mailchip_keys"> Mailchimp Keys </label>
                                <input id="mailchip_keys" type="text" class="form-control" name="mailchip_keys" value="{{ $businesssetting->mailchip_keys }}"  maxlength="91"  autofocus>
                                @if ($errors->has('mailchip_keys'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mailchip_keys') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div> -->
                        <div class="col-md-12">
                        <div class="row topcls">
                           <div class="col-md-6">
                              <div class="form-group {{ $errors->has('stripe_mode') ? ' has-error' : '' }}">
                        <label for="stripe_mode" class="bmd-label-floating">Stripe Mode</label>
                       <select id="stripe_mode" name="stripe_mode" class="form-control">
                         
                          <option value="Live" @if($businesssetting->stripe_mode=='Live') {{'selected'}} @endif>Live</option>
                         <option value="Test" @if($businesssetting->stripe_mode=='Test') {{'selected'}} @endif>Test</option>
  
                        </select>
                      @if ($errors->has('stripe_mode'))
                        <span class="help-block">
                         <strong>{{ $errors->first('stripe_mode') }}</strong>
                          </span>
                        @endif
                      </div>                         
                     </div>
                    <div class="col-md-6">
                            <div class="form-group{{ $errors->has('live_publish_key') ? ' has-error' : '' }}">
                            <label for="live_publish_key" class="bmd-label-floating"> Live Publish Key</label>
                                <input id="live_publish_key" type="text" class="form-control" name="live_publish_key" value="{{ $businesssetting->live_publish_key }}"  maxlength="91"  autofocus>
                                @if ($errors->has('live_publish_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('live_publish_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="col-md-12">
                        <div class="row topcls">
                     <div class="col-md-6">
                        <div class="form-group{{ $errors->has('live_secret_key') ? ' has-error' : '' }}">
                            <label for="live_secret_key" class="bmd-label-floating"> Live Secret Key</label>
                                <input id="live_secret_key" type="text" class="form-control" name="live_secret_key" value="{{ $businesssetting->live_secret_key }}"  maxlength="91" autofocus>
                                @if ($errors->has('live_secret_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('live_secret_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      <div class="col-md-6">
                              <div class="form-group{{ $errors->has('test_publish_key') ? ' has-error' : '' }}">
                            <label for="live_publish_key" class="bmd-label-floating"> Test Publish Key</label>
                                <input id="test_publish_key" type="text" class="form-control" name="test_publish_key" value="{{ $businesssetting->test_publish_key }}"  maxlength="91"  autofocus>
                                @if ($errors->has('live_publish_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('test_publish_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
              <!--    <div class="col-md-12">
                        <div class="row topcls">
                         <div class="col-md-6">
                              <div class="form-group{{ $errors->has('test_secret_key') ? ' has-error' : '' }}">
                            <label for="test_secret_key"> Test Secret Key*</label>
                                <input id="test_secret_key" type="text" class="form-control" name="test_secret_key" value="{{ $businesssetting->test_secret_key }}"  maxlength="91"  autofocus>
                                @if ($errors->has('test_secret_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('test_secret_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                     <div class="col-md-6">
                        <div class="form-group {{ $errors->has('paypal_mode') ? ' has-error' : '' }}">
                        <label for="paypal_mode">Pay Pal</label>
                       <select id="paypal_mode" name="paypal_mode" class="form-control">
                        
                          <option value="Live" @if($businesssetting->paypal_mode=='Live') {{'selected'}} @endif>Live</option>
                         <option value="Test" @if($businesssetting->paypal_mode=='Test') {{'selected'}} @endif>Test</option>
  
                        </select>
                      @if ($errors->has('paypal_mode'))
                        <span class="help-block">
                         <strong>{{ $errors->first('paypal_mode') }}</strong>
                          </span>
                        @endif
                      </div>                         
                    </div>
                </div>
            </div> -->    
          <!--   <div class="col-md-12">
                        <div class="row topcls">
                           <div class="col-md-6">
                            <div class="form-group{{ $errors->has('paypal_live_email') ? ' has-error' : '' }}">
                            <label for="paypal_live_email"> Paypal Live Email</label>
                                <input id="paypal_live_email" type="text" class="form-control" name="paypal_live_email" value="{{ $businesssetting->paypal_live_email }}"  maxlength="91"  autofocus>
                                @if ($errors->has('paypal_live_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('paypal_live_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                  <div class="col-md-6">
                           <div class="form-group{{ $errors->has('paypal_test_email') ? ' has-error' : '' }}">
                            <label for="paypal_test_email"> Paypal Test Email</label>
                                <input id="paypal_test_email" type="text" class="form-control" name="paypal_test_email" value="{{ $businesssetting->paypal_test_email }}"  maxlength="91"  autofocus>
                                @if ($errors->has('paypal_test_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('paypal_test_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div> --> 
                <div class="col-md-12">
                 <div class="row topcls">
                <div class="col-md-6">
                              <div class="form-group{{ $errors->has('booking_discount') ? ' has-error' : '' }}">
                                <label for="booking_discount" class="bmd-label-floating"> Booking Fee ( % ) </label>
                                <input id="booking_discount" type="text" class="form-control" name="booking_discount" value="{{ $businesssetting->booking_discount }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('booking_discount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('booking_discount') }}</strong>
                                    </span>
                                @endif
                                  </div>
                             </div>
                             <div class="col-md-6">
                        <div class="form-group{{ $errors->has('test_secret_key') ? ' has-error' : '' }}">
                            <label for="test_secret_key" class="bmd-label-floating"> Test Secret Key</label>
                                <input id="test_secret_key" type="text" class="form-control" name="test_secret_key" value="{{ $businesssetting->test_secret_key }}"  maxlength="91" autofocus>
                                @if ($errors->has('test_secret_key'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('test_secret_key') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                           </div>
                         </div>
                <div class="col-md-12">
                        <div class="topcls">
                     <div class="form-group">
                            <div class="buttondiv">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                     {{ Form::close() }}
            
        
    </div>
</div>
</div>
</div>
</div>
</div>
<script>
 $(document).ready( function() {

      var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
<script type="text/javascript">
 $(document).ready(function () {
     $(".numberinput").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
@endsection
