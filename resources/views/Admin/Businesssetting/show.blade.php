@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>


                    
                        
                        <div class="form-group">
                            <label for="business_name" class="col-md-4 control-label"> Business Name</label>
                            <div class="col-md-6">
                                <span class="form-control">{{ $businesssetting->business_name }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="business_email" class="col-md-4 control-label">Business Email</label>

                            <div class="col-md-6">
                               <span class="form-control"> {{ $businesssetting->business_email }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="business_phone" class="col-md-4 control-label">Business Phone</label>

                            <div class="col-md-6">
                               <span class="form-control">{{ $businesssetting->business_phone }}</span>
                            </div>
                        </div>

                         

                         <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Business City</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_city }}</span>
                            </div>
                        </div>

                       
                         <div class="form-group">
                            <label for="business_state" class="col-md-4 control-label">Business state</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_state }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_state" class="col-md-4 control-label">Business state</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_state }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_country" class="col-md-4 control-label">Business Country</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_country }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="business_address" class="col-md-4 control-label">Business Address</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_address }}</span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="business_address" class="col-md-4 control-label">Business zipcode </label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->business_zipcode }}</span>
                            </div>
                        </div>
                        
                          <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Twitter Link</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->twitter_link }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Facebook Link</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->facebook_link }}</span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Google Link</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->google_link }}</span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Mailchip keys</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->mailchip_keys }}</span>
                            </div>
                        </div>
                       <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Strip Mode</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->strip_mode }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Live Publish key</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->live_publish_key }}</span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Live Secrate Key</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->live_secrate_key }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Test Publish Key</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->test_publish_key }}</span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">test Secrate Key</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->test_secrate_key }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Paypal Mode</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->paypal_mode }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Paypal Mode</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->paypal_live_email }}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="business_city" class="col-md-4 control-label">Paypal Mode</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $businesssetting->paypal_test_email }}</span>
                            </div>
                        </div>






                       

                         
                        </div>
                      
            
    


@endsection
