@extends('layouts.userdefault')
@section('content')

@include('layouts.flash-message')

   <div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>

<div class="col-sm-9 dashboard_right">

        <div class="col-sm-4 selectdiv topbar_selectiv text-right">
            <div class="col-sm-5">
        <h3>Business Setting</h3>
      </div>
  </div>


<div class="row">
        <div class="col-md-12">

                    {{ Form::model($businesssetting, array('route' => array('adminbusinesssetting.update', $businesssetting->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm', 'method' => 'PUT')) }}

                        <div class="form-group{{ $errors->has('business_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label"> Business Name*</label>

                            <div class="col-md-6">
                                <input id="business_name" type="text" class="form-control" name="business_name" value="{{ $businesssetting->business_name }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>


                        <div class="form-group{{ $errors->has('business_email') ? ' has-error' : '' }}">
                            <label for="business_email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="business_email" type="business_email" class="form-control" name="business_email" value="{{ $businesssetting->business_email }}" maxlength="191" required readonly="readonly">
                                @if ($errors->has('business_email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      
                      <br>
                        <br>
                        <br>
                        <br>

                         <div class="form-group{{ $errors->has('business_phone') ? ' has-error' : '' }}">
                            <label for="business_phone" class="col-md-4 control-label">Phone*</label>

                            <div class="col-md-6">
                                <input id="business_phone" type="text" class="form-control" name="business_phone" value="{{ $businesssetting->business_phone }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('business_phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>


                        <div class="form-group{{ $errors->has('business_city') ? ' has-error' : '' }}">
                            <label for="business_city" class="col-md-4 control-label"> Business City*</label>

                            <div class="col-md-6">
                                <input id="business_city" type="text" class="form-control" name="business_city" value="{{ $businesssetting->business_city }}"  required autofocus>
                                @if ($errors->has('business_city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>

                         <div class="form-group{{ $errors->has('business_state') ? ' has-error' : '' }}">
                            <label for="business_state" class="col-md-4 control-label"> Business State*</label>

                            <div class="col-md-6">
                                <input id="business_state" type="text" class="form-control" name="business_state" value="{{ $businesssetting->business_state }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_state') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>
                        <div class="form-group{{ $errors->has('business_country') ? ' has-error' : '' }}">
                            <label for="business_country" class="col-md-4 control-label"> Business Country *</label>

                            <div class="col-md-6">
                                <input id="business_country" type="text" class="form-control" name="business_country" value="{{ $businesssetting->business_country }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>
                        <div class="form-group{{ $errors->has('business_address') ? ' has-error' : '' }}">
                            <label for="business_address" class="col-md-4 control-label"> Business Address *</label>

                            <div class="col-md-6">
                                <input id="business_address" type="text" class="form-control" name="business_address" value="{{ $businesssetting->business_address }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>
                        <div class="form-group{{ $errors->has('business_zipcode') ? ' has-error' : '' }}">
                            <label for="business_zipcode" class="col-md-4 control-label"> Business Zipcode *</label>

                            <div class="col-md-6">
                                <input id="business_zipcode" type="text" class="form-control" name="business_zipcode" value="{{ $businesssetting->business_zipcode }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                         <br>
                        <br>
                        <br>
                        <br>
                        <div class="form-group{{ $errors->has('business_zipcode') ? ' has-error' : '' }}">
                            <label for="business_zipcode" class="col-md-4 control-label"> Business Zipcode *</label>

                            <div class="col-md-6">
                                <input id="business_zipcode" type="text" class="form-control" name="business_zipcode" value="{{ $businesssetting->business_zipcode }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('business_zipcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('business_zipcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  
                    <div class="topcls">
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                     {{ Form::close() }}
            
        </div>





</div>

<div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
</div>
</div>
<script>
 $(document).ready( function() {

      var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
<style>
.col-md-6 {
    padding-bottom: 15px !important;
}
</style>


 
   <!--  <div class="row">
        <div class="col-md-12">


        	<table class="table-bordered table-striped">
                <thead>
                    <tr class="headings">
                        <th class="">Business Name</th>
                        <th class="">Business Email</th>
                        <th class="">Business Phone</th>
                        <th class="">Business City</th>
                        <th class="">Business State</th>
                        <th class="">Business Country</th>
                        <th class="">Business Address</th>
                        <th class="">Business Zipcode</th>
                        <th class="">Action</th>

                    </tr>
                </thead>
                <tbody>
                    @if(count($businesssetting)>0)
                     @foreach ($businesssetting as $businesssetting)
                    <tr class="tablesection">
                        <td class="">{{ $businesssetting->business_name }}</td>
                        <td class="">{{ $businesssetting->business_email }}</td>
                        <td class="">{{ $businesssetting->business_phone }}</td>
                        <td class="">{{ $businesssetting->business_city }}</td>
                        <td class="">{{ $businesssetting->business_state }}</td>
                        <td class="">{{ $businesssetting->business_country }}</td>
                        <td class="">{{ $businesssetting->business_address }}</td>
                        <td class="">{{ $businesssetting->business_zipcode }}</td>
                       
                        

                        <td class="action_btns"> 
                          <a class="tablebtn" href="{{ route('businesssetting.show',$businesssetting->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom">View</a>
                           <a class="tablebtn" href="{{ route('businesssetting.edit',$businesssetting->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"> Edit</a>
                     
                                      </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>

                       <div class="pagination_section">
                         
                        </div>

            
        </div>
    </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script> -->

@endsection
