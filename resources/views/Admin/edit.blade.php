@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-10 dashboard_right">
             <div class="line_div"><h3 class="com_busin_sec">Add-ons</h3></div>
      <div class="row">
        <div class="col-md-12">

                    {{ Form::model($addon, array('route' => array('adminaddons.update', $addon->id), 'id' => 'edtadonfrm', 'class' => 'edt_adon_frm formarea', 'method' => 'PUT', 'files' => true)) }}

                        <div class="col-md-12">
                         <div class="row">
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('adon_title') ? ' has-error' : '' }}">
                            <label for="adon_title"> Title*</label>
                                <input type="text" class="form-control" id="adon_title" name="adon_title" value="{{ $addon->adon_title }}" minlength="2" maxlength="191" required autofocus>
                                @if ($errors->has('adon_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adon_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                       <div class="col-md-6">
                            <div class="form-group{{ $errors->has('adon_price') ? ' has-error' : '' }}">
                            <label for="adon_price">Price*</label>
                                 <input type="text" class="form-control" id="adon_price" name="adon_price" value="{{ $addon->adon_price }}" maxlength="10" required>
                                @if ($errors->has('adon_price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adon_price') }}</strong>
                                    </span>
                                @endif  
                            </div>
                        </div>
                    </div>
                </div>
                        
                        <div class="col-md-12">
                                <div class="form-group{{ $errors->has('adon_description') ? ' has-error' : '' }}">
                                <label for="adon_description" >Description*</label>
                                 <textarea id="adon_description" name="adon_description" class="form-control" rows="5" cols="50"  maxlength="1001" required>{{ $addon->adon_description }}</textarea>

                                @if ($errors->has('adon_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('adon_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    <div class="col-md-12">
                         <div class="row">
                            <!-- <div class="col-md-4">
                               <div class="form-group">

                                <div class="col-md-3 nopadding" id="ftrd_browse_img" style="padding-top: 10px;">
                                    <label for="phone">Image</label>
                                      @if($addon->adon_image!='') 
                                        <img src="{{ url('public') }}/uploads/addonsimages/{{ $addon->adon_image }}" class="img-responsive" style="width: 100px; height: 100px;">
                                         @else
                                        <img src="{{ url('public') }}/uploads/addonsimages/addon_default_image.png" class="img-responsive" style="width: 100px; height: 100px;">
                                       @endif
                                        
                                    </div>

                                <div class="col-md-3 prflinput">
                                   <input id="adon_image" type="file" name="adon_image" accept="image/*">
                                  </div>

                            </div>
                        </div> -->

                    <div class="col-md-6">
                             <label for="status">Status</label>
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                          <div class="col-md-3 edit_radiobtn">
                                   <label class="radiodiv">Active
                                        <input type="radio" name="status" id="status1" value="1" <?php if($addon->status=="1"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                           </div>
                           <div class="col-md-3 edit_radiobtn">         
                                    <label class="radiodiv">Inactive
                                        <input type="radio" name="status" id="status2" value="0" <?php if($addon->status=="0"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                        
                    </div>
                </div>

               <div class="col-md-12 butn_edit_neupdates">
                        
                     <div class="form-group">
                            <div class="buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    
                </div>
                     {{ Form::close() }}
            
        </div>
    </div>
</div>
</div>

<script>
 $(document).ready( function() {

        $("#adon_image").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtadonfrm").validate({
        rules: {
                adon_title: {
                    required: true,
                    minlength: 2,
                    maxlength: 190
                },
                adon_description: {
                    required: true,
                    maxlength: 1000
                },
                adon_price: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                adon_title: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 190 characters allowed."
                },
                adon_description: {
                  required: "This is required field.", 
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 1000 characters allowed."
                },
                adon_price: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>

@endsection
