@extends('layouts.userdefault')
@section('content')
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
    
   <div class="col-sm-12 dashboard_right">
@include('layouts.flash-message')
<?php
       $event_id1=DB::table('requestdata')->where('id','=', $eventid)->first();
       $evntid=$event_id1->event_id;
        $customerdata = DB::table('users')
                    ->select('users.id','first_name','email','phone','orders.order_number')
                    ->join('orders', 'orders.user_id', '=', 'users.id')
                    ->where('orders.event_id','=', $evntid)
                    ->get();


        ?>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Customer Data</h4>
                 <p class="card-category"></p>
        </div>
       <div class="col-sm-12 tablediv spacertop card-body">
      <div class="tablewrapper table-responsive">
        @if(count($customerdata)>0)
      <h1 class="success_msg" style="color: green;">Mail Sended Successfully</h1>
      <table class="table-bordered table-striped table" id="cust_data">
              
                 <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name </th>
                        <th class="column4">Email</th>
                        <th class="column4">Phone</th>
                        <th class="column5">Order Number</th>
                        <th class="column3">Action</th>
                  </tr>
                </thead>
             
                <tbody>
                 
                 @foreach($customerdata as $customer)
               
                   <tr class="tabledata odd">
                       <td class="column3">{{$customer->first_name}}</td>

                        <td class="column4">{{$customer->email}}</td>

                        <td class="column4">{{$customer->phone}}</td>
                       
                        <td class="column5">{{$customer->order_number}}</td>


                     <td class="column3 action_btns"> 
                     <input type="checkbox" name="send_mail" value="{{ $customer->id }}" class="check_everone" id="check_all">
                    </td>
                      </tr>
               
               
               @endforeach

             </tbody>
      </table>
      <div>  <button class="send_mail_check btn btn-primary">Approve request</button></div>

       @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
               @endif


    </div>
    
                       
            
        </div>
      </div>
    </div>
  </div>
    </div>
     </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>

<script type="text/javascript">
 jQuery('.success_msg').hide();
  jQuery(".send_mail_check").click(function () {
    var selected = new Array();
    $('#cust_data input[type="checkbox"]:checked').each(function() {   
     selected.push($(this).attr('value'));
   });
    
    jQuery.ajax({
            type : 'GET',
            url  : "{{url('sendmail')}}",
            data:{ 'send_mail': selected,'reqId': <?php echo $eventid; ?>},
            success :  function(status) {
            if(status=='true') {
            jQuery('.success_msg').show();
            }
        }
    });
     
});
</script> 

@endsection
