@extends('layouts.innerdefault')
@section('content')

<section class="section-checkout">
   <div class="col-sm-12 dashboard_right">
     <?php   $user_id = Auth::user()->id ;
           $user_email=Auth::user()->email;?>
                                 
         
          <form action="{{url('/charge')}}" method="POST">
            {{ csrf_field() }}
            <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_6020SfAmkxN79Hq31s8W0WHZ"
                    data-amount="{{$amount*100}}"
                    data-name=""
                    data-description=""
                    data-image=""
                    data-email={{$user_email}}
                    data-locale="auto"
                    data-currency="gbp">
              </script>
              <input type="hidden" name="data_amount" value="{{$amount*100}}">
              <input type="hidden" name="user_id" value="{{$user_id}}">
			  <input type="hidden" name="requestId" value="{{$requestId}}">
        </form>
   
   </div>
</section>
@endsection



