@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
   <div class="col-sm-9 dashboard_right">
        <div class="col-md-12">

                     {!! Form::open(array('route' => 'adminclassbooking.store', 'method'=>'POST', 'id' => 'classbookingfrm',  'class' => 'edtclassbooking formarea', 'files' => true)) !!}

                      <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                            <label for="course" class="col-md-4 control-label"> Class Booking Date*</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control course" id="course" name="course" value="{{ @old(course) }}" required>
                                @if ($errors->has('course'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('course') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<?php 
  $classbooking = DB::table('photoclasses')
                               ->where('status', '=', 1)
                               ->where('deleted', '=', 0)
                                ->first();
                                $classbooking->name;

?>
                  <div class="form-group{{ $errors->has('tutor_id') ? ' has-error' : '' }}"">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-6">
                                <input type="hidden" class="form-control" id="tutor_id" name="tutor_id" value="{{ $classbooking->name }}">
    
                            </div>
                        </div>  

                        <div class="form-group{{ $errors->has('booking_amount') ? ' has-error' : '' }}">
                            <label for="booking_amount" class="col-md-4 control-label">Booking Amount*</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" id="booking_amount" name="booking_amount" value="{{ @old(booking_amount) }}" maxlength="10" required>
                                @if ($errors->has('booking_amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('booking_amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 

                        
                        <div class="form-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">
                            <label for="payment_status" class="col-md-4 control-label">Payment status</label>

                            <div class="col-md-6 edit_radiobtn">

                                   <label class="radiodiv">
                                        <input type="radio" name="payment_status" id="payment_status2" value="0" {{ old('payment_status')=="0" ? 'checked' : '' }} checked>
                                        <span class="checkmark"></span>Pending
                                   
                                    </label>

                                   <label class="radiodiv">
                                        <input type="radio" name="payment_status" id="payment_status1" value="1" {{ old('payment_status')=="1" ? 'checked' : '' }}>
                                        <span class="checkmark"></span>Completed
                                    </label>
                                    
                                @if ($errors->has('payment_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('payment_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('booking_status') ? ' has-error' : '' }}">
                            <label for="booking_status" class="col-md-4 control-label">Is this booking is open or closed?</label>

                            <div class="col-md-6 edit_radiobtn">
                                   <label class="radiodiv">
                                        <input type="radio" name="booking_status" id="booking_status2" value="0" {{ old('booking_status')=="0" ? 'checked' : '' }} checked>
                                        <span class="checkmark"></span> Open
                                    </label>

                                  <label class="radiodiv">
                                        <input type="radio" name="booking_status" id="booking_status1" value="1" {{ old('booking_status')=="1" ? 'checked' : '' }}>
                                        <span class="checkmark"></span> Closed
                                    </label>
                                   
                                @if ($errors->has('booking_status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('booking_status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div style="clear: both;"></div>

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6 edit_radiobtn">
                                   <label class="radiodiv">
                                        <input type="radio" name="status" id="status1" value="1" {{ old('status')=="1" ? 'checked' : '' }} checked>
                                        <span class="checkmark"></span>Active
                                    </label>
                                    <label class="radiodiv">
                                        <input type="radio" name="status" id="status2" value="0" {{ old('status')=="0" ? 'checked' : '' }}>
                                        <span class="checkmark"></span>Inactive
                                    </label>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                        

                     {{ Form::close() }}
            
        </div>
    </div>
     </div>
    </div>
<script>
 $(document).ready( function() {

        $(".course").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.course').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.course').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtclassbooking").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>

@endsection
