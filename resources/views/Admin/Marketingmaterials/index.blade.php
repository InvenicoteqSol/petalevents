@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>

   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
                  <h4 class="card-title">Marketing Materials </h4>              
                </div>
       

<div class="card-body marketing-mater">
        <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-4">
                          <div class="col_media">
                          <img src="{{ url('public') }}/images/petal-logo-black.jpg" class="img-circle" style="width:300px">
                           <a href="{{ url('public') }}/images/petal-logo-black.jpg" class="btn btn-primary" download > Download Here 
                       </a>
                     </div>
                     </div>
               <div class="col-md-4">
               <div class="col_media">
              <img src="{{ url('public') }}/images/petal-logo-bt.jpg" class="img-circle" style="width:300px">
              <a href="{{ url('public') }}/images/petal-logo-bt.jpg" class="btn btn-primary" download>Download Here
                       </a>
                      </div>
                      </div>
              <div class="col-md-4">
              <div class="col_media">
              <img src="{{ url('public') }}/images/petal-logo-bt1.jpg" class="img-circle" style="width:300px">
                    <a href="{{ url('public') }}/images/petal-logo-bt1.jpg" class="btn btn-primary" download>Download Here
                    </a>
                  </div>
                  </div>
                  </div>
                  <br>
                  <br>
             <div class="row">  
             <div class="col-md-4">
             <div class="col_media">
            <img src="{{ url('public') }}/images/petal-logo-white.jpg" class="img-circle" style="width:300px">
            <a href="{{ url('public') }}/images/petal-logo-white.jpg" class="btn btn-primary" download>Download Here
                  </a>
                  </div>
                  </div>
               <div class="col-md-4">
               <div class="col_media">
            <img src="{{ url('public') }}/images/petal-logo-wt.jpg" class="img-circle" style="width:300px">
            <a href="{{ url('public') }}/images/petal-logo-wt.jpg" class="btn btn-primary" download>Download Here
              </a>
                </div>
                </div>
             <div class="col-md-4">
            <div class="col_media">
            <img src="{{ url('public') }}/images/petal-logo-wt1.jpg" class="img-circle" style="width:300px">
            <a href="{{ url('public') }}/images/petal-logo-wt1.jpg" class="btn btn-primary" download>Download Here
              </a>
                </div>
                </div>
                </div>
                 <br>
                 <br>
            <div class="row">
            <div class="col-md-6"> 
            <div class="col_media">
            <a href="{{ url('public') }}/images/petal-logos.zip" class="btn btn-primary btn-block" download>Download all above Logos here
            </a>
            </div>
            </div>
            <div class="col-md-6"> 
            <div class="col_media">
            <a href="{{ url('public') }}/images/petal-logo-3.zip" class="btn btn-primary btn-block" download>Download all transparent Logos here
              </a>
            </div>
            </div>
            </div>
            </div>
            </div>
            </form>
            </div>
            </div> 
            </div>
            </div>
            </div>
            </div>

<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
</style>


@endsection
