@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>

    
   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
        <p class="pagetite">Event</p>
      </div>

      <div class="card-body">
          <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Event Date :</span> <span class="labldata"><?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->start_date)->format('d M, Y') }}<?php } ?> </span></p>
                        </div>
                        <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Event title :</span> <span class="labldata">{{ $eventbooking->event_title }}</span></p>
                        </div>
                      </div>
                               <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Tickets Remaining :</span> <span class="labldata">{{ $ticketsavailable }}</span></p>       </div>

                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Type of Ticket :</span> <span class="labldata">
                            @if(count($totalTickets1)>0)
                            @foreach($totalTickets1 as $ticket)
                            ,{{$ticket->ticket_type}}
                            @endforeach
                            @endif</span></p> 
                        </div>
                        </div>
                        <div class="row">
                         <div class="col-sm-12"> 
                    <p class="rightsidetext"><span class="lablname">Event info :</span> <span class="labldata"><br>{!! \Illuminate\Support\Str::words($eventbooking->event_info)  !!}</span></p>
                       </div> 
                        </div>
                      </div>
                    </div>
                      </form>

    </div>
      </div>
    </div>
  </div>
  <div class="row">
 <div class="col-md-12 col-xs-12">
  <div class="card">
            <div class="card-header card-header-primary">
        <p class="pagetite">Tickets Sold</p>
      </div>

      <div class="card-body">
        <table class="table">
                <thead class=" text-primary">
                  <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Name</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Quantity</th>
                        <th class="column3">Amount</th>
                        
                    </tr>
                </thead>
                <tbody>
            @if(count($eventTicketDetails)>0)
            @foreach ($eventTicketDetails as $order)
                      <tr class="tabledata odd">
                      <td class="column3">{{date('Y-M-d',strtotime($order->created_at))}}</td>
                      <td class="column3">{{ $order->first_name }} {{ $order->last_name }}</td>
                      <td class="column3">{{ $order->order_number }}</td>
                      <td class="column3">{{ $order->Quantity }}</td>
                    <!--   @if ($order->order_status_id == '1') <td class="column3">{{'Completed'}}</td>@elseif ($order->order_status_id == '0') <td class="column3">{{' Awaiting Payment '}}
                      </td>

                      @else  
                      @endif  -->
                     
                      <!--  @if ($order->Free_booking_fee == '1')  -->
                       <!--  <td class="column3">
                      <?php //$amount=$order->amount;
                        //$total=$amount+($amount*(10/100));
                        ?> 
                       {{$amount}} </td>
                       @else
                       <td class="column3">  {{$order->amount}}  </td> 
                       @endif  
                       -->
                         <td>{{$order->totalPrice}}</td>
                       </tr>
                      @endforeach
                      @else
                      <tr>
                        <td colspan="4">
                          No Tickets Booked So Far.....
                        </td>
                      </tr>
                      @endif
               
                </tbody>
              </table>
      </div>
    </div>
    <?php
        $d= strtotime("today");
        $date = date("Y-m-d", $d);
        $date1=  date("h:i:sa",$d);  
        $d1
           ?>
      
    @if(count($eventTicketDetails)>0 && (($date>=$eventbooking->end_date) ||($date1<=$eventbooking->end_time)))                    
    <?php  
      $event_title=$eventbooking->event_title;
      $username=  Auth::user()->username;
      $amount1=$amounttotal;
      $data1=json_decode($amount1, true);
                  foreach ($data1 as $row) { 
                    if($order->Free_booking_fee=='1'){
                     $totalamount = $row['count']; 
                     //$totalamount=$amount+($amount*(10/100));     
               } 
               else
               {
                     $totalamount = $row['count'];
                     
               }   
?>
     <div>
      <?php $quantitydata=json_decode($totalquantity,true);
      foreach ($quantitydata as $value) {
        $totalquantity1=$value['count'];
      ?>
      <div class="pull pull-right">
      <p>Total Tickets Sold: <strong>{{$totalquantity1}}</strong></p>
    <?php } ?>

      <p>Total Amount Paid: <strong>{{$totalamount}}</strong> </p>
    </div>
      <form action="{{url('/sendmailpetal')}}" method="GET">
      {{ csrf_field() }}
      <input type="hidden" name="totalamount" value="{{$totalamount}}">
      <input type="hidden" name="username" value="{{$username}}">
      <input type="hidden" name="event_title" value="{{$event_title}}">
      <button class="send_mail_check btn btn-primary">Send payment request</button></div>

      </form>
    <?php } ?>
    @endif
        <button onclick="myFunction()" target="_blank" class="btn btn-primary">Print Tickets</button>

     <script>
      function myFunction() {
        window.print();
      }
    </script>
 </div>
</div>
</div>
</div>
    <style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
 span.lablnamed {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
p.pagetite {
    font-size: 24px;
    padding-left: 15px;
    
}
</style>


@endsection
