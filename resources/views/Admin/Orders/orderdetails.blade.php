<?php use \App\Http\Controllers\OrdersController;?>
<li class="c_head"><label>Order Overview</label></li>
 <li class="item-list item-head">
                <div class="item-name">
                	<div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                    	<div class="col-sm-6 col-xs-6">
                            <b>Order No</b><br> {{ $order->order_number }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> {{ $order->first_name }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> {{ $order->last_name }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> {{ $order->totalPrice }}
                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  {{ $order->order_number }}
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br> {{date('Y-M-d',strtotime($order->created_at))}}
                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr><th>
                                Ticket
                            </th>
                             <th>
                                Quantity
                            </th>
                            
                            <th>
                                Price
                            </th>
                            
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <tr>
                                    <td>
                                        {{ $order->ticket_type }}
                                    </td>
                                    <td>
                                      {{ $order->Quantity }}   
                                    </td>
                                    <td>
                                     {{ $order->netPrice }}
                                    </td>
                                     <td>
                                     {{ $order->totalPrice }}
                                  
                                    </td>
                                </tr>
                                 
                            </tbody>
                        </table>

                    </div>
                </div>

              
            </div> <!-- /end modal body-->

            <div class="modal-footer">
               <button class="btn modal-close btn-danger" data-dismiss="" type="button">Close</button>
            </div>
