@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
  
<div class="col-sm-10 dashboard_right dasbagcol">
        <div class="col-sm-12 sectionbox ">
             <div class="col-sm-5">
                
                <div class="col-sm-8">
                   <p class="singalname">Coupon Name</p>
                  <p class="singalnamee">{{ $coupens->coupen_name }}</p>
                </div>
              </div>

              <div class="col-sm-7">
                <div class="col-sm-12 rightbox">
                  <p class="rightsidetext"><span class="lablname">Coupon Code :</span><span class="labldata"> {{ $coupens->coupen_code }} </span></P>
                    
                  <p class="rightsidetext"><span class="lablname">Coupon Amount :</span> <span class="labldata"> {{ $coupens->coupen_prices }}</span></p>

                   <!-- <p class="rightsidetext"><span class="lablname">Coupen Discount :</span> <span class="labldata"> {{ $coupens->coupen_discount }}</span></p> -->
                  <p class="rightsidetext"><span class="lablname">Coupon Start Data :</span> <span class="labldata"> {{ $coupens->vaild_start_date }}</span></p>

                 <p class="rightsidetext"><span class="lablname">Coupon End Data :</span> <span class="labldata"> {{ $coupens->vaild_end_date }}</span></p>


</div>
</div> 
<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
 span.lablnamed {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
p.singalnamee {
    font-size: 22px;
    color: #969da5;
}


</style>



@endsection
