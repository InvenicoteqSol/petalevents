@extends('layouts.userdefault')
@section('content')
   <div class="row">
   
    
   <div class="col-sm-12 dashboard_right">
    @include('layouts.flash-message')
<div class="col-sm-12 nopadding">
  <div class="row">
        <div class="col-sm-4 selectdiv topbar_selectiv ">
        <?php   $id_evnt=request()->route('id');?>
        </div>
        <div class="col-sm-8 selectdiv topbar_selectiv text-right createbtn">
  <div class="dropdown head-dropdown">
  <a class="btn btn-primary" id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    Create New Collaboration
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu" aria-labelledby="dLabel">
    <li> <a data-toggle="modal" data-target="#linkmodal_self" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">My Event Collaborations</a></li>
    <li> <a data-toggle="modal" data-target="#earlybird" data-whatever="@earlybird" style="cursor: pointer;" title="Send Request">Organiser Collaborations</a></li>
  
    
  </ul>
</div>

    </div>
    <div class="modal" id="linkmodal_self" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">My Event Collaborations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/selfevents">
          {{ csrf_field() }}
            <input type="hidden" class="form-control" name="coll_type" value="self">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="100" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
          <label for="event_id" class="control-label"> Event A*</label>
          <select class="form-control" name="event_id" required="">
            <option value=""> -- Select Event A -- </option>
            @foreach($eventbookings as $item)
              <option value="{{$item->id}}">{{$item->event_title}}</option>
            @endforeach
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
          <label for="ticket_id" class="control-label">Ticket*</label>
            <select class="form-control" name="ticket_id" required="">
            <option value=""> </option>
            @foreach($tickets as $item)
              <option value="{{$item->id}}">{{$item->ticket_type}}</option>
            @endforeach
          </select>
          </div>
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id2') ? ' has-error' : '' }}">
          <label for="event_id2" class="control-label"> Event B*</label>
          <select class="form-control" name="event_id2" required="">
            <option value=""> -- Select Event B -- </option>
            @foreach($eventbookings as $item)
              <option value="{{$item->id}}">{{$item->event_title}}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id2') ? ' has-error' : '' }}">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" required="">
            <option value=""> </option>
            @foreach($tickets as $item)
              <option value="{{$item->id}}">{{$item->ticket_type}}</option>
            @endforeach
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
           <div class="form-group">
          <label for="discount_type" class="control-label"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type" required="">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
          <option value="0"> Percentage</option>
          <option value="1"> Fixed</option>

          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{ @old(discount) }}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required="">
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="{{ @old(end_date) }}" required="">
          @if ($errors->has('end_date'))
          <span class="help-block">
              <strong>{{ $errors->first('end_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>

  <!------end first form for discount------------------------------->
    <!------Start second form for discount------------------------------->

  <div class="modal" id="earlybird" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Organiser Collaborations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/collabrationevent">
           <input type="hidden" class="form-control" name="coll_type" value="collticket">

          {{ csrf_field() }}
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="100" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
          <label for="event_id" class="control-label"> Event*</label>
          <select class="form-control" name="event_id" required="">
             <option value=""> -- Select Event A -- </option>
            @foreach($eventbookings as $item)
              <option value="{{$item->id}}">{{$item->event_title}}</option>
            @endforeach
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id" required="">
               <option value=""> </option>
            @foreach($tickets as $item)
              <option value="{{$item->id}}">{{$item->ticket_type}}</option>
            @endforeach
          </select>
          </div>
          </div>
          </div>
          </div>
            <?php       
                 $user_id=Auth::user()->id;
                 $users = DB::table('users')
                                     ->where('user_type','=',4)
                                     ->where('deleted', 0)
                                     ->where('id','!=',$user_id)
                                     ->get();
                                     ?> 
             <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('org_id') ? ' has-error' : '' }}">
          <label for="org_id" class="control-label"> Organisation name*</label>
          <select class="form-control" name="org_id" required="">
             <option value=""> -- Select Organisation name -- </option>
            @foreach($users as $item)
              <option value="{{$item->id}}">{{$item->organisation_name}}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
  
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id2') ? ' has-error' : '' }}">
          <label for="event_id2" class="control-label"> Event B*</label>     
          <select class="form-control" name="event_id2" required="">
            <option value=""> -- Select Event B -- </option>
            @foreach($eventbookings as $item)
              <option value="{{$item->id}}">{{$item->event_title}}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id2') ? ' has-error' : '' }}">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" id="ticket_sec2" required="">
           <option value=""> </option>
            @foreach($tickets as $item)
              <option value="{{$item->id}}">{{$item->ticket_type}}</option>
            @endforeach
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
             <div class="form-group{{ $errors->has('discount_type') ? ' has-error' : '' }}">
               <label for="discount_type" class="control-label"> Type of Discount </label>
                    <select class ="form-control" id="discount_type" name="discount_type" value="" required="">
                    <option disabled="disabled" selected="selected"> --- Select --- </option>
                    <option> Percentage</option>
                    <option> Fixed</option>
          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{ @old(discount) }}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required="">
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="{{ @old(end_date) }}" required="">
          @if ($errors->has('end_date'))
          <span class="help-block">
              <strong>{{ $errors->first('end_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>

         
          <!-----------------first discound ------------------>
       <div class="row">
       <div class="col-lg-12 col-md-12">
        <div class="card">
        <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">My Event Collaborations</h4>
                   </div>
                <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Discount</th>
                        <th class="column3">Start Date</th>
                        <th class="column3">End Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $ticketcollabrations = DB::table('ticketcollabration')
                  ->where('coll_type','=','self')
                  ->where('created_by','=',Auth::user()->id)
                  ->where('deleted',0)
                  ->get();  ?>
                    @if(count($ticketcollabrations)>0)
                     @foreach ($ticketcollabrations as $ticketcollabration)

                    <tr class="tabledata odd">
                        <td class="column3">{{ $ticketcollabration->name }}</td>
                        <td class="column3">{{ $ticketcollabration->discount }}</td>
                        <td class="column3"> {{ \Carbon\Carbon::parse($ticketcollabration->start_date)->format('d M Y')}} </td>
                        <td class="column3">  {{ \Carbon\Carbon::parse($ticketcollabration->end_date)->format('d M Y')}}</td>
                      <td class="column3 action_btns"> 
                        

 <div class="modal" id="linkmodaledit_{{$ticketcollabration->id}}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">My Event Collaborations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/selfeventsedit/{{$ticketcollabration->id}}">
          {{ csrf_field() }}
           <input type="hidden" class="form-control" name="coll_type" value="self">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$ticketcollabration->name}}" maxlength="100" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
          <label for="event_id" class="control-label"> Event A*</label>
          <select class="form-control" name="event_id" required="">
            @foreach($eventbookings as $item)
            <option value="{{ $item->id }}" {{ ($ticketcollabration->event_id ==  $item->id ) ? 'selected' : '' }}>{{ $item->event_title }}</option>
            @endforeach
          </select>
          </div>
          </div> 
           <?php 
          $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->where('event_id','=',$ticketcollabration->event_id)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id" required="">
            @foreach($tickets as $item)
           
              <option value="{{ $item->id }}" {{ ($ticketcollabration->ticket_id ==  $item->id ) ? 'selected' : '' }}>{{ $item->ticket_type }}</option>

            @endforeach
          </select>
          </div>
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id2') ? ' has-error' : '' }}">
          <label for="event_id2" class="control-label"> Event B*</label>
           <select class="form-control" name="event_id2" required="">
            @foreach($eventbookings as $item)
              <option value="{{ $item->id }}" {{ ($ticketcollabration->event_id2 ==  $item->id ) ? 'selected' : '' }}>{{ $item->event_title }}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id2') ? ' has-error' : '' }}">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" required="">
            @foreach($tickets as $item)
            <option value="{{ $item->id }}" {{ ($ticketcollabration->ticket_id2 ==  $item->id ) ? 'selected' : '' }}>{{ $item->ticket_type }}</option>
            @endforeach
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
           <div class="form-group">
          <label for="discount_type" class="col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type" value="{{$ticketcollabration->discount_type}}">
          <option> Percentage</option>
          <option> Fixed</option>

          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{$ticketcollabration->discount}}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($ticketcollabration->start_date!='' || $ticketcollabration->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($ticketcollabration->start_date)->format('d-m-Y') }}<?php } ?>" required="">
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($ticketcollabration->end_date!='' || $ticketcollabration->end_date!='0000-00-00'  || $ticketcollabration->end_date!=NULL){?>{{ \Carbon\Carbon::parse($ticketcollabration->end_date)->format('d-m-Y') }}<?php } ?>" required="">
          @if ($errors->has('end_date'))
          <span class="help-block">
              <strong>{{ $errors->first('end_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>
                          <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                      <a class="dropdown-item" data-toggle="modal" data-target="#linkmodaledit_{{$ticketcollabration->id}}" data-whatever="@linkmodaledit" style="cursor: pointer;" title="Send Request"><i class="fa fa-edit"></i>Edit</a>
                      <a class="dropdown-item">
                  <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroyticketcoll/{{$ticketcollabration->id}}">
                     
                        <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                 </div>
               </div>
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

            </tbody>
          </table>
        </div>
       </div>
     </div>

       <!-----------------End first discound ------------------>
        <!-----------------first discound ------------------>

            <div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                 <h4 class="card-title">Organiser Collaborations</h4>
                   </div>
                <div class="card-body table-responsive">
                 <table class="table table-hover">
                   <thead>
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Discount</th>
                         <th class="column3">Start Date</th>
                        <th class="column3">End Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                <?php 
                 $ticketcollabrations = DB::table('ticketcollabration')
                    ->where('coll_type','=','collticket')
                    ->where('created_by','=',Auth::user()->id)->
                    orwhere(function ($query1)  {
                      $query1->where('org_id','=', Auth::user()->id)
                          ->where('status', '=', 1);
                     })->where('deleted', 0)->get();
                    ?>
                    @if(count($ticketcollabrations)>0)
                     @foreach ($ticketcollabrations as $ticketcollabration)

                    <tr class="tabledata">
                       <td class="column3">{{ $ticketcollabration->name }}</td>

                        <td class="column3">{{ $ticketcollabration->discount }}</td>
                   <!--      <td class="column3">{{ $ticketcollabration->ticket_id }}</td>
                        <td class="column3">{{ $ticketcollabration->event_id2 }} </td>
                        <td class="column3">{{ $ticketcollabration->ticket_id2 }} </td> -->
                        <td class="column3"> {{ \Carbon\Carbon::parse($ticketcollabration->start_date)->format('d M Y')}} </td>
                        <td class="column3">  {{ \Carbon\Carbon::parse($ticketcollabration->end_date)->format('d M Y')}}</td>
                     <td class="column3 action_btns"> 
                      

 <div class="modal" id="linkmodaledit_{{$ticketcollabration->id}}" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Collaboration Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/earlybirdedit/{{$ticketcollabration->id}}">
          {{ csrf_field() }}
           <input type="hidden" class="form-control" name="coll_type" value="collticket">

                  <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$ticketcollabration->name}}" maxlength="100" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id') ? ' has-error' : '' }}">
          <label for="event_id" class="control-label"> Event*</label>
          <select class="form-control" name="event_id" required="">
            @foreach($eventbookings as $item)
            <option value="{{ $item->id }}" {{ ($ticketcollabration->event_id ==  $item->id ) ? 'selected' : '' }}>{{ $item->event_title }}</option>
            @endforeach
          </select>
          </div>
          </div> 
           <?php             
/*           $eventitmes = DB::table('eventbookings')->pluck('id', 'event_title'); 
*/           $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->where('created_by','=',Auth::user()->id)
                                     ->where('event_id','=',$ticketcollabration->event_id)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id" required=""> 
            @foreach($tickets as $item)
           
              <option value="{{ $item->id }}" {{ ($ticketcollabration->ticket_id ==  $item->id ) ? 'selected' : '' }}>{{ $item->ticket_type }}</option>

            @endforeach
          </select>
          </div>
          </div>
          </div>
          </div>
             <?php  
                 $user_id=Auth::user()->id;
                 $users = DB::table('users')
                     ->where('deleted', 0)
                     ->where('user_type','=',4)
                     ->where('id','!=',$user_id)
                     ->get();
                   ?> 
             <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('org_id') ? ' has-error' : '' }}">
          <label for="org_id" class="control-label"> Organisation name*</label>
          <select class="form-control" name="org_id" required="">
            @foreach($users as $item)
            <option value="{{ $item->id }}" {{ ($ticketcollabration->org_id ==  $item->id ) ? 'selected' : '' }}>{{ $item->organisation_name }}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
  
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('event_id2') ? ' has-error' : '' }}">
          <label for="event_id2" class="control-label"> Event B*</label>
          <select class="form-control" name="event_id2" required="">
            <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->where('user_id','=',$ticketcollabration->org_id)
                                     ->get();
                                     ?>     
             @foreach($eventbookings as $item)
              <option value="{{ $item->id }}" {{ ($ticketcollabration->event_id2 ==  $item->id ) ? 'selected' : '' }}>{{ $item->event_title }}</option>
            @endforeach
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticket_id2') ? ' has-error' : '' }}">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" id="ticket_sec2" required="">
             <?php             
/*           $eventitmes = DB::table('eventbookings')->pluck('id', 'event_title'); 
*/           $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->where('event_id','=',$ticketcollabration->event_id2)
                                     ->get();
                                     print_r($tickets);
                                     ?> 
             @foreach($tickets as $item)
            <option value="{{ $item->id }}" {{ ($ticketcollabration->ticket_id2 ==  $item->id ) ? 'selected' : '' }}>{{ $item->ticket_type }}</option>
            @endforeach
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
             <div class="form-group{{ $errors->has('discount_type') ? ' has-error' : '' }}">
               <label for="discount_type" class="control-label"> Type of Discount </label>
           <select class ="form-control" id="discount_type" name="discount_type" value="{{$ticketcollabration->discount_type}}" required="">
          <option> Percentage</option>
          <option> Fixed</option>

          </select>
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount*</label>
        <input type="number" class="form-control" id="discount" name="discount" value="{{$ticketcollabration->discount}}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
           <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($ticketcollabration->start_date!='' || $ticketcollabration->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($ticketcollabration->start_date)->format('d-m-Y') }}<?php } ?>" required="">
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($ticketcollabration->end_date!='' || $ticketcollabration->end_date!='0000-00-00'  || $ticketcollabration->end_date!=NULL){?>{{ \Carbon\Carbon::parse($ticketcollabration->end_date)->format('d-m-Y') }}<?php } ?>" required="">
          @if ($errors->has('end_date'))
          <span class="help-block">
              <strong>{{ $errors->first('end_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>
                     
                      <div class="dropdown d-action">
                      <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                     </a>
                     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                      <a class="dropdown-item" data-toggle="modal" data-target="#linkmodaledit_{{$ticketcollabration->id}}" data-whatever="@linkmodaledit" style="cursor: pointer;" title="Send Request"><i class="fa fa-edit"></i>Edit</a>

                        <a class="dropdown-item">
                    <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroyticketcoll/{{$ticketcollabration->id}}">
                     
                        <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                   </div>
                 </div>
                  </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif
            </tbody>
          </table>
        </div>
       </div>
     </div>

       <!-----------------End first discound ------------------>

 <!------ End second form for discount------------------------------->
 </div>
    </div>
     </div>
   </div>
 </div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<script>
 $(document).ready( function() {
    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
  <script type="text/javascript">
    $(document).ready(function() {
    $('select[name="event_id"]').on('change', function() {
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                  
                $('select[name="ticket_id"]').empty();
                $('select[name="ticket_id"]').append('<option value="0">--Please Select--</option>');

                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="ticket_id"]').append('<option value="'+ value["id"] +'">'+ value["ticket_type"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="ticket_id"]').empty();
              }
           });
        });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
    $('#ticket_sec2').hide();
    $('select[name="org_id"]').on('change', function() {
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration_org/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                $('select[name="event_id2"]').empty();
                $('select[name="event_id2"]').append('<option value="0">--Please Select--</option>');
                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="event_id2"]').append('<option value="'+ value["id"] +'">'+ value["event_title"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="event_id2"]').empty();
              }
           });
        });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('select[name="event_id2"]').on('change', function() {
         $('#ticket_sec2').show();
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                  
                $('select[name="ticket_id2"]').empty();
                $('select[name="ticket_id2"]').append('<option value="0">--Please Select--</option>');

                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="ticket_id2"]').append('<option value="'+ value["id"] +'">'+ value["ticket_type"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="ticket_id2"]').empty();
              }
           });
        });
    </script>
  
@endsection