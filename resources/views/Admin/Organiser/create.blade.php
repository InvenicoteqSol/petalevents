@extends('layouts.userdefault')
@section('content')
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-12 dashboard_right">
         @include('layouts.flash-message') 

          <div class="content">
        <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
        <div class="card-header card-header-primary">
         <div class="line_div"><h4 class="com_busin_sec card-title">Organiser</h4>
            <p class="card-category"> </p>
         </div>
     </div>
      

                    {!! Form::open(array('route' => 'adminorganiser.store', 'method'=>'POST', 'id' => 'edtpcfrm',  'class' => 'edt_post_frm', 'files' => true)) !!}

                        {{ csrf_field() }}

                            <div class="card-body">
                                    <form>

                        <div class="row">
                          <div class="col-md-6">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label bmd-label-floating"> First Name*</label>

                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" minlength="2" maxlength="91" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                <div class="col-md-6">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label bmd-label-floating">Surname*</label>

                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" minlength="2" maxlength="91" required>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                                 <div class="row">
                               <div class="col-md-6">
                         <div class="form-group{{ $errors->has('organisation_name') ? ' has-error' : '' }}">
                            <label for="organisation_name" class="col-md-4 bmd-label-floating">Organisation name*</label>

                                <input id="organisation_name" type="text" class="form-control" name="organisation_name" value="{{ old('organisation_name') }}" minlength="2" maxlength="91" required>

                                @if ($errors->has('organisation_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('organisation_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                    <div class="col-md-6">
                             <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label bmd-label-floating">Organisation website </label>
                           
                                <input id="website" type="text" class="form-control" name="website" value="{{ old('website') }}" minlength="2" maxlength="91">

                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>
                                        <div class="row">
                                    <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label bmd-label-floating">E-Mail Address*</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" maxlength="191" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                         <div class="col-md-6">
                             <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label bmd-label-floating">Phone*</label>

                                <input id="phone" type="text" class="form-control numberinput" name="phone" value="{{ old('phone') }}" minlength="10" maxlength="13" required>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('address_line_1') ? ' has-error' : '' }}">
                            <label for="address_line_1" class="col-md-4 control-label bmd-label-floating">Address line 1*</label>

                                <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="{{ old('address_line_1') }}" minlength="2" maxlength="91" required>

                                @if ($errors->has('address_line_1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                    <div class="col-md-6">
                             <div class="form-group{{ $errors->has('address_line_2') ? ' has-error' : '' }}">
                            <label for="address_line_2" class="col-md-4 control-label bmd-label-floating">Address line 2 </label>

                                <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="{{ old('address_line_2') }}" minlength="2" maxlength="91" >

                                @if ($errors->has('address_line_2'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address_line_2') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                                            <div class="row">
                                        <div class="col-md-6">
                             <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country" class="col-md-4 control-label bmd-label-floating">City/Town*</label>

                                <select class="form-control" id="country" name="country" required aria-required="true">
                                    <?php  $towns = DB::table('all_towns')->orderBy('town_name','asc')->pluck('id', 'town_name'); ?>
                                    <option value="">Select City/Town*</option>
                                    @foreach ($towns as $countryName=>$countryId)
                                        <option value="{{ $countryId }}" {{ ( old('country') == $countryId ) ? 'selected' : '' }}>{{ $countryName }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                    <div class="col-md-6">
                              <div class="form-group{{ $errors->has('postcode') ? ' has-error' : '' }}">
                            <label for="postcode" class="col-md-4 control-label bmd-label-floating">Postcode* </label>

                            
                                <input id="postcode" type="text" class="form-control numberinput" name="postcode" value="{{ old('postcode') }}" minlength="2" maxlength="91" required>

                                @if ($errors->has('postcode'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                            
                                    <div class="row">                 
                                   <div class="col-md-6">
                         <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label bmd-label-floating"> Username*</label>

                                <input id="username" type="text" class="form-control" name="username" minlength="6" maxlength="17" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label bmd-label-floating">Password*</label>

                                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                                 <div class="row">
                                <div class="col-md-6">
                        <div class="form-group{{ $errors->has('security_question') ? ' has-error' : '' }}">
                            <label for="security_question" class="col-md-4 control-label bmd-label-floating">Security question*</label>

                                <input id="security_question" type="text" class="form-control" name="security_question" minlength="6" maxlength="17" required>

                                @if ($errors->has('security_question'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('security_question') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                     <div class="col-md-6">
                         <div class="form-group{{ $errors->has('security_answer') ? ' has-error' : '' }}">
                            <label for="security_answer" class="col-md-4 control-label bmd-label-floating">Security answer*</label>

                                <input id="security_answer" type="text" class="form-control" name="security_answer" minlength="6" maxlength="17" required>

                                @if ($errors->has('security_answer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('security_answer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                            </form>
               

                         <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                            <div class="col-md-6">
                                <input type="hidden" value="4" name="user_type" id="user_type">  
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Submit
                                </button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                     {{ Form::close() }}
                    
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
 $(document).ready(function () {
     $(".numberinput").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
@endsection
