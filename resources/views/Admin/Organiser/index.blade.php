@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-12 dashboard_right">
        @include('layouts.flash-message') 

        

         <div class="content">
        <div class="container-fluid">
        <!--   row -->
          <div class="row">
          
         <div class="col-sm-7 table-search searchbar">
        <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="{{ route('adminorganiser.index') }}">
          <span class="bmd-form-group"><div class="input-group input-group-sm" "="">
             <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="">
             <div class="input-group-btn">
           <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </div></span>
        </form>
    </div>


        <div class="col-sm-5 selectdiv topbar_selectiv text-right createbtn">
               <a class="btn btn-primary" href="{{ route('adminorganiser.create') }}"> Create New Organiser </a>
         </div>
      
  
      </div>

          <!-- //  row -->
              
              <div class="row">
            <div class="col-md-12">

                <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Organisers</h4>
                  <p class="card-category"> </p>
                </div>
      
             <div class="card-body">
                  <div class="table-responsive">
                  <table class="table">

                <thead class=" text-primary">

                    <tr >
                       <th  class="">Image</th>
                        <th class="">Name</th>
                        <th class="">Email</th>
                        <th class="">Phone</th>
                        <th class="">Registered</th>
                       
                        <th class="">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($organisers)>0)
                     @foreach ($organisers as $organiser)
                    <tr class="tabledata">

                      <td class="column2 user_img">
                        @if($organiser->profile_picture!='') 
                        <img src="{{ url('public') }}/uploads/profileimages/{{ $organiser->profile_picture }}" class="img-responsive" style="width: 40px; height: 30px;">
                         @else
                        <img src="{{ url('public') }}/images/avtar.jpg" style="width: 40px; height: 30px;">
                       @endif
                        
                      </td>
                        
                        <td class="">{{ $organiser->first_name }}</td>
                       
                        <td class="">{{ $organiser->email }}</td>
                       
                        <td class="">{{ $organiser->phone }}</td>
                       
                        <td class="">{{ \Carbon\Carbon::parse($organiser->created_at)->format('d M Y')}}</td>
                       
                       <!--  <td class="">
                          <?php
                          if($organiser->status==1) {
                            echo 'Active';
                          } else {
                            echo 'Inactive';
                          }
                          ?>
                        </td> -->


                        <td>
                        <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">

                    <label> Organiser </label>
                     <a class="dropdown-item" href="{{ route('adminorganiser.edit',$organiser->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i> Edit </a>
                     <a class="dropdown-item" href="{{ route('adminorganiser.show',$organiser->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i> View </a>

                     <a class="dropdown-item">
                      <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroyorganiser/{{$organiser->id}}">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button></form>
                       </a>

                        </div>
                      </div>
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>
    </div>
     @if(count($organisers) > 0)
<div class="pagination_section">
   <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="{{ url('/adminorganiser') }}" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" @if($perpage=='10'){{'selected'}}@endif>10</option>
      <option value="20" @if($perpage=='20'){{'selected'}}@endif>20</option>
      <option value="50" @if($perpage=='50'){{'selected'}}@endif>50</option>
      <option value="100" @if($perpage=='100'){{'selected'}}@endif>100</option>
    </select>
    </form>
  </div>

<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

<!--   {!! $organisers->render() !!}
 --></nav>

  </div>
  
 
</div>
@endif
      
          <!--   <div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div> -->
      </div>


    </div>

    </div>
  </div>
</div>
</div>
</div>
</div>    

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
  input#search_input {
    height: 32px !important;
</style>

@endsection
