@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>

   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
            	<p class="singalname">{{ $contactus->name }} </p>              
                </div>
       		  <div class="card-body">
                          <div class="row">
                            <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Name :</span>{{ $contactus->name }}</p>
                            </div>
                          </div>
                        <div class="row">
                            <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                         	<p class="rightsidetext"><span class="lablname">Email :</span>{{ $contactus->email }}</p>
                            </div>
                          </div>
                        <div class="row">
                        	<div class="col-md-6">
                        		<p class="rightsidetext"><span class="lablname">Message :</span>{{ $contactus->message }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
p.singalemail {
    color: #969da5;
}
</style>


@endsection
