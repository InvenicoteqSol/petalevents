@extends('layouts.userdefault')
@section('content')

<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      @include('layouts.usersidebar')
  </div>
      <div class="col-sm-12 dashboard_right">
        @include('layouts.flash-message')
        <div class="col-sm-8 main">
        <div class="col-sm-9 searchbar">
        <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="{{ route('adminpayment.index') }}">

       <div class="input-group input-group-sm"">
                  <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="{{ $search_data }}" placeholder="Search here">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
           </div>
       </div>
    </form>
    </div>

      </div>

      <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Payment requests </h4>
                  
                </div>
      <div class="col-sm-12 tablediv spacertop">

        <div class="card-body">

      <table class="table table-hover">
        <thead class=" text-primary">
          <tr>
            <th class="column3">Name</th>
            <th class="column3">Event Name</th>
            <th class="column10">Price</th>
            <th class="column3">Action</th>
            </tr>
        </thead>
        <tbody>
          @if(count($petalmail)>0)
              @foreach ($petalmail as $mail)
          <tr class="tabledata classtable">
           
          <td class="column3">{{$mail->name}}</td>
            <td class="column10">{{$mail->eventtitle}}</td>
             <td class="column3">{{$mail->price}}</td>
             <td class="column3"> 
                        <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                              <a class="dropdown-item">
                         
        <?php
            $user_id = Auth::user()->id ;
            $user_email=Auth::user()->email;
         ?>
      <form action="{{url('/authPayment')}}/{{$mail->id}}" method="POST">
      {{ csrf_field() }}
      <button class="send_mail_check btn btn-primary">Authorise payment</button></div>
      </form>
  
                       </a>
                     
                    </td>
          </tr>
           @endforeach
        @else
          <tr class="tablesection">
            <td colspan="7" style="text-align: center;"><b>No record found</b></td>
          </tr>
        @endif                               
        </tbody>
      </table>
          
          

 @if(count($petalmail) > 0)
<div class="pagination_section">
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="{{ url('/adminpayment') }}" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" @if($perpage=='10'){{'selected'}}@endif>10</option>
      <option value="20" @if($perpage=='20'){{'selected'}}@endif>20</option>
      <option value="50" @if($perpage=='50'){{'selected'}}@endif>50</option>
      <option value="100" @if($perpage=='100'){{'selected'}}@endif>100</option>
    </select>
    </form>
  </div>
<div class="col-sm-4 total_div">
 
</div>

  
  

</div>
@endif

 <!--  -->
</div>

      </div>
        </div>
            </div>
              </div>
<!--booking-sec-end-->    
<!--booking-sec-end-->
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
  <script>
      CKEDITOR.replace( 'pc_requirement');
      CKEDITOR.replace( 'pc_learn' );
      CKEDITOR.replace( 'pc_description');
  </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
  .form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}
.maintit{
    padding-top: 6px;
  }

</style>
@endsection