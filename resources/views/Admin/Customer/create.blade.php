@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-12 dashboard_right">
         @include('layouts.flash-message') 

                 <div class="content">
                  <div class="container-fluid">
                     <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
               <div class="card-header card-header-primary">
             <div class="line_div"><h4 class="com_busin_sec card-title">Customers</h4>
                 <p class="card-category"></p>
                        </div>
                        </div>
                                

                     {!! Form::open(array('route' => 'admincustomer.store', 'method'=>'POST', 'id' => 'edtpcfrm',  'class' => 'edt_post_frm', 'files' => true)) !!}

                            <div class="card-body">
                                    <form>
                    
                         <div class="row">
                         <div class="col-md-6">
                             <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                             <label for="first_name" class="col-md-4 control-label bmd-label-floating"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                                        <div class="col-md-6">
                                   <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label bmd-label-floating">Last Name*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" minlength="2" maxlength="91" required>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                                    
                             <div class="row topcls">
                             <div class="col-md-6">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="bmd-label-floating">E-Mail Address*</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" maxlength="191" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> 
                            <div class="col-md-6">
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="bmd-label-floating">Phone*</label>
                            <input id="phone" type="text" class="form-control numberinput" name="phone" value="{{ old('phone') }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>             
                    </div>
    
                            
                        <div class="row topcls">      
                        <div class="col-md-6 edit_radiobtn">
                            <label for="phone" class="bmd-label-floating">Gender*</label>
                              <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                                   <label class="radiodiv">Male
                                        <input type="radio" name="gender" id="gender1" value="Male" required>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Female
                                        <input type="radio" name="gender" id="gender2" value="Female">
                                        <span class="checkmark"></span>
                                    </label>
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                           </div>
           
                              
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
    
                               <input type="hidden" value="3" name="user_type" id="user_type">
                            </div>
                        </div>
                
                                 <div class="col-md-6">
                                 <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label bmd-label-floating">Password*</label>
                                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>      
                    </div>      
                
                        </form>
                                    
                              
                             <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Save
                                </button>
                                <div class="clearfix"></div>
                                  
                            </div>
                        </div>
                    
                     {{ Form::close() }}
            
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
 $(document).ready(function () {
     $(".numberinput").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
@endsection
