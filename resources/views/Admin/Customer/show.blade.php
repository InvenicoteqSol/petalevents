@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>

   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
                  <p class="singalname">{{ $customer->first_name }} {{ $customer->last_name }}</p>              
                </div>
       

<div class="card-body">
        <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Phone :</span>{{ $customer->phone }}</p>
                            </div>
                            <div class="col-md-6">
                                 <p class="rightsidetext"> <span class="lablname">Email :</span>{{ $customer->email }}</p>
                               </div>
                             </div>
                              <div class="row">
                               <!--  <div class="col-md-6">
                                  <p class=""><span class="lablname">DOB:</span> <?php if($customer->dob!='' || $customer->dob!=NULL) { ?>
                                       {{ \Carbon\Carbon::parse($customer->dob)->format('d M Y') }}
                                <?php } ?> </p>
                                  </div> -->
                                  <div class="col-md-6">
                                    <p class="rightsidetext"><span class="lablname">Gender :</span> {{ $customer->gender }}</p>
                                  </div>
                                <!-- </div> -->

                                <!-- <div class="row"> -->
                         <div class="col-md-6">
                              <p class="rightsidetext"><span class="lablname">Status :</span> <?php 
                                 if($customer->status=='1') { 
                                    echo 'Active';
                                  } else {
                                    echo 'Inactive';
                                   }?></p>
                                 </div>
                               </div>

                                  <div class="row">
                         <div class="col-md-6">
               <!--  <div class="col-sm-4 profile_img text-center"> -->
               @if (Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL')
               <img src="{{ url('public') }}/uploads/profileimages/{{ $customer->profile_picture }}" class="img-circle" style="width:200px">
               @else
               <img src="{{ url('public') }}/public/images/avtar.jpg" class="img-circle">
              @endif
                </div>
              </div>
            </div>
              </div>
            </form>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
</style>


@endsection
