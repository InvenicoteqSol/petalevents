@extends('layouts.userdefault')
@section('title', 'Mrs Portrait | admin Dashboard')
@section('content')
@include('layouts.flash-message') 
    <div class="row">
        <div class="col-md-12">

                        <div class="form-group">
                            <label for="first_name" class="col-md-4 control-label"> First Name</label>
                            <div class="col-md-6">
                                <span class="form-control">{{ $photographer->first_name }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                               <span class="form-control"> {{ $photographer->last_name }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                               <span class="form-control">{{ $photographer->email }}</span>
                            </div>
                        </div>

                         

                          

                         <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $photographer->phone }}</span>
                            </div>
                        </div>


                        <!-- <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">DOB</label>

                            <div class="col-md-6">
                               <span class="form-control"> 
                                <?php if($photographer->dob!='' || $photographer->dob!=NULL) { ?>
                                       {{ \Carbon\Carbon::parse($photographer->dob)->format('d M Y') }}
                                <?php } ?>
                               </span>
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Gender</label>

                            <div class="col-md-6">
                                 <span class="form-control"> {{ $photographer->gender }} </span>
                            </div>
                        </div> -->

                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6">
                                 <span class="form-control"> <?php 
                                 if($photographer->status=='1') { 
                                    echo 'Active';
                                  } else {
                                    echo 'Inactive';
                                   }?></span>
                            </div>
                        </div>
                      
            
        </div>
    </div>


@endsection
