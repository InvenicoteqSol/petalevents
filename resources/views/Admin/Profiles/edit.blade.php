@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
        @include('layouts.flash-message') 

             <div class="content">
        <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
      <div class="card-header card-header-primary">
            <h4 class="card-title">Update Profile</h4>
            <p class="card-category">Update your Profile</p>
        </div>
        
             {{ Form::model($photographer, array('route' => array('adminprofiles.update', $photographer->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT', 'files' => true)) }}
             <div class="card-body">
            <form>
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-6">
         <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
            <label for="first_name" class="bmd-label-floating"> First Name*</label>
              <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $photographer->first_name }}" minlength="2" maxlength="91" required autofocus>
              @if ($errors->has('first_name'))
              <span class="help-block">
                  <strong>{{ $errors->first('first_name') }}</strong>
              </span>
              @endif
        </div>
      </div>
    <div class="col-md-6">
           <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
        <label for="last_name" class="control-label bmd-label-floating">Last Name*</label>
            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $photographer->last_name }}" minlength="2" maxlength="91" required>
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="row topcls">            
    <div class="col-md-6">
      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="email" class="control-label bmd-label-floating">E-Mail Address*</label>
          <input id="email" type="email" class="form-control" name="email" value="{{ $photographer->email }}" maxlength="191" required>
          @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
      </div>
    </div>
    <div class="col-md-6">
   <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                              <label for="country" class="bmd-label-floating">City/Town</label>
                            
                                <select class="form-control" id="country" name="country" required>
                                    @foreach ($towns as $townName=>$townId)
                                        <option value="{{ $townId }}" {{ ( $photographer->country == $townId ) ? 'selected' : '' }}>{{ $townName }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('country'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="row topcls">  
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
          <label for="phone" class="control-label bmd-label-floating">Phone*</label>
          <input id="phone" type="text" class="form-control numberinput" name="phone" value="{{ $photographer->phone }}" minlength="10" maxlength="13" required>
          @if ($errors->has('phone'))
              <span class="help-block">
                  <strong>{{ $errors->first('phone') }}</strong>
              </span>
          @endif
        </div>
      </div>
      <div class="col-md-6">
  <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
  <label for="password" class="bmd-label-floating">Password*</label>
 <input id="password" type="password" class="form-control" name="password" value="{{ $photographer->hdpwd }}">

             @if ($errors->has('password'))
                <span class="help-block">
         <strong>{{ $errors->first('password') }}</strong>
                </span>
             @endif
            </div>
        </div>
     <!--  <div class="col-md-6">
        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
        <label for="dob" class="control-label bmd-label-floating">DOB</label>
          <input id="dob" type="text" class="form-control" name="dob" 
  value="<?php if($photographer->dob!='' || $photographer->dob!=NULL){?>{{ \Carbon\Carbon::parse($photographer->dob)->format('d-m-Y') }}<?php } ?>">
          @if ($errors->has('dob'))
              <span class="help-block">
                  <strong>{{ $errors->first('dob') }}</strong>
              </span>
          @endif
      </div>
      </div> -->
    </div>
</div>
<!--  <div class="col-md-12">
  <div class="row topcls">  
    <div class="col-md-6">
      <div class="form-group edit_radiobtn{{ $errors->has('gender') ? ' has-error' : '' }}">
      <label for="phone" class="control-label lblcls bmd-label-floating">Gender</label>

           <label class="radiodiv">Male
                <input type="radio" name="gender" id="gender1" value="Male" <?php if($photographer->gender=="Male"){ echo "checked";}?>>
                <span class="checkmark"></span>
            </label>
            <label class="radiodiv">Female
                <input type="radio" name="gender" id="gender2" value="Female" <?php if($photographer->gender=="Female"){ echo "checked";}?>>
                <span class="checkmark"></span>
            </label>
        @if ($errors->has('gender'))
            <span class="help-block">
                <strong>{{ $errors->first('gender') }}</strong>
            </span>
        @endif
    </div>
  </div>
</div>
</div> -->
  
   <div class="col-md-12">
  <div class="row topcls">
               <div class="col-md-2">
          <div class="col-md-3 nopadding" id="ftrd_browse_img">
              @if($photographer->profile_picture!='') 
               <img src="{{ url('public') }}/uploads/profileimages/{{ $photographer->profile_picture }}" class="img-responsive" style="width: 200px;">
                   @else
                  <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 200px;">
                 @endif
                  
              </div>
              <div class="col-md-3 prflinput">
              <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
            </div>
      </div>
                    
  </div>
</div>
                         
<div class="col-md-12">
  <div class="topcls"> 
   <div class="form-group">
        <div class="buttondiv">
            <button type="submit" class="btn btn-primary pull-right">
                Update
            </button>
        </div>
    </div>
  </div>
</div>
{{ Form::close() }}
            
        </div>
    </div>
</div>

</div>
<script>
 $(document).ready( function() {

      var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                town: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                town: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
<script type="text/javascript">
 $(document).ready(function () {
     $(".numberinput").forceNumeric();
 });

 // forceNumeric() plug-in implementation
 jQuery.fn.forceNumeric = function () {
     return this.each(function () {
         $(this).keydown(function (e) {
             var key = e.which || e.keyCode;

             if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
             // numbers   
                 key >= 48 && key <= 57 ||
             // Numeric keypad
                 key >= 96 && key <= 105 ||
             // comma, period and minus, . on keypad
                key == 190 || key == 188 || key == 109 || key == 110 ||
             // Backspace and Tab and Enter
                key == 8 || key == 9 || key == 13 ||
             // Home and End
                key == 35 || key == 36 ||
             // left and right arrows
                key == 37 || key == 39 ||
             // Del and Ins
                key == 46 || key == 45)
                 return true;

             return false;
         });
     });
 }
</script>
<!--booking-sec-end-->
@endsection