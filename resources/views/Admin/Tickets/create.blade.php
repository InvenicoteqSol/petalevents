@extends('layouts.userdefault')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header card-header-primary">
                  <h4 class="card-title">Create Tickets</h4>
                  <p class="card-category">Create tickets for </p>
                </div>
                <div class="card-body">
                  {!! Form::open(array('route' => 'ticket.store', 'method'=>'POST', 'id' => 'edtbookingfrm',  'class' => 'edtbookingfrm formarea', 'files' => true)) !!}
                   <input type="hidden" name="event_id" value="{{request()->route('id')}}">
                   <div class="row">
                     <div class="col-md-6">
     <div class="form-group{{ $errors->has('ticket_type') ? ' has-error' : '' }}">
    <label for="ticket_type" class="control-label">Ticket Title*</label>
        <input type="text" class="form-control" id="ticket_type" name="ticket_type" value="{{ @old(ticket_type) }}" maxlength="30" required>
        @if ($errors->has('ticket_type'))
            <span class="help-block">
                <strong>{{ $errors->first('ticket_type') }}</strong>
            </span>
        @endif
    </div>
</div> 
    <div class="col-md-3">
          <div class="form-group{{ $errors->has('prices') ? ' has-error' : '' }}">
    <label for="prices" class="control-label">Ticket Price*</label>
        <input type="number" class="form-control" id="prices" name="prices" value="{{ @old(prices) }}" maxlength="10" required>
        @if ($errors->has('prices'))
            <span class="help-block">
                <strong>{{ $errors->first('prices') }}</strong>
            </span>
        @endif
    </div>
  </div> 
  <div class="col-md-3">
     <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
    <label for="stock" class="control-label">Stock*</label>
        <input type="number" class="form-control" id="stock" name="stock" value="{{ @old(stock) }}" maxlength="10" required>
        @if ($errors->has('stock'))
            <span class="help-block">
                <strong>{{ $errors->first('stock') }}</strong>
            </span>
        @endif
    </div>
</div> 
</div>
 <div class="row">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <label for="start_date" class="control-label"> Available From Date-Time*</label>
            <div class="input-group">
            <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
            <input type="text" class="form-control time" id="start_time" name="start_time" value="{{ @old(start_time) }}" required>
          </div>
            
            @if ($errors->has('start_date'))
              <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
              </span>
            @endif
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <label for="end_date" class="control-label"> Available To Date-Time*</label>
            <div class="input-group">
            <input type="text" class="form-control end_date" id="end_date" name="end_date" value="{{ @old(end_date) }}" required>
            <input type="text" class="form-control time" id="end_time" name="end_time" value="{{ @old(end_time) }}" required>
          </div>
            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
        </div>
    </div>

  </div>
  <div class="row">
    <div class="col-md-6">
       <div class="form-group{{ $errors->has('ticket_detail') ? ' has-error' : '' }}">
            <label for="ticket_detail" class="control-label"> Ticket Details</label><br>
            
            <textarea class="form-control" rows="5" cols="50" id="ticket_detail" name="ticket_detail">
            </textarea>
          </div>

    </div>
      <div class="col-md-4">
      <div class="form-group{{ $errors->has('stock_status') ? ' has-error' : '' }}">
          <label for="stock_status" class="control-label">  Ticket Availability </label>
          <select class ="form-control" id="stock_status" name="stock_status" required="">
          <option value="Available">Available</option>
          <option value="Unavailable">Unavailable</option>
          </select>
          </div>
      </div>
    <div class="col-md-2">
       <div class="form-group">
        <label for="Free_booking_feechk" class="control-label"> Free Booking Fee</label>
      <input type="checkbox" id="Free_booking_feechk" data-toggle="toggle">
      <input type="hidden" id="Free_booking_fee" value="0">
    </div>
    </div>

    
  </div>


 <div class="col-md-12">
  <div class=" topcls">
         <div class="form-group">
            <div class="buttondiv">
                <button type="submit" class="btn btn-primary" id="saveData">
                    Save
                </button>
            </div>
             
        </div>
    </div>
</div>
                        

                     {{ Form::close() }}
                </div>

    </div>
  </div>
  </div>

<script>
 $(document).ready( function() {
  $('.time').timepicker({
    'timeFormat':'H:i:s',
  });


    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
            $('#start_time').focus();
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
           $('#end_time').focus();
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        
        });
      $("#edtbookingfrm").submit();


 });
 
</script>
<script>
 document.getElementById('saveData').addEventListener('click', function(e) {
  // Open Checkout with further options:
  var value= $('#Free_booking_feechk').prop('checked');
  if(value==true){
            $("#edtbookingfrm").append('<input type="hidden" name="Free_booking_fee" value="1" />').submit();
          }
          else
          {
            $("#edtbookingfrm").append('<input type="hidden" name="Free_booking_fee" value="0" />').submit();
          }
});
  </script>
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'ticket_detail' );
</script>
@endsection
