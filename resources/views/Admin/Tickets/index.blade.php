@extends('layouts.userdefault')
@section('content')
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
    
   <div class="col-sm-12 dashboard_right">
@include('layouts.flash-message')


<div class="col-sm-12 nopadding">
  <div class="row">

       <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Ticket</h4>
                 <p class="card-category"></p>
      
  </div>
<?php   $id_evnt=request()->route('id');?>
     

       <div class="col-sm-12 tablediv spacertop">
        <div class="card-body">
      <div class="tablewrapper table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class="text-primary">
                   <tr class="headings">
                        <th class="column3">Ticket type</th>
                        <th class="column2">Prices</th>
                        <th class="column6">Stock</th>
                        <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $tickets = DB::table('tickets')
                               ->where('deleted', 0)
                               ->where('event_id','=', $id_evnt)
                               ->get();
                                ?>
                    @if(count($tickets)>0)
                     @foreach ($tickets as $ticket)
                    <tr class="tabledata odd">
                       <td class="column3">{{ $ticket->ticket_type }}</td>

                        <td class="column2">{{ $ticket->prices }}</td>

                        <td class="column6">  {{ $ticket->stock }} </td>
                       
                        

                     <td class="column3 action_btns"> 
                      <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>           
                                </a>

                           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="tablebtn dropdown-item" href="{{ route('ticket.edit',$ticket->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i>Edit</a>

                     <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroyticket/{{$ticket->id}}">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                          </form>
                        </a>
                      </div>
                    </div>

                       {!! Form::close() !!}
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>




 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>

@endsection
