@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-10 dashboard_right">
        <div class="line_div">
              <h3 class="com_busin_sec">New Discount </h3>
        </div>
   
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(array('route' => 'discounts.store','method'=>'POST', 'id' => 'edtbookingfrm',  'class' => 'edtbookingfrm formarea', 'files' => true)) !!}
  
  <input type="hidden" name="event_id" value="{{request()->route('id')}}">

        <div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
    <label for="code_name" class="control-label">Code Name*</label>
        <input type="text" class="form-control" id="code_name" name="code_name" value="{{ @old(code_name) }}" maxlength="10" required>
        @if ($errors->has('code_name'))
            <span class="help-block">
                <strong>{{ $errors->first('code_name') }}</strong>
            </span>
        @endif
    </div>
</div> 
         <div class="col-md-6">
        <div class="form-group{{ $errors->has('discount_type') ? ' has-error' : '' }}">
        <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
                    <select class ="form-control" id="discount_type" name="discount_type">
                    <option disabled="disabled" selected="selected"> --- Select --- </option>
                    <option> percentage</option>
                    <option> fixed</option>
                    
                    </select>
                      @if ($errors->has('discount_type'))
                          <span class="invalid-feedback">
                              <strong>{{ $errors->first('discount_type') }}</strong>
                          </span>
                      @endif  
    </div>
  </div>
 </div>
</div>

 <div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
    <label for="discount" class="control-label">Discount price*</label>
        <input type="number" class="form-control" id="discount" name="discount" value="{{ @old(discount) }}" maxlength="10" required>
        @if ($errors->has('discount'))
            <span class="help-block">
                <strong>{{ $errors->first('discount') }}</strong>
            </span>
        @endif
    </div>
</div> 
 </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <label for="start_date" class="control-label"> Start Date*</label>
            <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
            @if ($errors->has('start_date'))
              <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
              </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
            <label for="expiry_date" class="control-label"> Expiry Date *</label>
            <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="{{ @old(expiry_date) }}" required>
            @if ($errors->has('expiry_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('expiry_date') }}</strong>
                </span>
            @endif
        </div>
    </div>
  </div>
</div>

 <div class="col-md-12">
  <div class=" topcls">
         <div class="form-group">
            <div class="buttondiv">
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>
                        

                     {{ Form::close() }}
            
        </div>
    </div>
     </div>
    </div>
<script>
 $(document).ready( function() {
    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".expiry_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>

@endsection
