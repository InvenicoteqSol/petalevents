@extends('layouts.userdefault')
@section('content')
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
    
   <div class="col-sm-10 dashboard_right">
@include('layouts.flash-message')
<div class="col-sm-12 nopadding">
  <div class="row">
        <div class="col-sm-4 selectdiv topbar_selectiv ">
         
        <h3>Ticket</h3>
      
  </div>
<?php/* echo $id_evnt=request()->route('id')*/;?>
</div>
      </div>

       <div class="col-sm-12 tablediv spacertop">
      <div class="tablewrapper">
      <table class="table-bordered table-striped">
               
                <thead>
                   <tr class="headings">
                        <th class="column3">Ticket type</th>
                        <th class="column2">Prices</th>
                        <th class="column6">Stock</th>
                        <th class="column4">Max Ticket</th>
                  
                        <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $tickets = DB::table('tickets')
                                 ->where('deleted', 0)
                                 ->get();
                                ?>
                    @if(count($tickets)>0)
                     @foreach ($tickets as $ticket)
                    <tr class="tabledata odd">
                       <td class="column3">{{ $ticket->ticket_type }}</td>

                        <td class="column2">{{ $ticket->prices }}</td>

                        <td class="column6">  {{ $ticket->stock }} </td>
                       
                        

                     <td class="column3 action_btns"> 
                     <a class="tablebtn" href="{{ route('ticket.edit',$ticket->id) }}" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i></a>
                     <!-- <a class="tablebtn" href="{{ route('ticket.show',$ticket->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a> -->
                     {!! Form::open(['method' => 'DELETE','route' => ['ticket.destroy', $ticket->id],'style'=>'display:inline']) !!}
                     {!! Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']) !!}

                       {!! Form::close() !!}
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

        </tbody>
      </table>

<div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
       </div>
    </div>
    
                       
            
        </div>
    </div>
     </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>

@endsection
