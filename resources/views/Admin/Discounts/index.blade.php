@extends('layouts.userdefault')
@section('content')
   <div class="dashboard_section">
   <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
   </div>
    
   <div class="col-sm-12 dashboard_right">
    @include('layouts.flash-message')
<div class="col-sm-12 nopadding">
  <div class="row">
        <div class="col-sm-4 selectdiv topbar_selectiv ">
        <?php   $id_evnt=request()->route('id');?>
        </div>
        <div class="col-sm-8 selectdiv topbar_selectiv text-right createbtn">
  <div class="dropdown head-dropdown">
  <a id="dLabel" data-target="#" class="btn btn-primary" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    Create New Discount
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu" aria-labelledby="dLabel">
    <li> <a data-toggle="modal" data-target="#linkmodal_{{$id_evnt}}" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Discount</a></li>
    <li> <a data-toggle="modal" data-target="#earlybird_{{$id_evnt}}" data-whatever="@earlybird" style="cursor: pointer;" title="Send Request">Early Bird Discount</a></li>
    <li><a data-toggle="modal" data-target="#bundleoffer_{{$id_evnt}}" data-whatever="@bundleoffer" style="cursor: pointer;" title="Send Request">Bundle Offer Ticket Discount</a></li>
    <li><a data-toggle="modal" data-target="#ticketx_{{$id_evnt}}" data-whatever="@ticketx" style="cursor: pointer;" title="Send Request">Ticket X Discount</a></li>
  </ul>
</div>

    </div>

  <!------Start first form for discount------------------------------->

          <div class="modal" id="linkmodal_{{$id_evnt}}" tabindex="-1" role="dialog" aria-labelledby="linkmodal_{{$id_evnt}}">
            <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title">Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/discountsticket/{{$id_evnt}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <input type="hidden" class="form-control" name="availability" value="dc_one">
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{ @old(code_name) }}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount price*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{ @old(discount) }}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>
          </div> 
           <div class="col-md-6">
          <div class="form-group">
          <label for="discount_type" class="col-md-4 col-form-label text-md-right">  </label>
          <select class ="form-control" id="discount_type" name="discount_type">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
          <option value="0"> Percentage</option>
          <option value="1"> Fixed</option>

          </select>
          
          </div>
          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
          <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="{{ @old(expiry_date) }}" required>
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
           @if(count($Tickets)>0)
           <label for="expiry_date" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
              <option value="{{ $item->id }}">{{ $item->ticket_type }}</option>
            @endforeach
          </select>
          @else
         {{ "No tickets created for this event, kindly create a ticket first" }} 
          @endif
                   
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Submit</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
        </div>

 <!------ End First form for discount------------------------------->
 <!------Start second form for discount------------------------------->
         
          <div class="modal" id="earlybird_{{$id_evnt}}" tabindex="-1" role="dialog" aria-labelledby="earlybird_{{$id_evnt}}">
           <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
        <h5 class="modal-title">Early Bird Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/earlybirddiscounts/{{$id_evnt}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <input type="hidden" class="form-control" name="availability" value="dc_two">
          
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{ @old(code_name) }}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount price*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{ @old(discount) }}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>
          </div> 
           <div class="col-md-6">
          <div class="form-group">
          <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
          <option value="0"> Percentage</option>
          <option value="1"> Fixed</option>

          </select>
          
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
          <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="{{ @old(expiry_date) }}" required>
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <label for="expiry_date" class="control-label"> Associated Ticket *</label>
          @if(count($Tickets)>0)
            <select name="ticketId" id="ticketId" class="form-control">
          @foreach($Tickets as $item)
              <option value="{{ $item->id }}">{{ $item->ticket_type }}</option>
            @endforeach
          </select>
          @else
          {{ "No tickets created for this event, kindly create a ticket first" }} 
          @endif
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Submit</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
          </div>
           <!------ End second form for discount------------------------------->
           <!------Start third form for discount------------------------------->
         
          <div class="modal" id="bundleoffer_{{$id_evnt}}" tabindex="-1" role="dialog" aria-labelledby="bundleoffer_{{$id_evnt}}">
             <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title">Bundle Offer Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/bundleofferdiscounts/{{$id_evnt}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <input type="hidden" class="form-control" name="availability" value="dc_three">
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{ @old(code_name) }}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('bundle_offer_ticket') ? ' has-error' : '' }}">
          <label for="bundle_offer_ticket" class="control-label">Bundle offer ticket*</label>
          <input type="text" class="form-control" id="bundle_offer_ticket" name="bundle_offer_ticket" value="{{ @old(bundle_offer_ticket) }}" maxlength="10" required>
          @if ($errors->has('bundle_offer_ticket'))
          <span class="help-block">
          <strong>{{ $errors->first('bundle_offer_ticket') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('bundle_offer_price') ? ' has-error' : '' }}">
          <label for="bundle_offer_price" class="control-label">Bundle offer price*</label>
          <input type="text" class="form-control" id="bundle_offer_price" name="bundle_offer_price" value="{{ @old(bundle_offer_price) }}" maxlength="10" required>
          @if ($errors->has('bundle_offer_price'))
          <span class="help-block">
          <strong>{{ $errors->first('bundle_offer_price') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
          <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="{{ @old(expiry_date) }}" required>
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
         @if(count($Tickets)>0)
           <label for="expiry_date" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
              <option value="{{ $item->id }}">{{ $item->ticket_type }}</option>
            @endforeach
          </select>
          @else
         {{ "No tickets created for this event, kindly create a ticket first" }} 
          @endif
                   
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Submit</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
          </div>
           <!------ End third form for discount------------------------------->
           <!------Start four form for discount------------------------------->
         
          <div class="modal fade ticketx" id="ticketx_{{$id_evnt}}" tabindex="-1" role="dialog" aria-labelledby="ticketx_{{$id_evnt}}">
            <div class="modal-dialog" role="document">
          <div class="modal-content">
             <div class="modal-header">
        <h5 class="modal-title">Ticket X Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/ticketxdiscounts/{{$id_evnt}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <input type="hidden" class="form-control" name="availability" value="dc_four">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{ @old(name) }}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{ @old(code_name) }}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticketx') ? ' has-error' : '' }}">
          <label for="ticketx" class="control-label">Ticket X*</label>
          <input type="text" class="form-control" id="ticketx" name="ticketx" value="{{ @old(ticketx) }}" maxlength="10" required>
          @if ($errors->has('ticketx'))
          <span class="help-block">
          <strong>{{ $errors->first('ticketx') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('tickety') ? ' has-error' : '' }}">
          <label for="tickety" class="control-label">Ticket Y*</label>
          <input type="text" class="form-control" id="tickety" name="tickety" value="{{ @old(tickety) }}" maxlength="10" required>
          @if ($errors->has('tickety'))
          <span class="help-block">
          <strong>{{ $errors->first('tickety') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="{{ @old(start_date) }}" required>
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
          <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="{{ @old(expiry_date) }}" required>
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
            @if(count($Tickets)>0)
           <label for="expiry_date" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
              <option value="{{ $item->id }}">{{ $item->ticket_type }}</option>
            @endforeach
          </select>
          @else
         {{ "No tickets created for this event, kindly create a ticket first" }} 
          @endif
                   
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Submit</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
          </div>

           <!------ End four form for discount------------------------------->


          </div>
          </div>
       <!-----------------first discound ------------------>

          <div class="row">
            <div class="col-md-12">

                <div class="card">
                <div class="card-header card-header-primary">
       <div class="col-sm-12 tablediv spacertop">
                <h4 class="card-title">Discount</h4>
                 <p class="card-category"></p>
                </div>
              </div>

      <div class="tablewrapper table-responsive card-body">
        <div class="table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Code Name</th>
                        <th class="column3">Discount </th>
                        <th class="column3">Start Date</th>
                        <th class="column3">Expiry Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  $usertype=Auth::user()->user_type;
                  if($usertype==1){
                    $discounts = DB::table('discounts')
                               ->where('availability','=','dc_one')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->get();

                  }else{

                  $discounts = DB::table('discounts')
                               ->where('availability','=','dc_one')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->where('created_by','=',Auth::user()->id)
                               ->get();
                             }
                              
                                ?>
                    @if(count($discounts)>0)
                     @foreach ($discounts as $discount)

                    <tr class="tabledata odd">
                       <td class="column3">{{ $discount->name }}</td>

                        <td class="column3">{{ $discount->code_name }}</td>
                         <td class="column3">{{ $discount->discount }}</td>
                        <td class="column3">  {{ $discount->start_date }} </td>
                       
                        <td class="column3">{{ $discount->expiry_date }}</td>
                       
                     <td class="column3 action_btns"> 
                      


  <!------Start edit first form for discount------------------------------->
         
 

          <div class="modal" id="linkmodaledit_{{$discount->discount_Id}}" tabindex="-1" role="dialog" aria-labelledby="linkmodaledit_{{$discount->discount_Id}}">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
           <div class="modal-header">
        <h5 class="modal-title">Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="login_bg form_bg">

          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/discountsticketedit/{{$discount->discount_Id}}">
          {{ csrf_field() }}
          <input type="hidden" class="form-control" name="availability" value="dc_one">
          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$discount->name}}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{$discount->code_name}}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount price*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{$discount->discount}}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>
          </div> 
           <div class="col-md-6">
          <div class="form-group">
          <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
          <option value="{{$discount->discount_type}}"> Percentage</option>
          <option value="{{$discount->discount_type}}"> Fixed</option>

          </select>
          
          </div>
          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($discount->start_date!='' || $discount->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->start_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
           <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="<?php if($discount->expiry_date!='' || $discount->expiry_date!='0000-00-00'  || $discount->expiry_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->expiry_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
           @if(count($Tickets)>0)
          <label for="ticketId" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
            <option value="{{ $item->id }}" {{ ( $item->id == $discount->ticket_type_id) ? 'selected' : '' }}> {{ $item->ticket_type }} </option>

            @endforeach
          </select>
          @else
          @endif

  </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Update</button>

          </div>
          </form>
                 </div>
      </div>
      
    </div>
  </div>
</div>

 <!------ End edit First form for discount------------------------------->

                              <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                                <a class="dropdown-item" data-toggle="modal" data-target="#linkmodaledit_{{$discount->discount_Id}}" data-whatever="@linkmodaledit" style="cursor: pointer;" title="Edit" data-placement="bottom"><i class="fa fa-edit"></i>Edit</a>

                                    <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroydiscount/{{$discount->discount_Id}}">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                               
                               </div>
                            </div>
                   
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif


            </tbody>
          </table>
        </div>
       </div>
     </div>
   </div>
 </div>

       <!-----------------End first discound ------------------>
        <!-----------------Second discound ------------------>

              <div class="row">
            <div class="col-md-12">

                <div class="card">
                <div class="card-header card-header-primary">
       <div class="col-sm-12 tablediv spacertop">
       <h4 class="card-title">Early Bird Discount</h4>
       <p class="card-category"> </p>
                </div>
              </div>

      <div class="tablewrapper table-responsive card-body">
        <div class="table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Code Name</th>
                        <th class="column3">Start Date</th>
                        <th class="column3">Expiry Date</th>
                        <th class="column3"></th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php 
                  if($usertype==1){

                  $discounts = DB::table('discounts')
                               ->where('availability','=','dc_two')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->get();
                             }
                             else
                             {
                               $discounts = DB::table('discounts')
                               ->where('availability','=','dc_two')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->where('created_by','=',Auth::user()->id)
                               ->get();
                             }
                                ?>
                             
                    @if(count($discounts)>0)
                     @foreach ($discounts as $discount)
                    <tr class="tabledata odd">
                       <td class="column3">{{ $discount->name }}</td>

                        <td class="column3">{{ $discount->code_name }}</td>

                        <td class="column3">  {{ $discount->start_date }} </td>
                       
                        <td class="column3">{{ $discount->expiry_date }}</td>
                          <td class="column3"></td>
                     <td class="column3 action_btns"> 
                      
      
        <!---------------------------Start second form for discount------------------------------->
         


          <div class="modal" id="earlybirdedit_{{$discount->discount_Id}}" tabindex="-1" role="dialog" aria-labelledby="earlybirdedit_{{$discount->discount_Id}}">
            <div class="modal-dialog" role="document">
          <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Early Bird Discount</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/earlybirddiscountsedit/{{$discount->discount_Id}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="availability" value="dc_two">
          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$discount->name}}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{$discount->code_name}}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('discount') ? ' has-error' : '' }}">
          <label for="discount" class="control-label">Discount price*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="{{$discount->discount}}" maxlength="10" required>
          @if ($errors->has('discount'))
          <span class="help-block">
          <strong>{{ $errors->first('discount') }}</strong>
          </span>
          @endif
          </div>
          </div> 
           <div class="col-md-6">
          <div class="form-group">
          <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
           <option value="{{$discount->discount_type}}"> Percentage</option>
          <option value="{{$discount->discount_type}}"> Fixed</option>

          </select>
          
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($discount->start_date!='' || $discount->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->start_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
          <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="<?php if($discount->expiry_date!='' || $discount->expiry_date!='0000-00-00'  || $discount->expiry_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->expiry_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
             @if(count($Tickets)>0)
          <label for="ticketId" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
               <option value="{{ $item->id }}" {{ ( $item->id == $discount->ticket_type_id) ? 'selected' : '' }}> {{ $item->ticket_type }} </option>
            @endforeach
          </select>
          @else
          @endif
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Update</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
        </div>

           <!------ End edit second form for discount------------------------------->

                         <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">

                      <a class="dropdown-item" data-toggle="modal" data-target="#earlybirdedit_{{$discount->discount_Id}}" data-whatever="@earlybirdedit" style="cursor: pointer;" title="Edit"><i class="fa fa-edit"></i>Edit</a>

                     <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroydiscount/{{$discount->discount_Id}}">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
          
                   </div>
                 </div>
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

            </tbody>
          </table>
        </div>
       </div>
     </div>
   </div>
 </div>

       <!-----------------End second discound ------------------>
        <!-----------------third discound ------------------>

         <div class="row">
            <div class="col-md-12">

                <div class="card">
                <div class="card-header card-header-primary">
       <div class="col-sm-12 tablediv spacertop">
        <h4 class="card-title">Bundle Offer Ticket Discount</h4>
         <p class="card-category"></p>
                </div>
              </div>

      <div class="tablewrapper table-responsive card-body">
         <div class="table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Code Name</th>
                        <th class="column2">bundle offer ticket </th>
                        <th class="column2"> bundle offer price </th>
                        <th class="column3">Start Date</th>
                        <th class="column3">Expiry Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php
                  if($usertype=1){
                                      $discounts = DB::table('discounts')
                               ->where('availability','=','dc_three')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->get();

                  } 
                  else{
                    
                  $discounts = DB::table('discounts')
                               ->where('availability','=','dc_three')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->where('created_by','=',Auth::user()->id)
                               ->get();
                  }

                                ?>
                    @if(count($discounts)>0)
                     @foreach ($discounts as $discount)
                    <tr class="tabledata odd">
                       <td class="column3">{{ $discount->name }}</td>

                        <td class="column3">{{ $discount->code_name }}</td>
                        <td class="column2">{{ $discount->bundle_offer_ticket }}</td>

                        <td class="column2">{{ $discount->bundle_offer_price  }}</td>

                        <td class="column3">  {{ $discount->start_date }} </td>
                       
                        <td class="column3">{{ $discount->expiry_date }}</td>

                     <td class="column3 action_btns"> 
                      
       <!-----------------------------Start third form for discount------------------------------->
         
          <div class="modal" id="bundleofferedit_{{$discount->discount_Id}}" tabindex="-1" role="dialog" aria-labelledby="bundleofferedit_{{$discount->discount_Id}}">
            <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title">Bundle Offer Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/bundleofferdiscountsedit/{{$discount->discount_Id}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="availability" value="dc_three">
          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">
          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$discount->name}}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{$discount->code_name}}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('bundle_offer_ticket') ? ' has-error' : '' }}">
          <label for="bundle_offer_ticket" class="control-label">Bundle offer ticket*</label>
          <input type="text" class="form-control" id="bundle_offer_ticket" name="bundle_offer_ticket" value="{{$discount->bundle_offer_ticket}}" maxlength="10" required>
          @if ($errors->has('bundle_offer_ticket'))
          <span class="help-block">
          <strong>{{ $errors->first('bundle_offer_ticket') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('bundle_offer_price') ? ' has-error' : '' }}">
          <label for="bundle_offer_price" class="control-label">Bundle offer price*</label>
          <input type="text" class="form-control" id="bundle_offer_price" name="bundle_offer_price" value="{{$discount->bundle_offer_price}}" maxlength="10" required>
          @if ($errors->has('bundle_offer_price'))
          <span class="help-block">
          <strong>{{ $errors->first('bundle_offer_price') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($discount->start_date!='' || $discount->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->start_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
         <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="<?php if($discount->expiry_date!='' || $discount->expiry_date!='0000-00-00'  || $discount->expiry_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->expiry_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
             @if(count($Tickets)>0)
          <label for="ticketId" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
               <option value="{{ $item->id }}" {{ ( $item->id == $discount->ticket_type_id) ? 'selected' : '' }}> {{ $item->ticket_type }} </option>
            @endforeach
          </select>
          @else
          @endif
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Update</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
        </div>


           <!------ ------------------------End third form for discount------------------------------->

                        <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                      <a class="dropdown-item" data-toggle="modal" data-target="#bundleofferedit_{{$discount->discount_Id}}" data-whatever="@bundleofferedit_" style="cursor: pointer;" title="Edit"><i class="fa fa-edit"></i>Edit</a>
                  
                     <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroydiscount/{{$discount->discount_Id}}">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                     {!! Form::close() !!}
                   </div>
                 </div>
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

            </tbody>
          </table>
        </div>
       </div>
     </div>
   </div>
 </div>

       <!-----------------End third discound ------------------>

       <div class="row">
            <div class="col-md-12">

                <div class="card">
                <div class="card-header card-header-primary">
       <div class="col-sm-12 tablediv spacertop">
          <h4 class="card-title">Ticket X Discount</h4>
          <p class="card-category"> </p>
                </div>
              </div>

      <div class="tablewrapper table-responsive card-body">
        <div class="table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Code Name</th>
                        <th class="column2">Ticket X</th>
                        <th class="column2">Ticket Y</th>
                        <th class="column3">Start Date</th>
                        <th class="column3">Expiry Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php
                  if($usertype==1)
                  {
                     $discounts = DB::table('discounts')
                               ->where('availability','=','dc_four')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->get();

                  }
                  else
                  {
                     $discounts = DB::table('discounts')
                               ->where('availability','=','dc_four')
                               ->where('is_deleted','=',0)
                               ->where('event_id','=',$id)
                               ->where('created_by','=',Auth::user()->id)
                               ->get();
                  }
                                ?>
                    @if(count($discounts)>0)
                     @foreach ($discounts as $discount)
                    <tr class="tabledata odd">
                       <td class="column3">{{ $discount->name }}</td>

                        <td class="column3">{{ $discount->code_name }}</td>
                        <td class="column2">{{ $discount->ticketx }}</td>
                        <td class="column2">{{ $discount->tickety }}</td>


                        <td class="column3">  {{ $discount->start_date }} </td>
                       
                        <td class="column3">{{ $discount->expiry_date }}</td>

                     <td class="column3 action_btns"> 
                      

                    <!------Start four form for discount------------------------------->
         
          <div class="modal" id="ticketxedit_{{$discount->discount_Id}}" tabindex="-1" role="dialog" aria-labelledby="ticketxedit_{{$discount->discount_Id}}">
            <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
        <h5 class="modal-title">Bundle Offer Discount</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
          <div class="modal-body">
          <div class="login_bg form_bg">
            
          <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/ticketxdiscountsedit/{{$discount->discount_Id}}">
          {{ csrf_field() }}

          <input type="hidden" class="form-control" name="availability" value="dc_four">
          <input type="hidden" class="form-control" name="event_id" value="{{$id_evnt}}">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="{{$discount->name}}" maxlength="10" required>
          @if ($errors->has('name'))
          <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('code_name') ? ' has-error' : '' }}">
          <label for="code_name" class="control-label">Code Name*</label>
          <input type="text" class="form-control" id="code_name" name="code_name" value="{{$discount->code_name}}" maxlength="10" required>
          @if ($errors->has('code_name'))
          <span class="help-block">
          <strong>{{ $errors->first('code_name') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('ticketx') ? ' has-error' : '' }}">
          <label for="ticketx" class="control-label">Ticket X*</label>
          <input type="text" class="form-control" id="ticketx" name="ticketx" value="{{$discount->ticketx}}" maxlength="10" required>
          @if ($errors->has('ticketx'))
          <span class="help-block">
          <strong>{{ $errors->first('ticketx') }}</strong>
          </span>
          @endif
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('tickety') ? ' has-error' : '' }}">
          <label for="tickety" class="control-label">Ticket Y*</label>
          <input type="text" class="form-control" id="tickety" name="tickety" value="{{$discount->tickety}}" maxlength="10" required>
          @if ($errors->has('tickety'))
          <span class="help-block">
          <strong>{{ $errors->first('tickety') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>

          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($discount->start_date!='' || $discount->start_date!='0000-00-00'  || $discount->start_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->start_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('start_date'))
          <span class="help-block">
          <strong>{{ $errors->first('start_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group{{ $errors->has('expiry_date') ? ' has-error' : '' }}">
          <label for="expiry_date" class="control-label"> Expiry Date *</label>
         <input type="text" class="form-control expiry_date" id="expiry_date" name="expiry_date" value="<?php if($discount->expiry_date!='' || $discount->expiry_date!='0000-00-00'  || $discount->expiry_date!=NULL){?>{{ \Carbon\Carbon::parse($discount->expiry_date)->format('d-m-Y') }}<?php } ?>" required >
          @if ($errors->has('expiry_date'))
          <span class="help-block">
              <strong>{{ $errors->first('expiry_date') }}</strong>
          </span>
          @endif
          </div>
          </div>
          </div>
          </div>
           <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
            @if(count($Tickets)>0)
          <label for="ticketId" class="control-label"> Ticket Name*</label>
            <select name="ticketId" id="ticketId" class="form-control">
            @foreach($Tickets as $item)
               <option value="{{ $item->id }}" {{ ( $item->id == $discount->ticket_type_id) ? 'selected' : '' }}> {{ $item->ticket_type }} </option>
            @endforeach
          </select>
          @else
          @endif
          </div>
          </div>
          </div>
          <div class="submitbtn">
          <button type="submit" class="btn btn-primary mr-2"  value="Submit">Update</button>
          </div>
          </form>
          </div>
          </div>
          </div>
          </div>
          </div>
           <!------ End four form for discount------------------------------->

                        <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                      <a class="dropdown-item" data-toggle="modal" data-target="#ticketxedit_{{$discount->discount_Id}}" data-whatever="@ticketxedit" style="cursor: pointer;" title="Edit"><i class="fa fa-edit"></i>Edit</a>

                     <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="{{url('/')}}/destroydiscount/{{$discount->discount_Id}}">
                         <button type="submit" class="del_sec_mew "><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                    
                   </div>
                 </div>
                    </td>
                       
                    </tr>
                     @endforeach
                     @else
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    @endif

            </tbody>
          </table>
        </div>
       </div>
     


       <!-----------------End second discound ------------------>
       </div>
    </div>
     </div>
   </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<script>
 $(document).ready( function() {
    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".expiry_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>
@endsection