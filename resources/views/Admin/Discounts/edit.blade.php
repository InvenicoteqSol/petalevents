@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-10 dashboard_right">
            <div class="line_div">
            <h3 class="com_busin_sec">Update Booking</h3>
        </div>
    <div class="row">
        <div class="col-md-12">
            {{ Form::model($eventbooking, array('route' => array('ticket.update', $eventbooking->id), 'id' => 'edtbookingfrm', 'class' => 'edt_booking_frm', 'method' => 'PUT', 'files' => true)) }}
    

        <div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group{{ $errors->has('ticket_type') ? ' has-error' : '' }}">
    <label for="ticket_type" class="control-label">Ticket type*</label>
        <input type="text" class="form-control" id="ticket_type" name="ticket_type" value="{{ $eventbooking->ticket_type }}" maxlength="10" required>
        @if ($errors->has('ticket_type'))
            <span class="help-block">
                <strong>{{ $errors->first('ticket_type') }}</strong>
            </span>
        @endif
    </div>
</div> 
    <div class="col-md-6">
          <div class="form-group{{ $errors->has('prices') ? ' has-error' : '' }}">
    <label for="prices" class="control-label">Prices*</label>
        <input type="text" class="form-control" id="prices" name="prices" value="{{ $eventbooking->prices }}" maxlength="10" required>
        @if ($errors->has('prices'))
            <span class="help-block">
                <strong>{{ $errors->first('prices') }}</strong>
            </span>
        @endif
    </div>
  </div> 
 </div>
</div>

 <div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group{{ $errors->has('stock') ? ' has-error' : '' }}">
    <label for="stock" class="control-label">Stock*</label>
        <input type="number" class="form-control" id="stock" name="stock" value="{{ $eventbooking->stock }}" maxlength="10" required>
        @if ($errors->has('stock'))
            <span class="help-block">
                <strong>{{ $errors->first('stock') }}</strong>
            </span>
        @endif
    </div>
</div> 
    
 </div>
</div>


  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
            <label for="start_date" class="control-label"> Announcement time*</label>
                <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->start_date)->format('d-m-Y') }}<?php } ?>" required >
            @if ($errors->has('start_date'))
              <span class="help-block">
                <strong>{{ $errors->first('start_date') }}</strong>
              </span>
            @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
            <label for="end_date" class="control-label"> On-sale time *</label>
            <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($eventbooking->end_date!='' || $eventbooking->end_date!='0000-00-00'  || $eventbooking->end_date!=NULL){?>{{ \Carbon\Carbon::parse($eventbooking->end_date)->format('d-m-Y') }}<?php } ?>" required >

            @if ($errors->has('end_date'))
                <span class="help-block">
                    <strong>{{ $errors->first('end_date') }}</strong>
                </span>
            @endif
        </div>
    </div>
  </div>
</div>

 <div class="col-md-12">
  <div class=" topcls">
         <div class="form-group">
            <div class="buttondiv">
                <button type="submit" class="btn btn-primary">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>
                        

                     {{ Form::close() }}
            
        </div>
    </div>
     </div>
    </div>
<script>
 $(document).ready( function() {
    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>

@endsection
