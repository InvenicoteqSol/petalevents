@extends('layouts.userdefault')
@section('content')
@include('layouts.flash-message') 

 <div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
                    
                        
                        <div class="form-group">
                            <label for="first_name" class="col-md-4 control-label"> First Name</label>
                            <div class="col-md-6">
                                <span class="form-control">{{ $adminprofile->first_name }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                               <span class="form-control"> {{ $adminprofile->last_name }}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                               <span class="form-control">{{ $adminprofile->email }}</span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                        <?php $countryInfo = DB::table('countries')->where('id', $adminprofile->country)->first(); ?>
                             <span class="form-control"> {{ $countryInfo->country_name }} </span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <span class="form-control"> {{ $adminprofile->phone }}</span>
                            </div>
                        </div>


                       <!--  <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Dob</label>

                            <div class="col-md-6">
                               <span class="form-control"> {{ $adminprofile->phone }}</span>
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Gender</label>

                            <div class="col-md-6">
                                 <span class="form-control"> {{ $adminprofile->gender }} </span>
                            </div>
                        </div> -->
                      
            </div>
       


@endsection
