@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
    @include('layouts.usersidebar')
  </div>
      <div class="col-sm-9 dashboard_right">
        @include('layouts.flash-message') 
      <div class="row">
        <div class="col-md-12">

             {{ Form::model($photographer, array('route' => array('adminprofile.update', $photographer->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT', 'files' => true)) }}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label"> First Name*</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ $photographer->first_name }}" minlength="2" maxlength="91" required autofocus>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last Name*</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ $photographer->last_name }}" minlength="2" maxlength="91" required>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $photographer->email }}" maxlength="191" required readonly="readonly">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select class="form-control" id="country" name="country" required>
                                    @foreach ($countries as $countryName=>$countryId)
                                        <option value="{{ $countryId }}" {{ ( $photographer->country == $countryId ) ? 'selected' : '' }}>{{ $countryName }}</option>
                                    @endforeach
                                </select>
                                 @if ($errors->has('country'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('country') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone*</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ $photographer->phone }}" minlength="10" maxlength="13" required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                            <label for="dob" class="col-md-4 control-label">Dob</label>

                            <div class="col-md-6">
                                <input id="dob" type="text" class="form-control" name="dob" 
value="<?php if($photographer->dob!='' || $photographer->dob!=NULL){?>{{ \Carbon\Carbon::parse($photographer->dob)->format('d-m-Y') }}<?php } ?>">
                                @if ($errors->has('dob'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group edit_radiobtn{{ $errors->has('gender') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Gender</label>

                            <div class="col-md-6">
                                   <label class="radiodiv">Male
                                        <input type="radio" name="gender" id="gender1" value="Male" <?php if($photographer->gender=="Male"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Female
                                        <input type="radio" name="gender" id="gender2" value="Female" <?php if($photographer->gender=="Female"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                @if ($errors->has('gender'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            

                         <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">Profileeeeee Image</label>

                            <div class="col-md-6">


                            <div class="col-md-3 nopadding" id="ftrd_browse_img">
                                  <h2>jzxsjdfkjsdkfjsdj</h2>

                                  @if($photographer->profile_picture!='') 

                                  <h2>jzxsjdfkjsdkfjsdj {{$photographer->profile_picture}}</h2>
                                    <img src="{{ url('public') }}/uploads/profileimages/{{ $photographer->profile_picture }}" class="img-responsive" style="width: 100px; height: 100px;">
                                     @else
                                    <img src="{{ url('public') }}/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   @endif
                                    
                                </div>

                            <div class="col-md-3 prflinput">
                                <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
                              </div>

                            </div>
                        </div>

                         <!-- <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>

                            <div class="col-md-6 edit_radiobtn">
                                   <label class="radiodiv">Active
                                        <input type="radio" name="status" id="status1" value="1" <?php if($photographer->status=="1"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Inactive
                                        <input type="radio" name="status" id="status2" value="0" <?php if($photographer->status=="0"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                     {{ Form::close() }}
            
        </div>
    </div>
</div>

</div>
<!--booking-sec-end-->
@endsection