@extends('layouts.userdefault')
@section('content')
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-3 col-xs-12 dashboard_left nopadding">
       @include('layouts.usersidebar') 
  </div>
      <div class="col-sm-9 dashboard_right">
        <div class="col-sm-12">
          <div class="current_sessiondiv">
          <div class="col-sm-8 current_sessiontitle">
            <h3>Your Current Sessions</h3>
            <p>You dont have any Upcoming Session</p>
          </div>
        <div class="col-sm-4 user_name">
          <h2>Welcome , {{$photographer->name}}</h2>
        </div>
      </div>
    </div>
      <div class="col-sm-12 editbtn text-right">
        <a href="{{ route('adminprofiles.edit',$photographer->id) }}">Edit</a>
      </div>
      <div class="profile_sec">
        <div class="col-sm-2 profile_img text-center">
          

           
            @if (Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL')
            <img src="{{ url('public') }}/uploads/profileimg.png" class="img-circle">
            @else
            <img src="{{ url('public') }}/images/avtar.jpg" class="img-circle">
            @endif
            <p>{{ Auth::user()->name}} <br/><span>Admin</span></p>
         

          
          <ul class="social_media">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
        <div class="col-sm-10 prflinfo">
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-user"></i><span>{{$photographer->name}}</span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-phone"></i><span>{{$photographer->phone}}</span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-envelope"></i><span>{{$photographer->email}}</span>
          </div>
          <div class="col-sm-12 prflinfo_div">
            <i class="fa fa-file"></i><span>Categories - Anniversary , Birthday  ,Bridal Shower ,Bridal  ,Boudoir  , Couple   Corporate Event</span>
          </div>
            <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-map-marker"></i><span>{{$photographer->country}}</span>
          </div>
            <!-- <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-calendar"></i><span>12456-45785</span>
          </div> -->
          <div class="col-sm-12 aboutdiv">
            <h4>About</h4>
            <p>{{$photographer->description}}</p>
          </div>
        </div>
      </div>
</div>

</div>
<!--booking-sec-end-->
@endsection