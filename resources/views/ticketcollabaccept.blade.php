    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;subset=latin-ext" rel="stylesheet">
      <title>Petal Events</title>
      <style type="text/css">
      body {
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       padding-top: 0 !important;
       padding-bottom: 0 !important;
       margin:0 !important;
       width: 100% !important;
       -webkit-text-size-adjust: 100% !important;
       -ms-text-size-adjust: 100% !important;
       -webkit-font-smoothing: antialiased !important;
     }
     table.MainContainer.e-petalevent {
    font-family: 'Raleway', sans-serif;
    display: block;
    padding: 20px 0;
}
table.MainContainer.e-petalevent tbody {
    padding-bottom: 50px;
    display: block;
    width: 100%;
}
table.MainContainer.e-petalevent thead {
    display: block;
    width: 100%;
}
table.MainContainer tr
      {
     display: flex;
    width: 100%;
}
tr.col-foot {
    padding: 15px;
}
a.link1 {
    color: #e14143;
}
       td.col-12
       {
        width: 100%;
        display: block;
        float: left;
       }
   td.col-12.col-brand {
    background: #e14143;
    margin-bottom: 30px;
    padding: 15px;
}
span.heading {
    color: #e14143;
    font-size: 28px;
    font-weight: 500;
    letter-spacing: 1px;
    font-family: 'Raleway', sans-serif;
        padding: 15px 0;
    display: block;
}
 table.MainContainer tbody p {
    text-align: left;
    color: #999999;
    font-size: 14px;
    font-weight: normal;
    line-height: 19px;
}
 table.MainContainer tfoot {
    border-top: solid 1px #ccc !important;
        display: flex;
    margin: 25px 0;
    background: #f7f7f7;
}
tr.col-foot td.col-6 {
    width: 50%;
    display: block;
    float: left;
}
.col-foot p span {
    display: block;
}
.col-foot span.c-name {
    color: #e14143;
    font-size: 16px;
    padding-bottom: 15px;
    font-weight: 500;
}
.col-foot img {
    border: 0 !important;
    display: block !important;
    outline: none !important;
}
tr.col-foot td.col-6 table {
    padding: 40px 0;
}
tr.col-foot td.col-6 table img {
   margin-left: 15px;
}
tr.col-foot td.col-6 td.col-12 .contentEditable {
    display: inline-block;
}
td.col-6.col-social {
    text-align: right;
}
tr.col-foot td.col-6 p {
    text-align: left;
    color: #505050;
    font-size: 14px;
    font-weight: normal;
    line-height: 20px;
}
td.col-6.col-social tbody {
    padding: 0;
}
.copyright a {
    color: #e14143;
    padding: 0 10px;
    text-decoration: none;
}
</style>
  </head>
  <body paddingwidth="0" paddingheight="0"   style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
    <table width="680" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer e-petalevent" align="center">
 <!--  thead -->
      <thead>
        <tr>
          <td class="col-12 col-brand">
             <img src="{{url('/')}}/public/images/logo.png" width='230px' height='auto' alt='' data-default="placeholder" data-max-width="560">
          </td>
        </tr>
        </thead>
         <!--  //thead -->
         <!--  <tbody> -->
      <tbody>
        <tr>
          <td class="col-12">
            <span class="heading">Welcome to Petal Events</span>
          </td>
        </tr>
        <tr class="wellcome-content">
          <td class="col-12">
          
                                 
          <p class="u-message">
          Ticket collaboration request accepted successfully
             <br>
             
               <br>
              If you have any questions, please contact us by email or social media.
                <br><br>
                    Thank you,
                          <br><br>
                 Petal Events

               
              </p>
          </td>
        </tr>
     </tbody>
    <!--  //<tbody> -->
      <!--  <tfoot> -->
     <tfoot>
       <tr class="col-foot">
        <td class="col-6">
          <p>
          <span class="c-name">Petal Events</span>
          <span>Kemp House,
               160 City Road,
                London,
                EC1V 2NX
            </span>
          <span>info@petalevents.co.uk</span>
          <!-- <span>010-020-0340</span> -->
                                          
         </p>
         <div class="copyright">
          <p> 
            © 2019 |<a href="#">Petal Events</a>| All Rights Reserved 
          </p>
        </div>
        </td>
        <td class="col-6 col-social">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tbody>
         <tr>
         <td valign="top" class="col-12">
                
        <div class="contentEditable">
          <a target="_blank" href="https://www.facebook.com/PetalEventsUK"><img src="{{url('/')}}/public/images/facebook.png" width="52" height="53" alt="twitter icon" data-default="placeholder" data-max-width="52" data-customicon="true"></a>
         </div>
          <div class="contentEditable">
          <a target="_blank" href="https://twitter.com/petalevents"><img src="{{url('/')}}/public/images/twitter.png" width="52" height="53" alt="twitter icon" data-default="placeholder" data-max-width="52" data-customicon="true"></a>
        </div>
        <div class="contentEditable">
          <a target="_blank" href="https://www.instagram.com/petal.events/"><img src="{{url('/')}}/public/images/instagram.png" width="52" height="53" alt="twitter icon" data-default="placeholder" data-max-width="52" data-customicon="true"></a>
        </div>
               <div class="contentEditable">
          <a target="_blank" href="https://www.youtube.com/channel/UCMX8YneayHaK3YjPcvKbDOQ"><img src="{{url('/')}}/public/images/youtube.png" width="52" height="53" alt="twitter icon" data-default="placeholder" data-max-width="52" data-customicon="true"></a>
        </div>
      </td>
    </tr>
  </tbody>
   </table>
        </td>
       </tr>
     </tfoot>
        <!--  //<tfoot> -->
    </table>
      </body>
      </html>


