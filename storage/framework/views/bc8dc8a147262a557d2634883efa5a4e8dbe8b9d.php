<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <title>Petal Events</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Petal Events" />
    <script>
      addEventListener("load", function () {
      	setTimeout(hideURLbar, 0);
      }, false);
      
      function hideURLbar() {
      	window.scrollTo(0, 1);
      }
    </script>
    <!--//meta tags ends here-->
    <!--booststrap-->
     <link href="<?php echo e(url('/public')); ?>/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.theme.default.css">
     <link href="<?php echo e(url('/public')); ?>/front/css/jquery-sakura.css" rel="stylesheet" type="text/css">
    <!-- font-awesome icons -->
   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo e(url('/public')); ?>/front/css/style.css" rel='stylesheet' type='text/css' media="all">
    <link href="<?php echo e(url('/public')); ?>/front/css/bootstrap-datepicker3.css" rel='stylesheet' type='text/css' media="all">
    <link href="<?php echo e(url('/public')); ?>/front/css/bootstrap-select.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
   <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
   

</head>
<?php if($showsearch=="true"): ?>
  <body class="page-event inner-page">
    <?php else: ?>
    <body class="innerpage">
    <?php endif; ?>

    

      <?php echo $__env->make('layouts.innerheader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php echo $__env->yieldContent('content'); ?>

      <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
    

</body>
</html>

