  <section class="newsletter">
                <div class="container">
                  <div class="row">
             <!-- Begin Mailchimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
              #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            </style>
            <div id="mc_embed_signup">
            <form action="https://invenicoteq.us19.list-manage.com/subscribe/post?u=1b8a8a820abacab27fdcb0ca5&amp;id=441b7aa351" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
              <h2>Subscribe to our newsletter</h2>
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
              <label for="mce-EMAIL">Email Address  <span class="asterisk">*</span>
            </label>
              <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
              <div id="mce-responses" class="clear">
                <div class="response" id="mce-error-response" style="display:none"></div>
                <div class="response" id="mce-success-response" style="display:none"></div>
              </div>   
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_1b8a8a820abacab27fdcb0ca5_441b7aa351" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->

  </div>
 </div>
              
  </section>
<!--footer-section-start-->
<?php
$business_settings = DB::table('business_settings')->where('id', '=', '1')->first();
/*echo '<pre>';
print_r($business_settings);
echo '</pre>';*/
?>
 <footer class="py-lg-4 py-md-3 py-sm-3 py-3">
      <div class="container py-lg-5 py-md-5 py-sm-4 py-3">
        <div class="groom-w3layouts-footer text-center">
          <!-- <img src="images/ff1.png" alt="" class="image-fluid"> -->
          <div class="row">
            <div class="col-md-12">
            <div class="col-md-3 anus">
              <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('/public')); ?>/front/images/logo.png"></a>
            </div>
            <div class="col-md-3 anus">
              <ul>
                <li>
                  <a href="<?php echo e(url('/faq')); ?>">FAQs</a>
                </li>
                <li>
                  <a href="<?php echo e(url('/contact')); ?>">Contact Us</a>
                </li>
                <li>
                  <a href="<?php echo e(url('/privacy-policy')); ?>">Privacy Policy</a>
                </li>
                <li>
                 <a href="<?php echo e(url('/terms-conditions')); ?>">Terms and Conditions</a>
                </li>

              </ul>
            </div>
            <div class="col-md-3 anus">
              <ul>
                <li>
                <?php if( Auth::user() !=null): ?>
                   <?php if(Auth::user()->user_type=='1'): ?>
                <a href="<?php echo e(url('/admindashboard')); ?>">My Account</a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='3'): ?>
                  <a href="<?php echo e(url('/customerdashboard')); ?>"> My Account</a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='4'): ?>
                  <a href="<?php echo e(url('/organiserdashboard')); ?>"> My Account</a>
                <?php endif; ?>
                <?php else: ?>
                <a href="<?php echo e(url('/login')); ?>"> My Account</a>
                <?php endif; ?>
                </li>
                <li>
                    <a href="<?php echo e(url('/featured-events')); ?>">Featured Events</a>
                </li>
                <li>
                  <a href="<?php echo e(url('/ticket-deals')); ?>">Ticket Deals</a>
                </li>
              </ul>
            </div>
            <div class="col-md-3 anus">

              <ul>
                <li>
                 <a href="<?php echo e(url('/login')); ?>"> Event Organiser Login</a>
                </li>
                <li>
                  <a href="<?php echo e(url('/press')); ?>">Press Enquiries</a>
                </li>
                

              </ul>
            </div>
          </div>
          </div>
        </div>
        
        <div class="icons-footer text-center mt-lg-5 mt-md-4 mt-3">
          <ul>
            <li><a href="http://facebook.com/PetalEventsUK" target="_blank"><span class="fa fa-facebook"></span></a></li>
            <li><a href="http://twitter.com/petalevents" target="_blank"><span class="fa fa-twitter"></span></a></li>
            <li><a href="http://instagram.com/petal.events" target="_blank"><span class="fa fa-camera-retro"></span></a></li>
            <li><a href="https://www.youtube.com/channel/UCMX8YneayHaK3YjPcvKbDOQ" target="_blank"><span class="fa fa-youtube-play"></span></a></li>
            
          </ul>
        </div>
        <!-- <div class="copy-agile-right text-center pt-lg-4 pt-3">
          <p> 
            © 2018 Petal Events. All Rights Reserved 
          </p>
        </div> -->
      </div>
    </footer>
    <div class="copy-agile-right text-center pt-lg-4 pt-3">
          <p> 
            © 2018 |<a href="<?php echo e(url('')); ?>">Petal Events</a>| All Rights Reserved 
          </p>
        </div>
    <!--//footer -->
   <script src="<?php echo e(url('/public')); ?>/front/js/jquery-2.2.3.min.js"></script>
    <!--//js working-->
    <script src="<?php echo e(url('/public')); ?>/front/js/jquery-sakura.min.js"></script>
    <script>
      $(window).load(function () {
          $('body').sakura();
          $("#cartbody").append($('<div>').load("<?php echo e(url('/minCart')); ?>"));
      });
    </script>
 
   
    <!--bootstrap working-->
    <script src="<?php echo e(url('/public')); ?>/front/js/bootstrap.min.js"></script>
    <!-- //bootstrap working-->
    <script src="<?php echo e(url('/public')); ?>/front/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo e(url('/public')); ?>/front/js/bootstrap-select.js"></script>
        <script src="<?php echo e(url('/public')); ?>/front/js/bootstrap-notify.js"></script>
        <script src="<?php echo e(url('/public')); ?>/front/js/owl.carousel.js"></script>
    
  </body>
</html>