<?php $__env->startSection('content'); ?>
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
         <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
         <div class="line_div"><h3 class="com_busin_sec">Organiser</h3></div>
      <div class="row">
        <div class="col-md-12">

                    <?php echo Form::open(array('route' => 'adminorganiser.store', 'method'=>'POST', 'id' => 'edtpcfrm',  'class' => 'edt_post_frm', 'files' => true)); ?>


                        <?php echo e(csrf_field()); ?>

                        <div class="form-group<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
                            <label for="first_name" class="col-md-4 control-label"> First Name*</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo e(old('first_name')); ?>" minlength="2" maxlength="91" required autofocus>

                                <?php if($errors->has('first_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                            <label for="last_name" class="col-md-4 control-label">Surname *</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo e(old('last_name')); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('last_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('organisation_name') ? ' has-error' : ''); ?>">
                            <label for="organisation_name" class="col-md-4 control-label">Organisation name *</label>

                            <div class="col-md-6">
                                <input id="organisation_name" type="text" class="form-control" name="organisation_name" value="<?php echo e(old('organisation_name')); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('organisation_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('organisation_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                             <div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
                            <label for="website" class="col-md-4 control-label">Organisation website *</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" value="<?php echo e(old('website')); ?>" minlength="2" maxlength="91" placeholder="http://www.example.com" required>

                                <?php if($errors->has('website')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('website')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                       
                            <div class="form-group<?php echo e($errors->has('address_line_1') ? ' has-error' : ''); ?>">
                            <label for="address_line_1" class="col-md-4 control-label">Address line 1 </label>

                            <div class="col-md-6">
                                <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="<?php echo e(old('address_line_1')); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('address_line_1')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address_line_1')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                             <div class="form-group<?php echo e($errors->has('address_line_2') ? ' has-error' : ''); ?>">
                            <label for="address_line_2" class="col-md-4 control-label">Address line 2 *</label>

                            <div class="col-md-6">
                                <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="<?php echo e(old('address_line_2')); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('address_line_2')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address_line_2')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                              <div class="form-group<?php echo e($errors->has('postcode') ? ' has-error' : ''); ?>">
                            <label for="postcode" class="col-md-4 control-label">Postcode </label>

                            <div class="col-md-6">
                                <input id="postcode" type="text" class="form-control" name="postcode" value="<?php echo e(old('postcode')); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('postcode')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('postcode')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                            <div class="form-group<?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
                            <label for="country" class="col-md-4 control-label">Country</label>

                            <div class="col-md-6">
                                <select class="form-control" id="country" name="country" required>
                                    <?php  $countries = DB::table('countries')->pluck('id', 'country_name'); ?>
                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryName=>$countryId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($countryId); ?>" <?php echo e(( old('country') == $countryId ) ? 'selected' : ''); ?>><?php echo e($countryName); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <?php if($errors->has('country')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('country')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                         <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label for="phone" class="col-md-4 control-label">Phone*</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e(old('phone')); ?>" minlength="10" maxlength="13" required>

                                <?php if($errors->has('phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="col-md-4 control-label">E-Mail Address*</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" maxlength="191" required>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                            <label for="username" class="col-md-4 control-label"> Username *</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" minlength="6" maxlength="17" required>

                                <?php if($errors->has('username')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('username')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label">Password*</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('security_question') ? ' has-error' : ''); ?>">
                            <label for="security_question" class="col-md-4 control-label">Security question*</label>

                            <div class="col-md-6">
                                <input id="security_question" type="text" class="form-control" name="security_question" minlength="6" maxlength="17" required>

                                <?php if($errors->has('security_question')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('security_question')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('security_answer') ? ' has-error' : ''); ?>">
                            <label for="security_answer" class="col-md-4 control-label">Security answer*</label>

                            <div class="col-md-6">
                                <input id="security_answer" type="text" class="form-control" name="security_answer" minlength="6" maxlength="17" required>

                                <?php if($errors->has('security_answer')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('security_answer')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
               

                         <div class="form-group<?php echo e($errors->has('user_type') ? ' has-error' : ''); ?>">
                            <div class="col-md-6">
                                <input type="hidden" value="4" name="user_type" id="user_type">  
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </div>
                     <?php echo e(Form::close()); ?>

                    
            </div>
    </div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>