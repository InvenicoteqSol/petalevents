<?php $__env->startSection('content'); ?>
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
    
   <div class="col-sm-12 dashboard_right">
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


<div class="col-sm-12 selectdiv topbar_selectiv text-right createbtn">
  
        <h3><a class="btn btn-primary" href="<?php echo e(url('exportcustomer')); ?>/<?php echo e($event_id); ?>" class="btn_expt">EXPORT CSV</a></h3>
    </div>

<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Customer Data</h4>
                 <p class="card-category"></p>
        </div>

       <div class="col-sm-12 tablediv spacertop card-body">
      <div class="tablewrapper table-responsive">
      <table class="table-bordered table-striped table">
               
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column4">Name</th>
                        <th class="column4">Email</th>
                        <th class="column4">Phone</th>
                        <th class="column4">Order Number</th>
                       </tr>
                </thead>
                <tbody>
                  <?php 
                   $event_id;
                   $customerdata = DB::table('users')
                                  ->select('users.id','first_name','email','phone','orders.order_number','orders.amount')
                                  ->join('orders', 'orders.user_id', '=', 'users.id')
                                  ->join('customerrequestmapings','orders.user_id','=','customerrequestmapings.customer_id')
                                  ->where('orders.event_id','=', $event_id)
                                  ->get();
                                 ?>
                 <?php if(count($customerdata)>0): ?>
                 <?php $__currentLoopData = $customerdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">
                      <td class="column4"><?php echo e($customer->first_name); ?></td>
                      <td class="column4"><?php echo e($customer->email); ?></td>
                      <td class="column4"><?php echo e($customer->phone); ?></td>
                      <td class="column4"><?php echo e($customer->order_number); ?></td>
                     </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>

       </div>
    </div>
  </div>
</div>
</div>
</div>
</div>  
                       
            
        </div>
    </div>
     </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>