<?php $__env->startSection('content'); ?>

 <!-- CONTACT SECTION -->
<section id="organiser-register">
     <div class="container">
          <div class="row">

             <div class="col-md-12 col-sm-12 col-xs-12 col-head-area">
        <h2 class="main-s-heading">Organiser Registeration</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
      </div>
             </div>
             <div class="row">

               <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- CONTACT FORM HERE -->
                              <form id="contact-form" class="settingform" action="<?php echo e(url('organiserregistration/insert')); ?>" method="POST" enctype="multipart/form-data">
                                                        <?php echo e(csrf_field()); ?>


                              <div class="col-md-6 col-sm-6">
                               <!--  form-group -->
                                <div class="form-group">
                                   <input type="text" class="form-control" name="first_name" value="<?php echo e(old('first_name')); ?>" placeholder="First name">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="last_name" value="<?php echo e(old('last_name')); ?>" placeholder="Last name" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="organisation_name" value="<?php echo e(old('organisation_name')); ?>" placeholder="Organisation name" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="website" value="<?php echo e(old('website')); ?>" placeholder="Organisation website" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="address_line_1" value="<?php echo e(old('address_line_1')); ?>" placeholder="Address line 1" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="address_line_2" value="<?php echo e(old('address_line_2')); ?>" placeholder="Address line 2" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <input type="text" class="form-control" name="postcode" value="<?php echo e(old('postcode')); ?>"  placeholder="Postcode" required="">
                                 </div>
                                 <!--  //form-group -->
                                 </div>
                              <div class="col-md-6 col-sm-6">
                                 <!--  form-group -->
                                  <div class="form-group">
                                   <select class="form-control" id="country" name="country" required>
                                    <?php  $countries = DB::table('countries')->pluck('id', 'country_name'); ?>
                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryName=>$countryId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($countryId); ?>" <?php echo e(( old('country') == $countryId ) ? 'selected' : ''); ?>><?php echo e($countryName); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                  <input id="phone" type="text" class="form-control" value="<?php echo e(old('phone')); ?>" value="" minlength="10" maxlength="13" placeholder="Phone" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                  <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" value="" maxlength="191" placeholder="Email" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                  <input id="username" type="text" class="form-control" name="username" minlength="6" maxlength="17" placeholder="username" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                 <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" placeholder="password" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                 <input id="security_question" type="text" class="form-control" name="security_question" minlength="6" maxlength="17" placeholder="Security question" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                  <input id="security_answer" type="text" class="form-control" name="security_answer" minlength="6" maxlength="17" placeholder="Security answer" required="">

                                  <input type="hidden" value="4" name="user_type" id="user_type"> 
                                 </div>
                                 <!--  //form-group -->
                                </div>
                              
                              <div class="col-md-12 col-sm-12 form-action">
                              
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                        <a href="<?php echo e(url('/login')); ?>" class="btn btn-primary">Sign In</a>
                              
                              </div>
                        </form>
                   
               </div>


          </div>
     </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>