<?php use \App\Http\Controllers\CartingController;?>

<?php $__env->startSection('content'); ?>
   <?php
   $subtotal_amount=0;
   $discount_given=0;
   $discounted_amount=0;

   ?>
  <!-- CONTACT SECTION -->
<section class="section-checkout">
   
<div class="container">
  <!-- row -->
  <?php if(count($_discountobjlist)>0): ?>
<?php $__currentLoopData = $eventIdList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<!-- row -->
<?php

$thisEventItems=CartingController::get_array_details_front($Items,$_discountobjlist,'event_id');
$EventSubTotal=0;
 ?>
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
         <thead>
            <tr>
              <?php
              $eventname=DB::table('eventbookings')->select('event_title')->where('id','=',$Items)->first();
              ?>
               <th colspan="5" class="e-name"><?php echo e($eventname->event_title); ?></th>
            </tr>
            <tr>
              <th>Ticket Type</th>
              <th>Net Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php if(count($thisEventItems)>0): ?>
              <?php $__currentLoopData = $thisEventItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartItems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              
              <tr>
              <td><?php echo e($cartItems->TicketTitle); ?> </td>
              <td><i class="fa fa-gbp"></i> <?php echo e($cartItems->subtotal_amount); ?></td>
              <td>  <?php echo Form::open(['route' => ['cart.update',$cartItems->rowId], 'method' => 'PUT']); ?>

                        <input name="qty" type="number" class="form-control cartupdate" value="<?php echo e($cartItems->Quantity); ?>">
                        <input type="hidden" id="size" value="<?php echo e($cartItems->event_id); ?>">
                        <?php echo Form::close(); ?></td>
              <td><i class="fa fa-gbp"></i> <?php echo e($cartItems->discounted_amount); ?></td>
              <th>
                <form action="<?php echo e(route('cart.destroy',$cartItems->rowId)); ?>"  method="POST">
                           <?php echo e(csrf_field()); ?>

                           <?php echo e(method_field('DELETE')); ?>

                           <button class="btn btn-primary" type="submit"><i class="fa fa-trash"></i></button>
                         </form>
              </th>
              <?php
              $EventSubTotal=$EventSubTotal+$cartItems->discounted_amount;
              $subtotal_amount=$subtotal_amount+$cartItems->subtotal_amount;
              $discount_given=$discount_given+$cartItems->discount_given;
              $discounted_amount=$discounted_amount+$cartItems->discounted_amount;

              ?>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
              
            
            
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="total-amt">Sub-total: <i class="fa fa-gbp"></i><?php echo e($EventSubTotal); ?></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  <!--// row -->
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
  
 </div>
  <div class="container">
   <div class="row row-promo">
    <div class="col-md-12 col-xs-12">
   <div class="card">
          <h4 class="sub-heading">Apply Promo Codes</h4>
          <div class="card-body">
           
            <div class="input-group">
      <input type="text" class="form-control" aria-label="...">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default">Apply</button>
       
      </div><!-- /btn-group -->
    </div><!-- /input-group -->
        
          </div>
        </div>
      </div>
   </div>
   <!--// row -->
        <!-- row -->
  <div class="row">
    <div class="col-xs-12">
      <div class="table-responsive pricing-table">
        <table class="table table-bordered table-hover">
          <tbody>
            <tr>
               <th class="e-name text-left">Total Amount To Be Paid</th>
              <th  class="total-amt text-right"> 
                        <p>SubTotal: <i class="fa fa-gbp"></i> <?php echo e($subtotal_amount); ?></p>
                        <p>Discount Given:<i class="fa fa-gbp"></i> <?php echo e($discount_given); ?></p>
                        <p>Total: <i class="fa fa-gbp"></i><?php echo e($discounted_amount); ?></p>
                    
                    </th>
            </tr>
          </tbody>
        </table>
      </div><!--end of .table-responsive-->
     <?php   $user_id = Auth::user()->id ;
     $user_email=Auth::user()->email;

     ?>

        <form action="<?php echo e(url('/charge')); ?>" method="POST">
            <?php echo e(csrf_field()); ?>

            <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_6020SfAmkxN79Hq31s8W0WHZ"
                    data-amount="<?php echo e($discounted_amount*100); ?>"
                    data-name=""
                    data-description=""
                    data-image=""
                    data-email=<?php echo e($user_email); ?>

                    data-locale="auto"
                    data-currency="gbp">
              </script>
              <input type="hidden" name="data_amount" value="<?php echo e($discounted_amount*100); ?>">
              <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
        </form>
     <!--  <div class="col-pay-now">
          <a href="#" id="updateorder" updateorder="" class="btn btn-primary"> Pay Now! </a>
      </div> -->
    </div>
  </div>
   <!--// row -->
</div>


</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
   
   $(".cartupdate").change(function() {
     this.form.submit();
});
 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>