<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>

   <div class="container-fluid">


      <div class="row">
       
          <div class="col-sm-7 table-search searchbar">
        
    </div>
       <div class="col-sm-5 selectdiv topbar_selectiv text-right createbtn">
         <a class="btn btn-primary" href="<?php echo e(route('customerdashboard.edit',$customer->id)); ?>">   Edit  </a>
   </div>
      </div>

          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
                  <p class="singalname"><?php echo e($customer->first_name); ?> <?php echo e($customer->last_name); ?></p>   

                </div>
       
<div class="card-body">
        <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                            <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Name :</span><?php echo e($customer->first_name); ?></p>
                            </div>
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Phone :</span><?php echo e($customer->phone); ?></p>
                            </div>
                            <div class="col-md-6">
                                 <p class="rightsidetext"> <span class="lablname">Email :</span><?php echo e($customer->email); ?></p>
                               </div>
                               <div class="col-md-6">
                                 <p class="rightsidetext"> <span class="lablname">Gender:</span><?php echo e($customer->gender); ?></p>
                               </div>
                               
                             </div>
                            
                           <div class="row">
                         <div class="col-md-2">
               <!--  <div class="col-sm-4 profile_img text-center"> -->
               <?php if(Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL'): ?>
               <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e(Auth::user()->profile_picture); ?>" class="img-circle" style="width: 200px;">
               <?php else: ?>
               <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" class="img-circle">
              <?php endif; ?>
                </div>
              </div>
            </div>
              </div>
            </form>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
</style>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>