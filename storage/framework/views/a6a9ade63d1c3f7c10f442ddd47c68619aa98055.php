<?php $__env->startSection('content'); ?>
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
    
   <div class="col-sm-12 dashboard_right">
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php
       $event_id1=DB::table('requestdata')->where('id','=', $eventid)->first();
       $evntid=$event_id1->event_id;
        $customerdata = DB::table('users')
                    ->select('users.id','first_name','email','phone','orders.order_number')
                    ->join('orders', 'orders.user_id', '=', 'users.id')
                    ->where('orders.event_id','=', $evntid)
                    ->get();


        ?>
<div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Customer Data</h4>
                 <p class="card-category"> Here is a subtitle for this table</p>
        </div>
       <div class="col-sm-12 tablediv spacertop card-body">
      <div class="tablewrapper table-responsive">
        <?php if(count($customerdata)>0): ?>
      <h1 class="success_msg" style="color: green;">Mail Sended Successfully</h1>
      <table class="table-bordered table-striped table" id="cust_data">
              
                 <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Name </th>
                        <th class="column4">Email</th>
                        <th class="column4">Phone</th>
                        <th class="column5">Order Number</th>
                        <th class="column3">Action</th>
                  </tr>
                </thead>
             
                <tbody>
                 
                 <?php $__currentLoopData = $customerdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               
                   <tr class="tabledata odd">
                       <td class="column3"><?php echo e($customer->first_name); ?></td>

                        <td class="column4"><?php echo e($customer->email); ?></td>

                        <td class="column4"><?php echo e($customer->phone); ?></td>
                       
                        <td class="column5"><?php echo e($customer->order_number); ?></td>


                     <td class="column3 action_btns"> 
                     <input type="checkbox" name="send_mail" value="<?php echo e($customer->id); ?>" class="check_everone" id="check_all">
                    </td>
                      </tr>
               
               
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

             </tbody>
      </table>
      <div>  <button class="send_mail_check btn btn-primary">Approve request</button></div>

       <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
               <?php endif; ?>


    </div>
    
                       
            
        </div>
      </div>
    </div>
  </div>
    </div>
     </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>

<script type="text/javascript">
 jQuery('.success_msg').hide();
  jQuery(".send_mail_check").click(function () {
    var selected = new Array();
    $('#cust_data input[type="checkbox"]:checked').each(function() {   
     selected.push($(this).attr('value'));
   });
    
    jQuery.ajax({
            type : 'GET',
            url  : "<?php echo e(url('sendmail')); ?>",
            data:{ 'send_mail': selected,'reqId': <?php echo $eventid; ?>},
            success :  function(status) {
            if(status=='true') {
            jQuery('.success_msg').show();
            }
        }
    });
     
});
</script> 

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>