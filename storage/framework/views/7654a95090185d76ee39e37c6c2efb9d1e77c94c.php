<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
     <div class="line_div"><h3 class="com_busin_sec">Organiser </h3></div>
      <div class="row">
        <div class="col-md-12">
             <?php echo e(Form::model($organiser, array('route' => array('adminorganiser.update', $organiser->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT', 'files' => true))); ?>


            <div class="form-group<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
                    <div class="col-md-6">           
                            <label for="first_name"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo e($organiser->first_name); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('first_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                          <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                          <div class="col-md-6">
                            <label for="last_name">Surname*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo e($organiser->last_name); ?>" minlength="2" maxlength="91" required>
                                <?php if($errors->has('last_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                 <div class="form-group<?php echo e($errors->has('organisation_name') ? ' has-error' : ''); ?>">
                            <label for="organisation_name" class="col-md-4 control-label">Organisation name *</label>

                            <div class="col-md-6">
                                <input id="organisation_name" type="text" class="form-control" name="organisation_name" value="<?php echo e($organiser->organisation_name); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('organisation_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('organisation_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                             <div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
                            <label for="website" class="col-md-4 control-label">Organisation website *</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" value="<?php echo e($organiser->website); ?>" minlength="2" maxlength="91" placeholder="http://www.example.com" required >

                                <?php if($errors->has('website')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('website')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                       
                            <div class="form-group<?php echo e($errors->has('address_line_1') ? ' has-error' : ''); ?>">
                            <label for="address_line_1" class="col-md-4 control-label">Address line 1 </label>

                            <div class="col-md-6">
                                <input id="address_line_1" type="text" class="form-control" name="address_line_1" value="<?php echo e($organiser->address_line_1); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('address_line_1')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address_line_1')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                             <div class="form-group<?php echo e($errors->has('address_line_2') ? ' has-error' : ''); ?>">
                            <label for="address_line_2" class="col-md-4 control-label">Address line 2 *</label>

                            <div class="col-md-6">
                                <input id="address_line_2" type="text" class="form-control" name="address_line_2" value="<?php echo e($organiser->address_line_2); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('address_line_2')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('address_line_2')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                              <div class="form-group<?php echo e($errors->has('postcode') ? ' has-error' : ''); ?>">
                            <label for="postcode" class="col-md-4 control-label">Postcode </label>

                            <div class="col-md-6">
                                <input id="postcode" type="text" class="form-control" name="postcode" value="<?php echo e($organiser->postcode); ?>" minlength="2" maxlength="91" required>

                                <?php if($errors->has('postcode')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('postcode')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                             <label for="email" class="col-md-4 control-label">E-Mail Address* </label>

                          <div class="col-md-6">
                      
                                <input id="email" type="email" class="form-control" name="email" value="<?php echo e($organiser->email); ?>" maxlength="191" required >
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
                              <label for="country" class="col-md-4 control-label">Country</label>

                          <div class="col-md-6">
                            
                                <select class="form-control" id="country" name="country" required>
                                    <?php $__currentLoopData = $countries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $countryName=>$countryId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($countryId); ?>" <?php echo e(( $organiser->country == $countryId ) ? 'selected' : ''); ?>><?php echo e($countryName); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                 <?php if($errors->has('country')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('country')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label for="phone" class="col-md-4 control-label">Phone*</label>

                        <div class="col-md-6">
                          
                                <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e($organiser->phone); ?>" minlength="10" maxlength="13" required>
                                <?php if($errors->has('phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

             
                <div class="form-group<?php echo e($errors->has('username') ? ' has-error' : ''); ?>">
                            <label for="username" class="col-md-4 control-label"> Username *</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" minlength="6" value="<?php echo e($organiser->username); ?>" maxlength="17" required>

                                <?php if($errors->has('username')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('username')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label">Password*</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" value="<?php echo e($organiser->password); ?>" minlength="6" maxlength="17" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group<?php echo e($errors->has('security_question') ? ' has-error' : ''); ?>">
                            <label for="security_question" class="col-md-4 control-label">Security question*</label>

                            <div class="col-md-6">
                                <input id="security_question" type="text" class="form-control" name="security_question"  value="<?php echo e($organiser->security_question); ?>" minlength="6" maxlength="17" required>

                                <?php if($errors->has('security_question')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('security_question')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="form-group<?php echo e($errors->has('security_answer') ? ' has-error' : ''); ?>">
                            <label for="security_answer" class="col-md-4 control-label">Security answer*</label>

                            <div class="col-md-6">
                                <input id="security_answer" type="text" class="form-control" name="security_answer"  value="<?php echo e($organiser->security_answer); ?>" minlength="6" maxlength="17" required>

                                <?php if($errors->has('security_answer')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('security_answer')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
           
                  <div class="col-md-12">
                         <div class="row topcls">
                          <div class="col-md-6">
                            <label for="roles">Featured Image</label>
                            <div class="form-group">
                                  <div class="col-md-3 nopadding" id="ftrd_browse_img" style="padding-top: 10px;">

                                  <?php if($organiser->profile_picture!=''): ?> 
                                    <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($organiser->profile_picture); ?>" class="img-responsive" style="width: 100px; height: 100px;">
                                     <?php else: ?>
                                    <img src="<?php echo e(url('public')); ?>/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   <?php endif; ?>
                                    
                                </div>

                                <div class="col-md-3 prflinput">
                                  <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
                              </div>

                            </div>
                        </div>
                         <div class="col-md-6">
                         </div>
                    </div>
                </div>
                  <div class="col-md-12 butn_edit_neupdates">
                        <div class="topcls">
                     <div class="form-group">
                            <div class="buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                     <?php echo e(Form::close()); ?>

            
         </div>
    </div>
</div>
    </div>
<script>
 $(document).ready( function() {

    /*  var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })*/

    $("#profile_picture").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });    

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                },
                dob:{
                   required: true
                },
                 gender:{
                   required: true
                },
                 status:{
                   required: true
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                },
                 dob: {
                  required: "This is required field."
                },
                 gender: {
                  required: "This is required field."
                },
                 status: {
                  required: "This is required field."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
    
       $('.datepicker').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });
 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>