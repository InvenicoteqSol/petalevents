<?php $__env->startSection('content'); ?>
   <div class="row">
   
    
   <div class="col-sm-12 dashboard_right">
    <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="col-sm-12 nopadding">
  <div class="row">
        <div class="col-sm-4 selectdiv topbar_selectiv ">
        <?php   $id_evnt=request()->route('id');?>
        </div>
        <div class="col-sm-8 selectdiv topbar_selectiv text-right createbtn">
  <div class="dropdown head-dropdown">
  <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
    Create New Collabration
    <span class="caret"></span>
  </a>
  <ul class="dropdown-menu" aria-labelledby="dLabel">
    <li> <a data-toggle="modal" data-target="#linkmodal_self" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Self Events</a></li>
    <li> <a data-toggle="modal" data-target="#earlybird" data-whatever="@earlybird" style="cursor: pointer;" title="Send Request">Collabration Event</a></li>
  
    
  </ul>
</div>

    </div>
    <div class="modal" id="linkmodal_self" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Self Events</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/selfevents">
          <?php echo e(csrf_field()); ?>

            <input type="hidden" class="form-control" name="coll_type" value="self">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo e(@old(name)); ?>" maxlength="10" required>
          <?php if($errors->has('name')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('name')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id') ? ' has-error' : ''); ?>">
          <label for="event_id" class="control-label"> Event A*</label>
          <select class="form-control" name="event_id">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id') ? ' has-error' : ''); ?>">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id2') ? ' has-error' : ''); ?>">
          <label for="event_id2" class="control-label"> Event B*</label>
          <select class="form-control" name="event_id2">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id2') ? ' has-error' : ''); ?>">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
           <div class="form-group">
          <label for="discount_type" class="col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type">
          <option disabled="disabled" selected="selected"> --- Select Type of Discount --- </option>
          <option value="0"> Percentage</option>
          <option value="1"> Fixed</option>

          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group<?php echo e($errors->has('discount') ? ' has-error' : ''); ?>">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="<?php echo e(@old(discount)); ?>" maxlength="10" required>
          <?php if($errors->has('discount')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('discount')); ?></strong>
          </span>
          <?php endif; ?>
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('start_date') ? ' has-error' : ''); ?>">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php echo e(@old(start_date)); ?>">
          <?php if($errors->has('start_date')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('start_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('end_date') ? ' has-error' : ''); ?>">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php echo e(@old(end_date)); ?>">
          <?php if($errors->has('end_date')): ?>
          <span class="help-block">
              <strong><?php echo e($errors->first('end_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>

  <!------end first form for discount------------------------------->
    <!------Start second form for discount------------------------------->

  <div class="modal" id="earlybird" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Collabration Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/collabrationevent">
           <input type="hidden" class="form-control" name="coll_type" value="collticket">

          <?php echo e(csrf_field()); ?>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo e(@old(name)); ?>" maxlength="10" required>
          <?php if($errors->has('name')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('name')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id') ? ' has-error' : ''); ?>">
          <label for="event_id" class="control-label"> Event*</label>
          <select class="form-control" name="event_id">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id') ? ' has-error' : ''); ?>">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div>
          </div>
          </div>
          <?php  $users = DB::table('users')
                                     ->where('user_type','=',4)
                                     ->get();
                                     ?> 
             <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('org_id') ? ' has-error' : ''); ?>">
          <label for="org_id" class="control-label"> Organiser*</label>
          <select class="form-control" name="org_id">
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->first_name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
  
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id2') ? ' has-error' : ''); ?>">
          <label for="event_id2" class="control-label"> Event B*</label>
          <select class="form-control" name="event_id2">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id2') ? ' has-error' : ''); ?>">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" id="ticket_sec2">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>"><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
             <div class="form-group<?php echo e($errors->has('discount_type') ? ' has-error' : ''); ?>">
               <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
                    <select class ="form-control" id="discount_type" name="discount_type" value="">
                    <option disabled="disabled" selected="selected"> --- Select --- </option>
                    <option> Percentage</option>
                    <option> Fixed</option>
          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group<?php echo e($errors->has('discount') ? ' has-error' : ''); ?>">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="<?php echo e(@old(discount)); ?>" maxlength="10" required>
          <?php if($errors->has('discount')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('discount')); ?></strong>
          </span>
          <?php endif; ?>
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('start_date') ? ' has-error' : ''); ?>">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php echo e(@old(start_date)); ?>">
          <?php if($errors->has('start_date')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('start_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('end_date') ? ' has-error' : ''); ?>">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php echo e(@old(end_date)); ?>">
          <?php if($errors->has('end_date')): ?>
          <span class="help-block">
              <strong><?php echo e($errors->first('end_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>

         
          <!-----------------first discound ------------------>
       <div class="row">
       <div class="col-lg-6 col-md-12">
        <div class="card">
        <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Self Events</h4>
                   </div>
                <div class="card-body">
                 <table class="table table-hover">
                   <thead>
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Discount</th>
                        <th class="column3">Start Date</th>
                        <th class="column3">End Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $ticketcollabrations = DB::table('ticketcollabration')
                  ->where('coll_type','=','self')
                  ->where('deleted',0)
                  ->get();  ?>
                    <?php if(count($ticketcollabrations)>0): ?>
                     <?php $__currentLoopData = $ticketcollabrations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticketcollabration): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr class="tabledata odd">
                        <td class="column3"><?php echo e($ticketcollabration->name); ?></td>
                        <td class="column3"><?php echo e($ticketcollabration->discount); ?></td>
                        <td class="column3"> <?php echo e(\Carbon\Carbon::parse($ticketcollabration->start_date)->format('d M Y')); ?> </td>
                        <td class="column3">  <?php echo e(\Carbon\Carbon::parse($ticketcollabration->end_date)->format('d M Y')); ?></td>
                      <td class="column3 action_btns"> 
                      <a data-toggle="modal" data-target="#linkmodaledit_<?php echo e($ticketcollabration->id); ?>" data-whatever="@linkmodaledit" style="cursor: pointer;" title="Send Request"><i class="fa fa-edit"></i></a>

 <div class="modal" id="linkmodaledit_<?php echo e($ticketcollabration->id); ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Self Events</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/selfeventsedit/<?php echo e($ticketcollabration->id); ?>">
          <?php echo e(csrf_field()); ?>

           <input type="hidden" class="form-control" name="coll_type" value="self">

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo e($ticketcollabration->name); ?>" maxlength="10" required>
          <?php if($errors->has('name')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('name')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id') ? ' has-error' : ''); ?>">
          <label for="event_id" class="control-label"> Event A*</label>
          <select class="form-control" name="event_id">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->event_id ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id') ? ' has-error' : ''); ?>">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
              <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->ticket_id ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->ticket_type); ?></option>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id2') ? ' has-error' : ''); ?>">
          <label for="event_id2" class="control-label"> Event B*</label>
           <select class="form-control" name="event_id2">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->event_id2 ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id2') ? ' has-error' : ''); ?>">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->ticket_id2 ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
           <div class="form-group">
          <label for="discount_type" class="col-form-label text-md-right"> Type of Discount </label>
          <select class ="form-control" id="discount_type" name="discount_type" value="<?php echo e($ticketcollabration->discount_type); ?>">
          <option> Percentage</option>
          <option> Fixed</option>

          </select>
          
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group<?php echo e($errors->has('discount') ? ' has-error' : ''); ?>">
          <label for="discount" class="control-label">Discount*</label>
          <input type="number" class="form-control" id="discount" name="discount" value="<?php echo e($ticketcollabration->discount); ?>" maxlength="10" required>
          <?php if($errors->has('discount')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('discount')); ?></strong>
          </span>
          <?php endif; ?>
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('start_date') ? ' has-error' : ''); ?>">
          <label for="start_date" class="control-label"> Start Date*</label>
          <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($ticketcollabration->start_date!='' || $ticketcollabration->start_date!='0000-00-00'  || $discount->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($ticketcollabration->start_date)->format('d-m-Y')); ?><?php } ?>" required >
          <?php if($errors->has('start_date')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('start_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('end_date') ? ' has-error' : ''); ?>">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($ticketcollabration->end_date!='' || $ticketcollabration->end_date!='0000-00-00'  || $ticketcollabration->end_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($ticketcollabration->end_date)->format('d-m-Y')); ?><?php } ?>" required >
          <?php if($errors->has('end_date')): ?>
          <span class="help-block">
              <strong><?php echo e($errors->first('end_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>
                  <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/destroy/<?php echo e($ticketcollabration->id); ?>">
                     
                        <button type="submit"><i class="fa fa-trash"></i></button>
                     </form>
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

            </tbody>
          </table>
        </div>
       </div>
     </div>

       <!-----------------End first discound ------------------>
        <!-----------------first discound ------------------>

            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                 <h4 class="card-title">Collabration Event</h4>
                   </div>
                <div class="card-body table-responsive">
                 <table class="table table-hover">
                   <thead>
                   <tr class="headings">
                        <th class="column3">Name</th>
                        <th class="column3">Discount</th>
                         <th class="column3">Start Date</th>
                        <th class="column3">End Date</th>
                       <th class="column3">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                  <?php $ticketcollabrations = DB::table('ticketcollabration')
                  ->where('coll_type','=','collticket')
                  ->where('deleted', 0)
                  ->get();  ?>
                    <?php if(count($ticketcollabrations)>0): ?>
                     <?php $__currentLoopData = $ticketcollabrations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticketcollabration): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                    <tr class="tabledata">
                       <td class="column3"><?php echo e($ticketcollabration->name); ?></td>

                        <td class="column3"><?php echo e($ticketcollabration->discount); ?></td>
                   <!--      <td class="column3"><?php echo e($ticketcollabration->ticket_id); ?></td>
                        <td class="column3"><?php echo e($ticketcollabration->event_id2); ?> </td>
                        <td class="column3"><?php echo e($ticketcollabration->ticket_id2); ?> </td> -->
                        <td class="column3"> <?php echo e(\Carbon\Carbon::parse($ticketcollabration->start_date)->format('d M Y')); ?> </td>
                        <td class="column3">  <?php echo e(\Carbon\Carbon::parse($ticketcollabration->end_date)->format('d M Y')); ?></td>
                     <td class="column3 action_btns"> 
                      <a data-toggle="modal" data-target="#linkmodaledit_<?php echo e($ticketcollabration->id); ?>" data-whatever="@linkmodaledit" style="cursor: pointer;" title="Send Request"><i class="fa fa-edit"></i></a>

 <div class="modal" id="linkmodaledit_<?php echo e($ticketcollabration->id); ?>" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Collabration Event</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
        
          <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/earlybirdedit/<?php echo e($ticketcollabration->id); ?>">
          <?php echo e(csrf_field()); ?>

           <input type="hidden" class="form-control" name="coll_type" value="collticket">

                  <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-12">
          <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
          <label for="name" class="control-label"> Name*</label>
          <input type="text" class="form-control" id="name" name="name" value="<?php echo e($ticketcollabration->name); ?>" maxlength="10" required>
          <?php if($errors->has('name')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('name')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div> 
      
          </div>
          </div>             
          <?php  $eventbookings = DB::table('eventbookings')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?>          
           <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id') ? ' has-error' : ''); ?>">
          <label for="event_id" class="control-label"> Event*</label>
          <select class="form-control" name="event_id">
            <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->event_id ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
           <?php  $tickets = DB::table('tickets')
                                     ->where('deleted', 0)
                                     ->get();
                                     ?> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id') ? ' has-error' : ''); ?>">
          <label for="ticket_id" class="control-label">Ticket*</label>
             <select class="form-control" name="ticket_id">
            <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
           
              <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->ticket_id ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->ticket_type); ?></option>

            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div>
          </div>
          </div>
          <?php  $users = DB::table('users')
                                     ->where('user_type','=',4)
                                     ->get();
                                     ?> 
             <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('org_id') ? ' has-error' : ''); ?>">
          <label for="org_id" class="control-label"> Organiser*</label>
          <select class="form-control" name="org_id">
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->org_id ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->first_name); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
  
          </div>
          </div>
          </div>
            <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('event_id2') ? ' has-error' : ''); ?>">
          <label for="event_id2" class="control-label"> Event B*</label>
          <select class="form-control" name="event_id2">
             <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->event_id2 ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->event_title); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
          </div>
          </div> 
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('ticket_id2') ? ' has-error' : ''); ?>">
          <label for="ticket_id2" class="control-label">Ticket *</label>
          <select class="form-control" name="ticket_id2" id="ticket_sec2">
             <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <option value="<?php echo e($item->id); ?>" <?php echo e(($ticketcollabration->ticket_id2 ==  $item->id ) ? 'selected' : ''); ?>><?php echo e($item->ticket_type); ?></option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
         
          </div>
          </div>
          </div>
          </div>

          <div class="row topcls">
          <div class="col-md-12">
          <div class="col-md-6">
             <div class="form-group<?php echo e($errors->has('discount_type') ? ' has-error' : ''); ?>">
               <label for="discount_type" class="col-md-4 col-form-label text-md-right"> Type of Discount </label>
           <select class ="form-control" id="discount_type" name="discount_type" value="<?php echo e($ticketcollabration->discount_type); ?>">
          <option> Percentage</option>
          <option> Fixed</option>

          </select>
          </div>
          </div> 
           <div class="col-md-6">
              <div class="form-group<?php echo e($errors->has('discount') ? ' has-error' : ''); ?>">
          <label for="discount" class="control-label">Discount*</label>
        <input type="number" class="form-control" id="discount" name="discount" value="<?php echo e($ticketcollabration->discount); ?>" maxlength="10" required>
          <?php if($errors->has('discount')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('discount')); ?></strong>
          </span>
          <?php endif; ?>
          </div>

          </div>
          </div>
          </div>
          <div class="row">
          <div class="col-md-12">
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('start_date') ? ' has-error' : ''); ?>">
          <label for="start_date" class="control-label"> Start Date*</label>
           <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php if($ticketcollabration->start_date!='' || $ticketcollabration->start_date!='0000-00-00'  || $discount->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($ticketcollabration->start_date)->format('d-m-Y')); ?><?php } ?>" required >
          <?php if($errors->has('start_date')): ?>
          <span class="help-block">
          <strong><?php echo e($errors->first('start_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('end_date') ? ' has-error' : ''); ?>">
          <label for="end_date" class="control-label"> End Date *</label>
          <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php if($ticketcollabration->end_date!='' || $ticketcollabration->end_date!='0000-00-00'  || $ticketcollabration->end_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($ticketcollabration->end_date)->format('d-m-Y')); ?><?php } ?>" required >
          <?php if($errors->has('end_date')): ?>
          <span class="help-block">
              <strong><?php echo e($errors->first('end_date')); ?></strong>
          </span>
          <?php endif; ?>
          </div>
          </div>
          </div>
          </div>
          <div class="modal-footer">
          <button type="submit" class="btn btn-primary mr-2">Save changes</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
          </form>
          </div>
      </div>
      
    </div>
  </div>
</div>
                    <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/destroy/<?php echo e($ticketcollabration->id); ?>">
                     
                        <button type="submit"><i class="fa fa-trash"></i></button>
                     </form>
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

            </tbody>
          </table>
        </div>
       </div>
     </div>

       <!-----------------End first discound ------------------>

 <!------ End second form for discount------------------------------->
 </div>
    </div>
     </div>
   </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<script>
 $(document).ready( function() {
    $(".event_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true
        });

        $(".start_date").datepicker({
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });


    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>
  <script type="text/javascript">
    $(document).ready(function() {
    $('select[name="event_id"]').on('change', function() {
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                  
                $('select[name="ticket_id"]').empty();

                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="ticket_id"]').append('<option value="'+ value["id"] +'">'+ value["ticket_type"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="ticket_id"]').empty();
              }
           });
        });
    </script>

    <script type="text/javascript">
    $(document).ready(function() {
    $('#ticket_sec2').hide();
    $('select[name="org_id"]').on('change', function() {
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration_org/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                  
                $('select[name="event_id2"]').empty();

                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="event_id2"]').append('<option value="'+ value["id"] +'">'+ value["event_title"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="event_id2"]').empty();
              }
           });
        });
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
    $('select[name="event_id2"]').on('change', function() {
         $('#ticket_sec2').show();
        var countryID = $(this).val();
            if(countryID) {
            $.ajax({
                url: 'collabration/'+encodeURI(countryID),
                type: "GET",
                dataType: "json",
                success:function(data) {
                  
                $('select[name="ticket_id2"]').empty();

                $.each(data, function(key, value) {
                  console.log(value);
                    $('select[name="ticket_id2"]').append('<option value="'+ value["id"] +'">'+ value["ticket_type"] +'</option>');
                    });
                }
            });
            }else{
            $('select[name="ticket_id"]').empty();
              }
           });
        });
    </script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>