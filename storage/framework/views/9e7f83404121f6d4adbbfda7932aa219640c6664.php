<?php use \App\Http\Controllers\CartingController;?>
<li class="c_head"><label>My cart</label></li>
 <li class="item-list item-head">
                <div class="item-name">
                <label>Ticket / Event</label>
             
                </div>
                 <div class="item-quantity">
                   <label>Quantity</label>
                 </div>
                 <div class="item-price">
                   <label>Price</label>
                   
                   </div>
                   <div class="item-action">
                   <label>Action</label>
                   </div>
                  </li>
                  
                  <?php 
                  $EventSubTotal=0;?>

                  <?php $__currentLoopData = $_discountobjlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>         

              <!--  //item-list -->
          <li class="item-list ">
                <div class="item-name">
                
<!--   <span class="e-media"><img src="images/e2.jpg"></span>-->             
    <span class="t-type">
                  <span class="t-name"><?php echo e($cartItem->TicketTitle." tickets for ".$cartItem->Event_Name->event_title); ?></span>
                </span>
                </div>
                 <div class="item-quantity">
                  
                   <span class="t-qty">
                    <div class="input-group">
              <span class="form-control input-number" ><?php echo e($cartItem->Quantity); ?></span>

              </div>
               </span>
                </div>
                 <div class="item-price">
                 
                   <span class="t-price"><i class="fa fa-gbp"></i><?php echo e($cartItem->subtotal_amount); ?></span>
                   </div>
                     <div class="item-action">
                 
                 
                    <form action="<?php echo e(route('cart.destroy',$cartItem->rowId)); ?>"  method="POST">
                           <?php echo e(csrf_field()); ?>

                           <?php echo e(method_field('DELETE')); ?>

                            <button class="btn btn-primary" type="submit" value="Delete"><i class="fa fa-trash-o"></i></button>
                         </form>
                     
                   </div>
                  </li>
              <?php
              $EventSubTotal=$EventSubTotal+$cartItem->subtotal_amount;
            

              ?>
              <!--  //item-list -->
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                   <!--  item-list -->
              <li class="item-list">
                <span class="total-price">
                  Total Price: <i class="fa fa-gbp"></i> <?php echo e($EventSubTotal); ?>



                  </span>
              </li>
                  <!--  //item-list -->
                   <!--  item-list -->
                 <li class="item-checkout">
               <a href="<?php echo e(url('/cart')); ?>" class="btn btn-primary">
                CHECKOUT
               </a>
                </li>
                 <!--  //item-list -->


