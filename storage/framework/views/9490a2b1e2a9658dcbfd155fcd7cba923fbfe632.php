<?php $__env->startSection('content'); ?>

<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
        <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="col-sm-4 maintit">
        <h3>Mail Listing</h3>
      </div>
      <div class="col-sm-8 main">
          <div class="col-sm-9 searchbar">
        <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="<?php echo e(route('admincontactus.index')); ?>">

       <div class="input-group input-group-sm"">
                  <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="<?php echo e($search_data); ?>" placeholder="Search here">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
    </form>
    </div>

      </div>
      <div class="col-sm-12 tablediv spacertop">
<div class="tablewrapper">
<table class="table-bordered table-striped">
        <thead>
          <tr class="headings">
            <th class="column3">Name</th>
            <th class="column3">Email</th>
            <th class="column10">Message</th>
            <th class="column3">Action</th>
            </tr>
        </thead>
        <tbody>
          <?php if(count($contactus)>0): ?>
              <?php $__currentLoopData = $contactus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <tr class="tabledata classtable">
           
          <td class="column3"><?php echo e($contact->name); ?></td>
           <td class="column3"><?php echo e($contact->email); ?></td>
            <td class="column10"><?php echo e($contact->message); ?></td>
             <td class="column3"> 
                    
                     <?php echo Form::open(['method' => 'DELETE','route' => ['admincontactus.destroy', $contact->id],'style'=>'display:inline']); ?>

                     <?php echo Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']); ?>

                       <?php echo Form::close(); ?>

                    </td>
          </tr>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
          <tr class="tablesection">
            <td colspan="7" style="text-align: center;"><b>No record found</b></td>
          </tr>
        <?php endif; ?>                               
        </tbody>
      </table>
          
          
</div>
 <?php if(count($contactus) > 0): ?>
<div class="pagination_section">
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="<?php echo e(url('/admincontactus')); ?>" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" <?php if($perpage=='10'): ?><?php echo e('selected'); ?><?php endif; ?>>10</option>
      <option value="20" <?php if($perpage=='20'): ?><?php echo e('selected'); ?><?php endif; ?>>20</option>
      <option value="50" <?php if($perpage=='50'): ?><?php echo e('selected'); ?><?php endif; ?>>50</option>
      <option value="100" <?php if($perpage=='100'): ?><?php echo e('selected'); ?><?php endif; ?>>100</option>
    </select>
    </form>
  </div>
<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

  <?php echo $contactus->render(); ?>

</nav>

  </div>
  
  

</div>
<?php endif; ?>

 <!--  -->
</div>

      </div>
            </div>
<!--booking-sec-end-->    
<!--booking-sec-end-->
<script src="<?php echo e(asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')); ?>"></script>
  <script>
      CKEDITOR.replace( 'pc_requirement');
      CKEDITOR.replace( 'pc_learn' );
      CKEDITOR.replace( 'pc_description');
  </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
  .form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}
.maintit{
    padding-top: 6px;
  }

</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>