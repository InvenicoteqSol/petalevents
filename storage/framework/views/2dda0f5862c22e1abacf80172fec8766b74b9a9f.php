<?php $__env->startSection('content'); ?>
  <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
          <div class="col-sm-7 table-search searchbar">
            <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="">
             <span class="bmd-form-group"><div class="input-group input-group-sm" "="">
             <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="">
             <div class="input-group-btn">
           <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
          </div>
        </div></span>
     </form>
    </div>
    <div class="col-sm-5 selectdiv topbar_selectiv text-right createbtn">
    <a class="btn btn-primary" href="<?php echo e(route('eventbookings.create')); ?>"><i class="fa fa-pencil" aria-hidden="true" ></i> Create New Event</a>
  </div>
  </div>
  <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Event Listing</h4>
                  
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                   <table class="table">
    
               
                <thead class=" text-primary">

                    <?php if(Auth::user()->user_type=='1'): ?>
                    
                  <tr>
                        <th class="column3">Event date</th>
                        <th class="column3">Event title</th>
                        <th class="column3">Type of event</th>
                        
                        <th class="column3">Action</th>
                        
                        
                    </tr>
                    <?php else: ?>
                     <tr>
                        <th class="column3">Event date</th>
                        <th class="column3">Event title</th>
                        
                        <th class="column3">Type of event</th>
                        
                        <th class="column3">Action</th>
                     </tr>

                    <?php endif; ?>

                </thead>
                <tbody>
                    
                     <?php if(Auth::user()->user_type=='1'): ?>
                     <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eventbooking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">

                        <td class="column3"><?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->start_date)->format('d M, Y')); ?><?php } ?></td>

                        <td class="column3"><?php echo e($eventbooking->event_title); ?></td>
                         <td class="column3"><?php echo e($eventbooking->type_of_event); ?></td>

                        <td>

                        <div class="dropdown d-action">
                          <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                               
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">

                              <label> Ticket</label>

                         <a class="dropdown-item" href="<?php echo e(url('ticket/create/'.$eventbooking->id)); ?>" title="Create Ticket" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i> Create Ticket </a>
                         <a class="dropdown-item" href="<?php echo e(url('ticket/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i> View </a>

                         <label>Event</label>
                           <a class="dropdown-item" href="<?php echo e(route('eventbookings.edit',$eventbooking->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i>Edit</a>
                            <a class="dropdown-item" href="<?php echo e(route('eventbookings.show',$eventbooking->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>Show</a>

                    <!--  <?php echo Form::open(['method' => 'DELETE','route' => ['eventbookings.destroy', $eventbooking->id],'style'=>'display:inline']); ?>

                     <?php echo Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']); ?>


                       <?php echo Form::close(); ?> -->
                     <a class="dropdown-item">
                      <form name="request" id="r


                      equest" class="request" method="GET" action="<?php echo e(url('/')); ?>/destroyevent/<?php echo e($eventbooking->id); ?>">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button></form>
                       </a>

                        <label>Discount</label>

                       <a class="dropdown-item" href="<?php echo e(url('discounts/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>Discount</a>
</div>
</div>

                     </td>
            <td class="column action_btns"> 
                       </td>
                       <td class="column"> 
                     </td>
                     
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                     <?php elseif(Auth::user()->user_type=='4'): ?>
                      <?php 
                     $user_id = Auth::user()->id;
                     $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->where('user_id','=',$user_id)
                               ->get();
                              
                               ?>
                     <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eventbooking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    

                    <tr class="tabledata odd">

                        <td class="column3"><?php if($eventbooking->start_date!='' || $eventbooking->start_date!='0000-00-00'  || $eventbooking->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->start_date)->format('d M, Y')); ?><?php } ?></td>

                        <td class="column2"><?php echo e($eventbooking->event_title); ?></td>

                         <td class="column3"><?php echo e($eventbooking->type_of_event); ?></td>

                         <td class="column3 action_btns"> 
<div class="modal fade linkmodel" id="linkmodal_<?php echo e($eventbooking->id); ?>" tabindex="-1" role="dialog" aria-labelledby="linkmodal_<?php echo e($eventbooking->id); ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        </div>
      <div class="modal-body">
  <div class="login_bg form_bg">
  <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/requestdata">
    <?php echo e(csrf_field()); ?>

  <h1>Thankyou for requesting</h1>
  <input type="hidden" class="form-control" name="event_id" value="<?php echo e($eventbooking->id); ?>">
  <input type="hidden" class="form-control" name="org_id" value="<?php echo e($eventbooking->user_id); ?>">
  <div  class="submitbtn">
    <button type="submit" class="close_btn" value="submit">Close</button>
  </div>
</form>
    </div>
      </div>
    </div>
</div>
</div>

                     <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                             </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                          <label> Ticket </label>
                          <a class="tablebtn dropdown-item" href="<?php echo e(url('ticket/create/'.$eventbooking->id)); ?>" title="Create Ticket" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ticket"></i>Create</a>
                           <a class="tablebtn dropdown-item" href="<?php echo e(url('ticket/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>View</a>
                            <label> Customer data </label>
                                  <?php  $customerrequestmapings = DB::table('customerrequestmapings')
                                                  ->where('customer_id',$eventbooking->user_id)
                                                  ->where('status','=',1)->first();?>
                         <?php if($customerrequestmapings): ?>
                         <a class="tablebtn dropdown-item" href="<?php echo e(url('customerdata/index/'.$eventbooking->id)); ?>" title="customerdata" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>View</a>
                         <?php else: ?>
                         <!-- <td class="column3"><p>-------</p> </td> -->
                         <?php endif; ?>

                       <label> Event </label>
                      <a class="tablebtn dropdown-item" data-toggle="modal" data-target="#linkmodal_<?php echo e($eventbooking->id); ?>" data-whatever="@mdo" style="cursor: pointer;" title="Send Request"><strong>Link</strong></a>
                      <a class="tablebtn dropdown-item" href="<?php echo e(route('eventbookings.edit',$eventbooking->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i>Edit</a>
                      <a class="tablebtn dropdown-item" href="<?php echo e(route('eventbookings.show',$eventbooking->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>View</a>
                       <a class="tablebtn dropdown-item">
                      <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/destroyevent/<?php echo e($eventbooking->id); ?>">
                       <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button></form>
                        </a>
                   
                     <label>Discount</label>

                       <a class="dropdown-item" href="<?php echo e(url('discounts/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>Discount</a>
                     </div>
                   </div>
                      
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>


        </tbody>
    
      </table>

</div>
</div>
</div>
</div>
</div>

                
  <?php if(count($eventbookings) > 0): ?>
<div class="pagination_section">
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="<?php echo e(url('/eventbookings')); ?>" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" <?php if($perpage=='10'): ?><?php echo e('selected'); ?><?php endif; ?>>10</option>
      <option value="20" <?php if($perpage=='20'): ?><?php echo e('selected'); ?><?php endif; ?>>20</option>
      <option value="50" <?php if($perpage=='50'): ?><?php echo e('selected'); ?><?php endif; ?>>50</option>
      <option value="100" <?php if($perpage=='100'): ?><?php echo e('selected'); ?><?php endif; ?>>100</option>
    </select>
    </form>
  </div>
<div class="col-sm-4 total_div">
 
</div>


</div>
<?php endif; ?>
</div>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.searchbar form {
    width: auto !important;
    padding: 0px;
}

.form_bg {
    border: 1px solid #efefef;
    float: left;
    padding: 30px 120px;
    width: 600px !important;
}
.close_btn {
    background: #9124a3;
    color: #fff;
    border-color: #9124a3;
    border: none;
    padding: 8px 32px;
    font-size: 18px;
}

.login_bg.form_bg {
    text-align: center;
}
</style>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>