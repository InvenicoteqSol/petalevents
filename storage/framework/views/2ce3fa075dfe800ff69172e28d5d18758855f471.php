<?php $__env->startSection('content'); ?>
<section class="section-upcoming-events">
    <div class="container">
      <div class="row">
        <div class="col-md-12 cil-sm-12 col-xs-12">
          <h3 class="section-heading">Featured Events</h3>
    </div>
   </div>
    <!-- row -->
     <!-- row -->
    <div class="row">

     <div class="col-md-12 col-sm-12 col-xs-12">
      <div id="carousel-featured-event" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
  	 <?php for($i=0;$i<=count($events);$i++): ?>
  		<?php if($i==0): ?>
  		
  			<li data-target="#carousel-featured-event" data-slide-to="$i" class="active"></li>

  		
  		<?php else: ?>
    		<li data-target="#carousel-featured-event" data-slide-to="$i" class=""></li>
    		<?php endif; ?>
    		<?php endfor; ?>
		

	
    
    
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  	<?php $counter=0;?>
  	<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eventItems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  	<?php if($counter==0): ?>
    <div class="item active">
    	<?php else: ?>
    	<div class="item">
    	<?php endif; ?>
       <!-- card-event -->
      <div class="col-md-12 col-sm-12 col-xs-12 ">
      <div class="card-event single-event">
        <div class="card-image">
       
         <?php if($eventItems->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
            <div class="category-overlay">
          <?php echo e($eventItems->type_of_event); ?>

        </div>
       <span class="card-title"><?php echo e($eventItems->event_title); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
             <div class="col-action">
             	<?php              	
             	$eventdate= date('F-j-Y',strtotime($eventItems->start_date));
             	$datesplit=explode('-', $eventdate);
             	$month=$datesplit[0];
             	$date=$datesplit[1];
             	$year=$datesplit[2];
             	
             	?>
         <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
         <span>
           <div class="btn-act">
      <a href="<?php echo e(url('event-details')); ?>/<?php echo e($eventItems->event_title); ?>" class="btn btn-primary">Buy Tickets</a>
    </div>
         </span>
       </div>
     <span class="e-loc">
      
      <span class="l-con"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
      <span class="l-name"><label><?php echo e($eventItems->venue_name); ?></label><?php echo e($eventItems->venue_address); ?></span>
    </span>
       </div>
       </div>
    
     </div>
    <!-- ///event-detail -->

    </div>
   <!-- // card-body -->
        </div>
      </div>
       <!-- END card-event -->
    </div>
    <?php $counter=$counter+1;?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <!--   item -->
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-featured-event" role="button" data-slide="prev">
    <span class="fa fa-chevron-circle-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-featured-event" role="button" data-slide="next">
    <span class="fa fa-chevron-circle-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
     </div>

   </div>
    <!-- row -->
   </div>
 </section>
 <!--  realted-events -->
    <section id="realted-events">
      <div class="container">
      <!--   row -->
    <div class="row">
        <div class="col-md-12 cil-sm-12 col-xs-12">
          <h3 class="section-heading">Pending Events</h3>
    </div>
   </div>
   <!-- row -->
       <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="owl-carousel owl-theme">
          	<?php $__currentLoopData = $featuredEventshappening; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $featuredItems): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="item">
            <!--   card-event -->
    <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
          <?php echo e($featuredItems->type_of_event); ?>

        </div>
        <a href="<?php echo e(url('event-details')); ?>/<?php echo e($featuredItems->event_title); ?>">
         <?php if($featuredItems->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($featuredItems->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title"><?php echo e($featuredItems->type_of_event); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc"><?php echo e($featuredItems->venue_name); ?></span>
       <span class="e-date-time"><?php echo e(date('F j, Y',strtotime($featuredItems->start_date))); ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="<?php echo e(url('event-details')); ?>/<?php echo e($featuredItems->event_title); ?>" class="btn btn-primary">Buy Tickets</a>
    </div>
     </div>
    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <?php 
       $ticketLowest=DB::table('tickets')->where('event_id', '=',$featuredItems->id)->orderBy('prices')->first();
       $countcheck=count($ticketLowest);
       $priceval=0;
       if($countcheck>0)
       {
         $priceval=$ticketLowest->prices;
       }
       ?>
       <i class="fa fa-gbp" aria-hidden="true"></i><?php echo e($priceval); ?>

       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
 </div>
          <!--  end card-event -->
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           
        
            </div>
            </div>
            </div>
            </div>
      
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: false
                  },
                  1000: {
                    items: 5,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              });
            } );
          </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>