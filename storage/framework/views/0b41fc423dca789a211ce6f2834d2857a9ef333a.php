       <?php
    if(Auth::user()->photography_role!='' && Auth::user()->photography_role!='NULL'){
        $photographerroles = explode(',', Auth::user()->photography_role); 
    } else { 
        $photographerroles = array();
    }

  ?>
    <div class="sidebar" data-color="purple" data-background-color="black" data-image="<?php echo e(url('/public')); ?>/assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="<?php echo e(url('')); ?>" class="simple-text logo-normal">
          <img class="imglogo" src="<?php echo e(url('/public')); ?>/front/images/logo.png">
        </a>
      </div>
      
      <div class="sidebar-wrapper">
        <?php if(Auth::user()->user_type=='1'): ?>
        <ul class="nav">
          <li class="nav-item" id="dash">
            <a class="nav-link" href="<?php echo e(url('/admindashboard')); ?>"><i class="material-icons">dashboard</i><p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item " id="settings">
            <a class="nav-link" href="<?php echo e(url('/adminbusinesssetting')); ?>">
              <i class="material-icons ">settings</i>
              <p>Settings</p>
            </a>
          </li>
          <li class="nav-item " id="profile">
            <a class="nav-link" href="<?php echo e(url('/adminprofiles')); ?>">
              <i class="material-icons">perm_identity</i>
              <p>Profile</p>
            </a>
          </li>
          <li class="nav-item " id="organiser">
            <a class="nav-link" href="<?php echo e(url('/adminorganiser')); ?>">
              <i class="fa fa-sitemap"></i>
              <p>Organisers</p>
            </a>
          </li>
          <li class="nav-item" id="customer">
            <a class="nav-link" href="<?php echo e(url('/admincustomer')); ?>">
              <i class="material-icons">bubble_chart</i>
              <p>Customers</p>
            </a>
          </li>
          <li class="nav-item " id="events">
            <a class="nav-link" href="<?php echo e(url('/eventbookings')); ?>">
              <i class="material-icons">location_ons</i>
              <p>Events</p>
            </a>
          </li>
          <li class="nav-item " id="orders">
            <a class="nav-link" href="<?php echo e(url('/orders')); ?>">
              <i class="material-icons">notifications</i>
              <p>Orders</p>
            </a>
          </li>
             <li class="nav-item " id="collabration">
            <a class="nav-link" href="<?php echo e(url('/ticketcollabration')); ?>">
              <i class="material-icons">Ticket </i>
              <p>Ticket Collabration</p>
            </a>
          </li>
           <li class="nav-item " id="contactus">
            <a class="nav-link" href="<?php echo e(url('/admincontactus')); ?>">
              <i class="material-icons">notifications</i>
              <p>Enquiries</p>
            </a>
          </li>
           <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
          <?php echo e(csrf_field()); ?>

          </form>
          </li>
          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
         <?php endif; ?>
        
       <?php if(Auth::user()->user_type=='3'): ?>

        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="<?php echo e(url('/customerdashboard')); ?>"><i class="fa fa-user"></i><p>Profile</p>
            </a>
          </li> 
            <li class="nav-item ">
            <a class="nav-link" href="<?php echo e(url('/orders')); ?>">
              <i class="material-icons">notifications</i>
              <p>My Orders</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
          <?php echo e(csrf_field()); ?>

          </form>
          </li>
           </ul>
           
      <?php endif; ?>

      <?php if(Auth::user()->user_type=='4'): ?>
        <ul class="nav">
          <li class="nav-item active  ">
            <a class="nav-link" href="<?php echo e(url('/organiserdashboard')); ?>"><i class="fa fa-user"></i><p>Profile</p>
            </a>
          </li> 
           <li class="nav-item ">
            <a class="nav-link" href="<?php echo e(url('/eventbookings')); ?>"><i class="fa fa-calendar"></i>
              <p>My Events</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo e(url('/ticketcollabration')); ?>">
              <i class="material-icons">Ticket </i>
              <p>Ticket Collabration</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?php echo e(url('/orders')); ?>">
              <i class="material-icons">notifications</i>
              <p>My Orders</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"> </i> <p>Logout</p> </a>
          <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
          <?php echo e(csrf_field()); ?>

          </form>
          </li>
           </ul>
            
      <?php endif; ?>

      </div>
    </div>
     
     
  