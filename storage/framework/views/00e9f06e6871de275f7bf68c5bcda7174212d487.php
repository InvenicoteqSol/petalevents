<?php $__env->startSection('content'); ?>
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
    
   <div class="col-sm-10 dashboard_right">
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="col-sm-12 nopadding">
  <div class="row">
        





  <div class="col-sm-8 main">
    
          <div class="col-sm-9 searchbar">
            <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="<?php echo e(route('eventbookings.index')); ?>">
        <!-- <div class="form-group">
          <label>Search</label>
          <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="<?php echo e($search_data); ?>" placeholder="Search here">
        </div> -->

          <div class="input-group input-group-sm"">
                  <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="<?php echo e($search_data); ?>" placeholder="Search here">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>

      </form>
    </div>
    <div class="col-sm-6 selectdiv topbar_selectiv text-right createbtn">
         <a class="editbtn" href="<?php echo e(route('eventbookings.create')); ?>"> Create New Event </a>
  </div>
  </div>
</div>
      </div>

       


        <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Event Listing</h4>
                  
                </div>

                <div class="card-body">
                 <table class="table table-hover">
    
               
                <thead>

                    <?php if(Auth::user()->user_type=='1'): ?>
                  <tr class="text-warning">
                        <th class="column3">Event date</th>
                        <th class="column3">Event title</th>
                        <th class="column3">Supporting art</th>
                        <th class="column3">Type of event</th>
                        <th class="column3">Ticket</th>
                        <th class="column3">Action</th>
                        <th class="column2">Discount </th>
                        
                    </tr>
                    <?php else: ?>
                     <tr class="text-warning">
                        <th class="column3">Event date</th>
                        <th class="column2">Event title</th>
                        <th class="column3">Supporting art</th>
                        <th class="column3">Type of event</th>
                        <th class="column2">Ticket</th>
                        <th class="column3">Customer data / Pay</th>
                        <th class="column3">Action</th>
                     </tr>

                    <?php endif; ?>

                </thead>
                <tbody>
                    
                     <?php if(Auth::user()->user_type=='1'): ?>
                     <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eventbooking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">

                        <td class="column3"><?php if($eventbooking->event_date!='' || $eventbooking->event_date!='0000-00-00'  || $eventbooking->event_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->event_date)->format('d M, Y')); ?><?php } ?></td>

                        <td class="column3"><?php echo e($eventbooking->event_title); ?></td>

                        <td class="column3">
                          <?php echo e($eventbooking->supporting_art); ?>


                        </td>
                       
                        <td class="column3"><?php echo e($eventbooking->type_of_event); ?></td>

                        <td class="column3"> <a class="tablebtn" href="<?php echo e(url('ticket/create/'.$eventbooking->id)); ?>" title="Create Ticket" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ticket"></i></a>
                         <a class="tablebtn" href="<?php echo e(url('ticket/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a></td>
                       
                    <td class="column3 action_btns"> 
                     <a class="tablebtn" href="<?php echo e(route('eventbookings.edit',$eventbooking->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i></a>
                     <a class="tablebtn" href="<?php echo e(route('eventbookings.show',$eventbooking->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                     <?php echo Form::open(['method' => 'DELETE','route' => ['eventbookings.destroy', $eventbooking->id],'style'=>'display:inline']); ?>

                     <?php echo Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']); ?>


                       <?php echo Form::close(); ?>

                    </td>
                    <td class="column2"> 
                     <a class="tablebtn" href="<?php echo e(url('discounts/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a></td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    
                     <?php elseif(Auth::user()->user_type=='4'): ?>
                      <?php 
                     $user_id = Auth::user()->id;
                     $eventbookings = DB::table('eventbookings')
                               ->where('deleted', 0)
                               ->where('user_id','=',$user_id)
                               ->get();
                              
                               ?>
                     <?php $__currentLoopData = $eventbookings; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eventbooking): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    

                    <tr class="tabledata odd">

                        <td class="column3"><?php if($eventbooking->event_date!='' || $eventbooking->event_date!='0000-00-00'  || $eventbooking->event_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->event_date)->format('d M, Y')); ?><?php } ?></td>

                        <td class="column2"><?php echo e($eventbooking->event_title); ?></td>

                        <td class="column3">
                          <?php echo e($eventbooking->supporting_art); ?>


                        </td>
                       
                        <td class="column3"><?php echo e($eventbooking->type_of_event); ?></td>

                        <td class="column2"> <a class="tablebtn" href="<?php echo e(url('ticket/create/'.$eventbooking->id)); ?>" title="Create Ticket" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ticket"></i></a>
                         <a class="tablebtn" href="<?php echo e(url('ticket/index/'.$eventbooking->id)); ?>" title="View" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a></td>
                         <?php  

                        $customerrequestmapings = DB::table('customerrequestmapings')
                                                  ->where('customer_id',$eventbooking->user_id)
                                                  ->where('status','=',1)->first();
                          ?>
                         <?php if($customerrequestmapings): ?>
                         <td class="column3"> 
                         <a class="tablebtn" href="<?php echo e(url('customerdata/index/'.$eventbooking->id)); ?>" title="customerdata" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a></td>
                         <?php else: ?>
                         <td class="column3"><p>-------</p> </td>

                         <?php endif; ?>
                    <td class="column3 action_btns"> 
                     <a data-toggle="modal" data-target="#linkmodal_<?php echo e($eventbooking->id); ?>" data-whatever="@mdo" style="cursor: pointer;" title="Send Request"><strong>Link</strong></a>

<div class="modal fade linkmodal" id="linkmodal_<?php echo e($eventbooking->id); ?>" tabindex="-1" role="dialog" aria-labelledby="linkmodal_<?php echo e($eventbooking->id); ?>">
    <div class="modal-content">
      <div class="modal-body">
  <div class="login_bg form_bg">
  <form name="request" id="request" class="request" method="GET" action="http://13.56.193.9/event/requestdata">
    <?php echo e(csrf_field()); ?>

  <h1>Thankyou for requesting</h1>
  <input type="hidden" class="form-control" name="event_id" value="<?php echo e($eventbooking->id); ?>">
  <input type="hidden" class="form-control" name="org_id" value="<?php echo e($eventbooking->user_id); ?>">
  <div class="submitbtn">
  <button type="submit" class="close_btn"  value="Submit">Close</button>
 </div>
</form>
    </div>
      </div>
    </div>
</div>
  <a class="tablebtn" href="<?php echo e(route('eventbookings.edit',$eventbooking->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i></a>
                     <a class="tablebtn" href="<?php echo e(route('eventbookings.show',$eventbooking->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
               
                   <!--   <a class="tablebtn" href="<?php echo e(url('ticket/edit/'.$eventbooking->id)); ?>" title="Ticket" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-ticket"></i></a> -->
                    

                     <?php echo Form::open(['method' => 'DELETE','route' => ['eventbookings.destroy', $eventbooking->id],'style'=>'display:inline']); ?>

                     <?php echo Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']); ?>


                       <?php echo Form::close(); ?>

                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;padding: 15px;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
        
      </table>


  <?php if(count($eventbookings) > 0): ?>
<div class="pagination_section">
  <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="<?php echo e(url('/eventbookings')); ?>" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" <?php if($perpage=='10'): ?><?php echo e('selected'); ?><?php endif; ?>>10</option>
      <option value="20" <?php if($perpage=='20'): ?><?php echo e('selected'); ?><?php endif; ?>>20</option>
      <option value="50" <?php if($perpage=='50'): ?><?php echo e('selected'); ?><?php endif; ?>>50</option>
      <option value="100" <?php if($perpage=='100'): ?><?php echo e('selected'); ?><?php endif; ?>>100</option>
    </select>
    </form>
  </div>
<div class="col-sm-4 total_div">
 
</div>


</div>
<?php endif; ?>
      </div>

     </div>
    </div>
     </div>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}
.form_bg {
    border: 1px solid #efefef;
    float: left;
    padding: 30px 120px;
    width: 600px !important;
}

.login_bg.form_bg {
    margin-right: 30%;
    margin-left: 30%;
    margin-top: 5%;
}
.login_bg.form_bg {
    text-align: center;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>