<?php $__env->startSection('content'); ?>

<section class="evnt-slide">
  <div class="container">
  
     <!-- row -->
  <div class="row">
  <!-- slider -->
  <?php $counter=0;?>
    <?php $__currentLoopData = $Ticketdeals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ticketdeal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($counter==0): ?>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <h3 class="section-heading">Ticktes</h3>
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
    
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <!--    item -->
 <div class="item active">
  <div class="row">
   <?php  $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();
    ?>
    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
         <div class="category-overlay">
         <?php echo e($event->type_of_event); ?>


        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title"><?php echo e($event->event_title); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
       <?php                
              $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
              $datesplit=explode('-', $eventdate);
              $month=$datesplit[0];
              $date=$datesplit[1];
              $year=$datesplit[2];
              
              ?>
              <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="#" class="btn btn-primary">Buy Tickets</a>
    </div>
     </div>
    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <i class="fa fa-usd" aria-hidden="true"></i> 
       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
 </div>
        </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php  $event_id2=$Ticketdeal->event_id2;
    $events=DB::table('eventbookings')->where('id','=',$event_id2)->get();?>
    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
          <div class="category-overlay">
        <?php echo e($event->type_of_event); ?>

        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
          <span class="card-title"><?php echo e($event->event_title); ?></span>
          <div class="date-and-place">
          <div class="venue-details">
          <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
       <?php                
              $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
              $datesplit=explode('-', $eventdate);
              $month=$datesplit[0];
              $date=$datesplit[1];
              $year=$datesplit[2];
              
              ?>
              <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="#" class="btn btn-primary">Buy Tickets</a>
       </div>
       </div>
    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <i class="fa fa-usd" aria-hidden="true"></i> 
       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
 </div>
        </div>
      </div>
  <!--     row -->
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <!--item End -->

 </div></div> </div>
  <?php else: ?>

 <?php endif; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>
 </div>
     <!--  endslider -->
      <!-- slider -->
</section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: false
                  },
                  1000: {
                    items: 5,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              });
            } );
          </script>
          <style type="text/css">
            h3.section-heading {
                  margin-top: 50px;
              }
         </style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>