<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
        <div class="line_div">
            <h3 class="com_busin_sec">New Event</h3>
        </div>
   
    <div class="row">
        <div class="col-md-12">
            <?php echo Form::open(array('route' => 'eventbookings.store', 'method'=>'POST', 'id' => 'edtbookingfrm',  'class' => 'edtbookingfrm formarea', 'files' => true)); ?>

  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('event_title') ? ' has-error' : ''); ?>">
            <label for="event_title" class="control-label"> Event Title *</label>
            <input type="text" class="form-control event_title" id="event_title" name="event_title" value="<?php echo e(@old(event_title)); ?>" required>
            <?php if($errors->has('event_title')): ?>
              <span class="help-block">
                <strong><?php echo e($errors->first('event_title')); ?></strong>
              </span>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('supporting_art') ? ' has-error' : ''); ?>">
            <label for="supporting_art" class="control-label"> Supporting Art*</label>
            <input type="text" class="form-control supporting_art" id="supporting_art" name="supporting_art" value="<?php echo e(@old(supporting_art)); ?>" required>
            <?php if($errors->has('supporting_art')): ?>
                <span class="help-block">
                    <strong><?php echo e($errors->first('supporting_art')); ?></strong>
                </span>
            <?php endif; ?>
        </div>
    </div>
  </div>
</div>
<div class="row topcls">
 <div class="col-md-12">
    <div class="col-md-12">
    <div class="form-group<?php echo e($errors->has('event_info') ? ' has-error' : ''); ?>">
    <label for="event_info" class="control-label">Event Info *</label>
         <textarea id="event_info" name="event_info" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e(@old(event_info)); ?></textarea>

        <?php if($errors->has('event_info')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('event_info')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
    </div>
  </div>
</div>
<div class="row topcls">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('type_of_event') ? ' has-error' : ''); ?>">
        <label for="type_of_event" class="col-md-4 col-form-label text-md-right"> Type of Event </label>
                    <select class ="form-control" id="type_of_event" name="type_of_event">
                    <option disabled="disabled" selected="selected"> --- Select --- </option>
                    <option> Arts & Culture</option>
                    <option> Bar & Pub</option>
                    <option> Burlesque </option>
                    <option> Cabaret </option>
                    <option> Celebrity</option>
                    <option> Charity </option>
                    <option> Children </option>
                    <option> Cinema </option>
                    <option> Clubbing </option>
                    <option> Comedy</option>
                    <option> Concert </option>
                    <option> Conference </option>
                    <option>Cosplay </option>
                    <option> Exhibition</option>
                    <option> Festival</option>
                    <option> Gentlemen's Club</option>
                    <option> Hotel</option>
                    <option> Karaoke</option>
                    <option> Latin Dance</option>
                    <option> Live Music</option>
                    <option> Magic</option>
                    <option> Museum</option>
                    <option> Pantomime</option>
                    <option> Poetry</option>
                    <option> Restaurant</option>
                    <option> Speaker</option>
                    <option> Sport</option>
                    <option> Theatre</option>


                    </select>
                      <?php if($errors->has('type_of_event')): ?>
                          <span class="invalid-feedback">
                              <strong><?php echo e($errors->first('type_of_event')); ?></strong>
                          </span>
                      <?php endif; ?>  
    </div>
  </div>
        <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('age_limit') ? ' has-error' : ''); ?>">
        <label for="age_limit" class="col-md-4 col-form-label text-md-right"> Age Limit </label>
                    <select class ="form-control" id="age_limit" name="age_limit">
                    <option disabled="disabled" selected="selected"> --- Select --- </option>
                       <option>  All ages </option>
                       <option> Under 18 </option>
                       <option> 18 and over  </option>                              
                             
                    </select>
                      <?php if($errors->has('age_limit')): ?>
                          <span class="invalid-feedback">
                              <strong><?php echo e($errors->first('age_limit')); ?></strong>
                          </span>
                      <?php endif; ?>     
    </div>
  </div>
</div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('start_date') ? ' has-error' : ''); ?>">
            <label for="start_date" class="control-label"> Start Date*</label>
            <input type="text" class="form-control start_date" id="start_date" name="start_date" value="<?php echo e(@old(start_date)); ?>" required>
            <?php if($errors->has('start_date')): ?>
              <span class="help-block">
                <strong><?php echo e($errors->first('start_date')); ?></strong>
              </span>
            <?php endif; ?>
        </div>
    </div>
<div class="col-md-6">
   <div class="form-group<?php echo e($errors->has('end_date') ? ' has-error' : ''); ?>">
            <label for="end_date" class="control-label"> End Date *</label>
            <input type="text" class="form-control end_date" id="end_date" name="end_date" value="<?php echo e(@old(end_date)); ?>" required>
            <?php if($errors->has('end_date')): ?>
                <span class="help-block">
                    <strong><?php echo e($errors->first('end_date')); ?></strong>
                </span>
            <?php endif; ?>
        </div>
     </div>
  </div>
</div>
 <div class="line_div">
            <h3 class="com_busin_sec">Venue details </h3>
        </div>
<div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group<?php echo e($errors->has('venue_name') ? ' has-error' : ''); ?>">
    <label for="venue_name" class="control-label">Venue Name*</label>
        <input type="text" class="form-control" id="venue_name" name="venue_name" value="<?php echo e(@old(venue_name)); ?>" maxlength="10" required>
        <?php if($errors->has('venue_name')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('venue_name')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
</div> 
    <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('telephone') ? ' has-error' : ''); ?>">
    <label for="telephone" class="control-label">Telephone*</label>
        <input type="number" class="form-control" id="telephone" name="telephone" value="<?php echo e(@old(telephone)); ?>" maxlength="10" required>
        <?php if($errors->has('telephone')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('telephone')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
  </div> 
 </div>
</div>
<div class="row topcls">
 <div class="col-md-12">                         
    <div class="col-md-6 edit_radiobtn">
          <div class="form-group<?php echo e($errors->has('details') ? ' has-error' : ''); ?>">
    <label for="details" class="control-label">Details *</label>
         <textarea id="details" name="details" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e(@old(details)); ?></textarea>

        <?php if($errors->has('details')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('details')); ?></strong>
            </span>
        <?php endif; ?>
    </div>  
 </div>
<div class="col-md-6 edit_radiobtn">
      <div class="form-group<?php echo e($errors->has('venue_address') ? ' has-error' : ''); ?>">
    <label for="venue_address" class="control-label">Venue Address *</label>
         <textarea id="venue_address" name="venue_address" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e(@old(venue_address)); ?></textarea>

        <?php if($errors->has('venue_address')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('venue_address')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
  </div>
 </div>
</div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
                              <label for="website" class="control-label"> Website *</label>
                              <input type="text" class="form-control" id="website" name="website" value="<?php echo e(@old(website)); ?>" required>
                              <?php if($errors->has('website')): ?>
                                <span class="help-block">
                                  <strong><?php echo e($errors->first('website')); ?></strong>
                                </span>
                              <?php endif; ?>
                          </div>
                    </div>
                      <div class="col-md-6">
                              <label for="event_image" >Venue Image</label>
                               <div class="form-group flx_mid" style="float: left;width: 100%;">
                                <div class="col-md-3 nopadding" id="event_image" style="padding-top: 10px; padding-bottom: 20px;">
                                 <img src="<?php echo e(url('public')); ?>/images/dummy.jpg" style="width: 100px; height: 100px;" class="img-responsive">     
                                 </div>
                                 <div class="col-md-3">
                                    <input id="event_image" type="file" name="event_image" accept="image/*">
                                 </div>
                            <div class="prflinput" style="padding-top: 20px;">
                              
                              </div>

                            </div>
                        </div>
                    </div>
                    </div>
                  <div class="col-md-12">
                  <div class=" topcls">
                  <div class="form-group">
                    <div class="buttondiv">
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                    </div>
                  </div>
                  </div>
                  </div>


                     <?php echo e(Form::close()); ?>

        </div>
    </div>
     </div>
    </div>

<script>
 $(document).ready( function() {
  var someDate = new Date();
var numberOfDaysToAdd = 2;
someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
  /*$( ".event_date" ).datepicker({ 
       startDate: someDate,
       format: 'dd-mm-yyyy',
       todayHighlight: true,
       autoclose: true });
*/
  $(".start_date").datepicker({
          startDate: someDate,
          format: 'dd-mm-yyyy',
          todayHighlight: true,
          autoclose: true,
        }).on('changeDate', function (selected) {
            var startDate = new Date(selected.date.valueOf());
            $('.end_date').datepicker('setStartDate', startDate);
        }).on('clearDate', function (selected) {
            $('.end_date').datepicker('setStartDate', null);
        });

        $(".end_date").datepicker({
           format: 'dd-mm-yyyy',
           todayHighlight: true,
           autoclose: true,
        }).on('changeDate', function (selected) {
           var endDate = new Date(selected.date.valueOf());
           $('.start_date').datepicker('setEndDate', endDate);
        }).on('clearDate', function (selected) {
           $('.start_date').datepicker('setEndDate', null);
        });

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                event_date: {
                    required: true
                   },
                event_title: {
                    required: true
                },
                supporting_art: {
                    required: true
                   }
            },
        messages: {
                event_date: {
                required: " * required: You must select Event"
                },
                event_title: {
                  required: "This is required field." 
                },
                supporting_art: {
                  required: "This is required field."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
});


</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>