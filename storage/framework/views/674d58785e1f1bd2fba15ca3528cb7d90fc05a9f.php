 <table>
                   <tbody>
                    <tr class="t-head">
                      <td class="item-name">Item</th>
                      <td class="item-qty">Tickets</th>
                      <td class="item-price">Price</th>
                    </tr>
                   <!--  item list -->
                      <?php $__currentLoopData = $cartItems; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cartItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($cartItem->name); ?></td>
                     <td><?php echo e($cartItem->price); ?></td>
                   
                    <td>
                        <?php echo Form::open(['route' => ['cart.update',$cartItem->rowId], 'method' => 'PUT']); ?>

                        <input name="qty" type="number" value="<?php echo e($cartItem->qty); ?>">
                        <input type="submit" class="button success small" value="Update Quantity">
                        <?php echo Form::close(); ?>

                    </td>
                   <td>
                        <form action="<?php echo e(route('cart.destroy',$cartItem->rowId)); ?>"  method="POST">
                           <?php echo e(csrf_field()); ?>

                           <?php echo e(method_field('DELETE')); ?>

                           <input class="button small alert" type="submit" value="Delete">
                         </form>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             <tr class="col-action-check">
                        <td class="c-space"></td>
                        <td  class="c-act">

                          <span class="total-amt">
                          <label>Total</label>
                          <span class="t-pamt"><i class="fa fa-gbp"></i> <?php echo e($discounted_amount); ?></span>
                        </span>
                          <span><a href="" class="btn btn-primary">Checkout</a></span>
                        </td>
                    </tr>
                   </tbody>
                 </table>