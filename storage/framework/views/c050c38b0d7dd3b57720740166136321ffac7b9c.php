<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
        <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

        
      <div class="col-sm-6 searchform main">
          <div class="col-sm-12 searchbar text-right">
        <form class="navbar-form navbar-left" name="searchfrm" id="searchfrm" method="GET" action="<?php echo e(route('adminorganiser.index')); ?>">
<!-- 
        <div class="form-group">
          <label>Search</label>
          <input type="text" class="form-control" placeholder="Search here" name="search_input" id="search_input" value="<?php echo e($search_data); ?>" placeholder="Search here">
        </div> -->

        <div class="col-sm-6 selectdiv topbar_selectiv text-right createbtn">
               <a class="editbtn" href="<?php echo e(route('adminorganiser.create')); ?>"> Create New Organiser </a>
         </div>
      </form>
    </div>
  
      </div>






       <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Organisers</h4>
                  
                </div>
      
             <div class="card-body table-responsive">
                  <table class="table table-hover">

                <thead>
                    <tr class="text-warning">
                       <th  class="">Image</th>
                        <th class="">Name</th>
                        <th class="">Email</th>
                        <th class="">Phone</th>
                        <th class="">Registered</th>
                        <th class="">Status</th>
                        <th class="">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($organisers)>0): ?>
                     <?php $__currentLoopData = $organisers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $organiser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata">

                      <td class="column2 user_img">
                        <?php if($organiser->profile_picture!=''): ?> 
                        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($organiser->profile_picture); ?>" class="img-responsive" style="width: 40px; height: 30px;">
                         <?php else: ?>
                        <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" style="width: 40px; height: 30px;">
                       <?php endif; ?>
                        
                      </td>
                        
                        <td class=""><?php echo e($organiser->first_name); ?></td>
                       
                        <td class=""><?php echo e($organiser->email); ?></td>
                       
                        <td class=""><?php echo e($organiser->phone); ?></td>
                       
                        <td class=""><?php echo e(\Carbon\Carbon::parse($organiser->created_at)->format('d M Y')); ?></td>
                       
                        <td class="">
                          <?php
                          if($organiser->status==1) {
                            echo 'Active';
                          } else {
                            echo 'Inactive';
                          }
                          ?>
                        </td>

                    <td class="action_btns"> 
                     <a class="tablebtn" href="<?php echo e(route('adminorganiser.edit',$organiser->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit"></i></a>
                     <a class="tablebtn" href="<?php echo e(route('adminorganiser.show',$organiser->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                     <?php echo Form::open(['method' => 'DELETE','route' => ['adminorganiser.destroy', $organiser->id],'style'=>'display:inline']); ?>

                     <?php echo Form::button('<i class="fa fa-trash"></i>', ['class' => 'deletebtn','data-toggle'=>'confirmation']); ?>


                       <?php echo Form::close(); ?>

                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>
    </div>
     <?php if(count($organisers) > 0): ?>
<div class="pagination_section">
   <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="<?php echo e(url('/adminorganiser')); ?>" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" <?php if($perpage=='10'): ?><?php echo e('selected'); ?><?php endif; ?>>10</option>
      <option value="20" <?php if($perpage=='20'): ?><?php echo e('selected'); ?><?php endif; ?>>20</option>
      <option value="50" <?php if($perpage=='50'): ?><?php echo e('selected'); ?><?php endif; ?>>50</option>
      <option value="100" <?php if($perpage=='100'): ?><?php echo e('selected'); ?><?php endif; ?>>100</option>
    </select>
    </form>
  </div>

<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

  <?php echo $organisers->render(); ?>

</nav>

  </div>
  
 
</div>
<?php endif; ?>
      
            <div class="dashboard_footer">
        <div class="col-sm-8 left_dashboardfooter">
          <ul>
            <li>Copyright <span>Mrs.Portrait.</span>All rights reserved</li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Privacy Policy</a></li>
          </ul>
        </div>
        <div class="col-sm-4 right_dashboardfooter text-right">
          <a href="#">Feedback</a>
        </div>
      </div>
      </div>


    </div>

    </div>



                       

            
    

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
  input#search_input {
    height: 32px !important;
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>