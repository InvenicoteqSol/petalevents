<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
            <div class="line_div">
            <h3 class="com_busin_sec">Update Booking</h3>
        </div>
    <div class="row">
        <div class="col-md-12">
            <?php echo e(Form::model($eventbooking, array('route' => array('eventbookings.update', $eventbooking->id), 'id' => 'edtbookingfrm', 'class' => 'edt_booking_frm', 'method' => 'PUT', 'files' => true))); ?>

   <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('event_title') ? ' has-error' : ''); ?>">
            <label for="event_title" class="control-label"> Event title *</label>
            <input type="text" class="form-control event_title" id="event_title" name="event_title" value="<?php echo e($eventbooking->event_title); ?>" required>
            <?php if($errors->has('event_title')): ?>
              <span class="help-block">
                <strong><?php echo e($errors->first('event_title')); ?></strong>
              </span>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('supporting_art') ? ' has-error' : ''); ?>">
            <label for="supporting_art" class="control-label"> Supporting art*</label>
            <input type="text" class="form-control supporting_art" id="supporting_art" name="supporting_art" value="<?php echo e($eventbooking->supporting_art); ?>" required>
            <?php if($errors->has('supporting_art')): ?>
                <span class="help-block">
                    <strong><?php echo e($errors->first('supporting_art')); ?></strong>
                </span>
            <?php endif; ?>
        </div>
    </div>
  </div>
</div>
<div class="row topcls">
 <div class="col-md-12">
    <div class="col-md-12">
    <div class="form-group<?php echo e($errors->has('event_info') ? ' has-error' : ''); ?>">
    <label for="event_info" class="control-label">Event info *</label>
         <textarea id="event_info" name="event_info" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e($eventbooking->event_info); ?></textarea>

        <?php if($errors->has('event_info')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('event_info')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
    </div>
  </div>
</div>
<div class="row topcls">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('type_of_event') ? ' has-error' : ''); ?>">
        <label for="type_of_event" class="col-md-4 col-form-label text-md-right"> Type of event </label>
                    <select class ="form-control" id="type_of_event" name="type_of_event" value="<?php echo e($eventbooking->type_of_event); ?>">
                    <option> Arts & Culture</option>
                    <option> Bar & Pub</option>
                    <option> Burlesque </option>
                    <option> Cabaret </option>
                    <option> Celebrity</option>
                    <option> Charity </option>
                    <option> Children </option>
                    <option> Cinema </option>
                    <option> Clubbing </option>
                    <option> Comedy</option>
                    <option> Concert </option>
                    <option> Conference </option>
                    <option>Cosplay </option>
                    <option> Exhibition</option>
                    <option> Festival</option>
                    <option> Gentlemen's Club</option>
                    <option> Hotel</option>
                    <option> Karaoke</option>
                    <option> Latin Dance</option>
                    <option> Live Music</option>
                    <option> Magic</option>
                    <option> Museum</option>
                    <option> Pantomime</option>
                    <option> Poetry</option>
                    <option> Restaurant</option>
                    <option> Speaker</option>
                    <option> Sport</option>
                    <option> Theatre</option>
                    </select>
                      <?php if($errors->has('type_of_event')): ?>
                          <span class="invalid-feedback">
                              <strong><?php echo e($errors->first('type_of_event')); ?></strong>
                          </span>
                      <?php endif; ?>  
    </div>
  </div>
        <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('age_limit') ? ' has-error' : ''); ?>">
        <label for="age_limit" class="col-md-4 col-form-label text-md-right"> Age Limit </label>
                    <select class ="form-control" id="age_limit" name="age_limit" value="<?php echo e($eventbooking->age_limit); ?>">
                      <option>  All ages </option>
                       <option> Under 18 </option>
                       <option> 18 and over  </option>         
                    </select>
                      <?php if($errors->has('age_limit')): ?>
                          <span class="invalid-feedback">
                              <strong><?php echo e($errors->first('age_limit')); ?></strong>
                          </span>
                      <?php endif; ?>     
    </div>
  </div>
</div>
</div>
  <div class="row">
    <div class="col-md-12">

    <div class="col-md-6">
            <div class="form-group<?php echo e($errors->has('event_date') ? ' has-error' : ''); ?>">
            <label for="event_date" class="control-label"> Date*</label>
          <input id="event_date" type="text" class="form-control datepicker" name="event_date" value="<?php echo e($eventbooking->event_date); ?>"  data-date-format="yyyy-mm-dd">
            <?php if($errors->has('event_date')): ?>
                <span class="help-block">
                    <strong><?php echo e($errors->first('event_date')); ?></strong>

                </span>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-md-6">
      <label for="waive_booking_fee" class="control-label lblcls">Waive Booking Fee</label>
       <input type="checkbox" name="waive_booking_fee" id="waive_booking_fee" value="1" <?php if($eventbooking->waive_booking_fee== '1') echo " checked "?>>
     </div>
  </div>
</div>
 <div class="line_div">
            <h3 class="com_busin_sec">Venue details </h3>
        </div>
<div class="row topcls">
 <div class="col-md-12">
   <div class="col-md-6">
     <div class="form-group<?php echo e($errors->has('venue_name') ? ' has-error' : ''); ?>">
    <label for="venue_name" class="control-label">Venue name*</label>
        <input type="text" class="form-control" id="venue_name" name="venue_name" value="<?php echo e($eventbooking->venue_name); ?>" maxlength="10" required>
        <?php if($errors->has('venue_name')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('venue_name')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
</div> 
    <div class="col-md-6">
          <div class="form-group<?php echo e($errors->has('telephone') ? ' has-error' : ''); ?>">
    <label for="telephone" class="control-label">Telephone*</label>
        <input type="number" class="form-control" id="telephone" name="telephone" value="<?php echo e($eventbooking->telephone); ?>" maxlength="10" required>
        <?php if($errors->has('telephone')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('telephone')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
  </div> 
 </div>
</div>
<div class="row topcls">
 <div class="col-md-12">                         
    <div class="col-md-6 edit_radiobtn">
          <div class="form-group<?php echo e($errors->has('details') ? ' has-error' : ''); ?>">
    <label for="details" class="control-label">Details *</label>
         <textarea id="details" name="details" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e($eventbooking->details); ?></textarea>

        <?php if($errors->has('details')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('details')); ?></strong>
            </span>
        <?php endif; ?>
    </div>  
 </div>
<div class="col-md-6 edit_radiobtn">
      <div class="form-group<?php echo e($errors->has('venue_address') ? ' has-error' : ''); ?>">
    <label for="venue_address" class="control-label">Venue address *</label>
         <textarea id="venue_address" name="venue_address" class="form-control" rows="5" cols="50"  maxlength="1001" required><?php echo e($eventbooking->venue_address); ?></textarea>

        <?php if($errors->has('venue_address')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('venue_address')); ?></strong>
            </span>
        <?php endif; ?>
    </div>
 
<!--     <div class="form-group<?php echo e($errors->has('payment_status') ? ' has-error' : ''); ?>">
        <label for="payment_status" class="control-label lblcls">Payment status</label>
        <label class="radiodiv"> 
                <input type="radio" name="payment_status" id="payment_status2" value="0" <?php echo e(old('payment_status')=="0" ? 'checked' : ''); ?> checked>
                <span class="checkmark"></span> Pending
        </label>
        <label class="radiodiv">
            <input type="radio" name="payment_status" id="payment_status1" value="1" <?php echo e(old('payment_status')=="1" ? 'checked' : ''); ?>>
            <span class="checkmark"></span> Completed
        </label>
            
        <?php if($errors->has('payment_status')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('payment_status')); ?></strong>
            </span>
        <?php endif; ?>
    </div> -->
  </div>
 </div>
</div>
  <div class="row">
    <div class="col-md-12">
      <div class="col-md-6">
        <div class="form-group<?php echo e($errors->has('website') ? ' has-error' : ''); ?>">
            <label for="website" class="control-label"> Website *</label>
            <input type="text" class="form-control" id="website" name="website" value="<?php echo e($eventbooking->website); ?>" required>
            <?php if($errors->has('website')): ?>
              <span class="help-block">
                <strong><?php echo e($errors->first('website')); ?></strong>
              </span>
            <?php endif; ?>
        </div>
  </div>
       <div class="col-md-6 feat_img_blog">     
                        <label for="phone">Featured Image</label>
                            <div class="form-group flx_mid">

                                  
                                     <div class="col-md-3" id="event_image" style="padding-top: 10px;">

                                     <?php if($eventbooking->event_image!=''): ?> 
                                        <img src="<?php echo e(url('public')); ?>/uploads/post/feature_post/<?php echo e($eventbooking->event_image); ?>" class="img-responsive" style="width: 100px; height: 100px;">
                                         <?php else: ?>
                                        <img src="<?php echo e(url('public')); ?>/uploads/photoclassimages/dummy.jpg" class="img-responsive" style="width: 100px; height: 100px;">
                                       <?php endif; ?>   

                                     </div>
                                    <div class="col-md-3">
                                      <input id="event_image" type="file" name="event_image" accept="image/*">
                                    </div>

                                <div class=" prflinput">
                                  
                                </div>

                            </div>
                        </div>
</div>
</div>
 <div class="col-md-12">
  <div class="topcls">
    <div class="form-group">
        <div class="buttondiv">
            <button type="submit" class="btn btn-primary">
                Update
            </button>
        </div>
    </div>
   </div>
</div>
 <?php echo e(Form::close()); ?>

    </div>
  </div>
 </div>
</div>
<script>
 $(document).ready( function() {

        $("#event_image").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });


        var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $($.parseHTML('<img style="margin:0 10px; width:100px; height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    };

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("currency", function (value, element) {
        return this.optional(element) || /^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
    },      "Please specify a valid amount");


    $("#edtbookingfrm").validate({
        rules: {
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                occassions: {
                    required: true
                },
                booking_amount: {
                    required: true,
                    currency: true,
                    range:[1,99999],
                    maxlength: 9
                }
            },
        messages: {
                start_date: {
                  required: "This is required field." 
                },
                end_date: {
                  required: "This is required field."
                },
                occassions: {
                  required: "This is required field." 
                },
                booking_amount: {
                  required: "This is required field.", 
                  currency: "Please specify a valid amount.",  
                  maxlength: "Maximum 9 characters allowed."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
    $('.datepicker').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });


 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>