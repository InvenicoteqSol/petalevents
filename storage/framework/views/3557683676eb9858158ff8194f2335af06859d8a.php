<?php $__env->startSection('content'); ?>

<section id="organiser-register">
     <div class="container">
          <div class="row">
 <div class="dashboard_section">
        <?php  $id = Auth::user()->id; 
         $orders = DB::table('orders')
                 ->where('id','=',$idorder)
                 ->where('user_id','=',$id)
                 ->first();
         $order_items=DB::table('order_items')->where('order_id','=',$idorder)->get();
      ?>

  
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : <?php echo e($orders->order_number); ?></h5>
       
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br><?php echo e(Auth::user()->first_name); ?></h5>
       
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> <?php echo e(Auth::user()->last_name); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> <?php echo e($orders->amount); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  <?php echo e($orders->order_number); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b> <br>  <?php echo e($orders->created_at); ?>

                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr><th>
                                Ticket
                            </th>
                            
                            <th>
                                Price
                            </th>
                            <th>
                              Quantity
                            </th>
                           
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <?php if(count($order_items)>0): ?>
                              <?php $__currentLoopData = $order_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <tr>  
                                
                                    <td>
                                   <?php $ticket_name=DB::table('tickets')->select('ticket_type')->where('id','=',$orderItem->ticket_id)->first();
                                   ?>
                                   <?php echo e($ticket_name->ticket_type); ?>

                                    </td>
                                    
                                    <td>
                                   <?php echo e($orderItem->netPrice); ?>

                                    </td>
                                    <td> <?php echo e($orderItem->Quantity); ?></td>
                                    <td>
                                      <?php echo e($orderItem->totalPrice); ?> 
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                <tr><td>No Records found!!</td></tr>
                                <?php endif; ?>
                                 
                            </tbody>
                        </table>

                    </div>
                </div>

              
            </div> <!-- /end modal body-->

           
        </div>
</div>
</div>

  </div></div>
  </div>
  <style type="text/css">
    .tablebtn {
    background: #666;
    color: #fff !important;
    padding: 5px 10px !important;
}
.modal-content {
    margin-left: 25%;
    margin-right: 25%;
    margin-right: 25%;
}

.modal-header.text-center {
    margin-top: 7%;
}
.btn.btn-primary.btn-sm.markPaymentReceived {
    color: #fff;
    padding: 5px 10px;
}
</style>

</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>