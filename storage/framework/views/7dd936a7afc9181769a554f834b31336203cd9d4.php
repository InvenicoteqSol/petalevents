<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>

    
   <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
        <p class="pagetite">Event</p>
      </div>

      <div class="card-body">
          <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Event Date :</span> <span class="labldata"><?php if($eventbooking->event_date!='' || $eventbooking->event_date!='0000-00-00'  || $eventbooking->event_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->event_date)->format('d M, Y')); ?><?php } ?> </span></p>
                        </div>
                        <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Event title :</span> <span class="labldata"><?php echo e($eventbooking->event_title); ?></span></p>
                        </div>
                      </div>
                               <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Tickets Remaining :</span> <span class="labldata"><?php echo e($ticketsavailable); ?></span></p>       </div>

                     <div class="col-sm-6"> 
                    <p class="rightsidetext"><span class="lablnamed">Event info :</span> <span class="labldata"><?php echo e($eventbooking->event_info); ?></span></p> </div>      
                        </div>
                         <div class="row">
                         <div class="col-md-6">
                          <p class="rightsidetext"><span class="lablname">Type of Ticket :</span> <span class="labldata">
                            <?php if(count($totalTickets1)>0): ?>
                            <?php $__currentLoopData = $totalTickets1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php echo e($ticket->ticket_type); ?> 
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?></span></p> 
                        </div>
                        </div>
                        </div>
                      </div>
                      </form>

    </div>
      </div>
    </div>
  </div>
  <div class="row">
 <div class="col-md-12 col-xs-12">
  <div class="card">
            <div class="card-header card-header-primary">
        <p class="pagetite">Tickets Sold</p>
      </div>

      <div class="card-body">
        <table class="table">
                <thead class=" text-primary">
                  <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                   <?php if(count($eventTicketDetails)>0): ?>
                     <?php $__currentLoopData = $eventTicketDetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">
                      <td class="column3"><?php echo e(date('Y-M-d',strtotime($order->created_at))); ?></td>
                      <td class="column3"><?php echo e($order->order_number); ?></td>
                      
                      <td class="column3"><?php echo e($order->amount); ?></td>
                      <?php if($order->order_status_id == '0'): ?> <td class="column3"><?php echo e('Completed'); ?></td><?php elseif($order->order_status_id == '1'): ?> <td class="column3"><?php echo e(' Awaiting Payment '); ?></td><?php else: ?>  <?php endif; ?> 
                      </tr>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php else: ?>
                      <tr>
                        <td colspan="4">
                          No Tickets Booked So Far.....
                        </td>
                      </tr>
                      <?php endif; ?>
                  
                </tbody>
              </table>
      </div>
    </div>
 </div>
</div>
</div>
</div>
    <style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
 span.lablnamed {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
p.pagetite {
    font-size: 24px;
    padding-left: 15px;
    
}
</style>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>