<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>


                    <?php echo e(Form::model($customer, array('route' => array('customerdashboard.update', $customer->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT', 'files' => true))); ?>


                    <div class="content">
        <div class="container-fluid">
          <div class="row">
 <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
        <h4 class="card-title">Update Customer </h4>
        <p class="card-category">Update your Details</p>
        </div>
      <div class="card-body">
        <form>
      <div class="row">
        <div class="col-md-12">
                    <div class="col-md-12">
                    <div class="row topcls">
                         <div class="col-md-6">
                             <div class="form-group<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
                             <label for="first_name" class="bmd-label-floating"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo e($customer->first_name); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('first_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                        <div class="col-md-6">
                                   <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                            <label for="last_name" class="bmd-label-floating">Last Name*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo e($customer->last_name); ?>" minlength="2" maxlength="91" required>
                                <?php if($errors->has('last_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                    </div>
                </div>

                     <div class="col-md-12">
                        <div class="row topcls">
                          <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="bmd-label-floating">E-Mail Address*</label>
                            <input id="email" type="email" class="form-control" name="email" value="<?php echo e($customer->email); ?>" maxlength="191" required readonly="readonly">
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                                <div class="col-md-6">
                        <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label for="phone" class="bmd-label-floating">Phone*</label>
                            <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e($customer->phone); ?>" minlength="10" maxlength="13" required>
                                <?php if($errors->has('phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                          </div>
                        </div>
                      </div>

                        <div class="col-md-12">
                        <div class="row topcls">
                        <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('country') ? ' has-error' : ''); ?>">
                            <label for="country" class="bmd-label-floating">City/Town</label>
                                <select class="form-control" id="country" name="country" required>
                                    <?php $__currentLoopData = $towns; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $townName=>$townId): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($townId); ?>" <?php echo e(( $customer->country == $townId ) ? 'selected' : ''); ?>><?php echo e($townName); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                 <?php if($errors->has('country')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('country')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                          <div class="col-md-6 edit_radiobtn">
                            <label for="phone" class="bmd-label-floating">Gender</label>
                              <div class="form-group<?php echo e($errors->has('gender') ? ' has-error' : ''); ?>">
                                   <label class="radiodiv">Male
                                        <input type="radio" name="gender" id="gender1" value="Male" <?php if($customer->gender=="Male"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Female
                                        <input type="radio" name="gender" id="gender2" value="Female" <?php if($customer->gender=="Female"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                <?php if($errors->has('gender')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('gender')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                           </div>
                            <!-- <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('dob') ? ' has-error' : ''); ?>">
                            <label for="dob" class="bmd-label-floating">Dob</label>
                                <input id="dob" type="text" class="form-control datepicker" name="dob" value="<?php echo e($customer->dob); ?>" placeholder="YYYY-MM-DD" data-date-format="yyyy-mm-dd">
                                <?php if($errors->has('dob')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('dob')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                      </div> -->
                    </div>
                   
                        <div class="col-md-12">
                           <div class="row topcls">                  
                          <!--   <div class="col-md-6 edit_radiobtn">
                            <label for="phone" class="bmd-label-floating">Gender</label>
                              <div class="form-group<?php echo e($errors->has('gender') ? ' has-error' : ''); ?>">
                                   <label class="radiodiv">Male
                                        <input type="radio" name="gender" id="gender1" value="Male" <?php if($customer->gender=="Male"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Female
                                        <input type="radio" name="gender" id="gender2" value="Female" <?php if($customer->gender=="Female"){ echo "checked";}?>>
                                        <span class="checkmark"></span>
                                    </label>
                                <?php if($errors->has('gender')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('gender')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                           </div> -->
                      <!--      <div class="col-md-6">
                                 <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="bmd-label-floating">Password*</label>
                                <input id="password" type="password" class="form-control" name="password" value="<?php echo e($customer->password); ?>">

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>  -->
                             
                      </div>
                    </div>

                       
                                 <div class="col-md-12">
                                  <div class="row topcls"> 
                                 <div class="col-md-6">
                            <div class="col-md-3 nopadding" id="ftrd_browse_img">
                                <?php if($customer->profile_picture!=''): ?> 
                                 <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($customer->profile_picture); ?>" class="img-responsive" style="width: 100px; height: 100px;">
                                     <?php else: ?>
                                    <img src="<?php echo e(url('public')); ?>/uploads/profileimages/default_profile_pic.jpeg" class="img-responsive" style="width: 100px; height: 100px;">
                                   <?php endif; ?>
                                    
                                </div>
                                <div class="col-md-3 prflinput">
                                <input id="profile_picture" type="file" name="profile_picture" accept="image/*">
                              </div>
                        </div>
                             <div class="col-md-6">
                                 <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="bmd-label-floating">Password*</label>
                                <input id="password" type="password" class="form-control" name="password" value="<?php echo e($customer->password); ?>">

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                    </div>
                </div>
                  



                <div class="col-md-12">
                        <div class="topcls">
                     <div class="form-group">
                            <div class="buttondiv">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                     <?php echo e(Form::close()); ?>

            
            </div>
    </div>

<script>
 $(document).ready( function() {

    $("#profile_picture").change(function() {
            var file = this.files[0];
            var imagefile = file.type;
            var reader = new FileReader();
                reader.onload = function(e) {
                    $("#ftrd_browse_img").html('<img src="' + e.target.result + '" style="width: 100px; height: 100px;" class="img-responsive" />');
                };
                reader.readAsDataURL(this.files[0]);
        });    

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                },
                dob:{
                   required: true
                },
                 gender:{
                   required: true
                },
                 status:{
                   required: true
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                },
                 dob: {
                  required: "This is required field."
                },
                 gender: {
                  required: "This is required field."
                },
                 status: {
                  required: "This is required field."
                }
            },
        submitHandler: function(form) {
            form.submit();
          }
        });
       $('.datepicker').datepicker({ 
        dateFormat: 'yy-mm-dd' 
    });


 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>