<?php $__env->startSection('content'); ?>
<?php  $id = Auth::user()->id; 
         $orders = DB::table('orders')
                 ->where('id','=',$idorder)
                 ->where('user_id','=',$id)
                 ->first();
         $order_items=DB::table('order_items')->where('order_id','=',$idorder)->get();
      ?>
      <section class="section-overview">
  <div class="container">
    <div class="row">
        <!--  col-oview -->
      <div class="col-md-12 col-oview">
         <div class="o-head">
          <h5>Order : <?php echo e($orders->order_number); ?></h5>
         </div>
      </div>
        <!--  //col-oview -->
         <!--  col-oview -->
      <div class="col-md-12 col-sm-12 col-xs-12 col-oview">
     
      <div class="o-body">
         <div class="o-head">
        <h4>Order Overview</h4>
      </div>
        <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br><?php echo e(Auth::user()->first_name); ?>

       
                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> <?php echo e(Auth::user()->last_name); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> <?php echo e($orders->amount); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  <?php echo e($orders->order_number); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b> <br>  <?php echo e($orders->created_at); ?>

                        </div>
                        
                   </div>
               </div>
          </div>
            <!--  //col-oview -->
                <!--  col-oview -->
      <div class="col-md-12 col-sm-12 col-xs-12 col-oview">
     
      <div class="o-body">
         <div class="o-head">
        <h4>Order Items</h4>
      </div>
     
        <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>
                                Ticket
                            </th>
                            
                            <th>
                                Price
                            </th>
                            <th>
                              Quantity
                            </th>
                           
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                            <?php if(count($order_items)>0): ?>
                              <?php $__currentLoopData = $order_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orderItem): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <tr>  
                                
                                    <td>
                                   <?php $ticket_name=DB::table('tickets')->select('ticket_type')->where('id','=',$orderItem->ticket_id)->first();
                                   ?>
                                   <?php echo e($ticket_name->ticket_type); ?>

                                    </td>
                                    
                                    <td>
                                   <?php echo e($orderItem->netPrice); ?>

                                    </td>
                                    <td> <?php echo e($orderItem->Quantity); ?></td>
                                    <td>
                                      <?php echo e($orderItem->totalPrice); ?> 
                                    </td>
                                </tr>
                        <tr> <td><strong> Booking Fee : </strong> <?php echo e($orders->totalbookingfeeamount); ?> </td>
                        </tr>
                        <tr>  <td><strong> Total Amount : </strong>  <?php echo e($orders->amount); ?> </td>
                        </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                <tr><td>No Records found!!</td></tr>
                                <?php endif; ?>
                     
                                                                                                 
                            </tbody>
                        </table>

                    </div>                
       
               </div>
          </div>
            <!--  //col-oview -->
      </div>
  </div>
 </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>