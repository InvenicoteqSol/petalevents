<?php $__env->startSection('content'); ?>
<!--content-section-start-->
<main>
 <section class="signup_section form_section innercontentpart p-policy">
   <div class="container">
    <div class="innercontent_text row">
      <div class="col-md-12 col-xs-12 policy-content">
      <h2 class="main-s-heading">General Terms and Conditions</h2>
      <p style="pa">Please read the following terms and conditions carefully. By using this site and by registering to buy tickets via it you will be deemed to have accepted all relevant terms and conditions.

Welcome to the terms and conditions for Commerce Global Ventures Ltd. Commerce Global Ventures Ltd acts as an agent facilitating the sale of Tickets to Events on behalf of Event Organisers (all as defined below). We therefore enter this agreement with you in our own right and on behalf of the Event Organisers.

These terms and conditions are divided into 2 parts:
1 Those relating to Ticket purchases made by you from the Event Organiser(s) of the Event(s) which you have selected via our Website (the "Ticket Purchase Terms"); and
2 Those relating to your use of this Website (the "Website Terms")
The expression "terms and conditions" is used to describe the combined provisions of the Ticket Purchase Terms and of the Website Terms.
In these terms and conditions;
• "we" and "us" means Commerce Global Ventures Ltd (registered as a company in England and Wales, number 10521270), whose registered office is at Kemp House, 160 City Road, London, EC1V 2NX.
• "Event" means the individual event or events advertised on our Website for which Tickets are available.
• "Event Organiser" means a third party supplier or suppliers of Tickets for the Events which may include a venue, performer, promoter or event organiser.
• "Tickets" or "Bookings" means rights to admission in the form of unique Booking references.
• "Website" means the website on which we make Tickets available and from which we promote Events.
• "you" means an individual purchasing tickets using this Website.
If there is any conflict between the Ticket Purchase Terms and the Website Terms, then the latter shall prevail

      </p>
    </div>
      <div class="terms-conditionsdiv">
        <h4>1. Overview</h4>
        <p>MRS PORTRAIT offers an internet-based service through its website, www.PORTRAIT.com (henceforth to be referred to as The “Site”), that allows you to book a photo shoot in several cities around the world by selecting a photographer. Protecting our customers’ private information is our 
highest priority. These T&C also cover the Privacy Policy and governs all data collection and usage by PORTRAIT including through the Site. By using the Site, you consent to the data collection and usage practices described in this Policy.</p>
      </div>
           <div class="terms-conditionsdiv">
        <h4>2. Data Collection</h4>
        <p>MRS PORTRAIT will collect personal data about you and your shopping behavior, past travel experiences, which includes but is not limited to your name and email, billing and credit card information, demographic information such as age and gender, with the aim to give you a better 
experience and show you relevant  information, and to complete the purchase transaction.
MRS PORTRAIT makes use of cookies to help you personalize your online experience. A cookie is a text file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you, and can only be read by a web server in the domain that issued the cookie to you. One of the primary purposes of cookies is to provide a convenience feature to save you time. The purpose of a cookie is to tell the Web server that you have returned to a specific page. You have the ability to accept or decline cookies. Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer.
MRS PORTRAIT stores information about your computer hardware and software. This information can include: your IP address; browser type; domain names; access times; and referring website addresses. This information is used for the operation of the Service, to maintain quality of the Service, and to provide general statistics regarding use of the Site. By using MRS PORTRAIT you agree to allow MRS PORTRAIT to collect your personal 
information. Your information will not be sold or given to other third-party services/products not related to MRS PORTRAIT
MRS PORTRAIT encourages you to review the privacy statements of websites you choose to link to from the MRS PORTRAIT website so that you can understand how those websites collect, use and share your information. SWEETESCAPE is not responsible for the privacy statements or other content on websites outside of the Site.</p>
      </div>
                 <div class="terms-conditionsdiv">
        <h4>3. Use Of Your Personal Information</h4>
        <p>MRS PORTRAIT collects and uses your personal information to operate the Site and deliver the services you have requested. MRS PORTRAIT may also use yourpersonally identifiable information to inform you of other products or services available from SWEETESCAPE and its affiliates. MRS PORTRAITdoes not sell, rent or lease its customer lists to third parties. SWEETESCAPE may keep track of the websites and pages our users visit within SMRS PORTRAIT in order to determine what SWEETESCAPE services are the most popular. This data is used to deliver customized content and advertising within MRS PORTRAIT to customers whose behaviour indicates that they are interested in a particular subject area.
MRS PORTRAIT will disclose your personal information, without notice, only if required to do so by law or in the good faith belief that such action is necessary to: (a) conform to the edicts of the law or comply with legal process served on SWEETESCAPE or the site; (b) protect and defend the rights or property of SWEETESCAPE; and, (c) act under exigent circumstances to protect the personal safety of users of MRS PORTRAIT, or the public.
</p>
      </div>
                       <div class="terms-conditionsdiv">
        <h4>4. Security Of Your Personal Information</h4>
        <p>MRS PORTRAIT secures your personal information from unauthorized access, use or disclosure. When personal information (such as a credit card number) is transmitted to other websites, it is protected through the use of encryption, such as the Secure Sockets Layer (SSL) protocol.
</p>
      </div>
                             <div class="terms-conditionsdiv">
        <h4>5. Email & Promotions</h4>
        <p>MRS PORTRAIT from time to time will contact you through email, push notifications, SMS, or other medium, to update you of special promotions or things that are going on with SWEETESCAPE. But using this service you agree to subscribe to receiving these emails and messages. You can 
unsubscribe at anytime by clicking our unsubscribe button at the bottom on the email or by contacting our support team.
</p>
      </div>
                                   <div class="terms-conditionsdiv">
        <h4>6. Opt Out & Unsubscribe</h4>
        <p>We respect your privacy and give you an opportunity to opt-out of receiving announcements of certain information. Users may opt-out of receiving any or all communications from MRS PORTRAIT by contacting us using the contact information listed in the Contact Information section of this Policy.

</p>
      </div>
                                         <div class="terms-conditionsdiv">
        <h4>7. Copyright</h4>
        <p>MRS PORTRAIT and you as our client will own the copyright of the pictures from your photoshoot. The photographer however does not own the 
copyright. By using MRS PORTRAIT you agree to let SWEETESCAPE use the photoshoot pictures at its own discretion on the media channels owned by MRS PORTRAIT, including by not limited to the MRS PORTRAIT BLOG, INSTAGRAM ACCOUNTS, HOMEPAGE, APPS.
You acknowledge that MRS PORTRAIT will own the copyrights of all the photos that MRS PORTRAIT created and produced, and we reserve the right to use them to promote the MRS PORTRAIT business, therefore, you agree to give SWEETESCAPE consent for all the photographs and videos produced by MRS PORTRAIT to be published, displayed or used for the benefit of SWEETESCAPE (e.g. editorial, and exhibition) and to release all the rights to claim for any the profit or further compensation, in whatsoever manner, that may arise from such use. However, MRS PORTRAIT will make every effort not to use your pictures in external media channels not owned by MRS PORTRAIT, without your permission. You agree to release MRS PORTRAIT and the relevant photographer from all liability for libel, invasion of privacy, and all causes of action whatsoever in relation
to the photos their making and use, you or your property including without limitation any liability for alteration of the photos, whether intentional or otherwise, that may occur during the making, editing or subsequent use of the photos.

</p>
      </div>
                                              <div class="terms-conditionsdiv">
        <h4>8. Booking Time</h4>
        <p>MRS PORTRAIT acts as a reservation system for photographers. By booking a photographer for example from 4pm till 6pm, you only have them available for you during those 2 hrs. If you choose to travel from point A to B, that will be part of the 2-hour reservation. Should you come late to the photoshoot, that will also be deducted from the 2-hour photoshoot.

</p>
      </div>
        <div class="terms-conditionsdiv">
        <h4>9. Photo Editing Time</h4>
        <p>Max 3 business days after the photoshoot.</p>
      </div>
       <div class="terms-conditionsdiv">
        <h4>10. Editing</h4>
        <p>MRS PORTRAIT editing team will edit all the images we provide to you. You can select and download up to 40 edited images. We edit for brightness and color. We do not photoshop images to make people look slimmer, thinner, or remove objects. We edit to bring out the beauty of the 
photoshoot. MRS PORTRAIT has its own editing style. You cannot request your own editing style. Photographer’s personal portfolios will look very different to SWEETESCAPE photoshoots, due to our editing style.</p>
      </div>
             <div class="terms-conditionsdiv">
        <h4>11. Travel Expense</h4>
        <p>Our photographers are local photographers in the city where you will do the photoshoot. Therefore, there are no travel or accommodation 
expenses. However, if you want your photoshoot to happen on remote places that are more than 1 hour outside of the city center, there might be an additional charge. Please check with our support team prior to your booking.</p>
      </div>
                  <div class="terms-conditionsdiv">
        <h4>12. Other Expense</h4>
        <p>Entrance fees to such places like Theme parks are not included in the 2-hour fee. Should there be an entrance fee, you will have to pay for the entrance fee for the photographer.</p>
      </div>
             <div class="terms-conditionsdiv">
        <h4>13. Theft</h4>
        <p>MRS PORTRAIT and the photographer are not responsible for theft during the duration of your photoshoot.</p>
      </div>
    </div>
   </div>
 </section>
</main>
<!--content-section-end-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>