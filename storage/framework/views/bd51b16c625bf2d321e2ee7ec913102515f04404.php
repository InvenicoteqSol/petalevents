<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-12 dashboard_right">
         <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

                 <div class="content">
                  <div class="container-fluid">
                     <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card">
               <div class="card-header card-header-primary">
             <div class="line_div"><h4 class="com_busin_sec card-title">Customers</h4>
                 <p class="card-category">Here is a subtitle for this table </p>
                        </div>
                        </div>
                                

                     <?php echo Form::open(array('route' => 'admincustomer.store', 'method'=>'POST', 'id' => 'edtpcfrm',  'class' => 'edt_post_frm', 'files' => true)); ?>


                            <div class="card-body">
                                    <form>
                    
                         <div class="row">
                         <div class="col-md-6">
                             <div class="form-group<?php echo e($errors->has('first_name') ? ' has-error' : ''); ?>">
                             <label for="first_name" class="col-md-4 control-label bmd-label-floating"> First Name*</label>
                                <input id="first_name" type="text" class="form-control" name="first_name" value="<?php echo e(old('first_name')); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('first_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('first_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                                        <div class="col-md-6">
                                   <div class="form-group<?php echo e($errors->has('last_name') ? ' has-error' : ''); ?>">
                            <label for="last_name" class="col-md-4 control-label bmd-label-floating">Last Name*</label>
                                <input id="last_name" type="text" class="form-control" name="last_name" value="<?php echo e(old('last_name')); ?>" minlength="2" maxlength="91" required>
                                <?php if($errors->has('last_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('last_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                    </div>
                                    
                             <div class="row topcls">
                             <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label for="email" class="bmd-label-floating">E-Mail Address*</label>
                            <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>" maxlength="191" required>
                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                            <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
                            <label for="phone" class="bmd-label-floating">Phone*</label>
                            <input id="phone" type="text" class="form-control" name="phone" value="<?php echo e(old('phone')); ?>" minlength="10" maxlength="13" required>
                                <?php if($errors->has('phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>             
                    </div>
    
                            
                        <div class="row topcls">      
                        <div class="col-md-6 edit_radiobtn">
                            <label for="phone" class="bmd-label-floating">Gender</label>
                              <div class="form-group<?php echo e($errors->has('gender') ? ' has-error' : ''); ?>">
                                   <label class="radiodiv">Male
                                        <input type="radio" name="gender" id="gender1" value="Male">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radiodiv">Female
                                        <input type="radio" name="gender" id="gender2" value="Female">
                                        <span class="checkmark"></span>
                                    </label>
                                <?php if($errors->has('gender')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('gender')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                           </div>
           
                              
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('user_type') ? ' has-error' : ''); ?>">
    
                               <input type="hidden" value="3" name="user_type" id="user_type">
                            </div>
                        </div>
                
                                 <div class="col-md-6">
                                 <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label for="password" class="col-md-4 control-label bmd-label-floating">Password*</label>
                                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>      
                    </div>      
                
                        </form>
                                    
                              
                             <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <button type="submit" class="btn btn-primary pull-right">
                                    Save
                                </button>
                                <div class="clearfix"></div>
                                  
                            </div>
                        </div>
                    
                     <?php echo e(Form::close()); ?>

            
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>