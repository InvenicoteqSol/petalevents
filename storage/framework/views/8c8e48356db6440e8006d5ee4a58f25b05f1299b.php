<?php $__env->startSection('content'); ?>

<section class="evnt-slide">
  <div class="container">
  <!-- row -->
  <div class="row">
  <!-- slider -->
  <?php $counter=0;?>
  <?php $__currentLoopData = $Ticketdeals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ticketdeal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <?php if($counter<=1): ?>
  <div class="col-md-6 col-sm-6 col-xs-12" id="tickets">
  <h3 class="section-heading">Tickets</h3>
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <!--    item -->
 <div class="item active">
  <div class="row">
    <?php $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();?>
    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <?php $ticket1=$Ticketdeal->ticket_id;
           $ticket2=$Ticketdeal->ticket_id2;
        $discount=$Ticketdeal->discount;
        $ticket=DB::table('tickets')->where('id','=',$ticket1)->first();
        $price =$ticket->prices;
        $total = $price-($price*($discount/100));
         ?>
        <div class="col-md-6 col-xs-12">
        <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
        <?php echo e($event->type_of_event); ?>

        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->image_thumb); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/image_thumbnail.jpg">
        <?php endif; ?>
        </a>
        </div>
        <!--  //card-body -->
        <div class="card-body">
        <div class="event-detail">
        <span class="card-title"><?php echo e($event->event_title); ?></span>
        <div class="date-and-place">
        <div class="venue-details">
        <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
       <?php                
          /*$eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];*/
          ?>
       <span class="e-date-time"><?php if($event->start_date!='' || $event->start_date!='0000-00-00'  || $event->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($event->start_date)->format('d M, Y')); ?><?php } ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" class="btn btn-primary mycartnew" id="mycart" data-ticket="<?php echo e($ticket1); ?>" data-ticket2="<?php echo e($ticket2); ?>">Buy Tickets</a>
        
       </div>
       </div>
       <!-- ///event-detail -->
       <div class="price">
       <i class="fa fa-ticket" aria-hidden="true"> </i>
       <div class="value ticke_value1">
      
         <i class="fa fa-gbp" aria-hidden="true"><?php echo e($total); ?></i> 
        

       </div>
       </div>
       <!--  //price -->
       </div>
       <!-- // card-body -->
       </div>
       </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php  $event_id2=$Ticketdeal->event_id2;
        $events=DB::table('eventbookings')->where('id','=',$event_id2)->get();?>
        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $ticket1=$Ticketdeal->ticket_id;
            $ticket2=$Ticketdeal->ticket_id2;
            $discount=$Ticketdeal->discount;
            $ticket=DB::table('tickets')->where('id','=',$ticket2)->first();
            $price =$ticket->prices;
            $total = $price-($price*($discount/100));
           ?>
          <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
          <div class="category-overlay">
        <?php echo e($event->type_of_event); ?>

        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->image_thumb); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/image_thumbnail.jpg">
        <?php endif; ?>
        </a>
       </div>
       <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
          <span class="card-title"><?php echo e($event->event_title); ?></span>
          <div class="date-and-place">
          <div class="venue-details">
          <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
          <?php                
          /*$eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];*/
          ?>
       <span class="e-date-time"><?php if($event->start_date!='' || $event->start_date!='0000-00-00'  || $event->start_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($event->start_date)->format('d M, Y')); ?><?php } ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" id="mycart" class="btn btn-primary mycartnew" data-ticket="<?php echo e($ticket1); ?>" data-ticket2="<?php echo e($ticket2); ?>">Buy Tickets</a>
       
       </div>
       </div>
      <!-- ///event-detail -->
      <div class="price">
      <i class="fa fa-ticket" aria-hidden="true"> </i>
      <div class="value ticke_value2">
       <i class="fa fa-gbp" aria-hidden="true"><?php echo e($total); ?></i> 
      </div>
      </div>
     <!--  //price -->
     </div>
     <!-- // card-body -->
     </div>
     </div>
      </div>
     <!--     row -->


    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <!--item End -->

 </div></div> </div>
  <?php $counter=$counter+1;?>
  <?php else: ?>
  <div  class="clearfix"></div>
  <div class="col-md-6 col-sm-6 col-xs-12" id="tickets">
  <h3 class="section-heading">Tickets</h3>
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
  <!--    item -->
 <div class="item active">
  <div class="row">
    <?php $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();?>
    <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
     <?php $ticket1=$Ticketdeal->ticket_id;
           $ticket2=$Ticketdeal->ticket_id2;
        $discount=$Ticketdeal->discount;
        $ticket=DB::table('tickets')->where('id','=',$ticket1)->first();
        $price =$ticket->prices;
        $total = $price-($price*($discount/100));
         ?>
        <div class="col-md-6 col-xs-12">
        <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
        <?php echo e($event->type_of_event); ?>

        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->image_thumb); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/image_thumbnail.jpg">
        <?php endif; ?>
        </a>
        </div>
        <!--  //card-body -->
        <div class="card-body">
        <div class="event-detail">
        <span class="card-title"><?php echo e($event->event_title); ?></span>
        <div class="date-and-place">
        <div class="venue-details">
        <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
       <?php                
        $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
        $datesplit=explode('-', $eventdate);
        $month=$datesplit[0];
        $date=$datesplit[1];
        $year=$datesplit[2];
        ?>
       <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" class="btn btn-primary mycartnew" id="mycart" data-ticket="<?php echo e($ticket1); ?>" data-ticket2="<?php echo e($ticket2); ?>">Buy Tickets</a>
        
       </div>
       </div>
       <!-- ///event-detail -->
       <div class="price">
       <i class="fa fa-ticket" aria-hidden="true"> </i>
       <div class="value ticke_value1">
      
         <i class="fa fa-gbp" aria-hidden="true"><?php echo e($total); ?></i> 
        

       </div>
       </div>
       <!--  //price -->
       </div>
       <!-- // card-body -->
       </div>
       </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php  $event_id2=$Ticketdeal->event_id2;
        $events=DB::table('eventbookings')->where('id','=',$event_id2)->get();?>
        <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php
            $ticket1=$Ticketdeal->ticket_id;
            $ticket2=$Ticketdeal->ticket_id2;
            $discount=$Ticketdeal->discount;
            $ticket=DB::table('tickets')->where('id','=',$ticket2)->first();
            $price =$ticket->prices;
            $total = $price-($price*($discount/100));
           ?>
          <div class="col-md-6 col-xs-12">
          <div class="card-event">
          <div class="card-image">
          <div class="category-overlay">
        <?php echo e($event->type_of_event); ?>

        </div>
        <a href="">
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->image_thumb); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/image_thumbnail.jpg">
        <?php endif; ?>
        </a>
       </div>
       <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
          <span class="card-title"><?php echo e($event->event_title); ?></span>
          <div class="date-and-place">
          <div class="venue-details">
          <span class="e-loc"><?php echo e($Ticketdeal->name); ?></span>
          <?php                
          $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
          $datesplit=explode('-', $eventdate);
          $month=$datesplit[0];
          $date=$datesplit[1];
          $year=$datesplit[2];
          ?>
       <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
       </div>
       </div>
       <div class="btn-act">
       <a href="javascript:void(0)" id="mycart" class="btn btn-primary mycartnew" data-ticket="<?php echo e($ticket1); ?>" data-ticket2="<?php echo e($ticket2); ?>">Buy Tickets</a>
       
       </div>
       </div>
      <!-- ///event-detail -->
      <div class="price">
      <i class="fa fa-ticket" aria-hidden="true"> </i>
      <div class="value ticke_value2">
       <i class="fa fa-gbp" aria-hidden="true"><?php echo e($total); ?></i> 
      </div>
      </div>
     <!--  //price -->
     </div>
     <!-- // card-body -->
     </div>
     </div>
      </div>
     <!--     row -->


    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <!--item End -->

 </div></div> </div>
  <?php $counter=0;?>

 <?php endif; ?>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</div>
 </div>
<!--  endslider -->
<!-- slider -->
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
 <script type="text/javascript">
   $(document).ready(function() {
   $(".mycartnew").click(function(){
      tickettype=$(this).data('ticket');
      tickettype2=$(this).data('ticket2');
           
            jQuery.ajax({
              method : 'GET',
              url  : "<?php echo e(url('/addtocartticket')); ?>",
              data :{'tickettype': tickettype,'tickettype2': tickettype2},
              success :  function(resp) {
              $.notify({  
              message: 'Basket Updated Redirecting To Cart...' 
                },{
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
              window.location.href = "<?php echo e(url('/cart')); ?>";
             }
            });
         
       });
   });
</script>
<script>
$(document).ready(function() {
$('.owl-carousel').owlCarousel({
loop: true,
margin: 10,
responsiveClass: true,
responsive: {
0: {
items: 1,
nav: true
},
600: {
items: 3,
nav: false
},
1000: {
items: 5,
nav: true,
loop: false,
margin: 20
}
}
});
} );
</script>
<style type="text/css">
h3.section-heading {
margin-top: 50px;
}
</style>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>