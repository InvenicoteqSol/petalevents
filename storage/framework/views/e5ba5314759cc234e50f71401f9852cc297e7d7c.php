<?php $__env->startSection('content'); ?>

<section class="section-upcoming-events">
  <div class="container">
  
     <!-- row -->
  <div class="row">
  <!-- slider -->
  <div class="col-md-12 col-sm-12 col-xs-12">
  <h3 class="section-heading">Ticket Deals</h3>
</div>
</div>
<!-- row -->
     <!-- row -->
    <div class="row">

  <div class="col-md-12 col-sm-12 col-xs-12">
   <div id="carousel-example-generic1" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic1" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $counter=0;?>
    <?php $__currentLoopData = $Ticketdeals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ticketdeal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($counter==0): ?>
  <!--    item -->
    <div class="item active">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
      <div class="card-event single-event">
      <?php else: ?>
      
      <?php endif; ?>
     </div>
        <div class="col-md-6 col-xs-12">
           <?php  $event_id=$Ticketdeal->event_id;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();
    ?>
          <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="card-event">
          <div class="card-image">
         <div class="category-overlay">
          <?php echo e($event->type_of_event); ?>


        </div>
         
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      
       
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title">  <?php echo e($event->event_title); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc"><?php echo e($Ticketdeal->name); ?> </span>
       <?php                
              $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
              $datesplit=explode('-', $eventdate);
              $month=$datesplit[0];
              $date=$datesplit[1];
              $year=$datesplit[2];
              
              ?>
       <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="#" id="mycart" class="btn btn-primary">>Buy Tickets</a>
    </div>
     </div>
    <!-- ///event-detail -->
    <?php 
      /* $ticketLowest=DB::table('tickets')->where('event_id', '=',$event_id)->orderBy('prices')->first();
       print_r($ticketLowest);

       $countcheck=count($ticketLowest);
       $priceval=0;
       if($countcheck>0)
       {
         $priceval=$ticketLowest->prices;
       }*/
       ?>
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
     <?php  echo $event_id=$Ticketdeal->ticket_id;
    $events=DB::table('tickets')->where('id','=',$event_id)->first();
    ?>  
       <!-- <div class="value">
       <i class="fa fa-gbp" aria-hidden="true"></i> 
       </div> -->
     </div>
   <!--  //price -->
    </div>
    
   <!-- // card-body -->
 </div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
   
      </div>
  <!--     row -->
    </div>
    <!--item End -->
     <?php $counter=$counter+1;?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <?php $counter=0;?>
    <?php $__currentLoopData = $Ticketdeals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Ticketdeal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($counter==0): ?>
  <!--    item -->
    <div class="item active">
      <div class="col-md-12 col-sm-12 col-xs-12 ">
      <div class="card-event single-event">
      <?php else: ?>
      
      <?php endif; ?>
     </div>
        <div class="col-md-6 col-xs-12">
           <?php  $event_id=$Ticketdeal->event_id2;
    $events=DB::table('eventbookings')->where('id','=',$event_id)->get();
    ?>
          <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="card-event">
          <div class="card-image">
         <div class="category-overlay">
          <?php echo e($event->type_of_event); ?>


        </div>
         
        <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/default-event.jpg">
        <?php endif; ?>
      
       
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title">  <?php echo e($event->event_title); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc"><?php echo e($Ticketdeal->name); ?> </span>
       <?php                
              $eventdate= date('F-j-Y',strtotime($Ticketdeal->start_date));
              $datesplit=explode('-', $eventdate);
              $month=$datesplit[0];
              $date=$datesplit[1];
              $year=$datesplit[2];
              
              ?>
       <span class="e-date-time"><?php echo e($month); ?> <label><?php echo e($date); ?></label> <?php echo e($year); ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="#" id="mycart" class="btn btn-primary">Buy Tickets</a>
     </div>
     </div>
    <!-- ///event-detail -->
    <?php 
      /* $ticketLowest=DB::table('tickets')->where('event_id', '=',$event_id)->orderBy('prices')->first();
       print_r($ticketLowest);

       $countcheck=count($ticketLowest);
       $priceval=0;
       if($countcheck>0)
       {
         $priceval=$ticketLowest->prices;
       }*/
       ?>
        <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <!-- <div class="value">
       <i class="fa fa-gbp" aria-hidden="true"></i> 
       </div> -->
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
 </div>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
      </div>
  <!--     row -->
    </div>
    <!--item End -->
     <?php $counter=$counter+1;?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  </div>
</div>
</div>
</div>
</div>
</div>
     <!--  endslider -->
      <!-- slider -->
 
</section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 3,
                    nav: false
                  },
                  1000: {
                    items: 5,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              });


              $("#mycart").click(function(){
  var table = $("#tickets tbody");

    table.find('tr').each(function (i) {
        var $tds = $(this).find('td'),
            tickettype = $tds.find("#ticketid").val(),
           
            Quantity = $tds.find("select").val();
            if(Quantity>0)
            {
             jQuery.ajax({
              method : 'GET',
            url  : "<?php echo e(url('/addtocart')); ?>",
            data :{'tickettype': tickettype,'quantity':Quantity},
            success :  function(resp) {
              
              $.notify({  // options
             message: 'Basket Updated Redirecting To Cart...' 
                },{
                  // settings
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
              window.location.href = "<?php echo e(url('/cart')); ?>";
             }
            });
            }
            
        
       
    });
  });
            } );
          </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>