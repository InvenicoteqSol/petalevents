<!DOCTYPE html>
<html lang="eng">
  <head>
    <title>Petal Events</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
<link href="<?php echo e(url('/public')); ?>/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.theme.default.css">
    <!-- font-awesome icons -->
   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo e(url('/public')); ?>/front/css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
   <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
   </head>
  <body class="page-login">
  <section class="section-login">
    <div class="container">

      <div class="row">
        <div class="col-md-12 col-xs-12">
          <label class="l-heading">Log in to access your events.</label>
        </div>
        <?php if(\Session::has('error')): ?>
                        <div class="alert alert-danger alert-dismissable">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                           <?php echo \Session::get('error'); ?>

                        </div>
                    <?php endif; ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <img class="img-login" src="<?php echo e(url('/public')); ?>/images/login-vector.png">
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">

          <form class="form-login" name="login" id="login" method="POST" action="<?php echo e(route('login')); ?>">
             <?php echo e(csrf_field()); ?>

             <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour.</p>
 <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                            <label>Username or Email</label>

                                <input id="email" type="text" class="form-control" name="email" value="<?php echo e(old('email')); ?>" maxlength="191" required autofocus>

                                <?php if($errors->has('email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            
                        </div>
 <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                            <label>Password*</label>

                            
                                <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" required>

                                <?php if($errors->has('password')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            
                        </div>

  <button type="submit" class="btn btn-default">Submit</button>
  <div class="pwd-act">
    <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Remember Me
                                    </label>
                                </div>
    <label class="col-p-act">
      
     <a href="<?php echo e(route('password.request')); ?>" class="password-reminder">Forgot your password?</a><br>
     Don't have an account? <a href="<?php echo e(url('/organiserregistration')); ?>">Sign up</a>
     </label>
     </div>
 </form>
        </div>
      </div>
      <span class="back"> Back to <a href="<?php echo e(url('')); ?>">Home</a></span>
    </div>
    
  </section>

 <!--  footer -->
   
    <div class="copy-agile-right text-center pt-lg-4 pt-3">
          <p> 
            © 2018 |<a href="#">Petal Events</a>| All Rights Reserved 
          </p>
        </div>
    <!--//footer -->
   <script src="<?php echo e(url('/public')); ?>/front/js/jquery-1.11.3.min.js"></script>
   <script src="<?php echo e(url('/public')); ?>/front/front/js/bootstrap.min.js"></script>
   <script src="<?php echo e(url('/public')); ?>/front/js/owl.carousel.js"></script>
<script src="<?php echo e(url('/public')); ?>/front/js/login.js"></script>     

    
  </body>
</html>
                <!-- <div class="panel-heading">Login</div> -->
                    
                    
                  
   



