<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
  <!--booking-sec-start-->
  <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-warning card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-users" aria-hidden="true"></i>

                  </div>
                  <p class="card-category">Customers</p>
                  <h3 class="card-title"><?php echo e($totalCustomersCount); ?>

                    
                  </h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                    <a href="<?php echo e(url('/admincustomer')); ?>">View All</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-success card-header-icon">
                  <div class="card-icon">
                   <i class="fa fa-sitemap" aria-hidden="true"></i>

                  </div>
                  <p class="card-category">Organisers</p>
                  <h3 class="card-title"><?php echo e($totalOrganisersCount); ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-danger card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                  </div>
                  <p class="card-category">Total Events</p>
                  <h3 class="card-title"><?php echo e($totalEventCount); ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
              <div class="card card-stats">
                <div class="card-header card-header-info card-header-icon">
                  <div class="card-icon">
                    <i class="fa fa-database" aria-hidden="true"></i>



                  </div>
                  <p class="card-category">Data Requests</p>
                  <h3 class="card-title"><?php echo e($data_request_Count); ?></h3>
                </div>
                <div class="card-footer">
                  <div class="stats">
                    <!-- <?php $data =  DB::table('requestdata')
                            ->where('deleted', 0)
                           ->orderBy('id','DESC')
                           ->first(); 
                        $id=$data->event_id;?> -->

                  
                    <a href="<?php echo e(url('/postsevent')); ?>">View All</a>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Customers</h4>
                  
                </div>
                <div class="card-body">
                 <table class="table table-hover">
               
                <thead>
                    <tr class="text-warning">
                        <th >Image</th>
                        <th >Name</th>
                        <th >Registered</th>
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($topFiveCustomers)>0): ?>
                     <?php $__currentLoopData = $topFiveCustomers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata">

                        <td >
                        <?php if($customer->profile_picture!=''): ?> 
                        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($customer->profile_picture); ?>" class="img-responsive" style="width: 40px; height: 30px;">
                         <?php else: ?>
                         <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" style="width: 40px; height: 30px;">
                       <?php endif; ?>
                        
                      </td>
                       
                        <td ><?php echo e($customer->first_name); ?></td>
                    
                        <td ><?php echo e(\Carbon\Carbon::parse($customer->created_at)->format('d M Y')); ?></td>
  
                      
                    <td class="action_btns 3"> 
                     <a class="tablebtn" href="<?php echo e(route('admincustomer.show',$customer->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>  </td>                     
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                    
                    <?php endif; ?>

        </tbody>
      </table>

                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="card">
                <div class="card-header card-header-warning">
                  <h4 class="card-title">Organisers</h4>
                  
                </div>
                <div class="card-body table-responsive">
                  <table class="table table-hover">
                <thead >
                    <tr class="text-warning">
                       <th> Image</th>
                        <th >Name</th>
                       
                        <th >Registered</th>
                       
                        <th >Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($topFiveOrganisers)>0): ?>
                     <?php $__currentLoopData = $topFiveOrganisers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photographer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata">

                      <td >
                        <?php if($photographer->profile_picture!=''): ?> 
                        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($photographer->profile_picture); ?>" class="img-responsive" style="width: 40px; height: 30px;">
                         <?php else: ?>
                        <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" style="width: 40px; height: 30px;">
                       <?php endif; ?>
                        
                      </td>
                        
                        <td ><?php echo e($photographer->first_name); ?></td>
                       
                       
                       
                        <td ><?php echo e(\Carbon\Carbon::parse($photographer->created_at)->format('d M Y')); ?></td>
                       
                        

                    <td class="action_btns 2"> 
                    
                     <a class="tablebtn" href="<?php echo e(route('adminorganiser.show',$photographer->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                    
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     
                    <?php endif; ?>

        </tbody>
      </table>
                </div>
              </div>
            </div>
          </div>

       

 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>