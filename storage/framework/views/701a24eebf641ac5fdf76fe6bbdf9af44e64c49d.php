<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
  <!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-12 dashboard_right">
                <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

<div class="content">
        <div class="container-fluid">
  <div class="row">
       
          <div class="col-sm-7 table-search searchbar">
        <form class="navbar-form navbar-right" name="searchfrm" id="searchfrm" method="GET" action="<?php echo e(route('admincustomer.index')); ?>">
          <span class="bmd-form-group"><div class="input-group input-group-sm" "="">
        <!-- <div class="form-group">
          <label>Search</label>
          <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="<?php echo e($search_data); ?>" placeholder="Search here">
        </div> -->

                  <input type="text" class="form-control" placeholder="Search here" name="search_input" id="s" value="<?php echo e($search_data); ?>" placeholder="Search here">
                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </span>
      </form>
    </div>
       <div class="col-sm-5 selectdiv topbar_selectiv text-right createbtn">
         <a class="btn btn-primary" href="<?php echo e(route('admincustomer.create')); ?>">  Create New Customers  </a>
   </div>
      </div>

      <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Customers</h4>
                  <p class="card-category"></p>
                </div>            

               <div class="card-body">
                <div class="table-responsive">
                 <table class="table">
                <thead class=" text-primary">
                    <tr>
                        <th class="">Image</th>
                        <th class="">Name</th>
                        <th class="">Email</th>
                        <th class="">Phone</th>
                        <th class="">Registered</th>
                        <th class="">Status</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($customers)>0): ?>
                     <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata">

                        <td>
                        <?php if($customer->profile_picture!=''): ?> 
                        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($customer->profile_picture); ?>" class="img-responsive" style="width: 40px; height: 30px;">
                         <?php else: ?>
                        <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" style="width: 40px; height: 30px;">
                       <?php endif; ?>
                        
                      </td>
                       
                        <td class=""><?php echo e($customer->first_name); ?></td>
                       
                        <td class=""><?php echo e($customer->email); ?></td>
                       
                        <td class=""><?php echo e($customer->phone); ?></td>
                       
                        <td class=""><?php echo e(\Carbon\Carbon::parse($customer->created_at)->format('d M Y')); ?></td>
                       
                        <!-- <td class="">
                          <?php
                          if($customer->status==1) {
                            echo 'Active';
                          } else {
                            echo 'Inactive';
                          }
                          ?>
                        </td> -->
                       
                      
                    <td>
                    <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                               
                            </a>

                           <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">

                    <label> Customer </label> 
                     <a class="dropdown-item" href="<?php echo e(route('admincustomer.edit',$customer->id)); ?>" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-edit"></i>Edit</a>
                     <a class="dropdown-item" href="<?php echo e(route('admincustomer.show',$customer->id)); ?>" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i>View</a>
                    
                      <a class="dropdown-item">
                         <form name="request" id="request" class="request" method="GET" action="<?php echo e(url('/')); ?>/destroy/<?php echo e($customer->id); ?>">
                         <button type="submit" class="del_sec_mew"><i class="fa fa-trash"></i>Delete</button>
                     </form>
                   </a>
                     </div>
                   </div>
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>
    </div>

        
 <?php if(count($customers) > 0): ?>
<div class="pagination_section">

   <div class="col-sm-3 selectdiv text-right">
    <form name="" id="page" method="get" action="<?php echo e(url('/admincustomer')); ?>" >
    <label>Showing:</label>
    <select name="perpage" id="perpage" onchange="document.getElementById('page').submit();">
       <option value="10" <?php if($perpage=='10'): ?><?php echo e('selected'); ?><?php endif; ?>>10</option>
      <option value="20" <?php if($perpage=='20'): ?><?php echo e('selected'); ?><?php endif; ?>>20</option>
      <option value="50" <?php if($perpage=='50'): ?><?php echo e('selected'); ?><?php endif; ?>>50</option>
      <option value="100" <?php if($perpage=='100'): ?><?php echo e('selected'); ?><?php endif; ?>>100</option>
    </select>
    </form>
  </div>
<div class="col-sm-4 total_div">
 
</div>
<div class="col-sm-5 pagination_div">
<nav aria-label="Page navigation">

  <?php echo $customers->render(); ?>

</nav>

  </div>
  

</div>
<?php endif; ?>
      </div>
         
      </div>
    </div>
                       

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>