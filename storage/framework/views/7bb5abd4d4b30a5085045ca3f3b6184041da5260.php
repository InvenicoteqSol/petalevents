<?php $__env->startSection('content'); ?>
<div class="clearfix"> </div>
<!-- --------------------//banner-slide------------------ -->

    <!-- section-upcoming-events -->
  <section class="section-event-details">
    <div class="container">
       <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12">
           <div class="event-info">
           <span class="card-title"><?php echo e($events[0]->event_title); ?></span>
           <p class="event-summry"><?php echo \Illuminate\Support\Str::words($events[0]->event_info); ?></p>
          </div>
          </div>
           <div class="col-md-5 col-sm-5 col-xs-12">
           <div class="event-info">
           <span class="card-title">Venue Information</span>
           <span class="e-loc">
      <span class="e-loc">
      
      <span class="l-con"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
      <span class="l-name"><label><?php echo e($events[0]->venue_name); ?></label><br><?php echo e($events[0]->venue_address); ?></span>
      <span class="contat-col"><?php echo e($events[0]->telephone); ?></span>
    </span>
          </div>
        </div>
       </div>
       <!-- //row end --> 
     </div>

          <?php
        $d= strtotime("today");
        $date = date("Y-m-d", $d);
        $date1=  date("h:i:sa",$d);  
           ?>

      <div class="container buy-tickets">
         <div class="row ">
          <div class="col-md-12 col-xs-12">
            <div class="row-heading">
              <span>Buy Tickets</span>
            </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-12">
            <table class="table table-hover" id="tickets">
<thead>
<tr>
<th>Ticket Type</th>
<th class="">Ticket Details</th>
<th>Price</th>
<th class="quantity">Quantity</th>
</tr>
</thead>
<tbody>
 <?php if(count($tickets)>0): ?>
 
 <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
<td class="ticket-type">
<h3><?php echo e($ticket->ticket_type); ?></h3>
<input type="hidden" value="<?php echo e($ticket->id); ?>" id="ticketid">
</td>
<td class="date-cell">
<nobr><?php echo \Illuminate\Support\Str::words($ticket->ticket_details); ?></nobr>
</td>
<td class="price-cell">

                   <!--  <?php if($ticket->prices> '0'): ?> 
                        <td class="column3">
                        <?php $amount=$ticket->prices;
                        $total=$amount+($amount*(10/100));
                        ?> 
                       <?php echo e($total); ?></td>
                       <?php else: ?>
                       <td class="column3"> <?php echo e($ticket->amount); ?> </td> 
                      
                       <?php endif; ?>  -->

<span class="c_value"><?php echo e($ticket->prices); ?><span>
</td>
<td class="quantity-cell">
  <?php 
  $stockAvailable=DB::table('orders')->where('event_id','=',$events[0]->id)->where('ticket_id','=',$ticket->id);
   $stockcount=($ticket->stock-$stockAvailable->count());
  ?>
    <?php if((($date>=$ticket->start_date)||($date1>=$ticket->start_time))&&(($date<=$ticket->end_date)||($date1<=$ticket->end_time))): ?>
    <?php if($ticket->stock_status=='Available'): ?>
    <?php if($stockcount>0): ?>
    <select name="" >
    <?php for($i = 0; $i <= $stockcount; $i++): ?>
    <option value="<?php echo e($i); ?>"> <?php echo e($i); ?> </option>
    <?php endfor; ?>
    </select>
    <?php else: ?>
    <h3>Sold Out</h3>
    <?php endif; ?>
    <?php else: ?>
     <h3>Unavailable</h3>
    <?php endif; ?>
    <?php else: ?>
    <h3>Sale of tickets closed.</h3>
    <?php endif; ?>
</td>
</tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php else: ?>
  <?php endif; ?>

</tbody>
</table>
<div class="t_action">
  <!--<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>-->
<div class="btn-group" role="group">
  <a href="#" id="mybasket" class="btn btn-primary"> Add To My Basket!</a>
<a href="#" id="mycart" class="btn btn-primary"> Buy Ticket Now!</a>
</div>

</div>

</div>
          <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="event-card">
            	<?php if($events[0]->event_image!="" ): ?>
              <img class="" src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($events[0]->event_image); ?>"><?php else: ?>
              <img class="" src="<?php echo e(url('public')); ?>/front/images/login-vector.png">
              <?php endif; ?>
            </div>
          </div>
         </div>
         <!-- //row end --> 
    </div>
 </section>
  <!-- //section-upcoming-events -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {

$("#mybasket").click(function(){
  var table = $("#tickets tbody");

    table.find('tr').each(function (i) {
        var $tds = $(this).find('td'),
            tickettype = $tds.find("#ticketid").val(),
           
            Quantity = $tds.find("select").val();
            
            console.log('tickettype'+tickettype+', quantity'+Quantity);
            if(Quantity>0)
            {
             jQuery.ajax({
              method : 'GET',
            url  : "<?php echo e(url('/addtocart')); ?>",
            data :{'tickettype': tickettype,'quantity':Quantity},
            success :  function(resp) {
              $("#cartCount").text(resp);
              $("#cartbody").html("");
              $("#cartbody").append($('<div>').load("<?php echo e(url('/minCart')); ?>"));
              $.notify({  // options
             message: 'Basket Updated' 
                },{
                  // settings
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
             }
            });
            }
            
        
       
    });
  });



   $("#mycart").click(function(){
  var table = $("#tickets tbody");

    table.find('tr').each(function (i) {
        var $tds = $(this).find('td'),
            tickettype = $tds.find("#ticketid").val(),
           
            Quantity = $tds.find("select").val();
            if(Quantity>0)
            {
             jQuery.ajax({
              method : 'GET',
            url  : "<?php echo e(url('/addtocart')); ?>",
            data :{'tickettype': tickettype,'quantity':Quantity},
            success :  function(resp) {
              
              $.notify({  // options
             message: 'Basket Updated Redirecting To Cart...' 
                },{
                  // settings
                  type: 'success',
                  allow_dismiss: true,
                  newest_on_top: true,
                  placement: {
                    from: "top",
                    align: "center"
                  }
                });
              window.location.href = "<?php echo e(url('/cart')); ?>";
             }
            });
            }
            
        
       
    });
  });
   });
</script>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>