<?php $__env->startSection('content'); ?>
   <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
   <div class="col-sm-12 dashboard_right">
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
        <?php // echo $id_evnt=request()->route('id');?>

            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                 <h4 class="card-title">Post-event</h4>
                 <p class="card-category"> </p>
        </div>
       <div class="col-sm-12 tablediv spacertop card-body">
      <div class="tablewrapper table-responsive">
      <table class="table-bordered table-striped table">
                <thead class=" text-primary">
                   <tr class="headings">
                        <th class="column3">Event title</th>
                        <th class="column3">Type of Event</th>
                        <th class="column3">Name</th>
                        <th class="column3">Email address</th>
                        <th class="column3">Phone</th>
                        <th class="column2">Action</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($Postevents)>0): ?>
                     <?php $__currentLoopData = $Postevents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $Postevent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                     
                      <tr class="tabledata odd">
                         <?php 
                        $event_id=$Postevent->event_id;
                        $eventdatas = DB::table('eventbookings')
                               ->where('id','=',$event_id)
                               ->first();
                          $user_id=$eventdatas->user_id;
                         
                         $orgdatas = DB::table('users')
                               ->where('id','=',$user_id)
                               ->first();
                        ?>
                    <td class="column3"><?php echo e($eventdatas->event_title); ?></td>
                    <td class="column3"> <?php echo e($eventdatas->type_of_event); ?> </td>
                     <td class="column3"><?php if($orgdatas): ?><?php echo e($orgdatas->first_name); ?> <?php else: ?>  <?php endif; ?></td>
                     <td class="column3"><?php if($orgdatas): ?><?php echo e($orgdatas->email); ?> <?php else: ?> <?php endif; ?></td>
                     <td class="column3"><?php if($orgdatas): ?><?php echo e($orgdatas->phone); ?> <?php else: ?> <?php endif; ?></td>
                   <td class="column2 action_btns"> 
                     <a class="tablebtn" href="<?php echo e(route('postsevent.show',$Postevent->id)); ?>" title="Customer Data" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye"></i></a>
                        </td>
                        
                    </tr>
                    
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                    

        </tbody>
      </table>

       </div>
    </div>
    </div>
  </div>
</div>
                       
            
        </div>
    </div>
    

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

  <script type="text/javascript">

    $(document).ready(function () {        

        $('[data-toggle=confirmation]').confirmation({

            rootSelector: '[data-toggle=confirmation]',

            onConfirm: function (event, element) {

                element.closest('form').submit();

            }

        });   

    });

</script>
<style type="text/css">
.form-group {
    margin-bottom: 15px;
    float: left;
    width: 100%;
}
.searchbar form {
    width: auto !important;
    padding: 0px;
}
tr.tabledata.odd td {
    padding: 8px 0 9px 10px;
}

</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>