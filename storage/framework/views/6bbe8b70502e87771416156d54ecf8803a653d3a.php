<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>

    
    <div class="col-sm-10 dashboard_right dasbagcol">
      <div class="col-sm-12 sectionbox">
        <p class="pagetite">Event</p>
        <div class="col-sm-6">
          <p class="rightsidetext"><span class="lablname">Event Date :</span> <span class="labldata"><?php if($eventbooking->event_date!='' || $eventbooking->event_date!='0000-00-00'  || $eventbooking->event_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($eventbooking->event_date)->format('d M, Y')); ?><?php } ?> </span></p>

          <p class="rightsidetext"><span class="lablname">Event title :</span> <span class="labldata"><?php echo e($eventbooking->event_title); ?></span></p>

          <p class="rightsidetext"><span class="lablname">Supporting art :</span> <span class="labldata"><?php echo e($eventbooking->supporting_art); ?></span></p>                           

        </div>


      </div>

  <div class="col-sm-12 sectionbox">
        <div class="col-sm-12"> <p class="rightsidetext"><span class="lablnamed">Event info :</span> <span class="labldata"><?php echo e($eventbooking->event_info); ?></span></p> </div>
        
        </div>
            


        </div>
    </div>
      </div>
    </div>
    <style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
 span.lablnamed {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
p.pagetite {
    font-size: 24px;
    padding-left: 15px;
    
}
</style>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>