<?php $__env->startSection('content'); ?>

<section id="organiser-register">
     <div class="container">
          <div class="row">
      <p style="text-align: center;"><?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> </p>

             <div class="col-md-12 col-sm-12 col-xs-12 col-head-area">
        <h2 class="main-s-heading">Customers Registeration</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen bookDD.</p>
      </div>
             </div>
             <div class="row">
              
               <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- CONTACT FORM HERE -->
                              <form id="contact-form" class="settingform" action="<?php echo e(url('customersregistration/insert')); ?>" method="POST" enctype="multipart/form-data">
                                                        <?php echo e(csrf_field()); ?>


                              <div class="col-md-6 col-sm-6">
                               <!--  form-group -->
                                <div class="form-group">
                                   <input type="text" class="form-control" name="first_name" placeholder="First name" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                   <div class="form-group">
                                  <input id="phone" type="text" class="form-control" name="phone" value="" minlength="10" maxlength="13" placeholder="Phone" required="">
                                 </div>
                                 <!--  //form-group -->
                                 <!--  form-group -->
                                  <div class="form-group">
                                  <input id="username" type="text" class="form-control" name="username" minlength="6" maxlength="17" placeholder="Username" required="">
                                 </div>
                                 <div class="form-group">
                                  <input id="password" type="password" class="form-control" name="password" minlength="6" maxlength="17" placeholder="Password" required="">
                                   </div>
                                       
                                 <!--  //form-group -->
                                 </div>
                                 <div class="col-md-6 col-sm-6">
                                                                 
                                          <!--  form-group -->

                                 <div class="form-group">
                           <input type="text" class="form-control" name="last_name" placeholder="Last name" required="">
                                 </div>

                                 
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                  <div class="form-group">
                                  <input id="email" type="email" class="form-control" name="email" value="" maxlength="191" placeholder="Email" required="">
                                 </div>
                                 <!--  //form-group -->
                                  <!--  form-group -->
                                 <div class="form-group form-gender">
                                 <label class="radiodiv">Gender</label>
                                  <ul>
                                        <li>
                                          <input type="radio" id="male"  name="gender" id="gender1" value="Male">
                                          <label for="male">Male</label>
    
                                          <div class="check"></div>
                                        </li>
  
                                        <li>
                                          <input type="radio" id="female"  name="gender" id="gender2" value="Female">
                                          <label for="female">Female</label>
    
                                          <div class="check"><div class="inside"></div></div>
                                        </li>
                                
                                      </ul>
                                 </div>
                                   <input type="hidden" value="3" name="user_type" id="user_type"> 




                                
                                 <!--  //form-group -->
                                 
                                </div>
                              
                              <div class="col-md-12 col-sm-12 form-action">
                              
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                                                 
                              </div>
                        </form>
                   
               </div>


          </div>
     </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>