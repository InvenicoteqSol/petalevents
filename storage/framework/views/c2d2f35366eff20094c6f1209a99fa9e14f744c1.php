<?php $__env->startSection('content'); ?>
<div class="clearfix"> </div>
<!-- --------------------//banner-slide------------------ -->

    <!-- section-upcoming-events -->
  <section class="section-event-details">
    <div class="container">
       <div class="row">
        <div class="col-md-7 col-sm-7 col-xs-12">
           <div class="event-info">
           <span class="card-title"><?php echo e($events[0]->event_title); ?></span>
           <p class="event-summry"><?php echo e($events[0]->event_info); ?></p>
          </div>
          </div>
           <div class="col-md-5 col-sm-5 col-xs-12">
           <div class="event-info">
           <span class="card-title">Venue Information</span>
           <span class="e-loc">
      <span class="e-loc">
      
      <span class="l-con"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
      <span class="l-name"><label><?php echo e($events[0]->venue_name); ?></label><br><?php echo e($events[0]->venue_address); ?></span>
      <span class="contat-col"><?php echo e($events[0]->telephone); ?></span>
    </span>
          </div>
        </div>
       </div>
       <!-- //row end --> 
     </div>
      <div class="container buy-tickets">
         <div class="row ">
          <div class="col-md-12 col-xs-12">
            <div class="row-heading">
              <span>Buy Tckets</span>
            </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-12">
            <table class="table table-hover">
<thead>
<tr>
<th>Ticket Type</th>
<th class="">Ends</th>
<th>Price</th>
<th class="quantity">Quantity</th>
</tr>
</thead>
<tbody>
	<?php if(count($tickets)>0): ?>
                     <?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
<td class="ticket-type">
<h3><?php echo e($ticket->ticket_type); ?></h3>
</td>
<td class="date-cell">
<nobr><?php if($ticket->end_date!='' || $ticket->end_date!='0000-00-00'  || $ticket->end_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($ticket->end_date)->format('d M, Y')); ?><?php } ?></nobr>
</td>
<td class="price-cell">
<span class="c_value"><?php echo e($ticket->prices); ?><span>
</td>
<td class="quantity-cell">
	<?php 
	$stockAvailable=DB::table('orders')->where('event_id','=',$events[0]->id)->where('ticket_id','=',$ticket->id);
	$stockcount=($ticket->stock-$stockAvailable->count());
	?>
	<select name="">
	<?php if($stockcount>0): ?>
	{
 	<?php for($i = 0; $i <= $stockcount; $i++): ?>
        <option value="<?php echo e($i); ?>"> <?php echo e($i); ?> </option>
    <?php endfor; ?>
   	}
	<?php else: ?>
	
 	<?php for($i = 0; $i <= 5; $i++): ?>
        <option value="<?php echo e($i); ?>"><?php echo e($i); ?></option>
    <?php endfor; ?>
	<?php endif; ?>
	</select>


</td>
</tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>

                      <?php endif; ?>

</tbody>
</table>
<div class="t_action">
  <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>

<a href="" class="btn btn-primary"> Buy Ticket Now!</a>
</div>

</div>
          <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="event-card">
            	<?php if($events[0]->event_image!="" ): ?>
              <img class="" src="<?php echo e(url('public')); ?>/uploads/post/feature_post/<?php echo e($events[0]->event_image); ?>"><?php else: ?>
              <img class="" src="<?php echo e(url('public')); ?>/front/images/login-vector.png">
              <?php endif; ?>
            </div>
          </div>
         </div>
         <!-- //row end --> 
    </div>
 </section>
  <!-- //section-upcoming-events -->



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>