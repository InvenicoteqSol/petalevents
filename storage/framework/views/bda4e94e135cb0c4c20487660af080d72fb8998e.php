<!DOCTYPE html>
<html lang="eng">
  <head>
    <title>Petal Events</title>
    <!--meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="" />
<link href="<?php echo e(url('/public')); ?>/front/css/bootstrap.css" rel="stylesheet" type="text/css" media="all">
    <!--//booststrap end-->
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo e(url('/public')); ?>/front/css/owl.theme.default.css">
    <!-- font-awesome icons -->
   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="<?php echo e(url('/public')); ?>/front/css/style.css" rel='stylesheet' type='text/css' media="all">
    <!--//stylesheets-->
   <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
   </head>
  <body class="page-login">
  <section class="section-login">
  <div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12">
          <label class="l-heading">Forgot Password.</label>
        </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
          <img class="img-login" src="<?php echo e(url('/public')); ?>/images/login-vector.png">
        </div>
      <div class="col-md-6 col-sm-6 col-xs-12">
           <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
      <form class="form-login" id="login_form" method="POST" action="<?php echo e(url('forgotpassword')); ?>">
        <?php echo e(csrf_field()); ?>

         <p>Please enter your email to reset the password.</p>
          <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
            <label>Enter Your Email*</label>
            <input type="email" class="form-control" id="email" placeholder="" name="email" value="<?php echo e(old('email')); ?>" required>
            <?php if($errors->has('email')): ?>
              <span class="help-block">
                  <strong><?php echo e($errors->first('email')); ?></strong>
              </span>
          <?php endif; ?>
          </div>
      <button type="submit" class="btn btn-default">Send</button>
      </form>
</div>
</div>
</div>
<!--login-content-end-->
</section>

 <!--  footer -->
   
    <div class="copy-agile-right text-center pt-lg-4 pt-3">
          <p> 
            © 2019 |<a href="#">Petal Events</a>| All Rights Reserved 
          </p>
        </div>
    <!--//footer -->
   <script src="<?php echo e(url('/public')); ?>/front/js/jquery-1.11.3.min.js"></script>
   <script src="<?php echo e(url('/public')); ?>/front/front/js/bootstrap.min.js"></script>
   <script src="<?php echo e(url('/public')); ?>/front/js/owl.carousel.js"></script>
<script src="<?php echo e(url('/public')); ?>/front/js/login.js"></script>     

    
  </body>
</html>
           
