<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
      <div class="col-sm-10 dashboard_right">
        <div class="col-sm-12">
          <div class="current_sessiondiv">
          <div class="col-sm-8 current_sessiontitle">
            <h3>Your Current Session</h3>
            <p>You dont have any Upcoming Session</p>
          </div>
        <div class="col-sm-4 user_name">
          <h2>Welcome , <?php echo e($photographer->first_name); ?></h2>
        </div>
      </div>
    </div>
      <div class="col-sm-12 editbtn text-right">
        <a href="<?php echo e(route('adminprofiles.edit',$photographer->id)); ?>">Edit</a>
      </div>
      <div class="profile_sec">
        <div class="col-sm-2 profile_img text-center">
          
            <?php if(Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL'): ?>
              <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($photographer->profile_picture); ?>" class="img-circle">
            <?php else: ?>
            <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" class="img-circle">
            <?php endif; ?>
            <p><?php echo e(Auth::user()->name); ?> <br/><span>Admin</span></p>
         
          <ul class="social_media">
            <li><a href="#"><i class="fa fa-facebook-official" id="fbicon"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter-square" id="twitricn"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" id="instaicon"></i></a></li>
          </ul>
        </div>
        <div class="col-sm-10 prflinfo">
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-user"></i><span><?php echo e($photographer->name); ?></span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-phone"></i><span><?php echo e($photographer->phone); ?></span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-envelope"></i><span><?php echo e($photographer->email); ?></span>
          </div>
          <div class="col-sm-12 prflinfo_div">
            <i class="fa fa-file"></i><span>Categories - Anniversary , Birthday  ,Bridal Shower ,Bridal  ,Boudoir  , Couple   Corporate Event</span>
          </div>
            <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-map-marker"></i><span><?php echo e($photographer->country); ?></span>
          </div>
            <!-- <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-calendar"></i><span>12456-45785</span>
          </div> -->
          <?php if($photographer->description != ''): ?>
          <div class="col-sm-12 aboutdiv">
            <h4>About</h4>
            <p><?php echo e($photographer->description); ?></p>
          </div>
          <?php endif; ?>
        </div>
      </div>
</div>

</div>
<!--booking-sec-end-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>