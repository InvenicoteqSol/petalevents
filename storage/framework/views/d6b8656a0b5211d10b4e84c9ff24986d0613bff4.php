<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
   <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>PETAL EVENTS</title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?php echo e(url('/public')); ?>/assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?php echo e(url('/public')); ?>/assets/demo/demo.css" rel="stylesheet" />
  <link href="<?php echo e(url('/public')); ?>/assets/css/jquery.timepicker.css" rel="stylesheet" />
  <script src="<?php echo e(url('/public')); ?>/assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?php echo e(url('/public')); ?>/assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?php echo e(url('/public')); ?>/assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="<?php echo e(url('/public')); ?>/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  
  <!-- Chartist JS -->
  <script src="<?php echo e(url('/public')); ?>/assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?php echo e(url('/public')); ?>/assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?php echo e(url('/public')); ?>/assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="<?php echo e(url('/public')); ?>/assets/demo/demo.js"></script>
  
  <script src="<?php echo e(url('/public')); ?>/js/bootstrap-datepicker.js"></script>
  <script src="<?php echo e(url('/public')); ?>/assets/js/moment.js"></script>
  <script src="<?php echo e(url('/public')); ?>/assets/js/jquery.timepicker.js"></script>
  
  <link href="<?php echo e(url('/public')); ?>/assets/css/style.css" rel="stylesheet" />
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>


  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
    <!-- Scripts -->
    <!-- <script src="<?php echo e(url('public')); ?>/js/app.js"></script> -->

</head>

<body class="dark-edition">
    <div class="wrapper">
     <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <div class="main-panel"> 
      <?php echo $__env->make('layouts.dashboardheader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="content">
        <div class="container-fluid">

      <?php echo $__env->yieldContent('content'); ?>
       </div>
        </div>
        <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="#">
                  Petal Events
                </a>
              </li>
              <li>
                <a href="#">
                  About Us
                </a>
              </li>
              <li>
                <a href="#">
                  Blog
                </a>
              </li>
              
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> for
            <a href="#" target="_blank">Petal Events</a> 
          </div>
        </div>
      </footer>
     </div>
      
    </div>


</body>
</html>