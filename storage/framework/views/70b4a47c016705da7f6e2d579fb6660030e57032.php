<?php $__env->startSection('content'); ?>

      <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
      <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-12 dashboard_right">
        <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="col-sm-12 nopadding">
  <div class="row">
        
        <div class="col-sm-12 selectdiv topbar_selectiv text-right createbtn">
        <?php if(Auth::user()->user_type=='1'){ ?>
        <h3><a class="btn btn-primary" href="<?php echo e(url('exportorders')); ?>" class="btn_expt">EXPORT CSV</a></h3>
       <?php } elseif(Auth::user()->user_type=='3'){ ?>
        <h3><a class="btn btn-primary" href="<?php echo e(url('exportorders')); ?>" class="btn_expt">EXPORT CSV</a></h3>
       <?php } elseif(Auth::user()->user_type=='4'){ ?>
        <h3><a class="btn btn-primary" href="<?php echo e(url('exportorders')); ?>" class="btn_expt">EXPORT CSV</a></h3>
       <?php } ?>
    </div>

  </div>
      </div>

           
      <div class="col-sm-12 tablediv spacertop">
        <?php if(Auth::user()->user_type=='1'): ?>
        <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">Event Orders</h4>
                  <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
                </div>
<div class="card-body table-responsive">
<table class="table table-hover ">
          <thead class=" text-primary">

                    <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Name</th>
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php if(count($orders)>0): ?>
                     <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">
                      <td class="column3"><?php echo e(date('Y-M-d',strtotime($order->created_at))); ?></td>
                      <td class="column3"><?php echo e($order->order_number); ?></td>
                      <td class="column3"><?php echo e($order->first_name); ?></td>
                      <td class="column3"><?php echo e($order->amount); ?></td>
                      <?php if($order->order_status_id == '1'): ?> <td class="column3"><?php echo e('Completed'); ?></td><?php elseif($order->order_status_id == '0'): ?> <td class="column3"><?php echo e(' Awaiting Payment '); ?></td><?php else: ?>  <?php endif; ?> 
                      
                         <td class="action_btns column3"> 
                          
<div class="modal" id="linkmodal_<?php echo e($order->id); ?>" tabindex="-1" role="dialog" aria-labelledby="linkmodal_<?php echo e($order->id); ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : <?php echo e($order->order_number); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> <?php echo e($order->first_name); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> <?php echo e($order->last_name); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> <?php echo e($order->amount); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  <?php echo e($order->order_number); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br> <?php echo e(date('Y-M-d',strtotime($order->created_at))); ?>

                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>
                                Event Name 
                            </th>
                              <th>
                                Ticket Type
                            </th>
                             <th>
                                Quantity
                            </th>
                            
                            <th>
                                Price
                            </th>
                            <!-- <th>
                             Discount
                            </th> -->
                            
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <?php 
                           $id=$order->id;
                           $orders_items=DB::table('order_items')
                                        ->Where('order_id','=',$id)->get(); ?> 
                           <?php $__currentLoopData = $orders_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php $tickets_id= $order_item->ticket_id;
                                 $event_id=$order_item->event_id;
                           $tickets=DB::table('tickets')
                                        ->Where('id','=',$tickets_id)->first(); 
                           $eventbooking=DB::table('eventbookings')
                                        ->Where('id','=',$event_id)->first(); ?>
                              <tr>   
                                  <td>
                                    <?php echo e($eventbooking->event_title); ?>  
                                    </td>
                                   <td>
                                    <?php echo e($tickets->ticket_type); ?>  
                                    </td>
                                    
                                    <td>
                                    <?php echo e($order_item->Quantity); ?>  
                                    </td>
                                     
                                    <td>
                                     <?php echo e($order_item->netPrice); ?>

                                    </td>
                                    <!--  <td>
                                    <?php echo e($order_item->discountAmount); ?>  
                                    </td> -->
                                   
                                    <td>
                                      <?php echo e($order_item->totalPrice); ?> 
                                      
                                     </td>
                                    
                               
                                     </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                  
 
                                </tbody>

                        </table>
                     <div style="float: right;"><b style="font-weight: 600;">Total Amount: </b> <strong style="font-weight: 500;"><?php echo e($order->amount); ?> </strong> </div>

                    </div>
                </div>
              
            </div> <!-- /end modal body-->
          
            <div class="modal-footer">
              
             
           <button class="btn modal-close btn-danger" data-dismiss="modal" type="button">Close</button>
         
            </div>
        </div>
</div>
</div>
</div>
                  <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="dropdown-item" href="<?php echo e(url('/cancelorder')); ?>/<?php echo e($order->id); ?>">Cancel</a>
                     <a class="dropdown-item" data-toggle="modal" data-target="#linkmodal_<?php echo e($order->id); ?>" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Details</a>
                     </div>
                     </div>  
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>
  </div>
</div>
<?php elseif(Auth::user()->user_type=='4'): ?>
<div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">My Event Orders</h4>
                  <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
                </div>
 <?php $id = Auth::user()->id; 
   $ordersorgniser = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->where('users.id','=',$id)
                             ->get(); ?>
 <div class="card-body table-responsive">
<table class="table table-hover ">
          <thead class=" text-primary">

                    <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Name</th>
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php if(count($ordersorgniser)>0): ?>
                     <?php $__currentLoopData = $ordersorgniser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">
                      <td class="column3"><?php echo e(date('Y-M-d',strtotime($order->created_at))); ?></td>
                      <td class="column3"><?php echo e($order->order_number); ?></td>
                      <td class="column3"><?php echo e($order->first_name); ?></td>
                      <td class="column3"><?php echo e($order->amount); ?></td>
                      <?php if($order->order_status_id == '1'): ?> <td class="column3"><?php echo e('Completed'); ?></td><?php elseif($order->order_status_id == '0'): ?> <td class="column3"><?php echo e(' Awaiting Payment '); ?></td><?php else: ?>  <?php endif; ?> 
                      
                         <td class="action_btns column3"> 
                          
<div class="modal" id="linkmodal_<?php echo e($order->id); ?>" tabindex="-1" role="dialog" aria-labelledby="linkmodal_<?php echo e($order->id); ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : <?php echo e($order->order_number); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> <?php echo e($order->first_name); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> <?php echo e($order->last_name); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> <?php echo e($order->amount); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  <?php echo e($order->order_number); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br> <?php echo e(date('Y-M-d',strtotime($order->created_at))); ?>

                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                               <th>
                                Event Name 
                            </th>
                              <th>
                                Ticket Type
                            </th>
                             <th>
                                Quantity
                            </th>
                            
                            <th>
                                Price
                            </th>
                            
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <?php 
                           $id=$order->id;
                           $orders_items=DB::table('order_items')
                                        ->Where('order_id','=',$id)->get(); ?> 
                           <?php $__currentLoopData = $orders_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php $tickets_id= $order_item->ticket_id;
                                 $event_id=$order_item->event_id;
                           $tickets=DB::table('tickets')
                                        ->Where('id','=',$tickets_id)->first(); 
                           $eventbooking=DB::table('eventbookings')
                                        ->Where('id','=',$event_id)->first(); ?>
                    
                              <tr>   

                                     <td>
                                    <?php echo e($eventbooking->event_title); ?>  
                                    </td>
                                   <td>
                                    <?php echo e($tickets->ticket_type); ?>  
                                    </td>
                                    <td>
                                    <?php echo e($order_item->Quantity); ?>  
                                    </td>
                                     
                                    <td>
                                     <?php echo e($order_item->netPrice); ?>

                                    </td>
                                
                                    <td>
                                      <?php echo e($order_item->totalPrice); ?>

                                     </td>
                                    
                               
                                     </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 
                            </tbody>
                        </table>
                              <div style="float: right;"><b style="font-weight: 600;">Total Amount: </b> <strong style="font-weight: 500;"><?php echo e($order->amount); ?> </strong> </div>
                    </div>
                </div>

              
            </div> <!-- /end modal body-->

            <div class="modal-footer">
               <button class="btn modal-close btn-danger" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
</div>
</div>
</div>
                  <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="dropdown-item" href="<?php echo e(url('/cancelorder')); ?>/<?php echo e($order->id); ?>">Cancel</a>
                     <a class="dropdown-item" data-toggle="modal" data-target="#linkmodal_<?php echo e($order->id); ?>" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Details</a>
                     </div>
                     </div>  
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>
  </div>
</div>
  <?php elseif(Auth::user()->user_type=='3'): ?>
  <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                 <h4 class="card-title">My Tickets</h4>
                  <!-- <p class="card-category"> Here is a subtitle for this table</p> -->
                </div>
 <?php  $id = Auth::user()->id; 
   $orderscustomer = DB::table('users')
                   ->select('users.id','first_name','last_name','email','phone','orders.order_number','orders.amount','orders.created_at','orders.booking_fee','orders.id','orders.order_status_id')
                             ->join('orders', 'orders.user_id', '=', 'users.id')
                             ->where('users.id','=',$id)
                             ->get(); ?>
<div class="card-body table-responsive">
<table class="table table-hover ">
          <thead class=" text-primary">

                    <tr class="headings">
                        <th class="column3">Order Date</th>
                        <th class="column3">Order No</th>
                        <th class="column3">Name</th>
                        <th class="column3">Amount</th>
                        <th class="column3">Status</th>
                        <th class="column3">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php if(count($orderscustomer)>0): ?>
                     <?php $__currentLoopData = $orderscustomer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr class="tabledata odd">
                      <td class="column3"><?php echo e(date('Y-M-d',strtotime($order->created_at))); ?></td>
                      <td class="column3"><?php echo e($order->order_number); ?></td>
                      <td class="column3"><?php echo e($order->first_name); ?></td>
                      <td class="column3"><?php echo e($order->amount); ?></td>
                      <?php if($order->order_status_id == '1'): ?> <td class="column3"><?php echo e('Completed'); ?></td><?php elseif($order->order_status_id == '0'): ?> <td class="column3"><?php echo e(' Awaiting Payment '); ?></td><?php else: ?>  <?php endif; ?> 
                      
                         <td class="action_btns column3"> 
                          
<div class="modal" id="linkmodal_<?php echo e($order->id); ?>" tabindex="-1" role="dialog" aria-labelledby="linkmodal_<?php echo e($order->id); ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Order : <?php echo e($order->order_number); ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login_bg form_bg">
            <h3>Order Overview</h3>
                <style>
                    .order_overview b {
                        text-transform: uppercase;
                    }
                    .order_overview .col-sm-4 {
                        margin-bottom: 10px;
                    }
                </style>
                <div class="p0 well bgcolor-white order_overview">
                    <div class="row">
                        <div class="col-sm-6 col-xs-6">
                            <b>First Name</b><br> <?php echo e($order->first_name); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Last Name</b><br> <?php echo e($order->last_name); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Amount</b><br> <?php echo e($order->amount); ?>

                        </div>

                        <div class="col-sm-6 col-xs-6">
                            <b>Reference</b><br>  <?php echo e($order->order_number); ?>

                        </div>
                        <div class="col-sm-6 col-xs-6">
                            <b>Date</b><br> <?php echo e(date('Y-M-d',strtotime($order->created_at))); ?>

                        </div>
                        

                        
                    </div>
                </div>

                <h3>Order Items</h3>
                <div class="well nopad bgcolor-white p0">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                              <th>
                                Event Name 
                            </th>
                              <th>
                                Ticket Type
                            </th>
                             <th>
                                Quantity
                            </th>
                            <th>
                                Price
                            </th>
                           
                            <th>
                                Total
                            </th>
                            </tr></thead>
                            <tbody>
                              <?php 
                            $id=$order->id;
                           $orders_items=DB::table('order_items')
                                        ->Where('order_id','=',$id)->get(); ?> 
                           <?php $__currentLoopData = $orders_items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order_item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <?php $tickets_id= $order_item->ticket_id;
                                 $event_id=$order_item->event_id;
                           $tickets=DB::table('tickets')
                                        ->Where('id','=',$tickets_id)->first(); 
                           $eventbooking=DB::table('eventbookings')
                                        ->Where('id','=',$event_id)->first(); ?>
                            
                              <tr>   
                                   
                                   <td>
                                    <?php echo e($eventbooking->event_title); ?>  
                                    </td>
                                   <td>
                                    <?php echo e($tickets->ticket_type); ?>  
                                    </td>
                                    
                                    <td>
                                    <?php echo e($order_item->Quantity); ?>  
                                    </td>
                                     
                                    <td>
                                     <?php echo e($order_item->netPrice); ?>

                                    </td>
                                   
                                    <td>
                                      <?php echo e($order_item->totalPrice); ?>

                                     </td>
                                    
                               
                                     </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                 
                            </tbody>
                        </table>
                        <div style="float: right;"><b style="font-weight: 600;">Total Amount: </b> <strong style="font-weight: 500;"><?php echo e($order->amount); ?> </strong> </div>

                    </div>
                </div>

              
            </div> <!-- /end modal body-->

            <div class="modal-footer">
               <button class="btn modal-close btn-danger" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
</div>
</div>
</div>
                  <div class="dropdown d-action">
                              <a class="btn-act" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                               <i class="fa fa-ellipsis-v" aria-hidden="true"></i>                            
                            </a>
                             <div class="dropdown-menu dropdown-menu-right" aria-labelledby="">
                     <a class="dropdown-item" href="<?php echo e(url('/cancelorder')); ?>/<?php echo e($order->id); ?>">Cancel</a>
                     <a class="dropdown-item" data-toggle="modal" data-target="#linkmodal_<?php echo e($order->id); ?>" data-whatever="@mdo" style="cursor: pointer;" title="Send Request">Details</a> 
                     </div>
                     </div> 
                    </td>
                       
                    </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <tr class="tablesection">
                        <td colspan="7" style="text-align: center;"><b>No record found</b></td>
                    </tr>
                    <?php endif; ?>

        </tbody>
      </table>
  </div>
 <?php endif; ?>
    </div>
    </div>
  </div>
</div>
  <style type="text/css">
    .tablebtn {
    background: #666;
    color: #fff !important;
    padding: 5px 10px !important;
}
.modal-content {
    margin-left: 25%;
    margin-right: 25%;
    margin-right: 25%;
}

.modal-header.text-center {
    margin-top: 7%;
}
.btn.btn-primary.btn-sm.markPaymentReceived {
    color: #fff;
    padding: 5px 10px;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>