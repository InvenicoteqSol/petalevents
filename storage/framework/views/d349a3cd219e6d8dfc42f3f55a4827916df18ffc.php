<div class="header-outs" id="header">
      <div class="header-w3layouts">
        <div class="container">
          <!--//navigation section -->
          <nav class="navbar navbar-expand-lg navbar-light">
            <div class="hedder-up " >
              <h1><a class="navbar-brand" href="index.html">
                <img class="imglogo"src="<?php echo e(url('/public')); ?>/front/images/logo.png">
              </a>
              </h1>
            </div>
            <div class="seventyper">
              <div class="logos">
              <ul>

                <?php if( Auth::user() !=null): ?>
                   <?php if(Auth::user()->user_type=='1'): ?>
                <a href="<?php echo e(url('/admindashboard')); ?>"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo e(Auth::user()->name); ?></a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='3'): ?>
                  <a href="<?php echo e(url('/customerdashboard')); ?>"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo e(Auth::user()->first_name); ?></a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='4'): ?>
                  <a href="<?php echo e(url('/organiserdashboard')); ?>"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> <?php echo e(Auth::user()->first_name); ?></a>
                <?php endif; ?>
                <?php else: ?>
                <a href="<?php echo e(url('/login')); ?>"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> Customer Login</a>
                <?php endif; ?>
                <li class="dropdown dropdown-cart">
                 <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart" aria-hidden="true"></i>Cart</a>
                <ul class="dropdown-menu">
                <li class="c_head"><label>My cart</label></li>
                <li>
                 <table>
                   <tbody>
                    <tr class="t-head">
                      <td class="item-name">Item</th>
                      <td class="item-qty">Tickets</th>
                      <td class="item-price">Price</th>
                    </tr>
                   <!--  item list -->
                     <tr>
                       <td class="item-name">
                         <span class="c-media"><img src="images/e2.JPG"></span>
                         <span class="e-name">Event name</span>
                       </td>
                       <td class="item-qty">5</td>
                       <td class="item-price">Price</td>
                       <td class="c-action"><a href="" class="btn-cart">Remove</a></td>
                     </tr>
                       <!--  //item list -->
                         <!--  item list -->
                     <tr>
                       <td class="item-name">
                         <span class="c-media"><img src="images/e2.JPG"></span>
                         <span class="e-name">Event name</span>
                       </td>
                       <td class="item-qty">5</td>
                       <td class="item-price">Price</td>
                       <td class="c-action"><a href="" class="btn-cart">Remove</a></td>
                     </tr>
                       <!--  //item list -->
                         <!--  item list -->
                     <tr>
                       <td class="item-name">
                         <span class="c-media"><img src="images/e2.JPG"></span>
                         <span class="e-name">Event name</span>
                       </td>
                       <td class="item-qty">5</td>
                       <td class="item-price">Price</td>
                       <td class="c-action"><a href="" class="btn-cart">Remove</a></td>
                     </tr>
                       <!--  //item list -->
                      <tr class="col-action-check">
                        <td class="c-space"></td>
                        <td  class="c-act">
                          <span class="total-amt">
                          <label>Total</label>
                          <span class="t-pamt">$5320</span>
                        </span>
                          <span><a href="" class="btn btn-primary">Checkout</a></span>
                        </td>
                    </tr>
                   </tbody>
                 </table>
              </li>
              </ul>
              </li>
             </ul>
              </div>
              <div class="tgl">
                <img class="imglogo1"src="<?php echo e(url('/public')); ?>/front/images/logo.png">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
               <ul class="navbar-nav ">
                <li class="nav-item active">
                  <a class="nav-link" href="<?php echo e(url('')); ?>">How It Works <span class="sr-only">(current)</span></a>
                </li>
               
                <li class="nav-item">
                  <a href="#" class="nav-link">Featured Events</a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link">Ticket Deals</a>
                </li>
                 <li class="nav-item">
                  <?php if( Auth::user() !=null): ?>
                   <?php if(Auth::user()->user_type=='1'): ?>
                <a href="<?php echo e(url('/eventbookings/create')); ?>"> <button type="button" class="btn btn-danger">Create Event</button></a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='3'): ?>
                  <a href="<?php echo e(url('/eventbookings/create')); ?>"> <button type="button" class="btn btn-danger">Create Event</button></a>
                <?php endif; ?>
                <?php if(Auth::user()->user_type=='4'): ?>
                  <a href="<?php echo e(url('/eventbookings/create')); ?>"><button type="button" class="btn btn-danger">Create Event</button> </a>
                <?php endif; ?>
                <?php else: ?>
                <a href="<?php echo e(url('/organiserregistration')); ?>">
                  <button type="button" class="btn btn-danger">Create Event</button></a>
                <?php endif; ?>
                </li>
              </ul>
            </div>
          </div>
          </div>
          </nav>
          <div class="clearfix"> </div>
        </div>
      </div>
      <!--//navigation section -->
      <div class="banner-slide-img text-center">
        <div class="banner-bride-name">
          <h4>Event Tickets Online & Mobile</h4>
        </div>
        <div class="banner-groom-name">
          <h5>The Petal Events Ticket Platform</h5>
        </div>
        <div class="searchitem">
          <div class="row megh">
        <div class="col">
            <div id="imaginary_container"> 
              <form name="search" id="eventsearch" method="POST" action="<?php echo e(url('event-listings')); ?>">
                <?php echo e(csrf_field()); ?>

                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control" id="search_input" name="search_input"  placeholder="EVENT NAME, LOCATION" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <!-- <span class="glyphicon glyphicon-search"></span> -->
                            Find Event
                        </button>  
                    </span>
                </div>
              </form>
            </div>
        </div>
  </div>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>