<?php $__env->startSection('content'); ?>

<section class="section-checkout">
   <div class="col-sm-12 dashboard_right">
     <?php   $user_id = Auth::user()->id ;
           $user_email=Auth::user()->email;?>
                                 
         
          <form action="<?php echo e(url('/chargeorg')); ?>" method="POST">
            <?php echo e(csrf_field()); ?>

            <script
                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                    data-key="pk_test_6020SfAmkxN79Hq31s8W0WHZ"
                    data-amount="<?php echo e($amount*100); ?>"
                    data-name=""
                    data-description=""
                    data-image=""
                    data-email=<?php echo e($user_email); ?>

                    data-locale="auto"
                    data-currency="gbp">
              </script>
              <input type="hidden" name="data_amount" value="<?php echo e($amount*100); ?>">
              <input type="hidden" name="user_id" value="<?php echo e($user_id); ?>">
        </form>
   
   </div>
</section>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>