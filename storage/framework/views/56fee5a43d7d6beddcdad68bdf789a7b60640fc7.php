<?php $__env->startSection('content'); ?>
<section class="section-trending">
    <div class="container">
      <div class="row">
        <div class="col-md-12 cil-sm-12 col-xs-12">
          <h3 class="section-heading">Search Results For "<?php echo e($search_data); ?>"</h3>
    </div>
   </div>
    <!-- row -->
    <div class="row">
      <?php $rowCount=0; ?>
    	<?php if(count($events)>0): ?>
                     <?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <!-- card-event -->
      <?php 
       $rowCount=$rowCount+1;
       
       ?>
      <div class="col-md-3 col-sm-3 col-xs-12">
      <div class="card-event">
        <div class="card-image">
        <div class="category-overlay">
          <?php echo e($event->type_of_event); ?>

        </div>
        <a href="<?php echo e(url('event-details')); ?>/<?php echo e($event->event_title); ?>" target="_blank">
          <?php if($event->event_image!=""): ?>
        <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($event->event_image); ?>">
        <?php else: ?>
        <img src="<?php echo e(url('public')); ?>/front/images/login-vector.png">
        <?php endif; ?>
      </a>
      </div>
      <!--  //card-body -->
        <div class="card-body">
          <div class="event-detail">
       <span class="card-title"><?php echo e($event->event_title); ?></span>
      <div class="date-and-place">
       <div class="venue-details">
     <span class="e-loc"><?php echo e($event->venue_name); ?></span>
       <span class="e-date-time"><?php if($event->event_date!='' || $event->event_date!='0000-00-00'  || $event->event_date!=NULL){?><?php echo e(\Carbon\Carbon::parse($event->event_date)->format('d M, Y')); ?><?php } ?></span>
    </div>
       </div>
       <div class="btn-act">
      <a href="<?php echo e(url('event-details')); ?>/<?php echo e($event->event_title); ?>" target="_blank" class="btn btn-primary">Buy Tickets</a>
    </div>
     </div>

    <!-- ///event-detail -->
   <div class="price">
   <i class="fa fa-ticket" aria-hidden="true"></i>
    <div class="value">
       <?php 
       $ticketLowest=DB::table('tickets')->where('event_id', '=',$event->id)->orderBy('prices')->first();
       $countcheck=count($ticketLowest);
       $priceval=0;
       if($countcheck>0)
       {
         $priceval=$ticketLowest->prices;
       }
       ?>
       <i class="fa fa-gbp" aria-hidden="true"></i><?php echo e($priceval); ?>

       </div>
     </div>
   <!--  //price -->
    </div>
   <!-- // card-body -->
        </div>
      </div>
      <?php if($rowCount==4): ?>
      
      
      <div class="clearfix"></div>
      <?php 
       $rowCount=0;
       
       ?>
     
       <?php endif; ?>
       

       <!-- END card-event -->
      
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     <?php else: ?>
                     <p>No Records Found</p>
                      <?php endif; ?>

    </div>
    <!-- row -->
   </div>
 </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.innerdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>