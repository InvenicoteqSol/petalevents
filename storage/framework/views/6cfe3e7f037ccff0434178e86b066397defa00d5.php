<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
    <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
      <div class="col-sm-10 dashboard_right">
         <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
         <div class="line_div"><h3 class="com_busin_sec">Business Settings</h3></div>
      <div class="row">
        <div class="col-md-12">
                    <?php echo e(Form::model($businesssetting, array('route' => array('adminbusinesssetting.update', $businesssetting->id), 'id' => 'edtcustomerfrm', 'class' => 'edt_customer_frm formarea', 'method' => 'PUT'))); ?>


                     <div class="col-md-12">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="form-group<?php echo e($errors->has('business_name') ? ' has-error' : ''); ?>">
                                <label for="first_name"> Business Name*</label>
                                <input id="business_name" type="text" class="form-control" name="business_name" value="<?php echo e($businesssetting->business_name); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('business_name')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_name')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                  </div>
                             </div>
                            <div class="col-md-6">
                                 <div class="form-group<?php echo e($errors->has('business_email') ? ' has-error' : ''); ?>">
                                 <label for="business_email">E-Mail Address*</label>
                                 <input id="business_email" type="business_email" class="form-control" name="business_email" value="<?php echo e($businesssetting->business_email); ?>" maxlength="191" required >
                                <?php if($errors->has('business_email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                  </div>
                                </div>
                           </div>
                        </div>

                        <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('business_phone') ? ' has-error' : ''); ?>">
                                <label for="business_phone">Phone*</label>
                                <input id="business_phone" type="text" class="form-control" name="business_phone" value="<?php echo e($businesssetting->business_phone); ?>" minlength="10" maxlength="13" required>
                                <?php if($errors->has('business_phone')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_phone')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                         <div class="col-md-6">
                         <div class="form-group<?php echo e($errors->has('business_city') ? ' has-error' : ''); ?>">
                                <label for="business_city"> Business City*</label>
                                <input id="business_city" type="text" class="form-control" name="business_city" value="<?php echo e($businesssetting->business_city); ?>"  required autofocus>
                                <?php if($errors->has('business_city')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_city')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="col-md-12">
                          <div class="row topcls">
                           <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('business_state') ? ' has-error' : ''); ?>">
                            <label for="business_state"> Business State*</label>
                            <input id="business_state" type="text" class="form-control" name="business_state" value="<?php echo e($businesssetting->business_state); ?>" minlength="2" maxlength="91" required autofocus>
                            <?php if($errors->has('business_state')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_state')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                            <div class="col-md-6">
                             <div class="form-group<?php echo e($errors->has('business_country') ? ' has-error' : ''); ?>">
                            <label for="business_country"> Business Country *</label>
                             <input id="business_country" type="text" class="form-control" name="business_country" value="<?php echo e($businesssetting->business_country); ?>" minlength="2" maxlength="91" required autofocus>
                             <?php if($errors->has('business_country')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_country')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                  <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('business_address') ? ' has-error' : ''); ?>">
                            <label for="business_address"> Business Address *</label>
                            <input id="business_address" type="text" class="form-control" name="business_address" value="<?php echo e($businesssetting->business_address); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('business_address')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_address')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                              <div class="form-group<?php echo e($errors->has('business_zipcode') ? ' has-error' : ''); ?>">
                              <label for="business_zipcode"> Business Zipcode *</label>
                                <input id="business_zipcode" type="text" class="form-control" name="business_zipcode" value="<?php echo e($businesssetting->business_zipcode); ?>" minlength="2" maxlength="91"  autofocus>
                                <?php if($errors->has('business_zipcode')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('business_zipcode')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
               <!--   <div class="col-md-12">
                          <div class="row topcls">
                            <div class="col-md-6">
                                <div class="form-group<?php echo e($errors->has('twitter_link') ? ' has-error' : ''); ?>">
                                <label for="twitter_link"> Twitter Link </label>
                                <input id="twitter_link" type="text" class="form-control" name="twitter_link" value="<?php echo e($businesssetting->twitter_link); ?>" minlength="2" maxlength="255"  autofocus>
                                <?php if($errors->has('twitter_link')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('twitter_link')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group<?php echo e($errors->has('facebook_link') ? ' has-error' : ''); ?>">
                            <label for="facebook_link"> Facebook Link </label>
                                <input id="facebook_link" type="text" class="form-control" name="facebook_link" value="<?php echo e($businesssetting->facebook_link); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('facebook_link')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('facebook_link')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                     </div>
                    </div> -->
                <!--      <div class="col-md-12">
                        <div class="row topcls">
                       <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('google_link') ? ' has-error' : ''); ?>">
                            <label for="google_link"> Google Link </label>
                                <input id="google_link" type="text" class="form-control" name="google_link" value="<?php echo e($businesssetting->google_link); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('google_link')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('google_link')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                           <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('mailchip_keys') ? ' has-error' : ''); ?>">
                            <label for="mailchip_keys"> Mailchimp Keys </label>
                                <input id="mailchip_keys" type="text" class="form-control" name="mailchip_keys" value="<?php echo e($businesssetting->mailchip_keys); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('mailchip_keys')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('mailchip_keys')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div> -->
                        <div class="col-md-12">
                        <div class="row topcls">
                           <div class="col-md-6">
                              <div class="form-group <?php echo e($errors->has('stripe_mode') ? ' has-error' : ''); ?>">
                        <label for="stripe_mode">Stripe Mode</label>
                       <select id="stripe_mode" name="stripe_mode" class="form-control">
                         
                          <option value="Live" <?php if($businesssetting->stripe_mode=='Live'): ?> <?php echo e('selected'); ?> <?php endif; ?>>Live</option>
                         <option value="Test" <?php if($businesssetting->stripe_mode=='Test'): ?> <?php echo e('selected'); ?> <?php endif; ?>>Test</option>
  
                        </select>
                      <?php if($errors->has('stripe_mode')): ?>
                        <span class="help-block">
                         <strong><?php echo e($errors->first('stripe_mode')); ?></strong>
                          </span>
                        <?php endif; ?>
                      </div>                         
                     </div>
                    <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('live_publish_key') ? ' has-error' : ''); ?>">
                            <label for="live_publish_key"> Live Publish Key</label>
                                <input id="live_publish_key" type="text" class="form-control" name="live_publish_key" value="<?php echo e($businesssetting->live_publish_key); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('live_publish_key')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('live_publish_key')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                   <div class="col-md-12">
                        <div class="row topcls">
                     <div class="col-md-6">
                        <div class="form-group<?php echo e($errors->has('live_secret_key') ? ' has-error' : ''); ?>">
                            <label for="live_secret_key"> Live Secret Key</label>
                                <input id="live_secret_key" type="text" class="form-control" name="live_secret_key" value="<?php echo e($businesssetting->live_secret_key); ?>"  maxlength="91" autofocus>
                                <?php if($errors->has('live_secret_key')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('live_secret_key')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                      <div class="col-md-6">
                              <div class="form-group<?php echo e($errors->has('test_publish_key') ? ' has-error' : ''); ?>">
                            <label for="live_publish_key"> Test Publish Key</label>
                                <input id="test_publish_key" type="text" class="form-control" name="test_publish_key" value="<?php echo e($businesssetting->test_publish_key); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('live_publish_key')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('test_publish_key')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
              <!--    <div class="col-md-12">
                        <div class="row topcls">
                         <div class="col-md-6">
                              <div class="form-group<?php echo e($errors->has('test_secret_key') ? ' has-error' : ''); ?>">
                            <label for="test_secret_key"> Test Secret Key*</label>
                                <input id="test_secret_key" type="text" class="form-control" name="test_secret_key" value="<?php echo e($businesssetting->test_secret_key); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('test_secret_key')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('test_secret_key')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div> 
                     <div class="col-md-6">
                        <div class="form-group <?php echo e($errors->has('paypal_mode') ? ' has-error' : ''); ?>">
                        <label for="paypal_mode">Pay Pal</label>
                       <select id="paypal_mode" name="paypal_mode" class="form-control">
                        
                          <option value="Live" <?php if($businesssetting->paypal_mode=='Live'): ?> <?php echo e('selected'); ?> <?php endif; ?>>Live</option>
                         <option value="Test" <?php if($businesssetting->paypal_mode=='Test'): ?> <?php echo e('selected'); ?> <?php endif; ?>>Test</option>
  
                        </select>
                      <?php if($errors->has('paypal_mode')): ?>
                        <span class="help-block">
                         <strong><?php echo e($errors->first('paypal_mode')); ?></strong>
                          </span>
                        <?php endif; ?>
                      </div>                         
                    </div>
                </div>
            </div> -->    
          <!--   <div class="col-md-12">
                        <div class="row topcls">
                           <div class="col-md-6">
                            <div class="form-group<?php echo e($errors->has('paypal_live_email') ? ' has-error' : ''); ?>">
                            <label for="paypal_live_email"> Paypal Live Email</label>
                                <input id="paypal_live_email" type="text" class="form-control" name="paypal_live_email" value="<?php echo e($businesssetting->paypal_live_email); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('paypal_live_email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('paypal_live_email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                  <div class="col-md-6">
                           <div class="form-group<?php echo e($errors->has('paypal_test_email') ? ' has-error' : ''); ?>">
                            <label for="paypal_test_email"> Paypal Test Email</label>
                                <input id="paypal_test_email" type="text" class="form-control" name="paypal_test_email" value="<?php echo e($businesssetting->paypal_test_email); ?>"  maxlength="91"  autofocus>
                                <?php if($errors->has('paypal_test_email')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('paypal_test_email')); ?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div> --> 
                <div class="col-md-12">
                 <div class="topcls">
                <div class="col-md-6">
                              <div class="form-group<?php echo e($errors->has('booking_discount') ? ' has-error' : ''); ?>">
                                <label for="booking_discount"> Booking Fee ( % ) </label>
                                <input id="booking_discount" type="text" class="form-control" name="booking_discount" value="<?php echo e($businesssetting->booking_discount); ?>" minlength="2" maxlength="91" required autofocus>
                                <?php if($errors->has('booking_discount')): ?>
                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('booking_discount')); ?></strong>
                                    </span>
                                <?php endif; ?>
                                  </div>
                             </div>
                           </div>
                         </div>
                <div class="col-md-12">
                        <div class="topcls">
                     <div class="form-group">
                            <div class="buttondiv">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                     <?php echo e(Form::close()); ?>

            
        
    </div>
</div>
</div>
<script>
 $(document).ready( function() {

      var today = new Date();
      var curyear = today.getFullYear();
      var lastYear = curyear - 18;

      var lastMonth = today.getMonth();
      var lastDay = today.getDate();

        var date_input=$('input[name="dob"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
          format: 'dd-mm-yyyy',
          container: container,
          todayHighlight: false,
          autoclose: true,
          endDate: lastMonth+'-'+lastDay+'-'+lastYear
        })

    $.validator.addMethod("EMAILVLD", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
            }, "Please enter a valid email.");

    $.validator.addMethod("NMBRVLD", function(value, element) {
                return this.optional(element) || /^(|[1-9]\d*)$/i.test(value);
            }, "Please enter a valid number.");

    $.validator.addMethod("URLVLD", function(value, element) {
                return this.optional(element) || /^(http(s)?:\/\/)?(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value);
            }, "Please enter a valid url.");

    $.validator.addMethod("LTRVLD", function(value, element) {
     return this.optional(element) || value == value.match(/^[ a-zA-Z]+$/) && value.match(/[a-zA-Z]/);
    });

    $("#edtcustomerfrm").validate({
        rules: {
                first_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                last_name: {
                    required: true,
                    LTRVLD: true,
                    minlength: 2,
                    maxlength: 90
                },
                email: {
                    required: true,
                    EMAILVLD: true,
                    maxlength: 190
                },
                country: {
                    required: true
                },
                phone: {
                    required: true,
                    NMBRVLD: true,
                    minlength: 10,
                    maxlength: 12
                }
            },
        messages: {
                first_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                last_name: {
                  required: "This is required field.", 
                  LTRVLD: "Letters only please.",  
                  minlength: "Minimum 2 characters required.",
                  maxlength: "Maximum 90 characters allowed."
                },
                email: {
                  required: "This is required field.",   
                  EMAILVLD: "Please enter a valid email.",
                  maxlength: "Maximum 190 characters allowed."
                },
                country: {
                  required: "This is required field."
                },
                phone: {
                  required: "This is required field.", 
                  NMBRVLD: "Numbers only please.",  
                  minlength: "Minimum 10 digits required.",
                  maxlength: "Maximum 12 digits allowed."
                } 
            },
        submitHandler: function(form) {
            form.submit();
          }
        });

 });
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>