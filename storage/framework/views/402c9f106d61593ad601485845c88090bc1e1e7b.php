<?php $__env->startSection('content'); ?>
<!--booking-sec-start-->
<div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>
      <div class="col-sm-10 dashboard_right">
        <?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

        <div class="col-sm-12">
          <div class="current_sessiondiv">
          <div class="col-sm-8 current_sessiontitle">
            <h3>Your Current Session</h3>
   
          </div>
        <div class="col-sm-4 user_name">
          <h2>Welcome, <?php echo e($organiser->username); ?></h2>
        </div>
      </div>
    </div>
      <div class="col-sm-12 editbtn text-right">
        <a href="<?php echo e(route('organiserdashboard.edit',$organiser->id)); ?>">Edit</a>
      </div>
      <div class="profile_sec">
        <div class="col-sm-2 profile_img text-center">
          <?php if(Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL'): ?>
            <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e(Auth::user()->profile_picture); ?>" class="img-circle">
            <?php else: ?>
            <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" class="img-circle">
            <?php endif; ?>
          <ul class="social_media">
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
        <div class="col-sm-10 prflinfo">
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-user"></i><span><?php echo e($organiser->username); ?></span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-phone"></i><span><?php echo e($organiser->phone); ?></span>
          </div>
          <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-envelope"></i><span><?php echo e($organiser->email); ?></span>
          </div>
            <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-building-o"></i><span><?php echo e($organiser->organisation_name); ?></span>
          </div>
           <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-map-marker"></i><span><?php echo e($organiser->postcode); ?></span>
          </div>
            <div class="col-sm-4 prflinfo_div">
            <i class="fa fa-globe"></i><span><?php echo e($organiser->website); ?></span>
          </div>
          <div class="col-sm-12 aboutdiv">
            <h4>Address</h4>
             <p><?php echo html_entity_decode($organiser->address_line_1 );?></p>

          </div>
        </div>
      </div>
</div>

</div>
<!--booking-sec-end-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>