<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.flash-message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

 <div class="dashboard_section">
  <div class="col-sm-2 col-xs-12 dashboard_left nopadding">
       <?php echo $__env->make('layouts.usersidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
  </div>

    <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-xs-12">
    <div class="card">
            <div class="card-header card-header-primary">
              <p class="singalname"><?php echo e($organiser->first_name); ?> <?php echo e($organiser->last_name); ?></p>
            </div>

            <div class="card-body">
        <form>
      <div class="row">
        <div class="col-md-12">
                         <div class="row">
                         <div class="col-md-6">
                           <p class="rightsidetext"><span class="lablname">Phone :</span> <?php echo e($organiser->phone); ?> </p>
                         </div>
                           <div class="col-md-6">
                              <p class="rightsidetext"><span class="lablname">Email :</span><?php echo e($organiser->email); ?></p>
                                </div>
                              </div>
            
                                       <div class="row">
                                       <div class="col-md-6">
                                         <p class="rightsidetext"><span class="lablname">Country :</span> <?php echo e($organiser->country); ?></p>
                                       </div>
                                       <!-- <div class="col-md-6">
                                        <p class="rightsidetext"><span class="lablname">Status :</span> <?php 
                                 if($organiser->status=='1') { 
                                    echo 'Active';
                                  } else {
                                    echo 'Inactive';
                                   }?></p>
                                 </div> -->
                               </div>
                               <div class="row">
                                <div class="col-md-6">
                                   <?php if(Auth::user()->profile_picture!='' && Auth::user()->profile_picture!='NULL'): ?>
                             <img src="<?php echo e(url('public')); ?>/uploads/profileimages/<?php echo e($organiser->profile_picture); ?>" class="img-circle" style="width:200px">
                                   <?php else: ?>
                            <img src="<?php echo e(url('public')); ?>/images/avtar.jpg" class="img-circle">
                              <?php endif; ?>
                                </div>
                              </div>
                            </div>
            
    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>


<style>
  span.lablname {
    font-size: 14px;
    font-weight: bold;
}
.dasbagcol{
  background: #f7f9fc;
margin-top: 0px;
padding-top: 42px;
}
.sectionbox {
    background: #ffff;
    padding-top: 13px;
    padding-bottom: 13px;
}
p.singalname {
    font-size: 27px;
}
span.lablname {
    float: left;
    width: 40%;
}
.rightbox {
    border-left: solid 1px #d2d2d2;
}

span.labldata {
    color: #969da5;
}
p.singalemail {
    color: #969da5;
}
</style>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.userdefault', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>